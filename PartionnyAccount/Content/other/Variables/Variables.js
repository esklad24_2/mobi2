﻿/*
    Variables
*/

//Название сервиса
var lanOrgName = " ИС 'Сервисный центр'";

//Системные
var varCopyrightSystem = "Copyright © 2017";
var verSystem = "версия 1.6.3.1000";
var varSystemDate = "от 2017.08.29";
var varSystemDevelop = "Сайт: <a href='http://intradecloud.com/' target=blank>Учет в Торговле и Сервисном центре</a>";


// *** *** *** Глобальные переменные *** *** *** *** *** 

/*
    HTTP
*/
//Help
var HTTP_Help = "http://intradecloud.com/pomoshch/";
var HTTP_Pay = "http://intradecloud.com/payment";
//MSSQL
var HTTP_DirCountriesGridMSSQL = "api/login/Dir/DirCountriesMSSQL/";
var HTTP_DirLanguagesGridMSSQL = "api/login/Dir/DirLanguagesMSSQL/";
var HTTP_DirCustomersMSSQL = "api/login/Dir/DirCustomersMSSQL/";
//Sys
var HTTP_SysSettingsSet = "api/sklad/Sys/SysSettings/"; //!!! Не для Грида !!!
var HTTP_SysSettings = "api/sklad/Sys/SysSettings/";
var HTTP_SysJourDispsGrid = "api/sklad/Sys/SysJourDisps/";
//Dir
//?type=Grid && ?type=Tree
var HTTP_DirNomens = "api/sklad/Dir/DirNomens/"; var HTTP_DirNomenHistories = "api/sklad/Dir/DirNomenHistories/"; //var HTTP_Image = "api/sklad/Dir/DirNomensImg/";
var HTTP_DirNomenCategories = "api/sklad/Dir/DirNomenCategories/";
var HTTP_DirServiceNomens = "api/sklad/Dir/DirServiceNomens/"; var HTTP_DirServiceNomenHistories = "api/sklad/Dir/DirServiceNomenHistories/";
var HTTP_DirServiceNomenCategories = "api/sklad/Dir/DirServiceNomenCategories/";
var HTTP_DirWarehouses = "api/sklad/Dir/DirWarehouses/"; 
var HTTP_DirCurrencies = "api/sklad/Dir/DirCurrencies/"; var HTTP_DirCurrencyHistories = "api/sklad/Dir/DirCurrencyHistories/"; 
var HTTP_DirEmployees = "api/sklad/Dir/DirEmployees/"; var HTTP_DirEmployeeHistories = "api/sklad/Dir/DirEmployeeHistories/"; var HTTP_DirEmployeeWarehouses = "api/sklad/Dir/DirEmployeeWarehouses/";
var HTTP_DirBonuses = "api/sklad/Dir/DirBonuses/"; var HTTP_DirBonusTabs = "api/sklad/Dir/DirBonusTabs/";
var HTTP_DirBonus2es = "api/sklad/Dir/DirBonus2es/"; var HTTP_DirBonus2Tabs = "api/sklad/Dir/DirBonus2Tabs/";
var HTTP_DirBanks = "api/sklad/Dir/DirBanks/";
var HTTP_DirCashOffices = "api/sklad/Dir/DirCashOffices/"; 
var HTTP_DirContractors = "api/sklad/Dir/DirContractors/"; 
var HTTP_DirContractor1Types = "api/sklad/Dir/DirContractor1Types/"; 
var HTTP_DirContractor2Types = "api/sklad/Dir/DirContractor2Types/"; 
var HTTP_DirVats = "api/sklad/Dir/DirVats/"; 
var HTTP_DirDiscounts = "api/sklad/Dir/DirDiscounts/"; var HTTP_DirDiscountTabs = "api/sklad/Dir/DirDiscountTabs/";
var HTTP_DirNomenTypes = "api/sklad/Dir/DirNomenTypes/";
var HTTP_DirCharColours = "api/sklad/Dir/DirCharColours/";
var HTTP_DirCharMaterials = "api/sklad/Dir/DirCharMaterials/";
var HTTP_DirCharNames = "api/sklad/Dir/DirCharNames/";
var HTTP_DirCharSeasons = "api/sklad/Dir/DirCharSeasons/";
var HTTP_DirCharSexes = "api/sklad/Dir/DirCharSexes/";
var HTTP_DirCharSizes = "api/sklad/Dir/DirCharSizes/";
var HTTP_DirCharStyles = "api/sklad/Dir/DirCharStyles/";
var HTTP_DirCharTextures = "api/sklad/Dir/DirCharTextures/";
var HTTP_DirPaymentTypes = "api/sklad/Dir/DirPaymentTypes/";
var HTTP_DirCashOfficeSumTypes = "api/sklad/Dir/DirCashOfficeSumTypes/";
var HTTP_DirBankSumTypes = "api/sklad/Dir/DirBankSumTypes/";
var HTTP_DirPriceTypes = "api/sklad/Dir/DirPriceTypes/";
var HTTP_DirDescriptions = "api/sklad/Dir/DirDescriptions/";
var HTTP_DirReturnTypes = "api/sklad/Dir/DirReturnTypes/";
var HTTP_DirMovementDescriptions = "api/sklad/Dir/DirMovementDescriptions/";
var HTTP_DirMovementStatuses = "api/sklad/Dir/DirMovementStatuses/";
var HTTP_DirDomesticExpenses = "api/sklad/Dir/DirDomesticExpenses/";
//Dir - Service
var HTTP_DirServiceContractors = "api/sklad/Dir/DirServiceContractors/";
var HTTP_DirServiceStatuses = "api/sklad/Dir/DirServiceStatuses/";
var HTTP_DirServiceJobNomens = "api/sklad/Dir/DirServiceJobNomens/"; var HTTP_DirServiceJobNomenHistories = "api/sklad/Dir/DirServiceJobNomenHistories/";
var HTTP_DirServiceComplects = "api/sklad/Dir/DirServiceComplects/";
var HTTP_DirServiceProblems = "api/sklad/Dir/DirServiceProblems/";
var HTTP_DirSmsTemplates = "api/sklad/Dir/DirSmsTemplates/";
var HTTP_DirServiceDiagnosticRresults = "api/sklad/Dir/DirServiceDiagnosticRresults/";
var HTTP_DirServiceNomenTypicalFaults = "api/sklad/Dir/DirServiceNomenTypicalFaults/";
var HTTP_DirServiceNomenPrices = "api/sklad/Dir/DirServiceNomenPrices/";
//Dir - SecondHand
var HTTP_DirSecondHandStatuses = "api/sklad/Dir/DirSecondHandStatuses/";
//Doc
var HTTP_DocCashOfficeSums = "api/sklad/Doc/DocCashOfficeSums/"; var HTTP_DocBankSums = "api/sklad/Doc/DocBankSums/";
var HTTP_DocPurches = "api/sklad/Doc/DocPurches/"; var HTTP_DocPurchTabs = "api/sklad/Doc/DocPurchTabs/";
var HTTP_DocSales = "api/sklad/Doc/DocSales/"; var HTTP_DocSaleTabs = "api/sklad/Doc/DocSaleTabs/";
var HTTP_DocMovements = "api/sklad/Doc/DocMovements/"; var HTTP_DocMovementTabs = "api/sklad/Doc/DocMovementTabs/";
var HTTP_DocReturnVendors = "api/sklad/Doc/DocReturnVendors/"; var HTTP_DocReturnVendorTabs = "api/sklad/Doc/DocReturnVendorTabs/";
var HTTP_DocActWriteOffs = "api/sklad/Doc/DocActWriteOffs/"; var HTTP_DocActWriteOffTabs = "api/sklad/Doc/DocActWriteOffTabs/";
var HTTP_DocReturnsCustomers = "api/sklad/Doc/DocReturnsCustomers/"; var HTTP_DocReturnsCustomerTabs = "api/sklad/Doc/DocReturnsCustomerTabs/";
var HTTP_DocActOnWorks = "api/sklad/Doc/DocActOnWorks/"; var HTTP_DocActOnWorkTabs = "api/sklad/Doc/DocActOnWorkTabs/";
var HTTP_DocAccounts = "api/sklad/Doc/DocAccounts/"; var HTTP_DocAccountTabs = "api/sklad/Doc/DocAccountTabs/";
var HTTP_DocInventories = "api/sklad/Doc/DocInventories/"; var HTTP_DocInventoryTabs = "api/sklad/Doc/DocInventoryTabs/";
var HTTP_DocRetails = "api/sklad/Doc/DocRetails/"; var HTTP_DocRetailTabs = "api/sklad/Doc/DocRetailTabs/";
var HTTP_DocRetailReturns = "api/sklad/Doc/DocRetailReturns/"; var HTTP_DocRetailReturnTabs = "api/sklad/Doc/DocRetailReturnTabs/";
var HTTP_DocRetailActWriteOffs = "api/sklad/Doc/DocRetailActWriteOffs/";
var HTTP_DocOrderInts = "api/sklad/Doc/DocOrderInts/";
var HTTP_DocNomenRevaluations = "api/sklad/Doc/DocNomenRevaluations/"; var HTTP_DocNomenRevaluationTabs = "api/sklad/Doc/DocNomenRevaluationTabs/";
//Doc - Service
var HTTP_DocServicePurches = "api/sklad/Doc/DocServicePurches/";
var HTTP_DocServicePurch1Tabs = "api/sklad/Doc/DocServicePurch1Tabs/";
var HTTP_DocServicePurch2Tabs = "api/sklad/Doc/DocServicePurch2Tabs/";
//Doc - SecondHand
var HTTP_DocSecondHandPurches = "api/sklad/Doc/DocSecondHandPurches/";
var HTTP_DocSecondHandPurch1Tabs = "api/sklad/Doc/DocSecondHandPurch1Tabs/";
var HTTP_DocSecondHandPurch2Tabs = "api/sklad/Doc/DocSecondHandPurch2Tabs/";
var HTTP_DocSecondHandRetails = "api/sklad/Doc/DocSecondHandRetails/"; var HTTP_DocSecondHandRetailTabs = "api/sklad/Doc/DocSecondHandRetailTabs/";
var HTTP_DocSecondHandRetailReturns = "api/sklad/Doc/DocSecondHandRetailReturns/";
var HTTP_DocSecondHandRetailActWriteOffs = "api/sklad/Doc/DocSecondHandRetailActWriteOffs/";
//Doc - Salary
var HTTP_DocSalaries = "api/sklad/Doc/DocSalaries/"; var HTTP_DocSalaryTabs = "api/sklad/Doc/DocSalaryTabs/";
//Doc - Salary
var HTTP_DocDomesticExpenses = "api/sklad/Doc/DocDomesticExpenses/"; var HTTP_DocDomesticExpenseTabs = "api/sklad/Doc/DocDomesticExpenseTabs/";
//Pay
var HTTP_Pays = "api/sklad/Pay/Pay/";
//Report
var HTTP_ReportTotalTrade = "api/sklad/Report/ReportTotalTrade/";
var HTTP_ReportBanksCashOffices = "api/sklad/Report/ReportBanksCashOffices/";
var HTTP_DocServicePurchesReport = "api/sklad/Report/DocServicePurchesReport/";
var HTTP_ReportLogistics = "api/sklad/Report/ReportLogistics/";
var HTTP_ReportSalaries = "api/sklad/Report/ReportSalaries/"; var HTTP_ReportSalariesWarehouses = "api/sklad/Report/ReportSalariesWarehouses/";
//Rem
var HTTP_RemParties = "api/sklad/Rem/RemParties/"; var HTTP_Rem2Parties = "api/sklad/Rem/Rem2Parties/";
var HTTP_RemPartyMinuses = "api/sklad/Rem/RemPartyMinuses/"; var HTTP_Rem2PartyMinuses = "api/sklad/Rem/Rem2PartyMinuses/";
//List
var HTTP_ListObjects = "api/sklad/List/ListObjects/";
var HTTP_ListObjectFieldNames = "api/sklad/List/ListObjectFieldNames/";
var HTTP_ListLanguages = "api/sklad/List/ListLanguages/";
var HTTP_ListObjectPFs = "api/sklad/List/ListObjectPFs/";
var HTTP_ListObjectPFTabs = "api/sklad/List/ListObjectPFTabs/";
//Service
var HTTP_ImportsDocPurchesExcel = "api/WebApi/ExchangeData/ImportsDocPurchesExcel/";
//Image
var HTTP_Image = "api/sklad/Dir/Image/"; //var HTTP_DirMshnsImg = "api/sklad/Dir/DirMshnsImg/";
//Log
var HTTP_LogServices = "api/sklad/Log/LogServices/";
var HTTP_LogMovements = "api/sklad/Log/LogMovements/";
var HTTP_LogOrderInts = "api/sklad/Log/LogOrderInts/";
var HTTP_LogSecondHands = "api/sklad/Log/LogSecondHands/";
//Sms
var HTTP_Sms = "api/sklad/SMS/Sms/";


/* OLD */
var HTTP_DirBankAccountsGrid = "api/sklad/Dir/DirBankAccounts/";
var HTTP_DirContractsGrid = "api/sklad/Dir/DirContracts/";
var HTTP_DirBankAccountTypesGrid = "api/sklad/Dir/DirBankAccountTypes/";
var HTTP_DirCountriesGrid = "api/sklad/Dir/DirCountries/";
var HTTP_DirDepartmentsGrid = "api/sklad/Dir/DirDepartments/";
var HTTP_DirPostsGrid = "api/sklad/Dir/DirPosts/"; var HTTP_DirPostGroups = "api/sklad/Dir/DirPostGroups/";
var HTTP_DirPriceTypesGrid = "api/sklad/Dir/DirPriceTypes/";
var HTTP_DirNomensGrid = "api/sklad/Dir/DirNomens/"; var HTTP_DirNomenGroups = "api/sklad/Dir/DirNomenGroups/"; var HTTP_DirNomenHistoriesGrid = "api/sklad/Dir/DirNomenHistories/";
var HTTP_DirBarCodesGrid = "api/sklad/Dir/DirBarCodes/";
var HTTP_DirUnitMeasuresGrid = "api/sklad/Dir/DirUnitMeasures/";
var HTTP_DirPointSalesGrid = "api/sklad/Dir/DirPointSales/";
var HTTP_DirBudgetClassesGrid = "api/sklad/Dir/DirBudgetClasses/";
var HTTP_DirBudgetLevelsGrid = "api/sklad/Dir/DirBudgetLevels/";
var HTTP_DirOKATOesGrid = "api/sklad/Dir/DirOKATOes/";
var HTTP_DirTaxCodesGrid = "api/sklad/Dir/DirTaxCodes/";
var HTTP_DirOrdersStatesGrid = "api/sklad/Dir/DirOrdersStates/"; var HTTP_DirOrdersStateGroups = "api/sklad/Dir/DirOrdersStateGroups/";
var HTTP_DirPostViewsGrid = "api/sklad/Dir/DirPostViews/";
var HTTP_DirPostTypesGrid = "api/sklad/Dir/DirPostTypes/";
var HTTP_DirPostImportancesGrid = "api/sklad/Dir/DirPostImportances/";
var HTTP_DirPostUrgenciesGrid = "api/sklad/Dir/DirPostUrgencies/";


//Контекстное Меню (правая кнопка мыши) для Грида
/*var contextMenu = Ext.create('Ext.menu.Menu', {
    width: 200,
    items: [
        {
            text: 'Preview',
            handler: function (par1, par2) {
                var record = grid ? grid.getSelection()[0] : null;
                if (!record) {
                    return;
                }
                alert(record.get('name'));
            }
        }
    ]
});*/



//Шрифты (Header menu)
var HeaderMenu_FontSize_1 = 3; //Верхнее меню, 1-й уровень (ContainerHeader.js)
var HeaderMenu_FontSize_2 = 2; //Верхнее меню, 2-й уровень (ContainerHeader.js)

//При изменении цен, что бы не изменялись все одновременно.
var varPriceChange_ReadOnly = false;
//ID-шник для Виджетов, которые выводящиеся на экран более 1-го раза.
var ObjectID = 0; // GLOBAL_ID
//ID-шник для Store спецификации (каждой позиции). Хотя может использоватся везде где только можно.
//Т.к. в 6-й версии появился "store.data.items[1].data.id" (в 4-й не было). 
//1. При вставке записи в спецификацию этот id-шник может повторятся, т.к. Мы можем вставить 2-ы одну и ту же запись.
//2. При проставлении количества "Quantity" глюки: меняем количество в переменно "rec", а оно меняется в store, т.к. id-шники обоих совпадают, если запись из подбора товара уже вставлена в спецификацию.
var ObjectTOtherID = 0; // GLOBAL_ID

//Расположение окон объектов
var ObjectXY = 0;

//Размеры окошка: Windows: height, width
var varArrSizeList = [250, 450];
var varArrSizeDir = [350, 600];
var varArrSizeJurn = [305, 650];
var varArrSizePay = [250, 600];

var SearchType_values = [
    [1, "Артикул"] //В товаре (код)
    /*
    ,
    [2, 'В товаре (старый код)'],
    [3, 'В товаре (артикул)'],
    [4, 'В товаре (наименование)'],
    [5, 'В товаре (полное наименование)'],
    [1000, 'В партиях (код, серии, штрих-код)'],
    */
];

var DirDomesticExpense_values = [
    [1, "Другие"],
    [2, "Зарплата"],
    [3, "Инкасация"]
];

var DirOrderIntType_values = [
    [1, "Мастерская"],
    [2, "Предзаказы"],
    [3, "Впрок"],
];

//Гарантия в Мастерскрй СС
var DocYear_values = [
    [2017, "2017"],
    [2018, "2018"],
    [2019, "2019"],
    [2020, "2020"],
    [2021, "2021"],
    [2022, "2022"],
    [2023, "2023"],
    [2024, "2024"],
    [2025, "2025"],
    [2026, "2026"],
    [2027, "2027"],
    [2028, "2028"],
];

//Гарантия в Мастерскрй СС
var ServiceTypeRepair_values = [
    [1, "1 месяц"],
    [2, "2 месяц"],
    [3, "3 месяц"],
    [4, "4 месяц"],
    [5, "5 месяц"],
    [6, "6 месяц"],
    [7, "7 месяц"],
    [8, "8 месяц"],
    [9, "9 месяц"],
    [10, "10 месяц"],
    [11, "11 месяц"],
    [12, "12 месяц"],
];

//Шаблоны СМС
//СЦ
var DirSmsTemplateType_values1 = [
    [1, "Сервисный центр: Другое"],
    [2, "Сервисный центр: Отремонтированный"],
    [3, "Сервисный центр: Отказной"],
    [4, "Сервисный центр: Выдан"],
];
//Логистика
var DirSmsTemplateType_values2 = [
    [1, "Логистика"],
];
//Заказы
var DirSmsTemplateType_values3 = [
    [1, "Заказы"],
];

var DirDomesticExpenses_Sign_values = [
    [-1, "Изъятие из Кассы/Банка"],
    [1, "Поступление в Кассу/Банк"],
];
var SalaryPercentTradeType_values = [
    [1, "Процент с продажи"],
    [2, "Процент с прибыли"],
    [3, "Фиксированная сумма с каждой продажи"],
];
var SalaryPercentService1TabsType_values = [
    [1, "Процент с суммы всех работ"],
    [2, "Фиксированная сумма за все работы в рамках одного ремонта"],
    [3, "Фиксированная сумма с каждого ремнта"],
];
var SalaryPercentService2TabsType_values = [
    [1, "Процент с продажи"],
    [2, "Процент с прибыли"],
    [3, "Фиксированная сумма за одну использованную к ремонту запчасти"],
];



//Гарантия в Мастерскрй СС
/*var IsAdminNameRu_values = [
    [1, "Администратор"],
    [0, "Не Администратор"],
];*/
var urlBaseImg = '../Scripts/sklad/images/';
var IsAdminNameRu_store = Ext.create('Ext.data.Store', {
    fields: ['IsAdmin', 'IsAdminNameRuName', "img"],
    data: [
        //{ IsAdminNameRu: 'Администратор', IsAdminNameRuName: urlBaseImg + 'add.png' },
        //{ IsAdminNameRu: 'Не Администратор', IsAdminNameRuName: urlBaseImg + 'cancel.png' },
        {
            //IsAdmin: true,
            IsAdminNameRu: 'Администратор',
            img: urlBaseImg + 'add.png'
        },
        {
            //IsAdmin: false,
            IsAdminNameRu: 'Не Администратор',
            img: urlBaseImg + 'cancel.png'
        },
    ]
});

//Таймаут по умолчанию:
var varTimeOutDefault = 60000; // 60 seconds, 1 minute



/*
    Request to Server
*/

//К-во попыток запроса в Реквестах к серверу в случае не удачи
var varCountErrorRequest = 2;

//Получаем значение переменно:
var varCountErrorSettingsRequest = 0;
var varJurDateS = "2016-08-01";     //Дата начала периода.
var varJurDatePo = "2020-12-31";    //Дата начала периода.
var varFractionalPartInSum = 2;     //К-во знаков после запятой.
var varFractionalPartInPrice = 2;   //К-во знаков после запятой.
var varFractionalPartInOther = 2;   //К-во знаков после запятой.
var varDirVatValue = 0;             //Наименование "Ставки НДС"
var varChangePriceNomen = false;    //Пересчитывать цены Номенклатуры, при проведении Приходной накладной.
var varMethodAccounting = 1;        //Метод учёта
var varDeletedRecordsShow = true;   //Показывать удалённые записи.
var varDirContractorIDOrg = 1;      //Организация по умолчанию
var varDirContractorNameOrg = "";      //Организация по умолчанию
var varDirCurrencyID = 1;           //Код "Валюты"
var varDirCurrencyName = "";        //"Валюта"
var varDirCurrencyNameShort = "";        //"Валюта"
var varDirCurrencyRate = 1;         //"Курс"
var varDirCurrencyMultiplicity = 1; //"Кратность"
var varDirWarehouseID = 1;          //По Умрлчанию в настройках
var varMarkupRetail = 30;
var varMarkupWholesale = 20;
var varMarkupIM = 20;
var varMarkupSales1 = 5;
var varMarkupSales2 = 10;
var varMarkupSales3 = 15;
var varMarkupSales4 = 20;
var varCashBookAdd = false;
var varReserve = false;
var varBarIntNomen = 9901;
var varBarIntContractor = 9902;
var varBarIntDoc = 9903;
var varSelectOneClick = true;
var varDateFormat = 1; var DateFormatStr = "Y-m-d";
//К-во выводимых строк в Гриде
var varPageSizeDir = 11;
var varPageSizeJurn = 9;
//Минимальный остаток
var varDirNomenMinimumBalance = 2;
//DirEmployees
var varDirWarehouseIDEmpl = 1;    //Сотрудник может совершать операции только по этому складу
var varDirWarehouseNameEmpl = "";
var varDirContractorIDOrgEmpl = 0;//Сотрудник может совершать операции только по этой организации
var varDirEmployeeID = 0
var varLoginMS = "..."
var varDirEmployeeLogin = "..."
var lanDirEmployeeName = "..."
var varReadinessDay = 5;          //Сервисный центр -> Приёмка: Число дней до даты готовности
var ServiceTypeRepair = 1;        //Сервисный центр -> Гарантия в Мастерской
var varPhoneNumberBegin = 7;      //Сервисный центр -> Приёмка: Телефон.начало
var varIsAdmin = true;            //Администратор торговой точки
//Sms
var SmsAutoShow = true;           //Сервисный центр -> Приёмка: Число дней до даты готовности
var SmsAutoShow9 = true;           //Сервисный центр -> Приёмка: Число дней до даты готовности
//Скидки в документах
var varRightDocDescriptionCheck = false;

/* GRID */
//Тип цен (Из Настроек)
var varDirPriceTypeID = 1;
var varStoreDirPriceTypesGrid; var varStoreDirPaymentTypesGrid; var varStoreDirServiceStatusesGrid;
var varStoreDirCharColoursGrid; var varStoreDirCharMaterialsGrid; var varStoreDirCharNamesGrid; var varStoreDirCharSeasonsGrid; var varStoreDirCharSexesGrid; var varStoreDirCharSizesGrid; var varStoreDirCharStylesGrid; var varStoreDirCharTexturesGrid; var varStoreDirCurrenciesGrid;
var varStoreDirServiceContractorsGrid;
var varStoreDirServiceDiagnosticRresultsGrid;
var varStoreDirServiceNomenTypicalFaultsGrid;
var varStoreDirReturnTypesGrid;
//Срок гарантии прошёл
var varWarrantyPeriodPassed = true;


// === Переменные для оплат клиента ===
var varDirPayServiceName = ""; //Тарифный план (Tariff1)
var varCountUser = ""; //Сотрудников (Tariff2)
//var varCountTT = ""; //Торговых точек (Tariff3)
var varCountNomen = ""; //Товаров (XXX)
var varPayDateEnd = ""; //Окончание (Tariff4)
//var varCountIM = ""; //Интернет магазинов (Tariff5)
var varPaymentExpired = false; //Оплата просрочена (вычисляется по переменной "varPayDateEnd")
var nDaysLeft = 999; //Через сколько дней заканчивается подписка (вычисляется по переменной "varPayDateEnd")

//Уже загружали настройки "Variables_SettingsRequest()"?
//Если "да", то не менять склад!
var Variables_SettingsRequest_Load = false;

try {

    function Variables_SettingsRequest() {

        Ext.Ajax.request({
            timeout: varTimeOutDefault,
            waitMsg: lanUpload,
            //url: HTTP_SettingsSelect + "?pID=Settings&FromVariables=1",
            url: HTTP_SysSettingsSet + "2",
            method: 'GET',
            success: function (result) {

                var sData = Ext.decode(result.responseText);
                if (sData.success == false) {
                    if (varCountErrorSettingsRequest < varCountErrorRequest + 5) {
                        varCountErrorSettingsRequest++;
                        Variables_SettingsRequest();
                        return;
                    }
                    else {
                        Ext.MessageBox.show({
                            title: lanOrgName,
                            msg: txtMsg017 + "<BR>" + sData.data,
                            icon: Ext.MessageBox.QUESTION, buttons: Ext.Msg.YESNO, width: 300, closable: false,
                            fn: function (buttons) {
                                if (buttons == "yes") { location.reload(); }
                            }
                        });
                        return;
                    }
                }
                else {
                    //Для "Employeess" and "Payment"
                    sData1 = sData.data; //Employeess + Currency
                    //Для "Settings"
                    sDataSysSettings = sData.data.sysSettings;
                    sDataDirEmployees = sData.data.dirEmployees;
                    //Для "SysDirConstants" - сложный запрос, поэтому отрабатывает отдельным запросом на Сервер
                    //sDataSysDirConstants = sData.data.sysDirConstants;


                    if (varType == "")
                    {
                        //Grids *** *** ***
                        //Тип цен и Характеристики
                        //Лоадер
                        var loadingMask = new Ext.LoadMask({ msg: 'Please wait...', target: Ext.getCmp("viewContainerCentral") }); loadingMask.show();

                        varStoreDirPriceTypesGrid = Ext.create("store.storeDirPriceTypesGrid"); varStoreDirPriceTypesGrid.setData([], false); varStoreDirPriceTypesGrid.proxy.url = HTTP_DirPriceTypes + "?type=Grid"; varStoreDirPriceTypesGrid.load({ waitMsg: lanLoading })
                        varStoreDirPriceTypesGrid.on('load', function () {
                            varStoreDirPaymentTypesGrid = Ext.create("store.storeDirPaymentTypesGrid"); varStoreDirPaymentTypesGrid.setData([], false); varStoreDirPaymentTypesGrid.proxy.url = HTTP_DirPaymentTypes + "?type=Grid"; varStoreDirPaymentTypesGrid.load({ waitMsg: lanLoading })
                            varStoreDirPaymentTypesGrid.on('load', function () {
                                varStoreDirServiceStatusesGrid = Ext.create("store.storeDirServiceStatusesGrid"); varStoreDirServiceStatusesGrid.setData([], false); varStoreDirServiceStatusesGrid.proxy.url = HTTP_DirServiceStatuses + "?type=Grid"; varStoreDirServiceStatusesGrid.load({ waitMsg: lanLoading })
                                varStoreDirServiceStatusesGrid.on('load', function () {
                                    varStoreDirCharColoursGrid = Ext.create("store.storeDirCharColoursGrid"); varStoreDirCharColoursGrid.setData([], false); varStoreDirCharColoursGrid.proxy.url = HTTP_DirCharColours + "?type=Grid"; varStoreDirCharColoursGrid.load({ waitMsg: lanLoading });
                                    varStoreDirCharColoursGrid.on('load', function () {
                                        varStoreDirCharMaterialsGrid = Ext.create("store.storeDirCharMaterialsGrid"); varStoreDirCharMaterialsGrid.setData([], false); varStoreDirCharMaterialsGrid.proxy.url = HTTP_DirCharMaterials + "?type=Grid"; varStoreDirCharMaterialsGrid.load({ waitMsg: lanLoading });
                                        varStoreDirCharMaterialsGrid.on('load', function () {
                                            varStoreDirCharNamesGrid = Ext.create("store.storeDirCharNamesGrid"); varStoreDirCharNamesGrid.setData([], false); varStoreDirCharNamesGrid.proxy.url = HTTP_DirCharNames + "?type=Grid"; varStoreDirCharNamesGrid.load({ waitMsg: lanLoading });
                                            varStoreDirCharNamesGrid.on('load', function () {
                                                varStoreDirCharSeasonsGrid = Ext.create("store.storeDirCharSeasonsGrid"); varStoreDirCharSeasonsGrid.setData([], false); varStoreDirCharSeasonsGrid.proxy.url = HTTP_DirCharSeasons + "?type=Grid"; varStoreDirCharSeasonsGrid.load({ waitMsg: lanLoading });
                                                varStoreDirCharSeasonsGrid.on('load', function () {
                                                    varStoreDirCharSexesGrid = Ext.create("store.storeDirCharSexesGrid"); varStoreDirCharSexesGrid.setData([], false); varStoreDirCharSexesGrid.proxy.url = HTTP_DirCharSexes + "?type=Grid"; varStoreDirCharSexesGrid.load({ waitMsg: lanLoading });
                                                    varStoreDirCharSexesGrid.on('load', function () {
                                                        varStoreDirCharSizesGrid = Ext.create("store.storeDirCharSizesGrid"); varStoreDirCharSizesGrid.setData([], false); varStoreDirCharSizesGrid.proxy.url = HTTP_DirCharSizes + "?type=Grid"; varStoreDirCharSizesGrid.load({ waitMsg: lanLoading });
                                                        varStoreDirCharSizesGrid.on('load', function () {
                                                            varStoreDirCharStylesGrid = Ext.create("store.storeDirCharStylesGrid"); varStoreDirCharStylesGrid.setData([], false); varStoreDirCharStylesGrid.proxy.url = HTTP_DirCharStyles + "?type=Grid"; varStoreDirCharStylesGrid.load({ waitMsg: lanLoading });
                                                            varStoreDirCharStylesGrid.on('load', function () {
                                                                varStoreDirCharTexturesGrid = Ext.create("store.storeDirCharTexturesGrid"); varStoreDirCharTexturesGrid.setData([], false); varStoreDirCharTexturesGrid.proxy.url = HTTP_DirCharTextures + "?type=Grid"; varStoreDirCharTexturesGrid.load({ waitMsg: lanLoading });
                                                                varStoreDirCharTexturesGrid.on('load', function () {
                                                                    varStoreDirServiceContractorsGrid = Ext.create("store.storeDirServiceContractorsGrid"); varStoreDirServiceContractorsGrid.setData([], false); varStoreDirServiceContractorsGrid.proxy.url = HTTP_DirServiceContractors + "/1/1/?type=Grid"; varStoreDirServiceContractorsGrid.load({ waitMsg: lanLoading });
                                                                    varStoreDirServiceContractorsGrid.on('load', function () {
                                                                        varStoreDirServiceDiagnosticRresultsGrid = Ext.create("store.storeDirServiceDiagnosticRresultsGrid"); varStoreDirServiceDiagnosticRresultsGrid.setData([], false); varStoreDirServiceDiagnosticRresultsGrid.proxy.url = HTTP_DirServiceDiagnosticRresults + "?type=Grid"; varStoreDirServiceDiagnosticRresultsGrid.load({ waitMsg: lanLoading });
                                                                        varStoreDirServiceDiagnosticRresultsGrid.on('load', function () {
                                                                            varStoreDirServiceNomenTypicalFaultsGrid = Ext.create("store.storeDirServiceNomenTypicalFaultsGrid"); varStoreDirServiceNomenTypicalFaultsGrid.setData([], false); varStoreDirServiceNomenTypicalFaultsGrid.proxy.url = HTTP_DirServiceNomenTypicalFaults + "?type=Grid"; varStoreDirServiceNomenTypicalFaultsGrid.load({ waitMsg: lanLoading });
                                                                            varStoreDirServiceNomenTypicalFaultsGrid.on('load', function () {
                                                                                varStoreDirReturnTypesGrid = Ext.create("store.storeDirReturnTypesGrid"); varStoreDirReturnTypesGrid.setData([], false); varStoreDirReturnTypesGrid.proxy.url = HTTP_DirReturnTypes + "?type=Grid"; varStoreDirReturnTypesGrid.load({ waitMsg: lanLoading });
                                                                                varStoreDirReturnTypesGrid.on('load', function () {
                                                                                    loadingMask.hide();
                                                                                });
                                                                            });
                                                                        });
                                                                    });
                                                                });
                                                            });
                                                        });
                                                    });
                                                });
                                            });
                                        });
                                    });
                                });
                            });
                        });
                    }


                    // === === === sDataSysSettings === === === 
                    if (sDataSysSettings != undefined) {
                        varJurDateS = Ext.Date.format(new Date(sDataSysSettings.JurDateS), 'Y-m-d');
                        varJurDatePo = Ext.Date.format(new Date(sDataSysSettings.JurDatePo), 'Y-m-d');

                        varFractionalPartInSum = parseInt(sDataSysSettings.FractionalPartInSum);
                        varFractionalPartInPrice = parseInt(sDataSysSettings.FractionalPartInPrice);
                        varFractionalPartInOther = parseInt(sDataSysSettings.FractionalPartInOther);
                        varDirVatValue = parseFloat(sDataSysSettings.DirVatValue);
                        varChangePriceNomen = sDataSysSettings.ChangePriceNomen;
                        varMethodAccounting = sDataSysSettings.MethodAccounting;
                        varDeletedRecordsShow = sDataSysSettings.DeletedRecordsShow;

                        varDirContractorIDOrg = sDataSysSettings.dirContractorOrg.DirContractorID;
                        varDirContractorNameOrg = sDataSysSettings.dirContractorOrg.DirContractorName;

                        varDirCurrencyID = parseInt(sDataSysSettings.DirCurrencyID);
                        varDirCurrencyName = sDataSysSettings.dirCurrency.DirCurrencyName;
                        varDirCurrencyNameShort = sDataSysSettings.dirCurrency.DirCurrencyNameShort;
                        varDirCurrencyRate = parseFloat(sData1.DirCurrencyRate);
                        varDirCurrencyMultiplicity = parseFloat(sData1.DirCurrencyMultiplicity);

                        if (!Variables_SettingsRequest_Load) {
                            varDirWarehouseID = parseInt(sDataSysSettings.DirWarehouseID);
                        }

                        varMarkupRetail = parseFloat(sDataSysSettings.MarkupRetail);
                        varMarkupWholesale = parseFloat(sDataSysSettings.MarkupWholesale);
                        varMarkupIM = parseFloat(sDataSysSettings.MarkupIM);
                        varMarkupSales1 = parseFloat(sDataSysSettings.MarkupSales1);
                        varMarkupSales2 = parseFloat(sDataSysSettings.MarkupSales2);
                        varMarkupSales3 = parseFloat(sDataSysSettings.MarkupSales3);
                        varMarkupSales4 = parseFloat(sDataSysSettings.MarkupSales4);

                        varCashBookAdd = sDataSysSettings.CashBookAdd;
                        varReserve = sDataSysSettings.CashBookAdd;
                        varBarIntNomen = parseInt(sDataSysSettings.BarIntNomen);
                        varBarIntContractor = parseInt(sDataSysSettings.BarIntContractor);
                        varBarIntDoc = parseInt(sDataSysSettings.BarIntDoc);
                        varSelectOneClick = sDataSysSettings.SelectOneClick;
                        varPageSizeDir = parseInt(sDataSysSettings.PageSizeDir);
                        varPageSizeJurn = parseInt(sDataSysSettings.PageSizeJurn);
                        varDirPriceTypeID = parseInt(sDataSysSettings.DirPriceTypeID);
                        varDirNomenMinimumBalance = parseInt(sDataSysSettings.DirNomenMinimumBalance);
                        varReadinessDay = parseInt(sDataSysSettings.ReadinessDay);
                        ServiceTypeRepair = parseInt(sDataSysSettings.ServiceTypeRepair);
                        varPhoneNumberBegin = sDataSysSettings.PhoneNumberBegin;
                        SmsAutoShow = sDataSysSettings.SmsAutoShow;
                        //Срок гарантии прошёл
                        varWarrantyPeriodPassed = sDataSysSettings.WarrantyPeriodPassed;

                        //Не работает в новой версии!
                        /*
                        DateFormat = parseInt(sDataSysSettings.DateFormat);
                        if (DateFormat == 1) DateFormatStr = "Y-m-d";
                        else if (DateFormat == 2) DateFormatStr = "d-m-Y";
                        */
                    }



                    // === === === sDataDirEmployees === === === 
                    if (sDataDirEmployees != undefined) {
                        //ID-шник вошедшего Сотрудника
                        varDirEmployeeID = sDataDirEmployees.DirEmployeeID;

                        if (!Variables_SettingsRequest_Load) {
                            if (sDataDirEmployees.DirWarehouseID != null) { varDirWarehouseIDEmpl = parseInt(sDataDirEmployees.DirWarehouseID); varDirWarehouseNameEmpl = sData1.DirWarehouseName; }
                            else varDirWarehouseIDEmpl = 0;
                        }

                        if (sDataDirEmployees.DirContractorIDOrg != null) varDirContractorIDOrgEmpl = parseInt(sDataDirEmployees.DirContractorIDOrg);
                        else varDirContractorIDOrgEmpl = 0;

                        /*if (varType == "Retail") {
                            //Ext.getCmp("viewContainerCentralX").setTitle(Ext.getCmp("viewContainerCentralX").getTitle() + " (" + varDirWarehouseNameEmpl + ")");
                            Ext.getCmp("varDirWarehouseNameEmpl").setText("Точка: " + varDirWarehouseNameEmpl);
                        }*/

                        //Для кнопки в верхнем меню - используется в app.js
                        if (Ext.getCmp("HeaderToolBarEmployees") != undefined) { //Если входим в модуль Розница
                            //Ext.getCmp("HeaderToolBarEmployees").setText("<font size=" + HeaderMenu_FontSize_1 + ">" + sData.data.DirEmployeeLogin + " (СкладX)" + "</font>")
                            //Ext.getCmp("HeaderToolBarEmployees").setText("<font size=" + HeaderMenu_FontSize_1 + ">" + sData.data.DirEmployeeName + " (СкладX)" + "</font>")
                            lanDirEmployeeName = sData.data.DirEmployeesName;
                        }


                        //Права *** *** ***

                        if (Ext.getCmp("RightSysSettings0") == undefined) return;

                        //Настройки
                        if (sDataDirEmployees.RightSysSettings0 == true) { Ext.getCmp("RightSysSettings0").setVisible(true); }
                        if (sDataDirEmployees.RightMyCompany == true) { Ext.getCmp("RightMyCompany").setVisible(true); }
                        if (sDataDirEmployees.RightDirEmployees == true) { Ext.getCmp("RightDirEmployees").setVisible(true); }
                        if (sDataDirEmployees.RightSysSettings == true) { Ext.getCmp("RightSysSettings").setVisible(true); }
                        if (sDataDirEmployees.RightSysJourDisps == true) { Ext.getCmp("RightSysJourDisps").setVisible(true); }
                        if (sDataDirEmployees.RightDataExchange == true) { Ext.getCmp("RightDataExchange").setVisible(true); }
                        if (sDataDirEmployees.RightYourData == true) { Ext.getCmp("RightYourData").setVisible(true); }
                        if (sDataDirEmployees.RightDiscPay == true) { Ext.getCmp("RightDiscPay").setVisible(true); }

                        //Справочники
                        if (sDataDirEmployees.RightDir0 == true) { Ext.getCmp("RightDir0").setVisible(true); }
                        if (sDataDirEmployees.RightDirNomensCheck == true) { Ext.getCmp("RightDirNomens").setVisible(true); }
                        if (sDataDirEmployees.RightDirNomenCategoriesCheck == true) { Ext.getCmp("RightDirNomenCategories").setVisible(true); }
                        if (sDataDirEmployees.RightDirContractorsCheck == true) { Ext.getCmp("RightDirContractors").setVisible(true); }
                        if (sDataDirEmployees.RightDirWarehousesCheck == true) { Ext.getCmp("RightDirWarehouses").setVisible(true); }
                        if (sDataDirEmployees.RightDirBanksCheck == true) { Ext.getCmp("RightDirBanks").setVisible(true); }
                        if (sDataDirEmployees.RightDirCashOfficesCheck == true) { Ext.getCmp("RightDirCashOffices").setVisible(true); }
                        if (sDataDirEmployees.RightDirCurrenciesCheck == true) { Ext.getCmp("RightDirCurrencies").setVisible(true); }
                        if (sDataDirEmployees.RightDirVatsCheck == true) { Ext.getCmp("RightDirVats").setVisible(true); }
                        if (sDataDirEmployees.RightDirDiscountsCheck == true) { Ext.getCmp("RightDirDiscounts").setVisible(true); }
                        if (sDataDirEmployees.RightDirBonusesCheck == true) { Ext.getCmp("RightDirBonuses").setVisible(true); } //Ext.getCmp("RightDirBonus2es").setVisible(true);
                        if (sDataDirEmployees.RightDirCharColoursCheck == true) { Ext.getCmp("RightDirCharColours").setVisible(true); }
                        if (sDataDirEmployees.RightDirCharMaterialsCheck == true) { Ext.getCmp("RightDirCharMaterials").setVisible(true); }
                        if (sDataDirEmployees.RightDirCharNamesCheck == true) { Ext.getCmp("RightDirCharNames").setVisible(true); }
                        if (sDataDirEmployees.RightDirCharSeasonsCheck == true) { Ext.getCmp("RightDirCharSeasons").setVisible(true); }
                        if (sDataDirEmployees.RightDirCharSexesCheck == true) { Ext.getCmp("RightDirCharSexes").setVisible(true); }
                        if (sDataDirEmployees.RightDirCharSizesCheck == true) { Ext.getCmp("RightDirCharSizes").setVisible(true); }
                        if (sDataDirEmployees.RightDirCharStylesCheck == true) { Ext.getCmp("RightDirCharStyles").setVisible(true); }
                        if (sDataDirEmployees.RightDirCharTexturesCheck == true) { Ext.getCmp("RightDirCharTextures").setVisible(true); }

                        //Торговля
                        if (sDataDirEmployees.RightDoc0 == true) { Ext.getCmp("RightDoc0").setVisible(true); }
                        if (sDataDirEmployees.RightDocPurchesCheck == true) { Ext.getCmp("RightDocPurches").setVisible(true); }
                        if (sDataDirEmployees.RightDocReturnVendorsCheck == true) { Ext.getCmp("RightDocReturnVendors").setVisible(true); }
                        if (sDataDirEmployees.RightDocMovementsCheck == true) { Ext.getCmp("RightDocMovements").setVisible(true); }
                        if (sDataDirEmployees.RightDocSalesCheck == true) { Ext.getCmp("RightDocSales").setVisible(true); }
                        if (sDataDirEmployees.RightDocReturnsCustomersCheck == true) { Ext.getCmp("RightDocReturnsCustomers").setVisible(true); }
                        if (sDataDirEmployees.RightDocActOnWorksCheck == true) { Ext.getCmp("RightDocActOnWorks").setVisible(true); }
                        if (sDataDirEmployees.RightDocAccountsCheck == true) { Ext.getCmp("RightDocAccounts").setVisible(true); }
                        if (sDataDirEmployees.RightDocActWriteOffsCheck == true) { Ext.getCmp("RightDocActWriteOffs").setVisible(true); }
                        if (sDataDirEmployees.RightDocInventoriesCheck == true) { Ext.getCmp("RightDocInventories").setVisible(true); }
                        if (sDataDirEmployees.RightDocRetailsCheck == true) { Ext.getCmp("RightDocRetails").setVisible(true); }
                        if (sDataDirEmployees.RightDocNomenRevaluationsCheck == true) { Ext.getCmp("RightDocNomenRevaluations").setVisible(true); }
                        //Скидки в документах
                        if (sDataDirEmployees.RightDocDescriptionCheck == true) { varRightDocDescriptionCheck = sDataDirEmployees.RightDocDescriptionCheck; }

                        //Сервис
                        if (sDataDirEmployees.RightDocService0 == true) { Ext.getCmp("RightDocService0").setVisible(true); }
                        if (sDataDirEmployees.RightDocServicePurchesCheck == true) { Ext.getCmp("RightDocServicePurches").setVisible(true); }
                        if (sDataDirEmployees.RightDocServiceWorkshopsCheck == true) { Ext.getCmp("RightDocServiceWorkshops").setVisible(true); }
                        //if (sDataDirEmployees.RightDocServiceOutputsCheck == true) { Ext.getCmp("RightDocServiceOutputs").setVisible(true); }
                        //if (sDataDirEmployees.RightDocServiceArchivesCheck == true) { Ext.getCmp("RightDocServiceArchives").setVisible(true); }
                        if (sDataDirEmployees.RightDocServicePurchesReportCheck == true) { Ext.getCmp("RightDocServicePurchesReport").setVisible(true); }
                        if (sDataDirEmployees.RightDirServiceNomensCheck == true) { Ext.getCmp("RightDirServiceNomens").setVisible(true); }
                        //if (sDataDirEmployees.RightDirServiceNomenCategoriesCheck == true) { Ext.getCmp("RightDirServiceNomenCategories").setVisible(true); }
                        if (sDataDirEmployees.RightDirServiceContractorsCheck == true) { Ext.getCmp("RightDirServiceContractors").setVisible(true); }
                        if (sDataDirEmployees.RightDirServiceJobNomensCheck == true) { Ext.getCmp("RightDirServiceJobNomens").setVisible(true); Ext.getCmp("RightDirServiceJobNomens1").setVisible(true); }
                        if (sDataDirEmployees.RightDirSmsTemplatesCheck == true) { Ext.getCmp("RightDirSmsTemplates").setVisible(true); }
                        if (sDataDirEmployees.RightDirServiceDiagnosticRresultsCheck == true) { Ext.getCmp("RightDirServiceDiagnosticRresults").setVisible(true); }
                        if (sDataDirEmployees.RightDirServiceNomenTypicalFaultsCheck == true) { Ext.getCmp("RightDirServiceNomenTypicalFaults").setVisible(true); }

                        //Б/У
                        if (sDataDirEmployees.RightDocSecondHands0 == true) { Ext.getCmp("RightDocSecondHands0").setVisible(true); }
                        if (sDataDirEmployees.RightDocSecondHandPurchesCheck == true) { Ext.getCmp("RightDocSecondHandPurches").setVisible(true); }
                        if (sDataDirEmployees.RightDocSecondHandWorkshopsCheck == true) { Ext.getCmp("RightDocSecondHandWorkshops").setVisible(true); }
                        if (sDataDirEmployees.RightDocSecondHandRetailsCheck == true) { Ext.getCmp("RightDocSecondHandRetails").setVisible(true); }
                        if (sDataDirEmployees.RightDocSecondHandsReportCheck == true) { Ext.getCmp("RightDocSecondHandsReport").setVisible(true); }

                        //Заказы
                        if (sDataDirEmployees.RightDocOrderInt0 == true) { Ext.getCmp("RightDocOrderInt0").setVisible(true); }
                        if (sDataDirEmployees.RightDocOrderIntsNewCheck == true) { Ext.getCmp("RightDocOrderIntsNew").setVisible(true); }
                        if (sDataDirEmployees.RightDocOrderIntsCheck == true) { Ext.getCmp("RightDocOrderInts").setVisible(true); }
                        if (sDataDirEmployees.RightDocOrderIntsReportCheck == true) { Ext.getCmp("RightDocOrderIntsReport").setVisible(true); }
                        //if (sDataDirEmployees.RightDocOrderIntsArchiveCheck == true) { Ext.getCmp("RightDocOrderIntsArchive").setVisible(true); }

                        //Деньги: Касса + Банк
                        if (sDataDirEmployees.RightDocBankCash0 == true) { Ext.getCmp("RightDocBankCash0").setVisible(true); }
                        if (sDataDirEmployees.RightDocBankSumsCheck == true) { Ext.getCmp("RightDocBankSums").setVisible(true); }
                        if (sDataDirEmployees.RightDocCashOfficeSumsCheck == true) { Ext.getCmp("RightDocCashOfficeSums").setVisible(true); }
                        if (sDataDirEmployees.RightReportBanksCashOfficesCheck == true) { Ext.getCmp("RightReportBanksCashOffices").setVisible(true); }
                        if (sDataDirEmployees.RightReportSalariesCheck == true) { Ext.getCmp("RightReportSalaries").setVisible(true); }
                        if (sDataDirEmployees.RightReportSalariesWarehousesCheck == true) { Ext.getCmp("RightReportSalariesWarehouses").setVisible(true); }
                        if (sDataDirEmployees.RightDirDomesticExpensesCheck == true) { Ext.getCmp("RightDirDomesticExpenses").setVisible(true); }
                        if (sDataDirEmployees.RightDocDomesticExpenses == true) { Ext.getCmp("RightDocDomesticExpenses").setVisible(true); }

                        //Логистика
                        if (sDataDirEmployees.RightLogistics0 == true) { Ext.getCmp("RightLogistics0").setVisible(true); }
                        if (sDataDirEmployees.RightDocMovementsLogisticsCheck == true) { Ext.getCmp("RightDocMovementsLogistics").setVisible(true); }

                        //Отчеты
                        //if (sDataDirEmployees.RightReport0 == true) { Ext.getCmp("RightReport0").setVisible(true); }
                        //if (sDataDirEmployees.RightReportPriceListCheck == true) { Ext.getCmp("RightReportPriceList").setVisible(true); }
                        //if (sDataDirEmployees.RightReportRemnantsCheck == true) { Ext.getCmp("RightReportRemnants").setVisible(true); }
                        //if (sDataDirEmployees.RightReportProfitCheck == true) { Ext.getCmp("RightReportProfit").setVisible(true); }


                        if (sDataDirEmployees.RightDevelopCheck == true) { Ext.getCmp("RightDevelop").setVisible(true); }


                    }



                    // === === === Остальное === === === 
                    if (sData != undefined) {
                        //Для кнопки в верхнем меню
                        /*
                        var _btnEmployees = Ext.getCmp("viewContainerHeader").getComponent('btnEmployees');
                        if (_btnEmployees != undefined) { //Если входим в модуль Розница
                            _btnEmployees.setText("<font size=" + HeaderMenu_FontSize_1 + ">" + sData1.DirEmployeeLogin + "</font>")
                            _btnEmployees.setTooltip("<font size=" + HeaderMenu_FontSize_1 + ">" + lanEmployee + ": " + sData1.DirEmployeeName + "</font>")
                            lanDirEmployeeName = sData1.DirEmployeeName;
                        }*/

                        //Под каким именем вошли
                        varLoginMS = sData1.LoginMS;
                        varDirEmployeeLogin = sData1.DirEmployeeLogin;
                        lanDirEmployeeName = sData1.DirEmployeeName;

                        //Оплаты
                        varDirPayServiceName = sData1.DirPayServiceName;
                        varCountUser = sData1.CountUser;
                        //varCountTT = sData1.CountTT;
                        varCountNomen = sData1.CountNomen;
                        varPayDateEnd = Ext.Date.format(new Date(sData1.PayDateEnd), 'Y-m-d');
                        //varCountIM = sData1.CountIM;


                        //Оплата: если осталось менее 4-х дней выводим "viewMain"
                        /*
                        var dToday = new Date(); // текущая дата
                        var dPayDateEnd = new Date(varPayDateEnd); // дата конца оплаты
                        nDaysLeft = dPayDateEnd > dToday ? Math.ceil((dPayDateEnd - dToday) / (1000 * 60 * 60 * 24)) : null; // а тут мы вычисляем, сколько же осталось дней — находим разницу в миллисекундах и переводим её в дни
                        if (nDaysLeft <= 5 || varPayDateEnd == "2150-01-01") {
                            varPaymentExpired = true; //Оплата просрочена

                            var Params = ["viewPanelMain"];
                            ObjectConfig("viewMain", Params);
                        }
                        */

                        /*
                        if (varType == "") {
                            var Params = ["viewPanelMain"];
                            ObjectConfig("viewMain", Params);
                        }
                        */
                    }


                    //Всё! Загрузили настройки!
                    Variables_SettingsRequest_Load = true;
                }

            },
            failure: function (result) {
                if (varCountErrorSettingsRequest < varCountErrorRequest + 5) {
                    varCountErrorSettingsRequest++;
                    Variables_SettingsRequest();
                }
                else {
                    Ext.MessageBox.show({
                        title: lanOrgName,
                        msg: txtMsg017,
                        icon: Ext.MessageBox.QUESTION, buttons: Ext.Msg.YESNO, width: 300, closable: false,
                        fn: function (buttons) {
                            if (buttons == "yes") { location.reload(); }
                        }
                    });
                }
            }
        });

    }

} catch (ex) {
    var exMsg = ex;
    if (exMsg.message != undefined) exMsg = ex.message;

    Ext.Msg.alert(lanOrgName, "Ошибка в запросе на сервер!<br />Подробности:" + exMsg);
}
