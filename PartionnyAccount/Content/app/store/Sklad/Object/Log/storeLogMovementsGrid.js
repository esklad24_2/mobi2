﻿//Хранилище только для Grid
Ext.define('PartionnyAccount.store.Sklad/Object/Log/storeLogMovementsGrid', {
    extend: 'Ext.data.Store',
    alias: "store.storeLogMovementsGrid",

    storeId: 'storeLogMovementsGrid',
    model: 'PartionnyAccount.model.Sklad/Object/Log/modelLogMovementsGrid',
    pageSize: varPageSizeJurn,
    //autoLoad: true,
    proxy: {
        type: 'ajax',
        url: HTTP_LogMovements,
        reader: {
            type: "json",
            rootProperty: "LogMovement" //pID
        },
        timeout: varTimeOutDefault,
    }
});