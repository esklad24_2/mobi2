﻿//Хранилище только для Grid
Ext.define('PartionnyAccount.store.Sklad/Object/Log/storeLogSecondHandsGrid', {
    extend: 'Ext.data.Store',
    alias: "store.storeLogSecondHandsGrid",

    storeId: 'storeLogSecondHandsGrid',
    model: 'PartionnyAccount.model.Sklad/Object/Log/modelLogSecondHandsGrid',
    pageSize: varPageSizeJurn,
    //autoLoad: true,
    proxy: {
        type: 'ajax',
        url: HTTP_LogSecondHands,
        reader: {
            type: "json",
            rootProperty: "LogSecondHand" //pID
        },
        timeout: varTimeOutDefault,
    }
});