﻿//Модель только для Grid
Ext.define('PartionnyAccount.model.Sklad/Object/Log/modelLogSecondHandsGrid', {
    extend: 'Ext.data.Model',

    fields: [
        { name: "LogSecondHandID" },
        { name: "DocSecondHandPurchID" },
        
        { name: "LogSecondHandDate", type: "date" },
        { name: "DirSecondHandLogTypeID" }, { name: "DirSecondHandLogTypeName" },
        { name: "DirSecondHandstatusID" }, { name: "DirSecondHandStatusName" },
        { name: "Msg", type: "string" },
        { name: "DirEmployeeID" }, { name: "DirEmployeeName" },

        { name: "Field1" },
        //{ name: "Field2" },
    ]
});