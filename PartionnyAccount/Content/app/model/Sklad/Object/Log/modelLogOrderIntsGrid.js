﻿//Модель только для Grid
Ext.define('PartionnyAccount.model.Sklad/Object/Log/modelLogOrderIntsGrid', {
    extend: 'Ext.data.Model',

    fields: [
        { name: "LogOrderIntID" },
        { name: "DocOrderIntPurchID" },

        { name: "LogOrderIntDate", type: "date" },
        { name: "DirOrderIntLogTypeID" }, { name: "DirOrderIntLogTypeName" },
        { name: "DirOrderIntStatusID" }, { name: "DirOrderIntStatusName" },
        { name: "Msg", type: "string" },
        { name: "DirEmployeeID" }, { name: "DirEmployeeName" },

        { name: "Field1" },
        //{ name: "Field2" },
    ]
});