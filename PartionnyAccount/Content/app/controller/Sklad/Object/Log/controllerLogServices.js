﻿Ext.define("PartionnyAccount.controller.Sklad/Object/Log/controllerLogServices", {
    //Расширить
    extend: "Ext.app.Controller",
    //views: ['Sys/viewContainerHeader'],

    init: function () {
        this.control({
            //Виджет (на котором расположены Грид и ...)
            //Закрыте
            'viewLogServices': { close: this.this_close },

            'viewLogServices button#btnSave': { click: this.onBtnSaveClick },

        });
    },


    //Только для "InterfaceSystem == 3" (layout: 'card')
    //Закрытие и сделать активным другой виджет
    this_close: function (aPanel) {
        funInterfaceSystem3_closePanel(aPanel);
    },


    onBtnSaveClick: function (aButton, aEvent, aOptions) {

        var Msg = Ext.getCmp("Msg" + aButton.UO_id).getValue();
        if (Msg.length < 3) { return; }

        //Форма на Виджете
        var widgetXForm = Ext.getCmp("form_" + aButton.UO_id);
        //Сохранение
        widgetXForm.submit({
            method: "POST",
            url: HTTP_LogServices,
            timeout: varTimeOutDefault,
            waitMsg: lanUploading,
            success: function (form, action) {
                Ext.getCmp("Msg" + aButton.UO_id).setValue("");

                //Грид на форме
                //Ext.getCmp("gridLog0_" + aButton.UO_id).getStore().load();
                Ext.getCmp("tabLog_" + Ext.getCmp(aButton.UO_idCall).UO_id).getActiveTab().getStore().load();

                //Вызваный грид - не нужно т.к. закрывем форму!
                //Ext.getCmp(aButton.UO_idCall).getStore().load();
                //Закрыть
                Ext.getCmp(aButton.UO_idMain).close();
            },
            failure: function (form, action) { funPanelSubmitFailure(form, action); }
        });
    },

});