﻿Ext.define("PartionnyAccount.controller.Sklad/Object/Doc/DocServicePurches/controllerDocServiceOutputsEdit", {
    //Расширить
    extend: "Ext.app.Controller",
    //views: ['Sys/viewContainerHeader'],

    init: function () {
        this.control({
            //Виджет (на котором расположены Грид и ...)
            //Закрыте
            'viewDocServiceOutputsEdit': { close: this.this_close },


            'viewDocServiceOutputsEdit button#btnDirServiceNomenReload': { click: this.onBtnDirServiceNomenReloadClick },
            'viewDocServiceOutputsEdit #TriggerSearchTree': {
                "ontriggerclick": this.onTriggerSearchTreeClick1,
                "specialkey": this.onTriggerSearchTreeClick2,
                "change": this.onTriggerSearchTreeClick3
            },



            //Контрагент - Перегрузить
            'viewDocServiceOutputsEdit button#btnDirServiceContractorEdit': { click: this.onBtnDirServiceContractorEditClick },
            'viewDocServiceOutputsEdit button#btnDirServiceContractorReload': { click: this.onBtnDirServiceContractorReloadClick },
            //Склад - Перегрузить
            'viewDocServiceOutputsEdit [itemId=DirWarehouseID]': { select: this.onDirWarehouseIDSelect },
            'viewDocServiceOutputsEdit button#btnDirWarehouseEdit': { click: this.onBtnDirWarehouseEditClick },
            'viewDocServiceOutputsEdit button#btnDirWarehouseReload': { click: this.onBtnDirWarehouseReloadClick },
            //Currencies - Перегрузить
            'viewDocServiceOutputsEdit [itemId=DirCurrencyID]': { select: this.onDirCurrencyIDSelect },
            'viewDocServiceOutputsEdit button#btnCurrencyEdit': { "click": this.onBtnCurrencyEditClick },
            'viewDocServiceOutputsEdit button#btnCurrencyReload': { "click": this.onBtnCurrencyReloadClick },




            // === Кнопки: Сохранение, Отмена и Помощь === === ===
            'viewDocServiceOutputsEdit button#btnRevision': { click: this.onBtnRevisionClick },
            'viewDocServiceOutputsEdit button#btnOutput': { click: this.onBtnOutputClick },
            'viewDocServiceOutputsEdit button#btnCancel': { "click": this.onBtnCancelClick },
            'viewDocServiceOutputsEdit button#btnHelp': { "click": this.onBtnHelpClick },
            //***
            'viewDocServiceOutputsEdit menuitem#btnPrintHtml': { click: this.onBtnPrintHtmlClick },
            'viewDocServiceOutputsEdit menuitem#btnPrintExcel': { click: this.onBtnPrintHtmlClick },
        });
    },


    //Только для "InterfaceSystem == 3" (layout: 'card')
    //Закрытие и сделать активным другой виджет
    this_close: function (aPanel) {
        funInterfaceSystem3_closePanel(aPanel);
    },

    
    //Обновить список Товаров
    onBtnDirServiceNomenReloadClick: function (aButton, aEvent, aOptions) {
        //var storeDirServiceNomensTree = Ext.getCmp(aButton.UO_idMain).storeDirServiceNomensTree;
        var storeDirServiceNomensTree = Ext.getCmp("tree_" + aButton.UO_id).store;
        storeDirServiceNomensTree.load();
    },

    //Поиск
    onTriggerSearchTreeClick1: function (aButton, aEvent) {
        controllerDirServiceNomens_onTriggerSearchTreeClick_Search(aButton, false);
    },
    onTriggerSearchTreeClick2: function (f, e) {
        if (e.getKey() == e.ENTER) {
            controllerDirServiceNomens_onTriggerSearchTreeClick_Search(f, false);
        }
    },
    onTriggerSearchTreeClick3: function (e, textReal, textLast) {
        if (textReal.length > 2) {
            //funGridDir(e.UO_id, textReal, HTTP_DirServiceNomens);
            //alert("В стадии разработки ...");
        }
    },



    // Группа (itemId=tree) === === === === === === === === === ===

    //Меню Группы
    onTree_expandAll: function (aButton, aEvent) {
        Ext.getCmp("tree_" + aButton.UO_id).expandAll();
    },
    onTree_collapseAll: function (aButton, aEvent) {
        Ext.getCmp("tree_" + aButton.UO_id).collapseAll();
    },

    onTree_folderNew: function (aButton, aEvent) {
        controllerDocServiceOutputsEdit_onTree_folderNew(aButton.UO_id);
    },
    onTree_folderNewSub: function (aButton, aEvent) {
        controllerDocServiceOutputsEdit_onTree_folderNewSub(aButton.UO_id);
    },
    onTree_FolderEdit: function (aButton, aEvent) {
        controllerDocServiceOutputsEdit_onTree_folderEdit(aButton.UO_id);
    },
    onTree_FolderCopy: function (aButton, aEvent) {
        controllerDocServiceOutputsEdit_onTree_folderCopy(aButton.UO_id);
    },
    onTree_folderDel: function (aButton, aEvent, aOptions) {
        controllerDocServiceOutputsEdit_onTree_folderDel(aButton.UO_id);
    },

    // Селект Группы
    onTree_selectionchange: function (model, records) {
        model.view.ownerGrid.down("#FolderNewSub").setDisabled(records.length === 0);
        model.view.ownerGrid.down("#FolderCopy").setDisabled(records.length === 0);
        model.view.ownerGrid.down("#FolderDel").setDisabled(records.length === 0);
    },
    // Клик по Группе
    onTree_itemclick: function (view, rec, item, index, eventObj) {

        var id = view.grid.UO_id;
        //Полный путь от Группы к выбранному объкту
        Ext.getCmp("DirServiceNomenPatchFull" + id).setValue(rec.get('DirServiceNomenPatchFull'));

        //Аппарат
        if (Ext.getCmp("DirServiceNomenID" + id).getValue() > 0) { Ext.Msg.alert(lanOrgName, "Вы сменили Аппарат с " + Ext.getCmp("DirServiceNomenName" + id).getValue() + " на " + rec.get('text')); }

        Ext.getCmp("DirServiceNomenID" + id).setValue(rec.get('id'));
        Ext.getCmp("DirServiceNomenName" + id).setValue(rec.get('text'));
    },
    // Дабл клик по Группе - не используется
    onTree_itemdblclick: function (view, rec, item, index, eventObj) {
        //alert("onTree_itemdbclick");
    },


    //beforedrop
    onTree_beforedrop: function (node, data, overModel, dropPosition, dropPosition1, dropPosition2, dropPosition3) {

        //Если это не узел, то выйти и сообщить об этом!
        if (overModel.data.leaf) { Ext.Msg.alert(lanOrgName, "В данную ветвь перемещать запрещено!"); return; }

        //Раскроем ветку с ID=1, перед перемещением
        var treePanel = Ext.getCmp("tree_" + data.view.panel.UO_id);
        var storeDirServiceNomensTree = treePanel.getStore();
        var node = storeDirServiceNomensTree.getNodeById(overModel.data.id);
        if (node != null) {
            storeDirServiceNomensTree.UO_OnStop = false;

            //Раскрытие нужного нода
            treePanel.expandPath(node.getPath());

            if (node.firstChild == null) {
                //Событие на раскрытие - раскрылось
                storeDirServiceNomensTree.on('load', function () {

                    if (storeDirServiceNomensTree.UO_OnStop) { return; }
                    else { storeDirServiceNomensTree.UO_OnStop = true; }

                    //Запрос на сервер - !!! ДВАЖДЫ ПОВТОРЯЕТСЯ !!! №1
                    Ext.Ajax.request({
                        timeout: varTimeOutDefault,
                        url: HTTP_DirServiceNomens + "?id=" + data.records[0].data.id + "&sub=" + overModel.data.id,
                        method: 'PUT',
                        success: function (result) {
                            var sData = Ext.decode(result.responseText);
                            if (sData.success == true) {
                                
                            } else {
                                Ext.getCmp("tree_" + data.view.panel.UO_id).view.store.load();
                                Ext.MessageBox.show({ title: lanOrgName, msg: sData.data, icon: Ext.MessageBox.ERROR, buttons: Ext.Msg.OK });
                            }
                        },
                        failure: function (form, action) {
                            //Права.
                            /*if (action.result.data.msgType == "1") { Ext.Msg.alert(lanOrgName, action.result.data.msg); return; }
                            Ext.Msg.alert(lanOrgName, txtMsg008 + action.result.data);*/
                            Ext.getCmp("tree_" + data.view.panel.UO_id).view.store.load();
                            funPanelSubmitFailure(form, action);
                        }
                    });

                });
            }
            else {
                //Запрос на сервер - !!! ДВАЖДЫ ПОВТОРЯЕТСЯ !!! №2
                Ext.Ajax.request({
                    timeout: varTimeOutDefault,
                    url: HTTP_DirServiceNomens + "?id=" + data.records[0].data.id + "&sub=" + overModel.data.id,
                    method: 'PUT',
                    success: function (result) {
                        var sData = Ext.decode(result.responseText);
                        if (sData.success == true) {
                            
                        } else {
                            Ext.getCmp("tree_" + data.view.panel.UO_id).view.store.load();
                            Ext.MessageBox.show({ title: lanOrgName, msg: sData.data, icon: Ext.MessageBox.ERROR, buttons: Ext.Msg.OK });
                        }
                    },
                    failure: function (form, action) {
                        //Права.
                        /*if (action.result.data.msgType == "1") { Ext.Msg.alert(lanOrgName, action.result.data.msg); return; }
                        Ext.Msg.alert(lanOrgName, txtMsg008 + action.result.data);*/
                        Ext.getCmp("tree_" + data.view.panel.UO_id).view.store.load();
                        funPanelSubmitFailure(form, action);
                    }
                });
            }
        }

    },
    //drop
    onTree_drop: function (node, data, overModel, dropPosition) {
        //Ext.Msg.alert("Группа перемещена!");
    },



    // *** DirServiceContractorName ***
    //Редактирование или добавление нового Поставщика
    onBtnDirServiceContractorEditClick: function (aButton, aEvent, aOptions) {
        var Params = [
            aButton.id,
            false, //UO_Center
            false, //UO_Modal
            //2     // 1 - Новое, 2 - Редактировать
        ]
        ObjectConfig("viewDirServiceContractors", Params);
    },
    //РеЛоад - перегрузить тригер, что бы появились новые записи
    onBtnDirServiceContractorReloadClick: function (aButton, aEvent, aOptions) {
        var storeDirServiceContractorsGrid = Ext.getCmp(aButton.UO_idMain).storeDirServiceContractorsGrid;
        storeDirServiceContractorsGrid.load();
    },

    // *** DirWarehouse ***
    //Редактирование или добавление нового Склада
    onDirWarehouseIDSelect: function (combo, records) {
        var tree_ = Ext.getCmp("tree_" + combo.UO_id);
        tree_.store.proxy.url = HTTP_DirServiceNomens + "?DirWarehouseID=" + records.data.DirWarehouseID;
    },
    onBtnDirWarehouseEditClick: function (combo, aEvent, aOptions) {
        var Params = [
            combo.id,
            false, //UO_Center
            false, //UO_Modal
            //2     // 1 - Новое, 2 - Редактировать
        ]
        ObjectConfig("viewDirWarehouses", Params);
    },
    //РеЛоад - перегрузить тригер, что бы появились новые записи
    onBtnDirWarehouseReloadClick: function (aButton, aEvent, aOptions) {
        var storeDirWarehousesGrid = Ext.getCmp(aButton.UO_idMain).storeDirWarehousesGrid;
        storeDirWarehousesGrid.load();
    },

    // *** DirCurrency ***
    onDirCurrencyIDSelect: function (combo, records) { //aButton, aEvent, aOptions
        //Запрос на сервер за курсом м кратностью
        Ext.Msg.show({
            title: lanOrgName,
            msg: "Изменить Курс и Кратность?",
            buttons: Ext.Msg.YESNO,
            fn: function (btn) {
                if (btn == "yes") {
                    Ext.getCmp("DirCurrencyRate" + combo.UO_id).setValue(records.data.DirCurrencyRate);
                    Ext.getCmp("DirCurrencyMultiplicity" + combo.UO_id).setValue(records.data.DirCurrencyMultiplicity);
                }
            },
            icon: Ext.window.MessageBox.QUESTION
        });
    },
    onBtnCurrencyEditClick: function (aButton, aEvent, aOptions) {
        var Params = [
            aButton.id,
            false, //UO_Center
            false, //UO_Modal
            //2     // 1 - Новое, 2 - Редактировать
        ]
        ObjectConfig("viewDirCurrencies", Params);
    },
    onBtnCurrencyReloadClick: function (aButton, aEvent, aOptions) {
        var storeDirCurrenciesGrid = Ext.getCmp(aButton.UO_idMain).storeDirCurrenciesGrid;
        storeDirCurrenciesGrid.load();
    },


    //Кнопки редактирования Енеблед
    onGrid_selectionchange: function (model, records) {
        model.view.ownerGrid.down("#btnGridEdit").setDisabled(records.length === 0);
        model.view.ownerGrid.down("#btnGridDelete").setDisabled(records.length === 0);
    },
    //Клик: Редактирования или выбор
    onGrid_itemclick: function (view, record, item, index, eventObj) {
        //Если запись удалена, то выдать сообщение и выйти
        if (funGridRecordDel(record)) { return; }

        if (varSelectOneClick) {
            if (Ext.getCmp(view.grid.UO_idMain).UO_Function_Grid == undefined) {
                var Params = [
                    view.grid.id, //UO_idCallб //"grid_" + aButton.UO_id, //UO_idCall
                    true, //UO_Center
                    true, //UO_Modal
                    2,    // 1 - Новое, 2 - Редактировать
                    true, // true - Признак того, что надо сохранять в Грид, а не на сервер, false - на сервер
                    index,        // Int32 - Если редактируем, то позиция в списке: 0, 1, 2, ...
                    record,       // Для загрузки данных в форму Б.С. и Договора,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    true
                ]
                ObjectEditConfig("viewDocServicePurchTabsEdit", Params);
            }
            else {
                Ext.getCmp(view.grid.UO_idMain).UO_Function_Grid(Ext.getCmp(view.grid.UO_idCall).UO_id, record);
                Ext.getCmp(view.grid.UO_idMain).close();
            }
        }
    },
    //ДаблКлик: Редактирования или выбор
    onGrid_itemdblclick: function (view, record, item, index, e) {
        if (Ext.getCmp(view.grid.UO_idMain).UO_Function_Grid == undefined) {
            var Params = [
                view.grid.id, //UO_idCall
                true, //UO_Center
                true, //UO_Modal
                2,    // 1 - Новое, 2 - Редактировать
                true, // true - Признак того, что надо сохранять в Грид, а не на сервер, false - на сервер
                index,        // Int32 - Если редактируем, то позиция в списке: 0, 1, 2, ...
                record,        // Для загрузки данных в форму Б.С. и Договора,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                true
            ]
            ObjectEditConfig("viewDocServicePurchTabsEdit", Params);
        }
        else {
            Ext.getCmp(view.grid.UO_idMain).UO_Function_Grid(Ext.getCmp(view.grid.UO_idCall).UO_id, record);
            Ext.getCmp(view.grid.UO_idMain).close();
        }
    },


    // === Кнопки === === ===
    //На доработку
    onBtnRevisionClick: function (aButton, aEvent, aOptions) {

        //alert("Только поменять статус на 'В ремонте'");
        controllerDocServiceOutputsEdit_onBtnSaveClick(aButton, aEvent, aOptions);

    },
    //Выдать
    onBtnOutputClick: function (aButton, aEvent, aOptions) {

        //alert("Выдать: Doc.Held=true, Деньги (Касса или Банк), ... ");
        controllerDocServiceOutputsEdit_onBtnSaveClick(aButton, aEvent, aOptions);

    },

    //Отменить
    onBtnCancelClick: function (aButton, aEvent, aOptions) {
        Ext.getCmp(aButton.UO_idMain).close();
    },
    //Help
    onBtnHelpClick: function (aButton, aEvent, aOptions) {
        window.open(HTTP_Help + "dokument-servis/", '_blank');
    },
    //***
    //Распечатать
    onBtnPrintHtmlClick: function (aButton, aEvent, aOptions) {
        //aButton.UO_Action: html, excel
        //alert(aButton.UO_Action);

        //Проверка: если форма ещё не сохранена, то выход
        if (Ext.getCmp("DocServicePurchID" + aButton.UO_id).getValue() == null) { Ext.Msg.alert(lanOrgName, txtMsg066); return; }

        //Открытие списка ПФ
        var Params = [
            aButton.id,
            true, //UO_Center
            true, //UO_Modal
            aButton.UO_Action, //UO_Function_Tree: Html или Excel
            undefined,
            undefined,
            undefined,
            Ext.getCmp("DocID" + aButton.UO_id).getValue(),
            40
        ]
        ObjectConfig("viewListObjectPFs", Params);

    },
});


//Функия сохранения
function controllerDocServiceOutputsEdit_onBtnSaveClick(aButton, aEvent, aOptions) {
    //Форма на Виджете
    var widgetXForm = Ext.getCmp("form_" + aButton.UO_id);

    var sMethod = "PUT";
    var sUrl = HTTP_DocServicePurches + "?id=" + parseInt(Ext.getCmp("DocServicePurchID" + aButton.UO_id).value) + "&UO_Action=" + aButton.UO_Action;

    var iTypeService = 3;
    if (aButton.UO_Action == "revision") { iTypeService = 4; }

    Ext.MessageBox.show({
        title: lanOrgName,
        msg: "После выдачи аппарата, распечатать документы будет невозможно! Выдать?",
        icon: Ext.MessageBox.QUESTION, buttons: Ext.Msg.YESNO, width: 300, closable: false,
        fn: function (buttons) {
            if (buttons == "yes") {

                //Сохранение
                widgetXForm.submit({
                    method: sMethod,
                    url: sUrl + "&iTypeService=" + iTypeService, //url: sUrl + "&iTypeService=3",
                    timeout: varTimeOutDefault,
                    waitMsg: lanUploading,
                    success: function (form, action) {
                        if (aButton.UO_Action == "held_cancel") { Ext.getCmp("btnRecord" + aButton.UO_id).setVisible(true); }
                        else if (aButton.UO_Action == "held") { Ext.getCmp("btnRecord" + aButton.UO_id).setVisible(false); }

                        //Закрыть
                        Ext.getCmp(aButton.UO_idMain).close();
                        //Перегрузить грид, если грид открыт
                        if (Ext.getCmp(aButton.UO_idCall) != undefined && Ext.getCmp(aButton.UO_idCall).store != undefined) { Ext.getCmp(aButton.UO_idCall).getStore().load(); }
                        Ext.getCmp("btnPrint" + aButton.UO_id).setVisible(true);
                    },
                    failure: function (form, action) { funPanelSubmitFailure(form, action); }
                });

            }
        }
    });

};