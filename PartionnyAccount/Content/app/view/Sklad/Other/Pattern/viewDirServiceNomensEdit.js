﻿Ext.define("PartionnyAccount.view.Sklad/Other/Pattern/viewDirServiceNomensEdit", {
    extend: "Ext.panel.Panel",
    //extend: InterfaceSystemObjName,
    alias: "widget.viewDirServiceNomensEdit",

    layout: "border",
    region: "center",
    //title: lanGoods,
    //width: 750, height: 350,
    autoScroll: false,

    UO_maximize: false,  //Максимизировать во весь экран
    UO_Center: false,    //true - в центре экрана, false - окна каскадом
    UO_Modal: false,     //true - Все остальные элементы не активные
    buttonAlign: 'left',

    UO_Function_Tree: undefined,  //Fn - если открыли для выбора или из Tree
    UO_Function_Grid: undefined,  //Fn - если открыли для выбора или из Грида

    conf: {},

    initComponent: function () {
        
        
        var GeneralPanel = Ext.create('Ext.panel.Panel', {
            id: "PanelGeneral_" + this.UO_id, UO_id: this.UO_id, UO_idMain: this.UO_idMain, UO_idCall: this.UO_idCall,

            region: "north", //!!! Важно для Ресайз-а !!!
            bodyStyle: 'background:transparent;',
            //title: lanGeneral,
            bodyPadding: 5,
            frame: true,
            monitorValid: true,
            defaultType: 'textfield',
            width: "100%", height: "50", //width: 500, height: 200,
            layout: 'anchor',
            defaults: { anchor: '100%' },
            autoScroll: true,

            items: [
                //ID
                { xtype: 'textfield', fieldLabel: "DirNomeID", name: "DirServiceNomenID", id: "DirServiceNomenID" + this.UO_id, allowBlank: true, hidden: true },
                { xtype: 'textfield', fieldLabel: "Sub", name: "Sub", id: "Sub" + this.UO_id, allowBlank: true, hidden: true },
                
                //Наименование
                { xtype: 'textfield', fieldLabel: lanName, name: "DirServiceNomenName", id: "DirServiceNomenName" + this.UO_id, flex: 1, allowBlank: false },
                { xtype: 'textfield', fieldLabel: lanNameFull, name: "DirServiceNomenNameFull", id: "DirServiceNomenNameFull" + this.UO_id, flex: 1, allowBlank: true },
            ],
        });


        //2. Tab
        var rowEditing = Ext.create('Ext.grid.plugin.RowEditing');
        var PanelGridDirServiceNomenPrice = Ext.create('Ext.grid.Panel', {
            id: "PanelGridDirServiceNomenPrice_" + this.UO_id, UO_id: this.UO_id, UO_idMain: this.UO_idMain, UO_idCall: this.UO_idCall,

            region: "center",
            loadMask: true,
            //autoScroll: true,
            //touchScroll: true,

            title: "Типовые неисправоности",
            itemId: "PanelGridDirServiceNomenPrice_grid",

            store: this.storeDirServiceNomenPricesGrid,

            columns: [
                { text: lanName, dataIndex: "DirServiceNomenTypicalFaultName", flex: 1 },
                { text: "Цена", dataIndex: "PriceVAT", width: 75, editor: { xtype: 'textfield' } }
            ],

            tbar: [
                {
                    UO_id: this.UO_id, UO_idMain: this.UO_idMain, UO_idCall: this.UO_idCall,
                    xtype: "button",
                    icon: '../Scripts/sklad/images/table_add.png', text: "Добавить все", tooltip: "Удалить и добавить все",
                    itemId: "btnGridAddAll",
                },
                {
                    UO_id: this.UO_id, UO_idMain: this.UO_idMain, UO_idCall: this.UO_idCall,
                    xtype: "button",
                    icon: '../Scripts/sklad/images/table_delete.png', text: "Удалить все", tooltip: lanDeletionFlag + "?", 
                    id: "btnGridDeletion" + this.UO_id, itemId: "btnGridDelete"
                },
            ],

            plugins: [rowEditing],
            rowEditing: rowEditing,

        });



        var groupPanel = Ext.create('Ext.panel.Panel', {
            id: "groupPanel_" + this.UO_id, UO_id: this.UO_id, UO_idMain: this.UO_idMain, UO_idCall: this.UO_idCall,
            UO_Loaded: this.UO_Loaded,

            //Если редактируем в других объектах, например в других справочниках (Контрагент -> Банковские счета, Договора)
            //Данные для Чтения/Сохранения с/на Сервер или с/в Грид
            UO_GridSave: this.UO_GridSave,     // true - Признак того, что надо сохранять в Грид, а не на сервер, false - на сервер
            UO_GridIndex: this.UO_GridIndex,   // Int32 - Если редактируем, то позиция в списке: 0, 1, 2, ...
            UO_GridRecord: this.UO_GridRecord, // Если пустое, то читаем/пишем с/на Сервера. Иначе Грид (Данные загружаются/пишутся с/на сервера, Данные загружаются/пишутся в Грид)


            bodyStyle: 'background:transparent;', //bodyStyle: 'opacity:0.5;',
            region: "center", //!!! Важно для Ресайз-а !!!
            monitorValid: true,
            defaultType: 'textfield',

            layout: 'border',
            defaults: { anchor: '100%' },

            region: "center",
            width: "100%", height: "100%",
            bodyPadding: 5,
            autoHeight: true,
            //autoScroll: true,


            items: [
                GeneralPanel, PanelGridDirServiceNomenPrice
            ],


            //buttonAlign: 'left',
            buttons: [
                {
                    UO_id: this.UO_id, UO_idMain: this.UO_idMain, UO_idCall: this.UO_idCall, itemId: "btnSave",
                    text: lanSave, icon: '../Scripts/sklad/images/save.png'
                },
                " ",
                {
                    UO_id: this.UO_id, UO_idMain: this.UO_idMain, UO_idCall: this.UO_idCall, itemId: "btnCancel",
                    text: lanCancel, icon: '../Scripts/sklad/images/cancel.png'
                },

                "-",

                {
                    UO_id: this.UO_id, UO_idMain: this.UO_idMain, UO_idCall: this.UO_idCall, itemId: "btnHelp",
                    text: lanHelp, icon: '../Scripts/sklad/images/help16.png'
                },

            ]

        });



        //body
        this.items = [
            
            groupPanel

        ],


        this.callParent(arguments);
    }

});