﻿Ext.define("PartionnyAccount.view.Sklad/Object/Doc/DocServicePurches/viewDocServicePurches", {
    //extend: "Ext.panel.Panel",
    extend: InterfaceSystemObjName,
    alias: "widget.viewDocServicePurches",

    layout: "border",
    region: "center",
    title: "Сервис - Приёмка",
    width: 750, height: 350,
    autoScroll: false,

    UO_maximize: false,  //Максимизировать во весь экран
    UO_Center: false,    //true - в центре экрана, false - окна каскадом
    UO_Modal: false,     //true - Все остальные элементы не активные
    buttonAlign: 'left',

    UO_Function_Tree: undefined,  //Fn - если открыли для выбора или из Tree
    UO_Function_Grid: undefined,  //Fn - если открыли для выбора или из Грида

    bodyStyle: 'background:white;',
    bodyPadding: varBodyPadding,

    conf: {},

    initComponent: function () {

        //body
        this.items = [

            /*
            Ext.create('Ext.tree.Panel', {
                id: "tree_" + this.UO_id,  //WingetName + ObjectID
                UO_id: this.UO_id,         //ObjectID
                UO_idMain: this.UO_idMain, //id-шник Панели, на которой находятся виджеты
                UO_idCall: this.UO_idCall, //id-шник Виджета, который визвал Виджет

                title: "Фильтр",
                region: "west",
                loadMask: true,
                rootVisible: false,
                width: 150,
                split: true,
                collapsible: true,
                useArrows: true,
                itemId: "tree",

                root: {
                    text: 'Фильтр',
                    expanded: true,
                    children:
                    [
                        {
                            id: 0,
                            text: "Все",
                            leaf: true,
                            icon: '../Scripts/sklad/images/doc_all.png'
                        },
                        {
                            id: 1,
                            text: "Статус-1",
                            leaf: true,
                            icon: '../Scripts/sklad/images/status16.png'
                        },
                        {
                            id: 2,
                            text: "Статус-2",
                            leaf: true,
                            icon: '../Scripts/sklad/images/status16.png'
                        },
                        {
                            id: 3,
                            text: "Статус-3",
                            leaf: true,
                            icon: '../Scripts/sklad/images/status16.png'
                        },
                        {
                            id: 4,
                            text: "Статус-4",
                            leaf: true,
                            icon: '../Scripts/sklad/images/status16.png'
                        },
                        {
                            id: 5,
                            text: "Статус-5",
                            leaf: true,
                            icon: '../Scripts/sklad/images/status16.png'
                        },
                        {
                            id: 6,
                            text: "Статус-6",
                            leaf: true,
                            icon: '../Scripts/sklad/images/status16.png'
                        },
                        {
                            id: 7,
                            text: "Статус-7",
                            leaf: true,
                            icon: '../Scripts/sklad/images/status16.png'
                        },
                        {
                            id: 8,
                            text: "Статус-8",
                            leaf: true,
                            icon: '../Scripts/sklad/images/status16.png'
                        }
                    ]
                }

            }),
            */

            // *** *** *** *** *** *** *** *** ***


            Ext.create('widget.viewGridDoc', {

                conf: {
                    id: "grid_" + this.UO_id,  //WingetName + ObjectID
                    UO_id: this.UO_id,         //ObjectID
                    UO_idMain: this.UO_idMain, //id-шник Панели, на которой находятся виджеты
                    UO_idCall: this.UO_idCall, //id-шник Виджета, который визвал Виджет
                    UO_View: this.UO_View,     //Название Виджета на котором расположен Грид, нужен для "Стилей" (раскраска грида)
                },

                store: this.storeGrid,

                columns: [
                    { text: "", dataIndex: "Status", width: 17, tdCls: 'x-change-cell2' },
                    { text: "№ desk", dataIndex: "DocID", width: 50, hidden: true, tdCls: 'x-change-cell' },
                    { text: "№", dataIndex: "DocServicePurchID", width: 50, tdCls: 'x-change-cell' },
                    { text: lanDocDate, dataIndex: "DocDate", width: 50, tdCls: 'x-change-cell' },

                    { text: lanOrg, dataIndex: "DirContractorNameOrg", flex: 1, tdCls: 'x-change-cell' },
                    { text: lanContractor, dataIndex: "DirContractorName", flex: 1, tdCls: 'x-change-cell' },
                    { text: lanWarehouse, dataIndex: "DirWarehouseName", flex: 1, tdCls: 'x-change-cell' },
                    { text: lanNomenclature, dataIndex: "DirServiceNomenName", flex: 1, tdCls: 'x-change-cell' },
                    { text: "Серийный", dataIndex: "SerialNumber", flex: 1, tdCls: 'x-change-cell' },
                    { text: "Неисправность", dataIndex: "ProblemClientWords", flex: 1, tdCls: 'x-change-cell', hidden: true },
                    { text: "Клиент", dataIndex: "DirServiceContractorName", flex: 1, tdCls: 'x-change-cell' },
                    { text: lanPhone, dataIndex: "DirServiceContractorPhone", flex: 1, tdCls: 'x-change-cell', hidden: true },
                    { text: "Срочный", dataIndex: "UrgentRepairs", flex: 1, tdCls: 'x-change-cell', hidden: true },
                    { text: "Готовность", dataIndex: "DateDone", width: 50, tdCls: 'x-change-cell', hidden: true },
                    { text: lanState, dataIndex: "DirServiceStatusName", flex: 1, tdCls: 'x-change-cell' },
                    { text: "Предоплата", dataIndex: "Prepayment", width: 50, tdCls: 'x-change-cell', hidden: true }
                ],

                //В Константах "нижнея панель" не нужна
                bbar: new Ext.PagingToolbar({
                    store: this.storeGrid,                      // указано хранилище
                    displayInfo: true,                          // вывести инфо обо общем числе записей
                    displayMsg: lanShowing + "  {0} - {1} " + lanOf + " {2}"     // формат инфо
                }),

            }),

        ],


        this.callParent(arguments);
    }

});