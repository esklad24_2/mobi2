﻿Ext.define("PartionnyAccount.view.Sklad/Object/Doc/DocServicePurches/viewDocServiceWorkshopsEdit", {
    //extend: "Ext.panel.Panel",
    extend: InterfaceSystemObjName,
    alias: "widget.viewDocServiceWorkshopsEdit",

    layout: "border",
    region: "center",
    title: "Сервис - Мастерская", 
    width: 800, height: 550,
    autoScroll: false,

    UO_maximize: false,  //Максимизировать во весь экран
    UO_Center: false,    //true - в центре экрана, false - окна каскадом
    UO_Modal: false,     //true - Все остальные элементы не активные
    buttonAlign: 'left',

    UO_Function_Tree: undefined,  //Fn - если открыли для выбора или из Tree
    UO_Function_Grid: undefined,  //Fn - если открыли для выбора или из Грида

    bodyStyle: 'background:white;',
    bodyPadding: varBodyPadding,

    conf: {},

    initComponent: function () {


        //Tab
        //*** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** ***

        var PanelCap1 = Ext.create('Ext.form.Panel', {
            id: "panel1_" + this.UO_id, UO_id: this.UO_id, UO_idMain: this.UO_idMain, UO_idCall: this.UO_idCall,

            //Если редактируем в других объектах, например в других справочниках (Контрагент -> Банковские счета, Договора)
            //Данные для Чтения/Сохранения с/на Сервер или с/в Грид
            UO_GridSave: this.UO_GridSave,     // true - Признак того, что надо сохранять в Грид, а не на сервер, false - на сервер
            UO_GridIndex: this.UO_GridIndex,   // Int32 - Если редактируем, то позиция в списке: 0, 1, 2, ...
            UO_GridRecord: this.UO_GridRecord, // Если пустое, то читаем/пишем с/на Сервера. Иначе Грид (Данные загружаются/пишутся с/на сервера, Данные загружаются/пишутся в Грид)
            
            region: "center", //!!! Важно для Ресайз-а !!!
            bodyStyle: 'background:transparent;',
            title: "Статус",
            frame: true,
            monitorValid: true,
            defaultType: 'textfield',
            width: "100%", height: "100%", //width: 500, height: 200,
            bodyPadding: 5,
            layout: 'anchor',
            defaults: { anchor: '100%' },
            autoScroll: true,
            autoHeight: true,

            items: [
                {
                    xtype: 'container', width: "95%", layout: { align: 'stretch', type: 'hbox' },
                    items: [
                        {
                            xtype: 'viewComboBox',
                            fieldLabel: lanState, flex: 1, allowBlank: false,
                            store: this.storeDirServiceStatusesGrid, // store getting items from server
                            valueField: 'DirServiceStatusID',
                            hiddenName: 'DirServiceStatusID',
                            displayField: 'DirServiceStatusName',
                            name: 'DirServiceStatusID', itemId: "DirServiceStatusID", id: "DirServiceStatusID" + this.UO_id, UO_id: this.UO_id, UO_idMain: this.UO_idMain, UO_idCall: this.UO_idCall,
                            //Поиск
                            //editable: true, typeAhead: true, minChars: 2
                        },

                        {
                            xtype: 'viewComboBox',
                            fieldLabel: "Мастер", flex: 1, allowBlank: false, //, emptyText: "..."
                            margin: "0 0 0 5",
                            store: this.storeDirEmployeesGrid, // store getting items from server
                            valueField: 'DirEmployeeID',
                            hiddenName: 'DirEmployeeID',
                            displayField: 'DirEmployeeName',
                            name: 'DirEmployeeIDMaster', itemId: "DirEmployeeIDMaster", id: "DirEmployeeIDMaster" + this.UO_id, UO_id: this.UO_id, UO_idMain: this.UO_idMain, UO_idCall: this.UO_idCall,
                            //Поиск
                            editable: false, typeAhead: false, minChars: 200,
                        },
                    ]
                }
            ],

        });

        var PanelCap2 = Ext.create('Ext.form.Panel', {
            id: "panel2_" + this.UO_id, UO_id: this.UO_id, UO_idMain: this.UO_idMain, UO_idCall: this.UO_idCall,

            //Если редактируем в других объектах, например в других справочниках (Контрагент -> Банковские счета, Договора)
            //Данные для Чтения/Сохранения с/на Сервер или с/в Грид
            UO_GridSave: this.UO_GridSave,     // true - Признак того, что надо сохранять в Грид, а не на сервер, false - на сервер
            UO_GridIndex: this.UO_GridIndex,   // Int32 - Если редактируем, то позиция в списке: 0, 1, 2, ...
            UO_GridRecord: this.UO_GridRecord, // Если пустое, то читаем/пишем с/на Сервера. Иначе Грид (Данные загружаются/пишутся с/на сервера, Данные загружаются/пишутся в Грид)

            region: "center", //!!! Важно для Ресайз-а !!!
            bodyStyle: 'background:transparent;',
            title: "Информация",
            frame: true,
            monitorValid: true,
            defaultType: 'textfield',
            width: "100%", height: "100%", //width: 500, height: 200,
            bodyPadding: 5,
            layout: 'anchor',
            defaults: { anchor: '100%' },
            autoScroll: true,
            autoHeight: true,

            items: [

                //Документ *** *** *** *** *** *** *** *** *** *** *** *** *** ***

                { xtype: 'textfield', fieldLabel: "DocID2", name: 'DocID2', id: 'DocID2' + this.UO_id, readOnly: true, allowBlank: true, hidden: true },
                { xtype: 'textfield', fieldLabel: "Held", name: 'Held', id: 'Held' + this.UO_id, readOnly: true, allowBlank: true, hidden: true },
                { xtype: 'textfield', fieldLabel: "DocID", name: "DocID", id: "DocID" + this.UO_id, readOnly: true, allowBlank: true, hidden: true },
                { xtype: 'textfield', fieldLabel: "№", name: "DocServicePurchID", id: "DocServicePurchID" + this.UO_id, readOnly: true, flex: 1, allowBlank: true, hidden: true },
                { xtype: 'textfield', fieldLabel: lanManual, name: "NumberInt", id: "NumberInt" + this.UO_id, margin: "0 0 0 5", flex: 1, allowBlank: true, hidden: true },
                { xtype: 'viewDateField', fieldLabel: lanDateCounterparty, name: "DocDate", id: "DocDate" + this.UO_id, margin: "0 0 0 5", allowBlank: false, editable: false, hidden: true },
                { xtype: 'textfield', name: "DirContractorIDOrg", readOnly: true, flex: 1, id: "DirContractorIDOrg" + this.UO_id, allowBlank: false, hidden: true },
                { xtype: 'textfield', name: "DirServiceContractorID", readOnly: true, flex: 1, id: "DirServiceContractorID" + this.UO_id, allowBlank: true, hidden: true },
                { xtype: 'textfield', name: "DirWarehouseID", readOnly: true, flex: 1, id: "DirWarehouseID" + this.UO_id, allowBlank: false, hidden: true },
                { xtype: 'textfield', name: "DirCurrencyID", readOnly: true, flex: 1, id: "DirCurrencyID" + this.UO_id, allowBlank: false, hidden: true },
                { xtype: 'textfield', name: "DirCurrencyRate", readOnly: true, flex: 1, id: "DirCurrencyRate" + this.UO_id, allowBlank: false, hidden: true },
                { xtype: 'textfield', name: "DirCurrencyMultiplicity", readOnly: true, flex: 1, id: "DirCurrencyMultiplicity" + this.UO_id, allowBlank: false, hidden: true },
                { xtype: 'textfield', name: "PriceVAT", readOnly: true, flex: 1, id: "PriceVAT" + this.UO_id, allowBlank: false, hidden: true },
                { xtype: 'textfield', name: "Prepayment", readOnly: true, flex: 1, id: "Prepayment" + this.UO_id, allowBlank: false, hidden: true },
                { xtype: 'viewDateField', fieldLabel: "Дата готовности", name: "DateDone", id: "DateDone" + this.UO_id, allowBlank: false, editable: false, margin: "0 0 0 5", hidden: true },

                {
                    xtype: 'container', width: "95%", layout: { align: 'stretch', type: 'hbox' },
                    items: [
                        { xtype: 'textfield', name: "DirServiceNomenName", readOnly: true, flex: 1, id: "DirServiceNomenName" + this.UO_id, allowBlank: false },
                        { xtype: 'textfield', fieldLabel: "DirServiceNomenID", margin: "0 0 0 15", name: "DirServiceNomenID", flex: 1, id: "DirServiceNomenID" + this.UO_id, allowBlank: false, hidden: true },

                        { xtype: 'textfield', name: "SerialNumber", readOnly: true, flex: 1, id: "SerialNumber" + this.UO_id, allowBlank: false },
                    ]
                },

                { xtype: 'container', height: 5 },

                {
                    xtype: 'container', width: "95%", layout: { align: 'stretch', type: 'hbox' },
                    items: [
                        { xtype: "checkbox", boxLabel: "Аппарат", name: "ComponentDevice", itemId: "ComponentDevice", inputValue: true, id: "ComponentDevice" + this.UO_id, readOnly: true },
                        { xtype: "checkbox", boxLabel: "Задняя крышка", name: "ComponentBackCover", itemId: "ComponentBackCover", inputValue: true, id: "ComponentBackCover" + this.UO_id, readOnly: true, margin: "0 0 0 15" },
                        { xtype: "checkbox", boxLabel: "Задняя крышка", name: "UrgentRepairs", itemId: "UrgentRepairs", inputValue: true, id: "UrgentRepairs" + this.UO_id, readOnly: true, margin: "0 0 0 15" },
                        { xtype: 'textfield', name: "ComponentOtherText", flex: 1, id: "ComponentOtherText" + this.UO_id, allowBlank: true, readOnly: true, margin: "0 0 0 15" },
                    ]
                },
                {
                    xtype: 'container', width: "95%", layout: { align: 'stretch', type: 'hbox' },
                    items: [
                        { xtype: "checkbox", boxLabel: "Аккумулятор", name: "ComponentBattery", itemId: "ComponentBattery", inputValue: true, id: "ComponentBattery" + this.UO_id, readOnly: true },
                        { xtype: 'textfield', fieldLabel: "с/н", name: "ComponentBatterySerial", flex: 1, id: "ComponentBatterySerial" + this.UO_id, allowBlank: true, readOnly: true },

                        { xtype: "checkbox", boxLabel: "Пароль на устройстве", margin: "0 0 0 15", name: "ComponentPass", itemId: "ComponentPass", inputValue: true, id: "ComponentPass" + this.UO_id, readOnly: true },
                        { xtype: 'textfield', fieldLabel: "с/н", name: "ComponentPasText", flex: 1, id: "ComponentPasText" + this.UO_id, allowBlank: true, readOnly: true },
                    ]
                },

                { xtype: 'container', height: 5 },

                {
                    xtype: 'container', width: "95%", layout: { align: 'stretch', type: 'hbox' },
                    items: [
                        { xtype: 'textfield', fieldLabel: "Неисправность", name: "ProblemClientWords", flex: 1, id: "ProblemClientWords" + this.UO_id, allowBlank: true, readOnly: true }, //fieldLabel: "Неисправность со слов клиента", labelAlign: "top", 
                        { xtype: 'textfield', name: "Note", flex: 1, id: "Note" + this.UO_id, allowBlank: true, readOnly: true }, //, fieldLabel: "Примечание", labelAlign: "top"
                    ]
                },

                { xtype: 'container', height: 5 },

                {
                    xtype: 'container', width: "95%", layout: { align: 'stretch', type: 'hbox' },
                    items: [
                        { xtype: 'textfield', fieldLabel: "Клиент", name: "DirServiceContractorName", flex: 1, id: "DirServiceContractorName" + this.UO_id, allowBlank: false, readOnly: true },
                        { xtype: 'textfield', name: "DirServiceContractorAddress", flex: 1, id: "DirServiceContractorAddress" + this.UO_id, allowBlank: true, readOnly: true },
                        { xtype: 'textfield', name: "DirServiceContractorPhone", flex: 1, id: "DirServiceContractorPhone" + this.UO_id, allowBlank: true, readOnly: true },
                        { xtype: 'textfield', name: "DirServiceContractorEmail", flex: 1, id: "DirServiceContractorEmail" + this.UO_id, allowBlank: true, readOnly: true },
                    ]
                }

            ],


        });


        //Tab-Panel
        var tabPanelDetails = Ext.create('Ext.tab.Panel', {
            id: "tab_" + this.UO_id,
            UO_id: this.UO_id,
            UO_idMain: this.UO_idMain,
            UO_idCall: this.UO_idCall,

            region: "center",
            bodyStyle: 'background:transparent;',
            //width: "100%", height: "100%",
            autoHeight: true,
            split: true,

            items: [
                PanelCap1, PanelCap2
            ]

        });

        //*** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** 



        //Grid
        //*** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** ***

        //1. Грид: Выполненная работа
        var rowEditing1 = Ext.create('Ext.grid.plugin.RowEditing');
        var PanelGrid1 = Ext.create('Ext.grid.Panel', { //widget.viewGridDoc
            id: "grid1_" + this.UO_id,  //WingetName + ObjectID
            UO_id: this.UO_id,         //ObjectID
            UO_idMain: this.UO_idMain, //id-шник Панели, на которой находятся виджеты
            UO_idCall: this.UO_idCall, //id-шник Виджета, который визвал Виджет
            UO_View: this.UO_View,     //Название Виджета на котором расположен Грид, нужен для "Стилей" (раскраска грида)

            itemId: "grid1",

            conf: {},

            region: "center", //!!! Важно для Ресайз-а !!!
            autoScroll: true,
            flex: 1,
            split: true,

            store: this.storeDocServicePurch1TabsGrid, //storeDocAccountTabsGrid,

            columns: [
                //Услуга
                { text: "№", dataIndex: "DirServiceJobNomenID", width: 50 },
                { text: lanNomenclature, dataIndex: "DirServiceJobNomenName", flex: 1 }, //flex: 1

                { text: lanPriceVatFull, dataIndex: "PriceCurrency", width: 100, editor: { xtype: 'textfield' } },

                //{ text: "DirCurrencyID", dataIndex: "DirCurrencyID", width: 100 },
                //{ text: "DirCurrencyRate", dataIndex: "DirCurrencyRate", width: 100 },
                //{ text: "DirCurrencyMultiplicity", dataIndex: "DirCurrencyMultiplicity", width: 100 },

            ],

            tbar: [

                { xtype: "label", text: "Выполненная работа " },
                "->",
                {
                    UO_id: this.UO_id, UO_idMain: this.UO_idMain, UO_idCall: this.UO_idCall,
                    //xtype: "button",
                    icon: '../Scripts/sklad/images/table_add.png', text: "Добавить работу", tooltip: lanAddPosition,
                    itemId: "btnGridAddPosition1",
                },
                {
                    UO_id: this.UO_id, UO_idMain: this.UO_idMain, UO_idCall: this.UO_idCall,
                    xtype: "button",
                    icon: '../Scripts/sklad/images/table_delete.png', text: lanDelete, tooltip: lanDeletionFlag + "?", disabled: true,
                    id: "btnGridDeletion1" + this.UO_id, itemId: "btnGridDelete1"
                },

            ],

            plugins: [rowEditing1],
            rowEditing1: rowEditing1,

        });


        //2. Грид: Запчасти
        var rowEditing2 = Ext.create('Ext.grid.plugin.RowEditing');
        var PanelGrid2 = Ext.create('Ext.grid.Panel', { //widget.viewGridDoc
            id: "grid2_" + this.UO_id,  //WingetName + ObjectID
            UO_id: this.UO_id,         //ObjectID
            UO_idMain: this.UO_idMain, //id-шник Панели, на которой находятся виджеты
            UO_idCall: this.UO_idCall, //id-шник Виджета, который визвал Виджет
            UO_View: this.UO_View,     //Название Виджета на котором расположен Грид, нужен для "Стилей" (раскраска грида)

            itemId: "grid2",

            conf: {},

            region: "center", //!!! Важно для Ресайз-а !!!
            autoScroll: true,
            flex: 1,
            split: true,

            store: this.storeDocServicePurch2TabsGrid, //storeDocAccountTabsGrid,

            columns: [
                //Партия
                { text: "Партия", dataIndex: "RemPartyID", width: 50 }, //, hidden: true
                //Услуга
                { text: "№", dataIndex: "DirNomenID", width: 50 },
                { text: lanNomenclature, dataIndex: "DirNomenName", flex: 1 }, //flex: 1

                { text: lanPriceVatFull, dataIndex: "PriceCurrency", width: 100, editor: { xtype: 'textfield' } },

                //{ text: "DirCurrencyID", dataIndex: "DirCurrencyID", width: 100 },
                //{ text: "DirCurrencyRate", dataIndex: "DirCurrencyRate", width: 100 },
                //{ text: "DirCurrencyMultiplicity", dataIndex: "DirCurrencyMultiplicity", width: 100 },
            ],

            tbar: [

                { xtype: "label", text: "Запчасти " },

                "->",

                {
                    UO_id: this.UO_id, UO_idMain: this.UO_idMain, UO_idCall: this.UO_idCall,
                    //xtype: "button",
                    icon: '../Scripts/sklad/images/table_add.png', text: "Добавить запчасть", tooltip: lanAddPosition,
                    itemId: "btnGridAddPosition2",
                },
                {
                    UO_id: this.UO_id, UO_idMain: this.UO_idMain, UO_idCall: this.UO_idCall,
                    xtype: "button",
                    icon: '../Scripts/sklad/images/table_delete.png', text: lanDelete, tooltip: lanDeletionFlag + "?", disabled: true,
                    id: "btnGridDeletion2" + this.UO_id, itemId: "btnGridDelete2"
                },

            ],

            plugins: [rowEditing2],
            rowEditing2: rowEditing2,

        });

        //*** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** ***



        //3. Футер
        var PanelFooter = Ext.create('Ext.panel.Panel', {
            id: "PanelFooter_" + this.UO_id, UO_id: this.UO_id, UO_idMain: this.UO_idMain, UO_idCall: this.UO_idCall,
            //region: "south",
            bodyStyle: 'background:transparent;',

            defaultType: 'textfield',
            bodyPadding: 5,
            layout: 'anchor',
            defaults: { anchor: '100%' },
            //split: true,

            items: [
                {
                    xtype: 'container', width: "95%", layout: { align: 'stretch', type: 'hbox' },
                    items: [
                        { xtype: 'textfield', fieldLabel: "Сумма работы", labelAlign: 'top', name: "SumDocServicePurch1Tabs", id: "SumDocServicePurch1Tabs" + this.UO_id, regex: /^[+\-]?\d+(?:\.\d+)?$/, flex: 1, readOnly: true, allowBlank: true },
                        { xtype: 'textfield', fieldLabel: "Сумма запчастей", labelAlign: 'top', name: "SumDocServicePurch2Tabs", id: "SumDocServicePurch2Tabs" + this.UO_id, regex: /^[+\-]?\d+(?:\.\d+)?$/, flex: 1, readOnly: true, allowBlank: true },
                        { xtype: 'textfield', fieldLabel: "Итого", labelAlign: 'top', name: "SumTotal", id: "SumTotal" + this.UO_id, regex: /^[+\-]?\d+(?:\.\d+)?$/, flex: 1, readOnly: true, allowBlank: true },
                        { xtype: 'textfield', fieldLabel: "Предоплата", margin: "0 0 0 15", labelAlign: 'top', name: "PrepaymentSum", id: "PrepaymentSum" + this.UO_id, regex: /^[+\-]?\d+(?:\.\d+)?$/, flex: 1, readOnly: true, allowBlank: true },
                        { xtype: 'textfield', fieldLabel: "Итого разница", labelAlign: 'top', name: "SumTotal2", id: "SumTotal2" + this.UO_id, regex: /^[+\-]?\d+(?:\.\d+)?$/, flex: 1, readOnly: true, allowBlank: true },
                    ]
                },

            ]
        });


        //Form *** *** ***

        //Form-Panel
        var formPanel = Ext.create('Ext.form.Panel', {
            id: "form_" + this.UO_id, UO_id: this.UO_id, UO_idMain: this.UO_idMain, UO_idCall: this.UO_idCall,
            UO_Loaded: this.UO_Loaded,

            //Если редактируем в других объектах, например в других справочниках (Контрагент -> Банковские счета, Договора)
            //Данные для Чтения/Сохранения с/на Сервер или с/в Грид
            UO_GridSave: this.UO_GridSave,     // true - Признак того, что надо сохранять в Грид, а не на сервер, false - на сервер
            UO_GridIndex: this.UO_GridIndex,   // Int32 - Если редактируем, то позиция в списке: 0, 1, 2, ...
            UO_GridRecord: this.UO_GridRecord, // Если пустое, то читаем/пишем с/на Сервера. Иначе Грид (Данные загружаются/пишутся с/на сервера, Данные загружаются/пишутся в Грид)


            bodyStyle: 'background:transparent;', //bodyStyle: 'opacity:0.5;',
            region: "center", //!!! Важно для Ресайз-а !!!
            monitorValid: true,
            defaultType: 'textfield',

            //layout: 'border',
            //defaults: { anchor: '100%' },
            layout: {
                type: 'vbox',
                align: 'stretch',
                pack: 'start',
                split: true,
            },
            split: true,

            width: "100%", height: "100%",
            bodyPadding: 5,
            autoHeight: true,
            //autoScroll: true,

            items: [
                tabPanelDetails,
                PanelGrid1,
                PanelGrid2,
                PanelFooter
            ]
        });


        //body
        this.items = [

            //Товар
            /*
            Ext.create('widget.viewTreeDir', {

                conf: {
                    id: "tree_" + this.UO_id,  //WingetName + ObjectID
                    UO_id: this.UO_id,         //ObjectID
                    UO_idMain: this.UO_idMain, //id-шник Панели, на которой находятся виджеты
                    UO_idCall: this.UO_idCall, //id-шник Виджета, который визвал Виджет
                    UO_View: this.UO_View,     //Название Виджета на котором расположен Грид, нужен для "Стилей" (раскраска грида)
                },

                region: 'west',
                width: 215,

                store: this.storeServiceNomenTree,

                root: {
                    nodeType: 'sync',
                    text: 'Группа',
                    draggable: true,
                    id: "DirServiceNomen"
                },


                columns: [
                    { text: "", dataIndex: "Status", width: 17, tdCls: 'x-change-cell2' },
                    //{ text: 'Удалён', dataIndex: 'Del', hidden: true, tdCls: 'x-change-cell' },
                    //this is so we know which column will show the tree
                    { xtype: 'treecolumn', text: 'Наименование', flex: 1, sortable: true, dataIndex: 'text' },
                    //{ text: 'Родитель', dataIndex: 'Sub', hidden: true, tdCls: 'x-change-cell' },
                    { text: 'Остаток', dataIndex: 'Remains', width: 50, hidden: true, tdCls: 'x-change-cell' },
                    //{ text: 'DirServiceNomenPatchFull', dataIndex: 'DirServiceNomenPatchFull', hidden: true, tdCls: 'x-change-cell' },
                ],

                listeners: {
                    itemcontextmenu: function (view, rec, node, index, e) {
                        e.stopEvent();
                        //Присваиваем ID-шник
                        contextMenuTree.UO_id = this.UO_id;
                        //Присваиваем Функции обработки
                        contextMenuTree.folderNew = controllerDocServiceWorkshopsEdit_onTree_folderNew;
                        contextMenuTree.folderNewSub = controllerDocServiceWorkshopsEdit_onTree_folderNewSub;
                        contextMenuTree.folderEdit = controllerDocServiceWorkshopsEdit_onTree_folderEdit;
                        contextMenuTree.folderCopy = controllerDocServiceWorkshopsEdit_onTree_folderCopy;
                        contextMenuTree.folderDel = controllerDocServiceWorkshopsEdit_onTree_folderDel;
                        contextMenuTree.folderSubNull = controllerDocServiceWorkshopsEdit_onTree_folderSubNull;
                        contextMenuTree.addSub = controllerDocServiceWorkshopsEdit_onTree_addSub;
                        //Выводим
                        contextMenuTree.showAt(e.getXY());
                        return false;
                    }
                }

            }),
            */

            // *** *** *** *** *** *** *** *** ***


            formPanel

        ],



        this.buttons = [
            {
                id: "btnRecord" + this.UO_id, UO_id: this.UO_id, UO_idMain: this.UO_idMain, UO_idCall: this.UO_idCall, 
                text: lanSave, icon: '../Scripts/sklad/images/save.png',
                menu:
                [
                    {
                        UO_id: this.UO_id, UO_idMain: this.UO_idMain, UO_idCall: this.UO_idCall, itemId: "btnSave",
                        UO_Action: "save",
                        text: lanRecord, icon: '../Scripts/sklad/images/save.png',
                    },
                    {
                        UO_id: this.UO_id, UO_idMain: this.UO_idMain, UO_idCall: this.UO_idCall, itemId: "btnSaveClose",
                        UO_Action: "save_close",
                        text: lanRecordClose, icon: '../Scripts/sklad/images/save.png',
                    }
                ]
            },
            " ",
            {
                UO_id: this.UO_id, UO_idMain: this.UO_idMain, UO_idCall: this.UO_idCall, itemId: "btnCancel",
                text: lanCancel, icon: '../Scripts/sklad/images/cancel.png'
            },
            
            "-",
            {
                UO_id: this.UO_id, UO_idMain: this.UO_idMain, UO_idCall: this.UO_idCall, itemId: "btnHelp",
                text: lanHelp, icon: '../Scripts/sklad/images/help16.png',
            }

        ],


        this.callParent(arguments);
    }

});