﻿Ext.define("PartionnyAccount.view.Sklad/Object/Doc/DocBankSums/viewDocBankSumsEdit", {
    extend: "Ext.Window", UO_Extend: "Window", //extend: InterfaceSystemObjName,
    alias: "widget.viewDocBankSumsEdit",

    layout: "border",
    region: "center",
    title: lanBank,
    width: 500, height: 200,
    autoScroll: false,

    UO_maximize: false,  //Максимизировать во весь экран
    UO_Center: false,    //true - в центре экрана, false - окна каскадом
    UO_Modal: false,     //true - Все остальные элементы не активные
    buttonAlign: 'left',

    UO_Function_Tree: undefined,  //Fn - если открыли для выбора или из Tree
    UO_Function_Grid: undefined,  //Fn - если открыли для выбора или из Грида

    bodyStyle: 'background:white;',
    bodyPadding: varBodyPadding,

    conf: {},

    initComponent: function () {
        
        //Form-Panel
        var formPanel = Ext.create('Ext.form.Panel', {
            id: "form_" + this.UO_id, UO_id: this.UO_id, UO_idMain: this.UO_idMain, UO_idCall: this.UO_idCall,
            UO_Loaded: this.UO_Loaded,

            //Если редактируем в других объектах, например в других справочниках (Контрагент -> Банковские счета, Договора)
            //Данные для Чтения/Сохранения с/на Сервер или с/в Грид
            UO_GridSave: this.UO_GridSave,     // true - Признак того, что надо сохранять в Грид, а не на сервер, false - на сервер
            UO_GridIndex: this.UO_GridIndex,   // Int32 - Если редактируем, то позиция в списке: 0, 1, 2, ...
            UO_GridRecord: this.UO_GridRecord, // Если пустое, то читаем/пишем с/на Сервера. Иначе Грид (Данные загружаются/пишутся с/на сервера, Данные загружаются/пишутся в Грид)


            bodyStyle: 'background:transparent;', //bodyStyle: 'opacity:0.5;',
            region: "center", //!!! Важно для Ресайз-а !!!
            monitorValid: true,
            defaultType: 'textfield',

            //layout: 'border',
            //defaults: { anchor: '100%' },
            layout: {
                type: 'vbox',
                align: 'stretch',
                pack: 'start',
                split: true,
            },
            split: true,

            width: "100%", height: "100%",
            bodyPadding: 5,
            autoHeight: true,
            //autoScroll: true,

            items: [
                
                //Header
                {
                    xtype: 'container', width: "95%", layout: { align: 'stretch', type: 'hbox' }, height: 55,
                    items: [
                        { xtype: 'textfield', fieldLabel: "№", name: "DirBankID", id: "DirBankID" + this.UO_id, readOnly: true, flex: 1, allowBlank: true, hidden: true },
                        { xtype: 'textfield', fieldLabel: "№", name: "DirBankSumTypeID", id: "DirBankSumTypeID" + this.UO_id, readOnly: true, flex: 1, allowBlank: true, hidden: true },
                        {
                            xtype: 'textfield',
                            //labelCls: 'textbigger', //fieldCls: 'textbigger', cls: 'textbigger',
                            labelAlign: 'top', fieldLabel: "Сумма на счете",
                            regex: /^[+\-]?\d+(?:\.\d+)?$/, allowBlank: false, flex: 1, 
                            name: "DirBankSum", itemId: "DirBankSum", id: "DirBankSum" + this.UO_id, UO_id: this.UO_id, UO_idMain: this.UO_idMain, UO_idCall: this.UO_idCall,
                            readOnly: true
                        },


                    ]
                },

                //Buttons
                {
                    xtype: 'container', width: "95%", layout: { align: 'stretch', type: 'hbox' },
                    items: [
                        {
                            xtype: "button", height: 40, flex: 1,
                            id: "btnMake" + this.UO_id, UO_id: this.UO_id, UO_idMain: this.UO_idMain, UO_idCall: this.UO_idCall, itemId: "btnMake",
                            UO_Action: "make",
                            text: "Поступление", icon: '../Scripts/sklad/images/table_row_ins.png'
                        },
                        
                        {
                            xtype: "button", height: 40, flex: 1,
                            id: "btnPay" + this.UO_id, UO_id: this.UO_id, UO_idMain: this.UO_idMain, UO_idCall: this.UO_idCall, itemId: "btnPay",
                            UO_Action: "pay",
                            text: "Снятие", icon: '../Scripts/sklad/images/table_row_del.png'
                        }
                    ]
                },


                //Для растояния между Контейнерами
                { xtype: 'container', height: 5 },


                //Footer *** *** ***

                //Новая "Сумма"
                {
                    xtype: 'container', width: "95%", layout: { align: 'stretch', type: 'hbox' }, 
                    id: "containerFooterX" + this.UO_id, hidden: true,
                    items: [
                        {
                            xtype: 'textfield',
                            //labelCls: 'textbigger', //fieldCls: 'textbigger', cls: 'textbigger',
                            labelAlign: 'top', fieldLabel: "Вид операции - ",
                            regex: /^[+\-]?\d+(?:\.\d+)?$/, allowBlank: false,
                            name: "DocBankSumSum", itemId: "DocBankSumSum", id: "DocBankSumSum" + this.UO_id, UO_id: this.UO_id, UO_idMain: this.UO_idMain, UO_idCall: this.UO_idCall
                        },
                    ]
                },
                //Примечание
                {
                    xtype: 'container', width: "95%", layout: { align: 'stretch', type: 'hbox' },
                    id: "containerFooterY" + this.UO_id, hidden: true,
                    items: [
                        {
                            xtype: 'textfield',
                            labelAlign: 'top', fieldLabel: "Примечание",
                            name: "Base", id: "Base" + this.UO_id, flex: 1, allowBlank: true
                        },
                    ]
                },
            ]
        });


        //body
        this.items = [

            formPanel

        ],


        this.buttons = [
            {
                id: "btnSave" + this.UO_id, UO_id: this.UO_id, UO_idMain: this.UO_idMain, UO_idCall: this.UO_idCall, itemId: "btnSave", hidden: true,
                text: lanSave, icon: '../Scripts/sklad/images/save.png', UO_Action: "save",
            },
            " ",
            {
                id: "btnCancel" + this.UO_id, UO_id: this.UO_id, UO_idMain: this.UO_idMain, UO_idCall: this.UO_idCall, itemId: "btnCancel", hidden: true,
                text: lanCancel, icon: '../Scripts/sklad/images/cancel.png', UO_Action: "cancel",
            },

            "->",

            {
                id: "btnClose" + this.UO_id, UO_id: this.UO_id, UO_idMain: this.UO_idMain, UO_idCall: this.UO_idCall, itemId: "btnClose",
                text: lanNotSave, icon: '../Scripts/sklad/images/cancel.png', UO_Action: "close",
            },
            {
                id: "btnHelp" + this.UO_id, UO_id: this.UO_id, UO_idMain: this.UO_idMain, UO_idCall: this.UO_idCall, itemId: "btnHelp",
                text: lanHelp, icon: '../Scripts/sklad/images/help16.png', UO_Action: "help",
            },
        ],


        this.callParent(arguments);
    }

});