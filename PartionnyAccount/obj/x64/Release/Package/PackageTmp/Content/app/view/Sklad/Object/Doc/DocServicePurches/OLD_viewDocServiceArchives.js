﻿Ext.define("PartionnyAccount.view.Sklad/Object/Doc/DocServicePurches/viewDocServiceArchives", {
    //extend: "Ext.panel.Panel",
    extend: InterfaceSystemObjName,
    alias: "widget.viewDocServiceArchives",

    layout: "border",
    region: "center",
    title: "Сервис - Выдача",
    width: 750, height: 350,
    autoScroll: false,

    UO_maximize: false,  //Максимизировать во весь экран
    UO_Center: false,    //true - в центре экрана, false - окна каскадом
    UO_Modal: false,     //true - Все остальные элементы не активные
    buttonAlign: 'left',

    UO_Function_Tree: undefined,  //Fn - если открыли для выбора или из Tree
    UO_Function_Grid: undefined,  //Fn - если открыли для выбора или из Грида

    bodyStyle: 'background:white;',
    bodyPadding: varBodyPadding,

    conf: {},

    initComponent: function () {

        //body
        this.items = [

            Ext.create('widget.viewGridDoc', {

                conf: {
                    id: "grid_" + this.UO_id,  //WingetName + ObjectID
                    UO_id: this.UO_id,         //ObjectID
                    UO_idMain: this.UO_idMain, //id-шник Панели, на которой находятся виджеты
                    UO_idCall: this.UO_idCall, //id-шник Виджета, который визвал Виджет
                    UO_View: this.UO_View,     //Название Виджета на котором расположен Грид, нужен для "Стилей" (раскраска грида)
                },

                store: this.storeGrid,

                columns: [
                    { text: "", dataIndex: "Status", width: 17, tdCls: 'x-change-cell2' },
                    { text: "№ desk", dataIndex: "DocID", width: 50, hidden: true, tdCls: 'x-change-cell' },
                    { text: "№", dataIndex: "DocServicePurchID", width: 50, tdCls: 'x-change-cell' },
                    { text: lanDocDate, dataIndex: "DocDate", width: 50, tdCls: 'x-change-cell' },

                    { text: lanOrg, dataIndex: "DirContractorNameOrg", flex: 1, tdCls: 'x-change-cell' },
                    { text: lanContractor, dataIndex: "DirContractorName", flex: 1, tdCls: 'x-change-cell' },
                    { text: lanWarehouse, dataIndex: "DirWarehouseName", flex: 1, tdCls: 'x-change-cell' },
                    { text: lanNomenclature, dataIndex: "DirServiceNomenName", flex: 1, tdCls: 'x-change-cell' },
                    { text: "Серийный", dataIndex: "SerialNumber", flex: 1, tdCls: 'x-change-cell' },
                    { text: "Неисправность", dataIndex: "ProblemClientWords", flex: 1, tdCls: 'x-change-cell', hidden: true },
                    { text: "Клиент", dataIndex: "DirServiceContractorName", flex: 1, tdCls: 'x-change-cell' },
                    { text: lanPhone, dataIndex: "DirServiceContractorPhone", flex: 1, tdCls: 'x-change-cell', hidden: true },
                    { text: "Срочный", dataIndex: "UrgentRepairs", flex: 1, tdCls: 'x-change-cell', hidden: true },
                    { text: "Готовность", dataIndex: "DateDone", width: 50, tdCls: 'x-change-cell', hidden: true },
                    { text: lanState, dataIndex: "DirServiceStatusName", flex: 1, tdCls: 'x-change-cell' },
                    { text: "Предоплата", dataIndex: "Prepayment", width: 50, tdCls: 'x-change-cell', hidden: true },
                    { text: "Аппарат", dataIndex: "Description", width: 50, tdCls: 'x-change-cell' }
                ],

                //В Константах "нижнея панель" не нужна
                bbar: new Ext.PagingToolbar({
                    store: this.storeGrid,                      // указано хранилище
                    displayInfo: true,                          // вывести инфо обо общем числе записей
                    displayMsg: lanShowing + "  {0} - {1} " + lanOf + " {2}"     // формат инфо
                }),

            }),

        ],


        this.callParent(arguments);
    }

});