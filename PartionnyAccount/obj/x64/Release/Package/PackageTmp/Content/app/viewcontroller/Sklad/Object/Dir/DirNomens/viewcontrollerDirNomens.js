﻿Ext.define('PartionnyAccount.viewcontroller.Sklad/Object/Dir/DirNomens/viewcontrollerDirNomens', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.viewcontrollerDirNomens',


    //Клик на изображение
    onImageShowClick: function (aButton, aEvent, param1) {
        var Params = [
            "imageShow" + Ext.getCmp(aButton.target.id).UO_id, //UO_idCall
            true, //UO_Center
            true, //UO_Modal
            1     // 1 - Новое, 2 - Редактировать
        ]
        ObjectEditConfig("viewDirNomensImg", Params);
    },

});