﻿Ext.define("PartionnyAccount.view.Sklad/Object/Log/viewLogServices", {
    extend: "Ext.Window", UO_Extend: "Window", //extend: InterfaceSystemObjName,
    alias: "widget.viewLogServices",

    layout: "border",
    region: "center",
    title: "Лог: комментарии и заметки",
    width: 450, height: 500,
    autoScroll: false,

    UO_maximize: false,  //Максимизировать во весь экран
    UO_Center: false,    //true - в центре экрана, false - окна каскадом
    UO_Modal: false,     //true - Все остальные элементы не активные
    buttonAlign: 'left',

    UO_Function_Tree: undefined,  //Fn - если открыли для выбора или из Tree
    UO_Function_Grid: undefined,  //Fn - если открыли для выбора или из Грида

    bodyStyle: 'background:white;',
    bodyPadding: varBodyPadding,

    conf: {},

    initComponent: function () {


        var PanelGridLog = Ext.create('Ext.grid.Panel', { //widget.viewGridDoc
            itemId: "gridLog",
            id: "gridLog0_" + this.UO_id,  //WingetName + ObjectID
            UO_id: this.UO_id,         //ObjectID
            UO_idMain: this.UO_idMain, //id-шник Панели, на которой находятся виджеты
            UO_idCall: this.UO_idCall, //id-шник Виджета, который визвал Виджет
            UO_View: this.UO_View,     //Название Виджета на котором расположен Грид, нужен для "Стилей" (раскраска грида)
            region: "center",
            flex: 1,

            store: this.storeGrid, //storeDocAccountTabsGrid,

            columns: [
                /*
                //Услуга
                //{ text: "№", dataIndex: "LogServiceID", width: 50 },
                { text: "Тип", dataIndex: "DirServiceLogTypeName", width: 100 },
                { text: "Сотрудник", dataIndex: "DirEmployeeName", width: 100 },
                { text: "Статус", dataIndex: "DirServiceStatusName", width: 100 },
                { text: "Дата", dataIndex: "LogServiceDate", width: 75 },
                { text: "Текст", dataIndex: "Msg", flex: 1 },
                */

                {
                    dataIndex: "Field1", flex: 1,
                    renderer: function (value, metaData, record, rowIndex, colIndex, view) {
                        metaData.style = "white-space: normal;";
                        return value;
                    }
                },
                /*{
                    dataIndex: "Field2", flex: 1,
                    renderer: function (value, metaData, record, rowIndex, colIndex, view) {
                        metaData.style = "white-space: normal;";
                        return value;
                    }
                },*/
            ],

            viewConfig: {
                getRowClass: function (record, index) {


                    for (var i = 0; i < record.store.model.fields.length; i++) {

                        //1. Если поле типа "Дата": "yyyy-MM-dd HH:mm:ss" => "yyyy-MM-dd"
                        if (record.store.model.fields[i].type == "date") {
                            //Если есть дата, может быть пустое значение
                            if (record.data[record.store.model.fields[i].name] != null) {

                                if (record.data[record.store.model.fields[i].name].length != 10) {
                                    //Ext.Date.format
                                    record.data[record.store.model.fields[i].name] = Ext.Date.format(new Date(record.data[record.store.model.fields[i].name]), "Y-m-d H:i:s");
                                }
                                else {
                                    //Рабочий метод, но нет смысла использовать
                                    //Ext.Date.parse and Ext.Date.format
                                    //record.data[record.store.model.fields[i].name] = Ext.Date.parse(record.data[record.store.model.fields[i].name], DateFormatStr);
                                    //record.data[record.store.model.fields[i].name] = Ext.Date.format(new Date(record.data[record.store.model.fields[i].name]), DateFormatStr);
                                }
                            }
                        }


                        //2.  === Формируем поле "Field1"  ===  ===  ===  ===  === 
                        var FN = record.store.model.fields[i].name;
                        if (FN == "DirServiceLogTypeName" || FN == "DirServiceStatusName" || FN == "Msg" || FN == "DirEmployeeName") {
                            if (record.data[record.store.model.fields[i].name] != null) {
                                record.data["Field1"] += record.data[record.store.model.fields[i].name];
                                //if (FN != "DirEmployeeName" || !(FN == "Msg" && record.data["Msg"] == "")) record.data["Field1"] += " - ";

                                if (FN == "DirEmployeeName" || (FN == "Msg" && record.data["Msg"] == "")) { }
                                else record.data["Field1"] += " - ";
                            }
                        }
                        else if (FN == "LogServiceDate") {
                            record.data["Field1"] += Ext.Date.format(new Date(record.data[record.store.model.fields[i].name]), "y-m-d H:i") + " - ";
                        }

                    }


                }, //getRowClass

                stripeRows: true,

            } //viewConfig

        });


        //Form-Panel
        var formPanel = Ext.create('Ext.form.Panel', {
            id: "form_" + this.UO_id, UO_id: this.UO_id, UO_idMain: this.UO_idMain, UO_idCall: this.UO_idCall,
            UO_Loaded: this.UO_Loaded,

            //Если редактируем в других объектах, например в других справочниках (Контрагент -> Банковские счета, Договора)
            //Данные для Чтения/Сохранения с/на Сервер или с/в Грид
            UO_GridSave: this.UO_GridSave,     // true - Признак того, что надо сохранять в Грид, а не на сервер, false - на сервер
            UO_GridIndex: this.UO_GridIndex,   // Int32 - Если редактируем, то позиция в списке: 0, 1, 2, ...
            UO_GridRecord: this.UO_GridRecord, // Если пустое, то читаем/пишем с/на Сервера. Иначе Грид (Данные загружаются/пишутся с/на сервера, Данные загружаются/пишутся в Грид)


            //bodyStyle: 'background:transparent;', //bodyStyle: 'opacity:0.5;',
            region: "south", //!!! Важно для Ресайз-а !!!
            monitorValid: true,
            defaultType: 'textfield',

            //layout: 'border',
            //defaults: { anchor: '100%' },
            layout: {
                type: 'vbox',
                align: 'stretch',
                pack: 'start',
                split: true,
            },
            split: true,

            width: "100%", height: 150,
            bodyPadding: 5,
            autoHeight: true,
            //autoScroll: true,

            items: [
                {
                    xtype: 'container', flex: 1, layout: { align: 'stretch', type: 'hbox' },
                    items: [

                        { xtype: 'textfield', name: "DocServicePurchID", id: "DocServicePurchID" + this.UO_id, flex: 1, allowBlank: false, hidden: true },
                        { xtype: 'textfield', name: "DirServiceLogTypeID", id: "DirServiceLogTypeID" + this.UO_id, flex: 1, allowBlank: false, hidden: true },
                        { xtype: 'textarea', name: "Msg", id: "Msg" + this.UO_id, flex: 1, allowBlank: true },

                    ]
                },
            ],

            buttons: [
                {
                    id: "btnSave" + this.UO_id, UO_id: this.UO_id, UO_idMain: this.UO_idMain, UO_idCall: this.UO_idCall, itemId: "btnSave", style: "width: 120px; height: 40px;",
                    UO_Action: "held_cancel",
                    text: lanRecord, icon: '../Scripts/sklad/images/save_held.png'
                },

            ],

        });



        //body
        this.items = [

            {
                region: 'center',
                xtype: 'panel',
                layout: 'border', // тип лэйоута - трехколонник с подвалом и шапкой
                items: [
                   PanelGridLog,
                   formPanel
                ]
            }

        ],


        this.callParent(arguments);
    }

});