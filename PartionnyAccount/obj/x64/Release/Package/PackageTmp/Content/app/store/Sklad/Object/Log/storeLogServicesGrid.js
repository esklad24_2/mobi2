﻿//Хранилище только для Grid
Ext.define('PartionnyAccount.store.Sklad/Object/Log/storeLogServicesGrid', {
    extend: 'Ext.data.Store',
    alias: "store.storeLogServicesGrid",

    storeId: 'storeLogServicesGrid',
    model: 'PartionnyAccount.model.Sklad/Object/Log/modelLogServicesGrid',
    pageSize: varPageSizeJurn,
    //autoLoad: true,
    proxy: {
        type: 'ajax',
        url: HTTP_LogServices,
        reader: {
            type: "json",
            rootProperty: "LogService" //pID
        },
        timeout: varTimeOutDefault,
    }
});