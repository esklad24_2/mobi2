﻿Ext.define("PartionnyAccount.view.Sklad/Object/Doc/DocServicePurches/viewDocServiceOutputsEdit", {
    //extend: "Ext.panel.Panel",
    extend: InterfaceSystemObjName,
    alias: "widget.viewDocServiceOutputsEdit",

    layout: "border",
    region: "center",
    title: "Сервис - Выдача", 
    width: 800, height: 375,
    autoScroll: false,

    UO_maximize: false,  //Максимизировать во весь экран
    UO_Center: false,    //true - в центре экрана, false - окна каскадом
    UO_Modal: false,     //true - Все остальные элементы не активные
    buttonAlign: 'left',

    UO_Function_Tree: undefined,  //Fn - если открыли для выбора или из Tree
    UO_Function_Grid: undefined,  //Fn - если открыли для выбора или из Грида

    bodyStyle: 'background:white;',
    bodyPadding: varBodyPadding,

    conf: {},

    initComponent: function () {
        

        var formPanelEdit = Ext.create('Ext.form.Panel', {
            id: "form_" + this.UO_id, UO_id: this.UO_id, UO_idMain: this.UO_idMain, UO_idCall: this.UO_idCall,

            //Если редактируем в других объектах, например в других справочниках (Контрагент -> Банковские счета, Договора)
            //Данные для Чтения/Сохранения с/на Сервер или с/в Грид
            UO_GridSave: this.UO_GridSave,     // true - Признак того, что надо сохранять в Грид, а не на сервер, false - на сервер
            UO_GridIndex: this.UO_GridIndex,   // Int32 - Если редактируем, то позиция в списке: 0, 1, 2, ...
            UO_GridRecord: this.UO_GridRecord, // Если пустое, то читаем/пишем с/на Сервера. Иначе Грид (Данные загружаются/пишутся с/на сервера, Данные загружаются/пишутся в Грид)

            region: "center", //!!! Важно для Ресайз-а !!!
            bodyStyle: 'background:transparent;',
            title: lanGeneral,
            frame: true,
            monitorValid: true,
            defaultType: 'textfield',
            width: "100%", height: "100%", //width: 500, height: 200,
            bodyPadding: 5,
            layout: 'anchor',
            defaults: { anchor: '100%' },
            autoScroll: true,
            autoHeight: true,

            items: [

                //Документ *** *** *** *** *** *** *** *** *** *** *** *** *** ***

                { xtype: 'textfield', fieldLabel: "DocID2", name: 'DocID2', id: 'DocID2' + this.UO_id, readOnly: true, allowBlank: true, hidden: true },  //, hidden: true
                { xtype: 'textfield', fieldLabel: "Held", name: 'Held', id: 'Held' + this.UO_id, readOnly: true, allowBlank: true, hidden: true },
                { xtype: 'textfield', fieldLabel: "DocID", name: "DocID", id: "DocID" + this.UO_id, readOnly: true, allowBlank: true, hidden: true },


                {
                    xtype: 'container', width: "95%", layout: { align: 'stretch', type: 'hbox' },
                    items: [
                        { xtype: 'textfield', fieldLabel: "№", name: "DocServicePurchID", id: "DocServicePurchID" + this.UO_id, readOnly: true, flex: 1, allowBlank: true, hidden: true },
                        { xtype: 'textfield', fieldLabel: lanManual, name: "NumberInt", id: "NumberInt" + this.UO_id, margin: "0 0 0 5", flex: 1, readOnly: true, allowBlank: true, hidden: true },
                        { xtype: 'viewDateField', fieldLabel: lanDate, name: "DocDate", id: "DocDate" + this.UO_id, margin: "0 0 0 5", allowBlank: false, readOnly: true, editable: false, hidden: true },
                    ]
                },


                //Аппарат *** *** *** *** *** *** *** *** *** *** *** *** *** ***

                { xtype: 'fieldset', width: "95%", layout: { align: 'stretch', type: 'column' }, title: "Аппарат", items: [] },

                {
                    xtype: 'container', width: "95%", layout: { align: 'stretch', type: 'hbox' },
                    items: [
                        { xtype: 'textfield', fieldLabel: "Аппарат", name: "DirServiceNomenName", readOnly: true, flex: 1, id: "DirServiceNomenName" + this.UO_id, readOnly: true, allowBlank: false },
                        { xtype: 'textfield', fieldLabel: "DirServiceNomenID", margin: "0 0 0 15", name: "DirServiceNomenID", flex: 1, id: "DirServiceNomenID" + this.UO_id, allowBlank: false, readOnly: true, hidden: true },
                    ]
                },

                {
                    xtype: 'container', width: "95%", layout: { align: 'stretch', type: 'hbox' },
                    items: [
                        //Серийный номер или IMEI
                        { xtype: 'textfield', fieldLabel: "Серийный номер или IMEI", name: "SerialNumber", flex: 1, id: "SerialNumber" + this.UO_id, readOnly: true, allowBlank: false },

                        { xtype: "checkbox", boxLabel: "Гарантийный", margin: "0 0 0 5", name: "TypeRepair", itemId: "TypeRepair", inputValue: true, id: "TypeRepair" + this.UO_id, readOnly: true },
                    ]
                },




                //Клиент *** *** *** *** *** *** *** *** *** *** *** *** *** ***

                { xtype: 'fieldset', width: "95%", layout: { align: 'stretch', type: 'column' }, title: "Клиент", items: [] },

                //Для растояния между Контейнерами
                { xtype: 'container', height: 5 },

                {
                    xtype: 'container', width: "95%", layout: { align: 'stretch', type: 'hbox' },
                    items: [
                        { xtype: 'textfield', fieldLabel: "ФИО", name: "DirServiceContractorName", flex: 1, id: "DirServiceContractorName" + this.UO_id, readOnly: true, allowBlank: false },
                        { xtype: 'textfield', fieldLabel: "Адрес", margin: "0 0 0 15", name: "DirServiceContractorAddress", flex: 1, id: "DirServiceContractorAddress" + this.UO_id, readOnly: true, allowBlank: true },
                    ]
                },
                {
                    xtype: 'container', width: "95%", layout: { align: 'stretch', type: 'hbox' },
                    items: [
                        { xtype: 'textfield', fieldLabel: "Телефон", name: "DirServiceContractorPhone", flex: 1, id: "DirServiceContractorPhone" + this.UO_id, readOnly: true, allowBlank: true },
                        { xtype: 'textfield', fieldLabel: "Email", margin: "0 0 0 15", name: "DirServiceContractorEmail", flex: 1, id: "DirServiceContractorEmail" + this.UO_id, readOnly: true, allowBlank: true },
                    ]
                },



                //Оплата *** *** *** *** *** *** *** *** *** *** *** *** *** ***

                { xtype: 'fieldset', width: "95%", layout: { align: 'stretch', type: 'column' }, title: "Оплата", items: [] },
                
                //Для растояния между Контейнерами
                { xtype: 'container', height: 5 },

                {
                    xtype: 'container', width: "95%", layout: { align: 'stretch', type: 'hbox' },
                    items: [
                        { xtype: 'textfield', fieldLabel: "Сумма работы", labelAlign: 'top', name: "SumDocServicePurch1Tabs", id: "SumDocServicePurch1Tabs" + this.UO_id, regex: /^[+\-]?\d+(?:\.\d+)?$/, flex: 1, readOnly: true, allowBlank: true },
                        { xtype: 'textfield', fieldLabel: "Сумма запчастей", labelAlign: 'top', name: "SumDocServicePurch2Tabs", id: "SumDocServicePurch2Tabs" + this.UO_id, regex: /^[+\-]?\d+(?:\.\d+)?$/, flex: 1, readOnly: true, allowBlank: true },
                        { xtype: 'textfield', fieldLabel: "Итого", labelAlign: 'top', name: "SumTotal", id: "SumTotal" + this.UO_id, regex: /^[+\-]?\d+(?:\.\d+)?$/, flex: 1, readOnly: true, allowBlank: true },
                        { xtype: 'textfield', fieldLabel: "Предоплата", margin: "0 0 0 15", labelAlign: 'top', name: "PrepaymentSum", id: "PrepaymentSum" + this.UO_id, regex: /^[+\-]?\d+(?:\.\d+)?$/, flex: 1, readOnly: true, allowBlank: true },
                        { xtype: 'textfield', fieldLabel: "<b style='color: red;'> Итого разница </b>", labelAlign: 'top', name: "SumTotal2", id: "SumTotal2" + this.UO_id, regex: /^[+\-]?\d+(?:\.\d+)?$/, flex: 1, readOnly: true, allowBlank: true },
                    ]
                },

            ],


            //buttonAlign: 'left',
            buttons: [
                {
                    UO_id: this.UO_id, UO_idMain: this.UO_idMain, UO_idCall: this.UO_idCall, itemId: "btnRevision",
                    UO_Action: "revision",
                    text: "На доработку", icon: '../Scripts/sklad/images/save.png',
                },
                "->",
                {
                    UO_id: this.UO_id, UO_idMain: this.UO_idMain, UO_idCall: this.UO_idCall, itemId: "btnOutput",
                    UO_Action: "output",
                    text: "Выдать", icon: '../Scripts/sklad/images/save.png',
                },
                " ",
                {
                    UO_id: this.UO_id, UO_idMain: this.UO_idMain, UO_idCall: this.UO_idCall, itemId: "btnCancel",
                    text: lanCancel, icon: '../Scripts/sklad/images/cancel.png'
                },

                " ",
                {
                    id: "btnPrint" + this.UO_id, UO_id: this.UO_id, UO_idMain: this.UO_idMain, UO_idCall: this.UO_idCall, itemId: "btnPrint", //hidden: true,
                    text: lanPrint, icon: '../Scripts/sklad/images/print.png',
                    menu:
                    [
                        {
                            UO_id: this.UO_id, UO_idMain: this.UO_idMain, UO_idCall: this.UO_idCall, itemId: "btnPrintHtml",
                            UO_Action: "html",
                            text: "Html", icon: '../Scripts/sklad/images/html.png',
                        },
                        {
                            UO_id: this.UO_id, UO_idMain: this.UO_idMain, UO_idCall: this.UO_idCall, itemId: "btnPrintExcel",
                            UO_Action: "excel",
                            text: "MS Excel", icon: '../Scripts/sklad/images/excel.png',
                        }
                    ]
                },
                "-",
                {
                    UO_id: this.UO_id, UO_idMain: this.UO_idMain, UO_idCall: this.UO_idCall, itemId: "btnHelp",
                    text: lanHelp, icon: '../Scripts/sklad/images/help16.png'
                },


                /*
                { xtype: 'viewDateField', fieldLabel: "", width: 90, name: "HistoryDate", id: "HistoryDate" + this.UO_id, allowBlank: true, editable: false },

                {
                    UO_id: this.UO_id, UO_idMain: this.UO_idMain, UO_idCall: this.UO_idCall, itemId: "btnChange",
                    text: lanChange, icon: '../Scripts/sklad/images/change.png'
                },
                */
            ]

        });


        //body
        this.items = [

            formPanelEdit

        ],


        this.callParent(arguments);
    }

});