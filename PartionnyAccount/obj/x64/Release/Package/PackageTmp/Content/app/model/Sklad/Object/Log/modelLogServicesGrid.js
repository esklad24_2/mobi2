﻿//Модель только для Grid
Ext.define('PartionnyAccount.model.Sklad/Object/Log/modelLogServicesGrid', {
    extend: 'Ext.data.Model',

    fields: [
        { name: "LogServiceID" },
        { name: "DocServicePurchID" },
        
        { name: "LogServiceDate", type: "date" },
        { name: "DirServiceLogTypeID" }, { name: "DirServiceLogTypeName" },
        { name: "DirServiceStatusID" }, { name: "DirServiceStatusName" },
        { name: "Msg", type: "string" },
        { name: "DirEmployeeID" }, { name: "DirEmployeeName" },

        { name: "Field1" },
        //{ name: "Field2" },
    ]
});