﻿//Модель только для Grid
Ext.define('PartionnyAccount.model.Sklad/Object/Log/modelLogMovementsGrid', {
    extend: 'Ext.data.Model',

    fields: [
        { name: "LogMovementID" },
        { name: "DocMovementPurchID" },
        
        { name: "LogMovementDate", type: "date" },
        { name: "DirMovementLogTypeID" }, { name: "DirMovementLogTypeName" },
        { name: "DirMovementStatusID" }, { name: "DirMovementStatusName" },
        { name: "Msg", type: "string" },
        { name: "DirEmployeeID" }, { name: "DirEmployeeName" },

        { name: "Field1" },
        //{ name: "Field2" },
    ]
});