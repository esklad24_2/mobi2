﻿//Хранилище только для Grid
Ext.define('PartionnyAccount.store.Sklad/Object/Log/storeLogOrderIntsGrid', {
    extend: 'Ext.data.Store',
    alias: "store.storeLogOrderIntsGrid",

    storeId: 'storeLogOrderIntsGrid',
    model: 'PartionnyAccount.model.Sklad/Object/Log/modelLogOrderIntsGrid',
    pageSize: varPageSizeJurn,
    //autoLoad: true,
    proxy: {
        type: 'ajax',
        url: HTTP_LogOrderInts,
        reader: {
            type: "json",
            rootProperty: "LogOrderInt" //pID
        },
        timeout: varTimeOutDefault,
    }
});