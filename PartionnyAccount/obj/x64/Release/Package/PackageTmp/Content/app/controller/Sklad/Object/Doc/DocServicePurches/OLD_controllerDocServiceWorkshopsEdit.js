﻿Ext.define("PartionnyAccount.controller.Sklad/Object/Doc/DocServicePurches/controllerDocServiceWorkshopsEdit", {
    //Расширить
    extend: "Ext.app.Controller",
    //views: ['Sys/viewContainerHeader'],

    init: function () {
        this.control({
            //Виджет (на котором расположены Грид и ...)
            //Закрыте
            'viewDocServiceWorkshopsEdit': { close: this.this_close },

            //Для статуса "Готов", должна быть хотя бы одна выполненная Работа!
            'viewDocServiceWorkshopsEdit [itemId=DirServiceStatusID]': { select: this.onDirServiceStatusIDSelect },


            //Грид1 (itemId=grid1)
            'viewDocServiceWorkshopsEdit button#btnGridAddPosition1': { click: this.onGrid_BtnGridAddPosition1 },
            'viewDocServiceWorkshopsEdit button#btnGridDelete1': { click: this.onGrid_BtnGridDelete1 },
            // Клик по Гриду
            'viewDocServiceWorkshopsEdit [itemId=grid1]': {
                selectionchange: this.onGrid_selectionchange1,
                itemclick: this.onGrid_itemclick1,
                itemdblclick: this.onGrid_itemdblclick1,

                edit: this.onGrid_selectionchange1_Edit
            },



            //Грид2 (itemId=grid2)
            'viewDocServiceWorkshopsEdit button#btnGridAddPosition2': { click: this.onGrid_BtnGridAddPosition2 },
            'viewDocServiceWorkshopsEdit button#btnGridEdit2': { click: this.onGrid_BtnGridEdit2 },
            'viewDocServiceWorkshopsEdit button#btnGridDelete2': { click: this.onGrid_BtnGridDelete2 },
            // Клик по Гриду
            'viewDocServiceWorkshopsEdit [itemId=grid2]': {
                selectionchange: this.onGrid_selectionchange2,
                itemclick: this.onGrid_itemclick2,
                itemdblclick: this.onGrid_itemdblclick2,

                edit: this.onGrid_selectionchange2_Edit
            },




            // === Кнопки: Сохранение, Отмена и Помощь === === ===
            'viewDocServiceWorkshopsEdit menuitem#btnSave': { click: this.onBtnSaveClick },
            'viewDocServiceWorkshopsEdit menuitem#btnSaveClose': { click: this.onBtnSaveClick },
            'viewDocServiceWorkshopsEdit button#btnCancel': { "click": this.onBtnCancelClick },
            'viewDocServiceWorkshopsEdit button#btnHelp': { "click": this.onBtnHelpClick },
            //***
            'viewDocServiceWorkshopsEdit menuitem#btnPrintHtml': { click: this.onBtnPrintHtmlClick },
            'viewDocServiceWorkshopsEdit menuitem#btnPrintExcel': { click: this.onBtnPrintHtmlClick },
        });
    },


    //Только для "InterfaceSystem == 3" (layout: 'card')
    //Закрытие и сделать активным другой виджет
    this_close: function (aPanel) {
        funInterfaceSystem3_closePanel(aPanel);
    },


    // *** DirServiceStatus ***
    //Для статуса "Готов", должна быть хотя бы одна выполненная Работа!
    onDirServiceStatusIDSelect: function (combo, records) {
        /*
        var tree_ = Ext.getCmp("tree_" + combo.UO_id);
        tree_.store.proxy.url = HTTP_DirNomens + "?DirWarehouseID=" + records.data.DirWarehouseID;
        */
        if (records.data.DirServiceStatusID == 7) {
            var length = Ext.getCmp("grid1_" + combo.UO_id).store.data.length;
            if (length == 0) {
                Ext.Msg.alert(lanOrgName, "Для статуса готов, должна присутствовать в списке работ, хотя бы одна выполненная работа!");
                return;
            }
        }
    },
    

    // *** DirEmployee ***
    //Редактирование или добавление нового Склада
    onDirEmployeeIDMasterSelect: function (combo, records) {
        var tree_ = Ext.getCmp("tree_" + combo.UO_id);
        tree_.store.proxy.url = HTTP_DirServiceNomens + "?DirEmployeeID=" + records.data.DirEmployeeID;
    },
    onBtnDirEmployeeEditClick: function (combo, aEvent, aOptions) {
        var Params = [
            combo.id,
            false, //UO_Center
            false, //UO_Modal
            //2     // 1 - Новое, 2 - Редактировать
        ]
        ObjectConfig("viewDirEmployees", Params);
    },
    //РеЛоад - перегрузить тригер, что бы появились новые записи
    onBtnDirEmployeeReloadClick: function (aButton, aEvent, aOptions) {
        var storeDirEmployeesGrid = Ext.getCmp(aButton.UO_idMain).storeDirEmployeesGrid;
        storeDirEmployeesGrid.load();
    },



    //Грид1 (itemId=grid1) === === === === ===

    //Новая: Добавить позицию
    onGrid_BtnGridAddPosition1: function (aButton, aEvent, aOptions) {
        var id = aButton.UO_id;
       
        var Params = [
            "grid1_" + id,
            true, //UO_Center
            true, //UO_Modal
            undefined,
            this.fn_onGrid_BtnGridAddPosition1, // true - Признак того, что надо сохранять в Грид, а не на сервер, false - на сервер
            true, //index,        // Int32 - Если редактируем, то позиция в списке: 0, 1, 2, ...
            true, //UO_GridRecord //record        // Для загрузки данных в форму Б.С. и Договора,
        ]
        ObjectConfig("viewDirServiceJobNomenPrices", Params);
    },
    //Заполнить 2-а поля
    fn_onGrid_BtnGridAddPosition1: function (idMy, idSelect, rec) {
        //Получаем тип цены
        var DirPriceTypeID = parseInt(Ext.getCmp("DirPriceTypeID" + idSelect).getValue());

        
        switch (DirPriceTypeID)
        {
            case 1:
                {
                    rec.data.PriceVAT = rec.data.PriceRetailVAT;
                    rec.data.PriceCurrency = rec.data.PriceRetailCurrency;
                    rec.data.DirCurrencyID = rec.data.DirCurrencyID;
                    rec.data.DirCurrencyRate = rec.data.DirCurrencyRate;
                    rec.data.DirCurrencyMultiplicity = rec.data.DirCurrencyMultiplicity;
                }
                break;
            case 2:
                {
                    rec.data.PriceVAT = rec.data.PriceRetailVAT;
                    rec.data.PriceCurrency = rec.data.PriceWholesaleCurrency;
                    rec.data.DirCurrencyID = rec.data.DirCurrencyID;
                    rec.data.DirCurrencyRate = rec.data.DirCurrencyRate;
                    rec.data.DirCurrencyMultiplicity = rec.data.DirCurrencyMultiplicity;
                }
                break;
            case 3:
                {
                    rec.data.PriceVAT = rec.data.PriceRetailVAT;
                    rec.data.PriceCurrency = rec.data.PriceIMCurrency;
                    rec.data.DirCurrencyID = rec.data.DirCurrencyID;
                    rec.data.DirCurrencyRate = rec.data.DirCurrencyRate;
                    rec.data.DirCurrencyMultiplicity = rec.data.DirCurrencyMultiplicity;
                }
                break;
        }
        

        var store = Ext.getCmp("grid1_" + idMy).getStore();
        store.insert(store.data.items.length, rec.data);

        controllerDocServiceWorkshopsEdit_RecalculationSums(idMy);
    },
    //Новая: Удалить позицию
    onGrid_BtnGridDelete1: function (aButton, aEvent, aOptions) {
        Ext.MessageBox.show({
            title: lanOrgName,
            msg: lanDelete + "?",
            icon: Ext.MessageBox.QUESTION, buttons: Ext.Msg.YESNO, width: 300, closable: false,
            fn: function (buttons) {
                if (buttons == "yes") {
                    var selection = Ext.getCmp("grid1_" + aButton.UO_id).getView().getSelectionModel().getSelection()[0];
                    if (selection) { Ext.getCmp("grid1_" + aButton.UO_id).store.remove(selection); }
                }
            }
        });
    },

    //Кнопки редактирования Енеблед
    onGrid_selectionchange1: function (model, records) {
        model.view.ownerGrid.down("#btnGridDelete1").setDisabled(records.length === 0);
    },
    //Клик: Редактирования или выбор
    onGrid_itemclick1: function (view, record, item, index, eventObj) {
        //Если запись удалена, то выдать сообщение и выйти
        if (funGridRecordDel(record)) { return; }

        /*
        if (varSelectOneClick) {

            var id = view.grid.UO_id;

            if (Ext.getCmp(view.grid.UO_idMain).UO_Function_Grid == undefined) {
                var Params = [
                    "grid1_" + id, //UO_idCall
                    true, //UO_Center
                    true, //UO_Modal
                    2,    // 1 - Новое, 2 - Редактировать
                    true,
                    Ext.getCmp("grid1_" + id).store.indexOf(Ext.getCmp("grid1_" + id).getSelectionModel().getSelection()[0]),       // Int32 - Если редактируем, то позиция в списке: 0, 1, 2, ...
                    Ext.getCmp("grid1_" + id).getSelectionModel().getSelection()[0], // Для загрузки данных в форму редактирования Табличной части
                    id,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    false
                ]
                ObjectEditConfig("viewDocSaleTabsEdit", Params);
            }
            else {
                Ext.getCmp(view.grid.UO_idMain).UO_Function_Grid(Ext.getCmp(view.grid.UO_idCall).UO_id, record);
                Ext.getCmp(view.grid.UO_idMain).close();
            }
        }
        */
    },
    //ДаблКлик: Редактирования или выбор
    onGrid_itemdblclick1: function (view, record, item, index, e) {
        /*
        if (Ext.getCmp(view.grid.UO_idMain).UO_Function_Grid == undefined) {

            var id = view.grid.UO_id;

            var Params = [
                "grid1_" + id, //UO_idCall
                true, //UO_Center
                true, //UO_Modal
                2,    // 1 - Новое, 2 - Редактировать
                true,
                Ext.getCmp("grid1_" + id).store.indexOf(Ext.getCmp("grid1_" + id).getSelectionModel().getSelection()[0]),       // Int32 - Если редактируем, то позиция в списке: 0, 1, 2, ...
                Ext.getCmp("grid1_" + id).getSelectionModel().getSelection()[0], // Для загрузки данных в форму редактирования Табличной части
                id,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                false
            ]
            ObjectEditConfig("viewDocSaleTabsEdit", Params);
        }
        else {
            Ext.getCmp(view.grid.UO_idMain).UO_Function_Grid(Ext.getCmp(view.grid.UO_idCall).UO_id, record);
            Ext.getCmp(view.grid.UO_idMain).close();
        }
        */
    },
    //Редактирование цены Грида
    onGrid_selectionchange1_Edit: function (view, record, item, index, eventObj) {
        var id = view.grid.UO_id;
        controllerDocServiceWorkshopsEdit_RecalculationSums(id);
    },



    //Грид2 (itemId=grid2) === === === === ===

    //Новая: Добавить позицию
    onGrid_BtnGridAddPosition2: function (aButton, aEvent, aOptions) {
        var id = aButton.UO_id;

        var Params = [
            "grid2_" + id,
            true, //UO_Center
            true, //UO_Modal
            undefined,
            this.fn_onGrid_BtnGridAddPosition2, // true - Признак того, что надо сохранять в Грид, а не на сервер, false - на сервер
            true, //index,        // Int32 - Если редактируем, то позиция в списке: 0, 1, 2, ...
            true, //UO_GridRecord //record        // Для загрузки данных в форму Б.С. и Договора,
        ]
        ObjectConfig("viewDirNomenRemParties", Params);
    },
    //Заполнить 2-а поля
    fn_onGrid_BtnGridAddPosition2: function (idMy, idSelect, rec) {
        //Получаем тип цены
        var DirPriceTypeID = parseInt(Ext.getCmp("DirPriceTypeID" + idSelect).getValue());


        switch (DirPriceTypeID) {
            case 1:
                {
                    rec.data.PriceVAT = rec.data.PriceRetailVAT;
                    rec.data.PriceCurrency = rec.data.PriceRetailCurrency;
                    rec.data.DirCurrencyID = rec.data.DirCurrencyID;
                    rec.data.DirCurrencyRate = rec.data.DirCurrencyRate;
                    rec.data.DirCurrencyMultiplicity = rec.data.DirCurrencyMultiplicity;
                }
                break;
            case 2:
                {
                    rec.data.PriceVAT = rec.data.PriceRetailVAT;
                    rec.data.PriceCurrency = rec.data.PriceWholesaleCurrency;
                    rec.data.DirCurrencyID = rec.data.DirCurrencyID;
                    rec.data.DirCurrencyRate = rec.data.DirCurrencyRate;
                    rec.data.DirCurrencyMultiplicity = rec.data.DirCurrencyMultiplicity;
                }
                break;
            case 3:
                {
                    rec.data.PriceVAT = rec.data.PriceRetailVAT;
                    rec.data.PriceCurrency = rec.data.PriceIMCurrency;
                    rec.data.DirCurrencyID = rec.data.DirCurrencyID;
                    rec.data.DirCurrencyRate = rec.data.DirCurrencyRate;
                    rec.data.DirCurrencyMultiplicity = rec.data.DirCurrencyMultiplicity;
                }
                break;
        }


        var store = Ext.getCmp("grid2_" + idMy).getStore();
        store.insert(store.data.items.length, rec.data);

        controllerDocServiceWorkshopsEdit_RecalculationSums(idMy);
    },
    //Новая: Удалить позицию
    onGrid_BtnGridDelete2: function (aButton, aEvent, aOptions) {
        Ext.MessageBox.show({
            title: lanOrgName,
            msg: lanDelete + "?",
            icon: Ext.MessageBox.QUESTION, buttons: Ext.Msg.YESNO, width: 300, closable: false,
            fn: function (buttons) {
                if (buttons == "yes") {
                    var selection = Ext.getCmp("grid2_" + aButton.UO_id).getView().getSelectionModel().getSelection()[0];
                    if (selection) { Ext.getCmp("grid2_" + aButton.UO_id).store.remove(selection); }
                }
            }
        });
    },

    //Кнопки редактирования Енеблед
    onGrid_selectionchange2: function (model, records) {
        model.view.ownerGrid.down("#btnGridDelete2").setDisabled(records.length === 0);
    },
    //Клик: Редактирования или выбор
    onGrid_itemclick2: function (view, record, item, index, eventObj) {
        //Если запись удалена, то выдать сообщение и выйти
        if (funGridRecordDel(record)) { return; }

        /*
        if (varSelectOneClick) {

            var id = view.grid.UO_id;

            if (Ext.getCmp(view.grid.UO_idMain).UO_Function_Grid == undefined) {
                var Params = [
                    "grid2_" + id, //UO_idCall
                    true, //UO_Center
                    true, //UO_Modal
                    2,    // 1 - Новое, 2 - Редактировать
                    true,
                    Ext.getCmp("grid2_" + id).store.indexOf(Ext.getCmp("grid2_" + id).getSelectionModel().getSelection()[0]),       // Int32 - Если редактируем, то позиция в списке: 0, 1, 2, ...
                    Ext.getCmp("grid2_" + id).getSelectionModel().getSelection()[0], // Для загрузки данных в форму редактирования Табличной части
                    id,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    false
                ]
                ObjectEditConfig("viewDocSaleTabsEdit", Params);
            }
            else {
                Ext.getCmp(view.grid.UO_idMain).UO_Function_Grid(Ext.getCmp(view.grid.UO_idCall).UO_id, record);
                Ext.getCmp(view.grid.UO_idMain).close();
            }
        }
        */
    },
    //ДаблКлик: Редактирования или выбор
    onGrid_itemdblclick2: function (view, record, item, index, e) {
        /*
        if (Ext.getCmp(view.grid.UO_idMain).UO_Function_Grid == undefined) {

            var id = view.grid.UO_id;

            var Params = [
                "grid2_" + id, //UO_idCall
                true, //UO_Center
                true, //UO_Modal
                2,    // 1 - Новое, 2 - Редактировать
                true,
                Ext.getCmp("grid2_" + id).store.indexOf(Ext.getCmp("grid2_" + id).getSelectionModel().getSelection()[0]),       // Int32 - Если редактируем, то позиция в списке: 0, 1, 2, ...
                Ext.getCmp("grid2_" + id).getSelectionModel().getSelection()[0], // Для загрузки данных в форму редактирования Табличной части
                id,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                false
            ]
            ObjectEditConfig("viewDocSaleTabsEdit", Params);
        }
        else {
            Ext.getCmp(view.grid.UO_idMain).UO_Function_Grid(Ext.getCmp(view.grid.UO_idCall).UO_id, record);
            Ext.getCmp(view.grid.UO_idMain).close();
        }
        */
    },
    //Редактирование цены Грида
    onGrid_selectionchange2_Edit: function (view, record, item, index, eventObj) {
        var id = view.grid.UO_id;
        controllerDocServiceWorkshopsEdit_RecalculationSums(id);
    },






    // === Кнопки === === ===
    //Сохранить или Сохранить и закрыть
    onBtnSaveClick: function (aButton, aEvent, aOptions) {

        controllerDocServiceWorkshopsEdit_onBtnSaveClick(aButton, aEvent, aOptions);

    },
    //Отменить
    onBtnCancelClick: function (aButton, aEvent, aOptions) {
        Ext.getCmp(aButton.UO_idMain).close();
    },
    //Help
    onBtnHelpClick: function (aButton, aEvent, aOptions) {
        window.open(HTTP_Help + "dokument-servis/", '_blank');
    },
    //***
    //Распечатать
    onBtnPrintHtmlClick: function (aButton, aEvent, aOptions) {
        //aButton.UO_Action: html, excel
        //alert(aButton.UO_Action);

        //Проверка: если форма ещё не сохранена, то выход
        if (Ext.getCmp("DocServicePurchID" + aButton.UO_id).getValue() == null) { Ext.Msg.alert(lanOrgName, txtMsg066); return; }

        //Открытие списка ПФ
        var Params = [
            aButton.id,
            true, //UO_Center
            true, //UO_Modal
            aButton.UO_Action, //UO_Function_Tree: Html или Excel
            undefined,
            undefined,
            undefined,
            Ext.getCmp("DocID" + aButton.UO_id).getValue(),
            40
        ]
        ObjectConfig("viewListObjectPFs", Params);

    },
});


//Функия сохранения
function controllerDocServiceWorkshopsEdit_onBtnSaveClick(aButton, aEvent, aOptions) {
    
    //Спецификация №1 (табличная часть)
    var recordsDocServicePurch1Tab = [];
    var storeDocServicePurch1Tabs = Ext.getCmp("grid1_" + aButton.UO_id).store;
    storeDocServicePurch1Tabs.data.each(function (rec) { recordsDocServicePurch1Tab.push(rec.data); });

    //Спецификация №2 (табличная часть)
    var recordsDocServicePurch2Tab = [];
    var storeDocServicePurch2Tabs = Ext.getCmp("grid2_" + aButton.UO_id).store;
    storeDocServicePurch2Tabs.data.each(function (rec) { recordsDocServicePurch2Tab.push(rec.data); });

    //Форма на Виджете
    var widgetXForm = Ext.getCmp("form_" + aButton.UO_id);

    //Новая или Редактирование
    /*var sMethod = "POST";
    var sUrl = HTTP_DocServicePurches + "?UO_Action=" + aButton.UO_Action;
    if (parseInt(Ext.getCmp("DocServicePurchID" + aButton.UO_id).value) > 0) {
        sMethod = "PUT";
        sUrl = HTTP_DocServicePurches + "?id=" + parseInt(Ext.getCmp("DocServicePurchID" + aButton.UO_id).value) + "&UO_Action=" + aButton.UO_Action;
    }*/
    var sMethod = "PUT";
    var sUrl = HTTP_DocServicePurches + "?id=" + parseInt(Ext.getCmp("DocServicePurchID" + aButton.UO_id).value) + "&UO_Action=" + aButton.UO_Action;

    //Сохранение
    widgetXForm.submit({
        method: sMethod,
        url: sUrl + "&iTypeService=2",
        params: { recordsDocServicePurch1Tab: Ext.encode(recordsDocServicePurch1Tab), recordsDocServicePurch2Tab: Ext.encode(recordsDocServicePurch2Tab) },

        timeout: varTimeOutDefault,
        waitMsg: lanUploading,
        success: function (form, action) {
                
            if (aButton.UO_Action == "held_cancel") {
                //Ext.getCmp("Held" + aButton.UO_id).setValue(false);
                //Ext.getCmp("btnHeldCancel" + aButton.UO_id).setVisible(false);
                //Ext.getCmp("btnHelds" + aButton.UO_id).setVisible(true);
                Ext.getCmp("btnRecord" + aButton.UO_id).setVisible(true);
            }
            else if (aButton.UO_Action == "held") {
                //Ext.getCmp("Held" + aButton.UO_id).setValue(true);
                //Ext.getCmp("btnHeldCancel" + aButton.UO_id).setVisible(true);
                //Ext.getCmp("btnHelds" + aButton.UO_id).setVisible(false);
                Ext.getCmp("btnRecord" + aButton.UO_id).setVisible(false);
            }


            //Если новая накладная присваиваем полученные номера!
            if (!Ext.getCmp('DocID' + aButton.UO_id).getValue()) {
                var sData = action.result.data;
                Ext.getCmp('DocID' + aButton.UO_id).setValue(sData.DocID);
                Ext.getCmp('DocServicePurchID' + aButton.UO_id).setValue(sData.DocServicePurchID);
                Ext.getCmp('NumberInt' + aButton.UO_id).setValue(sData.DocServicePurchID);
                Ext.Msg.alert(lanOrgName, lanDataSaved + "<br />" + txtMsg096 + sData.DocServicePurchID);
            }

            //Закрыть
            if (aButton.UO_Action == "save_close") { Ext.getCmp(aButton.UO_idMain).close(); }
            //Перегрузить грид, если грид открыт
            if (Ext.getCmp(aButton.UO_idCall) != undefined && Ext.getCmp(aButton.UO_idCall).store != undefined) { Ext.getCmp(aButton.UO_idCall).getStore().load(); }
        },
        failure: function (form, action) { funPanelSubmitFailure(form, action); }
    });
};



//Функция пересчета Сумм
//И вывода сообщения о пересчете Налога, если меняли "Налог из ..."
//Заполнить 2-а поля (id, rec)
//ShowMsg - выводить сообщение при смене налоговой ставик (в основном используется для смены "Налог из ...")
function controllerDocServiceWorkshopsEdit_RecalculationSums(id) {

    //1. Подсчет табличной части Работы "SumDocServicePurch1Tabs"
    //2. Подсчет табличной части Запчасти "SumDocServicePurch2Tabs"
    //3. Сумма 1+2 "SumTotal"
    //4. Константа "PrepaymentSum"
    //5. 3 - 4 "SumTotal2"
    

    //1. Подсчет табличной части Работы "SumDocServicePurch1Tabs"
    var storeDocServicePurch1TabsGrid = Ext.getCmp(Ext.getCmp("form_" + id).UO_idMain).storeDocServicePurch1TabsGrid;
    var SumDocServicePurch1Tabs = 0;
    for (var i = 0; i < storeDocServicePurch1TabsGrid.data.items.length; i++) {
        SumDocServicePurch1Tabs += parseFloat(storeDocServicePurch1TabsGrid.data.items[i].data.PriceCurrency);
    }
    Ext.getCmp('SumDocServicePurch1Tabs' + id).setValue(SumDocServicePurch1Tabs.toFixed(varFractionalPartInSum));


    //2. Подсчет табличной части Работы "SumDocServicePurch2Tabs"
    var storeDocServicePurch2TabsGrid = Ext.getCmp(Ext.getCmp("form_" + id).UO_idMain).storeDocServicePurch2TabsGrid;
    var SumDocServicePurch2Tabs = 0;
    for (var i = 0; i < storeDocServicePurch2TabsGrid.data.items.length; i++) {
        SumDocServicePurch2Tabs += parseFloat(storeDocServicePurch2TabsGrid.data.items[i].data.PriceCurrency);
    }
    Ext.getCmp('SumDocServicePurch2Tabs' + id).setValue(SumDocServicePurch2Tabs.toFixed(varFractionalPartInSum));


    //3. Сумма 1+2 "SumTotal"
    Ext.getCmp('SumTotal' + id).setValue((SumDocServicePurch1Tabs + SumDocServicePurch2Tabs).toFixed(varFractionalPartInSum));


    //4. Константа "PrepaymentSum"
    //...


    //5. 3 - 4 "SumTotal2"
    Ext.getCmp('SumTotal2' + id).setValue((SumDocServicePurch1Tabs + SumDocServicePurch2Tabs - parseFloat(Ext.getCmp('PrepaymentSum' + id).getValue())).toFixed(varFractionalPartInSum));

};