﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using PartionnyAccount.Models;
using PartionnyAccount.Models.Sklad.Doc;
using System.Data.SQLite;
using System.Web.Script.Serialization;
using System.Data.Entity.Migrations;

namespace PartionnyAccount.Classes.Update
{
    public class Update : DbMigration
    {
        #region Classes

        Models.DbConnectionLogin db = new Models.DbConnectionLogin("ConnStrMSSQL");
        Classes.Function.Variables.ConnectionString connectionString = new Classes.Function.Variables.ConnectionString();
        Classes.Function.FunctionMSSQL.Jurn.JurnDispError jurnDispError = new Function.FunctionMSSQL.Jurn.JurnDispError();
        Classes.Function.Exceptions.ExceptionEntry exceptionEntry = new Classes.Function.Exceptions.ExceptionEntry();
        Classes.Function.ReturnServer returnServer = new Classes.Function.ReturnServer();

        #endregion


        internal async Task<string> Start(string sMethod)
        {
            //return FreeStart(sMethod);

            return await ComercialStart(sMethod);
        }


        #region FreeStart

        private async Task<string[]> FreeStart(string sMethod)
        {
            try
            {
                string ConStr = connectionString.Return(0, null, false);
                return await UpdatingOne(ConStr, 0);
            }
            catch (Exception ex)
            {
                string[] ret = new string[2];
                ret[0] =  exceptionEntry.Return(ex);
                return ret;
            }
        }

        #endregion


        #region ComercialStart: Комерческое обновление (по циклу обновляются Бызы всех клиентов)

        private async Task<string> ComercialStart(string sMethod)
        {
            //Получаем список активных клиентов
            var query = db.DirCustomers.Where(x => x.Active == true).ToList();

            if (sMethod == "Update")
            {
                //Обновление сервиса

                #region var
                int iCountUpdate = 0, iCountUpdateNo = 0, iCountError = 0;
                string sError = "",
                    sOk = "", //Обновленно
                    sOkNo = "", //Уже было обновлено
                    sNo = ""; //Ошибка
                #endregion

                #region All
                try
                {
                    for (int i = 0; i < query.Count(); i++)
                    {
                        string ConStr = connectionString.Return(query[i].DirCustomersID, null, true);
                        string[] _result = await UpdatingOne(ConStr, query[i].DirCustomersID);

                        if (_result[0] == "true") { iCountUpdate++; sOk += query[i].Login + "<br />"; }
                        else if (_result[0] == "false") { iCountUpdateNo++; sOkNo += query[i].Login + "<br />"; }
                        else { iCountError++; sError = _result[0]; sNo += query[i].Login + "<br />"; }
                    }

                }
                catch (Exception ex) { return exceptionEntry.Return(ex); }
                #endregion

                #region Return
                return
                "Обновленно: " + iCountUpdate + "<br />" +
                "Уже было Обновленно: " + iCountUpdateNo + "<br />" +
                "Не обновленно: " + iCountError + "<br />" +
                "Ошибки: " + sError + "<br /><br /><br />" +

                "Обновленно Logins:<br />" + sOk + "<br /><br />" +
                "Уже было Обновленно Logins:<br />" + sOkNo + "<br /><br />" +
                "Не обновленно Logins:<br />" + sNo + "<br /><br />";
                #endregion
            }

            //RemParties - RemPartyMinuses

            else if (sMethod == "RemPartiesCheck")
            {
                //Проверить остатки

                #region var
                int iCountUpdate = 0, iCountUpdateNo = 0, iCountError = 0;
                string sError = "",
                    sOk = "", //Обновленно
                    sOkNo = "", //Уже было обновлено
                    sNo = ""; //Ошибка
                #endregion

                #region All
                for (int i = 0; i < query.Count(); i++)
                {
                    try
                    {
                        string ConStr = connectionString.Return(query[i].DirCustomersID, null, true);

                        string _result = await RemPartiesCheck(ConStr);
                        if (_result != "false")
                        {
                            iCountUpdateNo++; sOkNo += query[i].Login + " (К-во: " + _result + ")" + "<br />";
                        }
                        else
                        {
                            iCountUpdate++; sOk += query[i].Login + "<br />";
                        }
                    }
                    catch (Exception ex) { iCountError++; sError = ex.Message; sNo += query[i].Login + "<br />"; }
                }
                #endregion

                #region Return
                return
                "Без ошибок: " + iCountUpdate + "<br />" +
                "С ошибками остатков: " + iCountUpdateNo + "<br />" +
                "Ошибки подключения: " + iCountError + "<br />" +
                "Ошибки: " + sError + "<br /><br /><br />" +

                "Ошибки: " + sError + "<br /><br /><br />" +

                "Logins без ошибок:<br />" + sOk + "<br /><br />" + 
                "Logins с ошибками остаткив:<br />" + sOkNo + "<br /><br />" + 
                "Logins с ошибками соединения:<br />" + sNo + "<br /><br />";
                #endregion
            }
            else if (sMethod == "RemPartiesCorrect")
            {
                //Исправить остатки

                #region var
                int iCountUpdate = 0, iCountUpdateNo = 0, iCountError = 0;
                string sError = "",
                    sOk = "", //Обновленно
                    sOkNo = "", //Уже было обновлено
                    sNo = ""; //Ошибка

                string
                    SQL =
                    "SELECT " +
                    " RemParties.[DirNomenID], RemParties.[RemPartyID], " +
                    " RemParties.[Quantity], RemParties.[Remnant],  " +
                    " RemParties.[Remnant] - (RemParties.[Quantity] - RemPartyMinuses.[Quantity]) AS Error " +
                    "FROM RemParties, RemPartyMinuses " +
                    "WHERE " +
                    " RemParties.RemPartyID = RemPartyMinuses.RemPartyID and " +
                    " RemParties.[Remnant] - (RemParties.[Quantity] - RemPartyMinuses.[Quantity]) > 0 " +
                    "GROUP BY RemParties.[DirNomenID], RemParties.[RemPartyID]";
                #endregion

                #region All
                for (int i = 0; i < query.Count(); i++)
                {
                    try
                    {
                        string ConStr = connectionString.Return(query[i].DirCustomersID, null, true);
                        string _result = await RemPartiesCheck(ConStr);
                        if (_result != "false")
                        {
                            if (await RemPartiesCorrect(ConStr)) { iCountUpdateNo++; sOkNo += query[i].Login + " (К-во: " + _result + ")" + "<br />"; }
                        }
                        else
                        {
                            iCountUpdate++; sOk += query[i].Login + "<br />";
                        }
                    }
                    catch (Exception ex) { iCountError++; sError = ex.Message; sNo += query[i].Login + "<br />"; }
                }
                #endregion

                #region Return
                return
                "Без ошибок: " + iCountUpdate + "<br />" +
                "С исправлено: " + iCountUpdateNo + "<br />" +
                "Ошибки подключения: " + iCountError + "<br />" +
                "Ошибки: " + sError + "<br /><br /><br />" +

                "Ошибки: " + sError + "<br /><br /><br />" +

                "Logins без ошибок:<br />" + sOk + "<br /><br />" +
                "Logins исправлено:<br />" + sOkNo + "<br /><br />" +
                "Logins с ошибками подключения:<br />" + sNo + "<br /><br />";
                #endregion
            }

            //RemParties - RemRemnants

            else if (sMethod == "RemRemnantsCheck")
            {
                //Проверить остатки

                #region var
                int iCountUpdate = 0, iCountUpdateNo = 0, iCountError = 0;
                string sError = "",
                    sOk = "", //Обновленно
                    sOkNo = "", //Уже было обновлено
                    sNo = ""; //Ошибка
                #endregion

                #region All
                for (int i = 0; i < query.Count(); i++)
                {
                    try
                    {
                        string ConStr = connectionString.Return(query[i].DirCustomersID, null, true);

                        int _result = await RemRemnantsCheck(ConStr);
                        if (_result > 0)
                        {
                            iCountUpdateNo++; sOkNo += query[i].Login + " (К-во товара: " + _result.ToString() + ")" + "<br />";
                        }
                        else
                        {
                            iCountUpdate++; sOk += query[i].Login + "<br />";
                        }
                    }
                    catch (Exception ex) { iCountError++; sError = ex.Message; sNo += query[i].Login + "<br />"; }
                }
                #endregion

                #region Return
                return
                "Без ошибок: " + iCountUpdate + "<br />" +
                "С ошибками остатков: " + iCountUpdateNo + "<br />" +
                "Ошибки подключения: " + iCountError + "<br />" +
                "Ошибки: " + sError + "<br /><br /><br />" +

                "Ошибки: " + sError + "<br /><br /><br />" +

                "Logins без ошибок:<br />" + sOk + "<br /><br />" +
                "Logins с ошибками остаткив:<br />" + sOkNo + "<br /><br />" +
                "Logins с ошибками соединения:<br />" + sNo + "<br /><br />";
                #endregion
            }
            else if (sMethod == "RemRemnantsCorrect")
            {
                //Исправить остатки

                #region var
                int iCountUpdate = 0, iCountUpdateNo = 0, iCountError = 0;
                string sError = "",
                    sOk = "", //Обновленно
                    sOkNo = "", //Уже было обновлено
                    sNo = ""; //Ошибка

                string
                    SQL =
                    "SELECT " +
                    " RemParties.[DirNomenID], RemParties.[RemPartyID], " +
                    " RemParties.[Quantity], RemParties.[Remnant],  " +
                    " RemParties.[Remnant] - (RemParties.[Quantity] - RemPartyMinuses.[Quantity]) AS Error " +
                    "FROM RemParties, RemPartyMinuses " +
                    "WHERE " +
                    " RemParties.RemPartyID = RemPartyMinuses.RemPartyID and " +
                    " RemParties.[Remnant] - (RemParties.[Quantity] - RemPartyMinuses.[Quantity]) > 0 " +
                    "GROUP BY RemParties.[DirNomenID], RemParties.[RemPartyID]";
                #endregion

                #region All
                for (int i = 0; i < query.Count(); i++)
                {
                    try
                    {
                        string ConStr = connectionString.Return(query[i].DirCustomersID, null, true);
                        if (await RemRemnantsCheck(ConStr) > 0)
                        {
                            if (await RemRemnantsCorrect(ConStr)) { iCountUpdateNo++; sOkNo += query[i].Login + "<br />"; }
                        }
                        else
                        {
                            iCountUpdate++; sOk += query[i].Login + "<br />";
                        }
                    }
                    catch (Exception ex) { iCountError++; sError = ex.Message; sNo += query[i].Login + "<br />"; }
                }
                #endregion

                #region Return
                return
                "Без ошибок: " + iCountUpdate + "<br />" +
                "Исправлено ошибок: " + iCountUpdateNo + "<br />" +
                "Ошибки подключения: " + iCountError + "<br />" +
                "Ошибки: " + sError + "<br /><br /><br />" +

                "Ошибки: " + sError + "<br /><br /><br />" +

                "Logins без ошибок:<br />" + sOk + "<br /><br />" +
                "Logins исправлено:<br />" + sOkNo + "<br /><br />" +
                "Logins с ошибками подключения:<br />" + sNo + "<br /><br />";
                #endregion
            }

            else {
                return "Not found Method!";
            }
        }

        #endregion



        #region Remans (Check and Correct)

        //RemParties - RemPartyMinuses

        private async Task<string> RemPartiesCheck(string ConStr)
        {
            string ret = "false";

            string SQL =
                "SELECT " +
                " RemParties.[DirNomenID], RemParties.[RemPartyID], " +
                " RemParties.[Quantity], RemParties.[Remnant],  " +
                " SUM(RemPartyMinuses.[Quantity]), " +
                " (RemParties.[Quantity] - (RemParties.[Remnant]  + SUM(RemPartyMinuses.[Quantity]))) AS Error " + //" RemParties.[Remnant] - (RemParties.[Quantity] - RemPartyMinuses.[Quantity]) AS Error " +

                "FROM RemParties, RemPartyMinuses " +

                "WHERE " +
                " RemParties.RemPartyID = RemPartyMinuses.RemPartyID " +
                //" and RemParties.[Remnant] - (RemParties.[Quantity] - RemPartyMinuses.[Quantity]) != 0 " +

                "GROUP BY RemParties.[DirNomenID], RemParties.[RemPartyID],RemParties.[Quantity], RemParties.[Remnant] " +
                "HAVING (RemParties.[Quantity] - (RemParties.[Remnant]  + SUM(RemPartyMinuses.[Quantity]))) != 0 ";

            using (SQLiteConnection con = new SQLiteConnection(ConStr))
            {
                await con.OpenAsync(); //con.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(SQL, con))
                {
                    using (SQLiteDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            //iCountUpdateNo++; sOkNo += query[i].Login + " (К-во: " + dr["Error"] + ")" + "<br />";
                            ret = dr["Error"].ToString();
                        }
                        /*else
                        {
                            //iCountUpdate++; sOk += query[i].Login + "<br />";
                            ret = false;
                        }*/
                    }
                }
                con.Close();
            }

            return ret;
        }

        private async Task<bool> RemPartiesCorrect(string ConStr)
        {
            string SQL = 
                "UPDATE RemParties SET Remnant = Quantity - " + 
                "(" + 
                " SELECT IFNULL(SUM(RemPartyMinuses.Quantity), 0) " + 
                " FROM RemPartyMinuses " + 
                " WHERE RemPartyMinuses.RemPartyID = RemParties.RemPartyID " + 
                " GROUP BY RemPartyMinuses.RemPartyID " + 
				") " + 
                "WHERE RemPartyID = " + 
                "(" + 
                " SELECT RemPartyID " + 
                " FROM RemPartyMinuses " + 
                " WHERE RemPartyMinuses.RemPartyID = RemParties.RemPartyID " + 
                " GROUP BY RemPartyMinuses.RemPartyID " + 
				")";

            using (SQLiteConnection con = new SQLiteConnection(ConStr))
            {
                await con.OpenAsync(); //con.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(SQL, con))
                {
                    cmd.ExecuteNonQuery();
                }
                con.Close();
            }

            return true;
        }


        //RemParties - RemRemnants

        private async Task<int> RemRemnantsCheck(string ConStr)
        {
            int ret = 0;

            string SQL =
                "SELECT Count(DirNomenID) AS CountDirNomenID FROM " +
                "( " +
                " SELECT " +
                "  DirContractorIDOrg, DirWarehouseID, DirNomenID, SUM(Remnant), " +
                "  DirContractorIDOrg || '_' || DirWarehouseID || '_' || DirNomenID || '_' || round(SUM(Remnant), 2) AS X1 " +
                " FROM RemParties " +
                " GROUP BY DirContractorIDOrg, DirWarehouseID, DirNomenID " +
                ") " +
                "WHERE X1 NOT IN " +
                "( " +
                " SELECT " +
                "  DirContractorIDOrg || '_' || DirWarehouseID || '_' || DirNomenID || '_' || round(SUM(Quantity), 2) AS X1 " +
                " FROM RemRemnants " +
                " GROUP BY DirContractorIDOrg, DirWarehouseID, DirNomenID " +
                ")";

            using (SQLiteConnection con = new SQLiteConnection(ConStr))
            {
                await con.OpenAsync(); //con.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(SQL, con))
                {
                    using (SQLiteDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            ret = Convert.ToInt32(dr["CountDirNomenID"].ToString());
                        }
                    }
                }
                con.Close();
            }

            return ret;
        }

        private async Task<bool> RemRemnantsCorrect(string ConStr)
        {
            string SQL = "";

            using (SQLiteConnection con = new SQLiteConnection(ConStr))
            {
                await con.OpenAsync(); //con.Open();
                using (SQLiteCommand cmd = new SQLiteCommand("", con))
                {
                    cmd.CommandText = "DELETE FROM RemRemnants;";
                    cmd.ExecuteNonQuery();

                    cmd.CommandText =
                        "INSERT INTO RemRemnants " +
                        "(DirContractorIDOrg, DirNomenID, DirWarehouseID, Quantity) " +
                        "SELECT DirContractorIDOrg, DirNomenID, DirWarehouseID, SUM(Remnant) " +
                        "FROM RemParties " +
                        "GROUP BY DirContractorIDOrg, DirWarehouseID, DirNomenID";
                    cmd.ExecuteNonQuery();
                }
                con.Close();
            }

            return true;
        }

        #endregion



        #region Updating

        internal async Task<string[]> UpdatingOne(string ConStr, int DirCustomersID)
        {
            string[] ret = { "true", "true" };

            try
            {
                #region Update

                using (DbConnectionSklad db = new DbConnectionSklad(ConStr))
                {
                    //1.Получаем версию БД
                    int iSysVerNumber = db.SysVers.Where(x => x.SysVerID == 1).ToList()[0].SysVerNumber;

                    using (System.Data.Entity.DbContextTransaction ts = db.Database.BeginTransaction())
                    {
                        //2.Запускаем соответствующее обновление.
                        switch (iSysVerNumber)
                        {
                            case 0: await Task.Run(() => Update1(db)); break;
                            case 1: await Task.Run(() => Update2(db)); break;
                            case 2: await Task.Run(() => Update3(db)); break;
                            case 3: await Task.Run(() => Update4(db)); break;
                            case 4: await Task.Run(() => Update5(db)); break;
                            case 5: await Task.Run(() => Update6(db)); break;
                            case 6: await Task.Run(() => Update7(db)); break;
                            case 7: await Task.Run(() => Update8(db)); break;
                            case 8: await Task.Run(() => Update9(db)); break;
                            case 9: await Task.Run(() => Update10(db)); break;
                            case 10: await Task.Run(() => Update11(db)); break;
                            case 11: await Task.Run(() => Update12(db)); break;
                            case 12: await Task.Run(() => Update13(db)); break;
                            case 13: await Task.Run(() => Update14(db)); break;
                            case 14: await Task.Run(() => Update15(db)); break;
                            case 15: await Task.Run(() => Update16(db)); break;
                            case 16: await Task.Run(() => Update17(db)); break;
                            case 17: await Task.Run(() => Update18(db)); break;
                            case 18: await Task.Run(() => Update19(db)); break;
                            case 19: await Task.Run(() => Update20(db)); break;
                            case 20: await Task.Run(() => Update21(db)); break;
                            case 21: await Task.Run(() => Update22(db)); break;
                            case 22: await Task.Run(() => Update23(db)); break;
                            case 23: await Task.Run(() => Update24(db)); break;
                            case 24: await Task.Run(() => Update25(db)); break;
                            case 25: await Task.Run(() => Update26(db)); break;
                            case 26: await Task.Run(() => Update27(db)); break;
                            case 27: await Task.Run(() => Update28(db)); break;
                            case 28: await Task.Run(() => Update29(db)); break;
                            case 29: await Task.Run(() => Update30(db)); break;
                            case 30: await Task.Run(() => Update31(db)); break;
                            case 31: await Task.Run(() => Update32(db)); break;
                            case 32: await Task.Run(() => Update33(db)); break;
                            case 33: await Task.Run(() => Update34(db)); break;


                            default: ret[0] = "false"; break;
                        }
                        ts.Commit();
                    }
                }

                #endregion


                #region Vacuum, Reindex, ANALYZE, integrity_check


                Classes.Update.Prevention prevention = new Prevention();
                ret[1] = prevention.UpdatingOne(ConStr, DirCustomersID);

                #endregion

            }
            catch (Exception ex)
            {
                if (DirCustomersID > 0) try { jurnDispError.Write(db, exceptionEntry.Return(ex) + ". Class: PartionnyAccount.Controllers.Classes.Update.Update ", DirCustomersID); } catch { }
                ret[0] = exceptionEntry.Return(ex);
                //ret[1] = exceptionEntry.Return(ex);
            }


            return ret;
        }



        #region Update1 - СС - заполнение поля "DocServicePurches.Sums"

        private async Task<bool> Update1(DbConnectionSklad db)
        {
            //1. Алгоритм: 
            //Пробежатся по всем ремонтам.
            string SQL =
                "UPDATE DocServicePurches " +
                "SET " +
                " Sums = " +
                " (SELECT IFNULL(SUM(PriceCurrency), 0) FROM DocServicePurch1Tabs WHERE DocServicePurch1Tabs.DocServicePurchID = DocServicePurches.DocServicePurchID) + " +
                " (SELECT IFNULL(SUM(PriceCurrency), 0) FROM DocServicePurch2Tabs WHERE DocServicePurch2Tabs.DocServicePurchID = DocServicePurches.DocServicePurchID) - " +
                " IFNULL(DocServicePurches.PrepaymentSum, 0);";
            await Task.Run(() => db.Database.ExecuteSqlCommandAsync(SQL));


            //n.
            Models.Sklad.Sys.SysVer sysVer = await Task.Run(() => db.SysVers.FindAsync(1));
            sysVer.SysVerNumber = 1;
            sysVer.SysVerDate = DateTime.Now;

            db.Entry(sysVer).State = System.Data.Entity.EntityState.Modified;
            await Task.Run(() => db.SaveChangesAsync());

            return true;
        }

        #endregion


        #region Update2 - СС - заполнение поля "RemParties.DirEmployeeID.DocDate"

        private async Task<bool> Update2(DbConnectionSklad db)
        {
            //1. RemParties
            //1.1. DirEmployeeID из Docs
            string SQL =
                "UPDATE RemParties " +
                "SET DirEmployeeID = (SELECT DirEmployeeID FROM Docs WHERE RemParties.DocID = Docs.DocID);";
            await Task.Run(() => db.Database.ExecuteSqlCommandAsync(SQL));
            //1.2. DocDate из Docs
            SQL =
                "UPDATE RemParties " +
                "SET DocDate = (SELECT DocDate FROM Docs WHERE RemParties.DocID = Docs.DocID);";
            await Task.Run(() => db.Database.ExecuteSqlCommandAsync(SQL));


            //2. RemPartyMinuses
            //2.1. DirEmployeeID из Docs
            SQL =
                "UPDATE RemPartyMinuses " +
                "SET DirEmployeeID = (SELECT DirEmployeeID FROM Docs WHERE RemPartyMinuses.DocID = Docs.DocID);";
            await Task.Run(() => db.Database.ExecuteSqlCommandAsync(SQL));



            //2.2. DirEmployeeID из DocServicePurch2Tabs
            #region DocServicePurch2Tabs

            /*
            SQL =
                "UPDATE RemPartyMinuses " +
                "SET DirEmployeeID = (SELECT DocServicePurch2Tabs.DirEmployeeID FROM DocServicePurches, DocServicePurch2Tabs WHERE RemPartyMinuses.DocID = DocServicePurches.DocID and DocServicePurches.DocServicePurchID=DocServicePurch2Tabs.DocServicePurchID);";
            await Task.Run(() => db.Database.ExecuteSqlCommandAsync(SQL));
            */

            var querySelect = 
                (
                    from x in db.DocServicePurch2Tabs
                    select x
                ).ToList();

            for (int i = 0; i < querySelect.Count(); i++)
            {
                int? DocID = querySelect[i].docServicePurch.DocID, DocServicePurch2TabID = querySelect[i].DocServicePurch2TabID;

                var queryUpdate = db.RemPartyMinuses.Where(x => x.DocID == DocID && x.FieldID == DocServicePurch2TabID).ToList();
                if (queryUpdate.Count() > 0)
                {
                    Models.Sklad.Rem.RemPartyMinus remPartyMinus = queryUpdate[0];
                    remPartyMinus.DocDate = querySelect[i].docServicePurch.doc.DocDate;
                    remPartyMinus.DirEmployeeID = querySelect[i].docServicePurch.doc.DirEmployeeID;

                    db.Entry(remPartyMinus).State = EntityState.Modified;
                    await Task.Run(() => db.SaveChangesAsync());
                }
            }

            #endregion


            //1.2. DocDate из Docs
            SQL =
                "UPDATE RemPartyMinuses " +
                "SET DocDate = (SELECT DocDate FROM Docs WHERE RemPartyMinuses.DocID = Docs.DocID);";
            await Task.Run(() => db.Database.ExecuteSqlCommandAsync(SQL));




            //n.
            Models.Sklad.Sys.SysVer sysVer = await Task.Run(() => db.SysVers.FindAsync(1));
            sysVer.SysVerNumber = 2;
            sysVer.SysVerDate = DateTime.Now;

            db.Entry(sysVer).State = System.Data.Entity.EntityState.Modified;
            await Task.Run(() => db.SaveChangesAsync());

            return true;
        }

        #endregion


        #region Update3 - СС - заполнение поля "DocServicePurches.Sums"

        private async Task<bool> Update3(DbConnectionSklad db)
        {
            //1. Алгоритм: 
            //Пробежатся по всем ремонтам.
            string SQL =
                "UPDATE DocServicePurches " +
                "SET " +
                " Sums = " +
                " (SELECT IFNULL(SUM(PriceCurrency), 0) FROM DocServicePurch1Tabs WHERE DocServicePurch1Tabs.DocServicePurchID = DocServicePurches.DocServicePurchID) + " +
                " (SELECT IFNULL(SUM(PriceCurrency), 0) FROM DocServicePurch2Tabs WHERE DocServicePurch2Tabs.DocServicePurchID = DocServicePurches.DocServicePurchID) - " +
                " IFNULL(DocServicePurches.PrepaymentSum, 0);";
            await Task.Run(() => db.Database.ExecuteSqlCommandAsync(SQL));


            //n.
            Models.Sklad.Sys.SysVer sysVer = await Task.Run(() => db.SysVers.FindAsync(1));
            sysVer.SysVerNumber = 3;
            sysVer.SysVerDate = DateTime.Now;

            db.Entry(sysVer).State = System.Data.Entity.EntityState.Modified;
            await Task.Run(() => db.SaveChangesAsync());

            return true;
        }

        #endregion


        #region Update4 - СС - заполнение поля "DocServicePurches.Sums"

        private async Task<bool> Update4(DbConnectionSklad db)
        {
            //1. Алгоритм: 
            //Пробежатся по всем ремонтам.
            string SQL =
                "UPDATE DocServicePurches " +
                "SET " +
                " Sums = " +
                " (SELECT IFNULL(SUM(PriceCurrency), 0) FROM DocServicePurch1Tabs WHERE DocServicePurch1Tabs.DocServicePurchID = DocServicePurches.DocServicePurchID) + " +
                " (SELECT IFNULL(SUM(PriceCurrency), 0) FROM DocServicePurch2Tabs WHERE DocServicePurch2Tabs.DocServicePurchID = DocServicePurches.DocServicePurchID) - " +
                " IFNULL(DocServicePurches.PrepaymentSum, 0);";
            await Task.Run(() => db.Database.ExecuteSqlCommandAsync(SQL));


            //n.
            Models.Sklad.Sys.SysVer sysVer = await Task.Run(() => db.SysVers.FindAsync(1));
            sysVer.SysVerNumber = 4;
            sysVer.SysVerDate = DateTime.Now;

            db.Entry(sysVer).State = System.Data.Entity.EntityState.Modified;
            await Task.Run(() => db.SaveChangesAsync());

            return true;
        }

        #endregion


        #region Update5 - Приход.Контрагенты: Перенести

        private async Task<bool> Update5(DbConnectionSklad db)
        {
            //Алгоритм: 

            //1. Перенести из "DirCharStyles.DirCharStyleName" в "DirContractors.DirContractorName, ..."

            //2. В "DocPurchTabs" создать поле "DirCharStyleName"
            //   Перенести в DocPurchTabs.DirCharStyleName = DocPurchTabs.docPurchTab.DirCharStyleName
            //3. В "DocPurchTabs" создать поле "DirContractorID FK ..."

            //4. Найти "DocPurchTabs.DirCharStyleName" в "DirContractors.DirContractorName" и 
            //   и записать найденный "DirContractors.DirContractorID" в "DocPurchTabs.DirContractorID"


            //Ну и заменить в коде "DirCharStyleID" на "DirContractorID"



            //1. Перенести из "DirCharStyles.DirCharStyleName" в "DirContractors.DirContractorName, ..."
            string SQL =
                "INSERT INTO DirContractors (DirContractor1TypeID, DirContractor2TypeID, DirContractorName) " +
                "SELECT 1, 2, DirCharStyles.DirCharStyleName FROM DirCharStyles WHERE DirCharStyles.DirCharStyleName not in (SELECT DirContractors.DirContractorName FROM DirContractors);";
            await Task.Run(() => db.Database.ExecuteSqlCommandAsync(SQL));


            //2. В "DocPurchTabs" создать поле "DirCharStyleName"
            //   Перенести в DocPurchTabs.DirCharStyleName = DocPurchTabs.docPurchTab.DirCharStyleName - сделал ниже
            //3. В "DocPurchTabs" создать поле "DirContractorID FK ..."
            SQL =
                "ALTER TABLE DocPurchTabs ADD COLUMN DirCharStyleName TEXT(256);" +
                "ALTER TABLE DocPurchTabs ADD COLUMN [DirContractorID] INTEGER CONSTRAINT [FK_DocPurchTabs_DirContractorID] REFERENCES [DirContractors]([DirContractorID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED;";
            await Task.Run(() => db.Database.ExecuteSqlCommandAsync(SQL));


            //   Перенести в DocPurchTabs.DirCharStyleName = DocPurchTabs.docPurchTab.DirCharStyleName
            SQL =
                "UPDATE DocPurchTabs SET DirCharStyleName=" +
                "(SELECT DirCharStyleName FROM DirCharStyles WHERE DocPurchTabs.DirCharStyleID = DirCharStyles.DirCharStyleID);";
            await Task.Run(() => db.Database.ExecuteSqlCommandAsync(SQL));



            //4. Найти "DocPurchTabs.DirCharStyleName = DirContractors.DirContractorName" и 
            //   записать найденный "DirContractors.DirContractorID" в "DocPurchTabs.DirContractorID"
            SQL =
                "UPDATE DocPurchTabs SET DirContractorID=" +
                "(SELECT DirContractorID FROM DirContractors WHERE DocPurchTabs.DirCharStyleName = DirContractors.DirContractorName);";
            await Task.Run(() => db.Database.ExecuteSqlCommandAsync(SQL));


            //n.
            Models.Sklad.Sys.SysVer sysVer = await Task.Run(() => db.SysVers.FindAsync(1));
            sysVer.SysVerNumber = 5;
            sysVer.SysVerDate = DateTime.Now;

            db.Entry(sysVer).State = System.Data.Entity.EntityState.Modified;
            await Task.Run(() => db.SaveChangesAsync());

            return true;
        }

        #endregion


        #region Update6 - Добавления поля "DateStatusChange" + 2 индекса

        private async Task<bool> Update6(DbConnectionSklad db)
        {
            string SQL =
                "ALTER TABLE DocServicePurches ADD COLUMN DateStatusChange DATE; " +
                "CREATE INDEX [IDX_DocServicePurches_DateStatusChange] ON [DocServicePurches] ([DateStatusChange]); " +
                "CREATE INDEX [IDX_DocServicePurches_IssuanceDate] ON [DocServicePurches] ([IssuanceDate]); ";
            await Task.Run(() => db.Database.ExecuteSqlCommandAsync(SQL));

            SQL = "UPDATE DocServicePurches SET DateStatusChange=DocServicePurches.IssuanceDate; ";
            SQLiteParameter parDateStatusChange = new SQLiteParameter("@DateStatusChange", System.Data.DbType.Date) { Value = DateTime.Now };
            await Task.Run(() => db.Database.ExecuteSqlCommandAsync(SQL, parDateStatusChange));


            //n.
            Models.Sklad.Sys.SysVer sysVer = await Task.Run(() => db.SysVers.FindAsync(1));
            sysVer.SysVerNumber = 6;
            sysVer.SysVerDate = DateTime.Now;

            db.Entry(sysVer).State = System.Data.Entity.EntityState.Modified;
            await Task.Run(() => db.SaveChangesAsync());

            return true;
        }

        #endregion


        #region Update7 - Добавления поля "RightReportTotalTradePrice и RightReportTotalTradePriceCheck" в "DirEmployees"

        private async Task<bool> Update7(DbConnectionSklad db)
        {
            string SQL =
                "ALTER TABLE DirEmployees ADD COLUMN [RightReportTotalTradePrice] INTEGER DEFAULT 3; " +
                "ALTER TABLE DirEmployees ADD COLUMN [RightReportTotalTradePriceCheck] BOOL DEFAULT 0; " +
                "UPDATE DirEmployees SET RightReportTotalTradePrice=1, RightReportTotalTradePriceCheck=1 WHERE DirEmployeeID=1; ";
            await Task.Run(() => db.Database.ExecuteSqlCommandAsync(SQL));


            //n.
            Models.Sklad.Sys.SysVer sysVer = await Task.Run(() => db.SysVers.FindAsync(1));
            sysVer.SysVerNumber = 7;
            sysVer.SysVerDate = DateTime.Now;

            db.Entry(sysVer).State = System.Data.Entity.EntityState.Modified;
            await Task.Run(() => db.SaveChangesAsync());

            return true;
        }

        #endregion


        #region Update8 - Добавления поля "DateStatusChange" в "SysSettings"

        private async Task<bool> Update8(DbConnectionSklad db)
        {
            string SQL =
                "ALTER TABLE SysSettings ADD COLUMN [DocServicePurchSmsAutoShow] BOOL NOT NULL DEFAULT 0; " +
                "INSERT INTO DirSmsTemplates (DirSmsTemplateID, DirSmsTemplateName, DirSmsTemplateMsg, DirSmsTemplateType, MenuID)values(6, 'Приёмка аппарата', 'Ваш аппарата принят в ремонт', 1, 1); ";
            await Task.Run(() => db.Database.ExecuteSqlCommandAsync(SQL));


            //n.
            Models.Sklad.Sys.SysVer sysVer = await Task.Run(() => db.SysVers.FindAsync(1));
            sysVer.SysVerNumber = 8;
            sysVer.SysVerDate = DateTime.Now;

            db.Entry(sysVer).State = System.Data.Entity.EntityState.Modified;
            await Task.Run(() => db.SaveChangesAsync());

            return true;
        }

        #endregion


        #region Update9 - Добавления поля "ServiceKPD" в "SysSettings"

        private async Task<bool> Update9(DbConnectionSklad db)
        {
            string SQL =
                "ALTER TABLE SysSettings ADD COLUMN [ServiceKPD] REAL NOT NULL DEFAULT 300; ";
            await Task.Run(() => db.Database.ExecuteSqlCommandAsync(SQL));


            //n.
            Models.Sklad.Sys.SysVer sysVer = await Task.Run(() => db.SysVers.FindAsync(1));
            sysVer.SysVerNumber = 9;
            sysVer.SysVerDate = DateTime.Now;

            db.Entry(sysVer).State = System.Data.Entity.EntityState.Modified;
            await Task.Run(() => db.SaveChangesAsync());

            return true;
        }

        #endregion


        #region Update10 - Добавления поля "Sums1 and Sums2" в "DocServicePurches"

        private async Task<bool> Update10(DbConnectionSklad db)
        {
            #region 1. Суммы выполенных работ и запчастей

            string SQL =
                "ALTER TABLE DocServicePurches ADD COLUMN [Sums1] REAL NOT NULL DEFAULT 0; " +
                "ALTER TABLE DocServicePurches ADD COLUMN [Sums2] REAL NOT NULL DEFAULT 0; ";
            await Task.Run(() => db.Database.ExecuteSqlCommandAsync(SQL));

            #endregion


            #region 2. Удаляем старые тригерры

            SQL =
                "DROP TRIGGER TG_UPDATE_DocServicePurch1Tabs_DocServicePurches; " +
                "DROP TRIGGER TG_INSERT_DocServicePurch1Tabs_DocServicePurches; " +
                "DROP TRIGGER TG_DELETE_DocServicePurch1Tabs_DocServicePurches; " +
                "DROP TRIGGER TG_INSERT_DocServicePurch2Tabs_DocServicePurches; " +
                "DROP TRIGGER TG_UPDATE_DocServicePurch2Tabs_DocServicePurches; " +
                "DROP TRIGGER TG_DELETE_DocServicePurch2Tabs_DocServicePurches; ";
            await Task.Run(() => db.Database.ExecuteSqlCommandAsync(SQL));

            #endregion


            #region 3. Создаём новые тригерры

            SQL =
                "CREATE TRIGGER[TG_UPDATE_DocServicePurch1Tabs_DocServicePurches] " +
                "AFTER UPDATE " +
                "ON[DocServicePurch1Tabs] " +
                "BEGIN " +

                " UPDATE DocServicePurches " +
                " SET " +

                " Sums1 = " +
                "  ( " +
                "   SELECT IFNULL(SUM(DocServicePurch1Tabs.PriceCurrency), 0) FROM DocServicePurch1Tabs WHERE DocServicePurchID = " +
                "       ( " +
                "        SELECT DocServicePurch1Tabs.DocServicePurchID FROM DocServicePurch1Tabs WHERE DocServicePurch1TabID = NEW.DocServicePurch1TabID LIMIT 1 " +
                "       ) " +
                "  ), " +

                " Sums2 = " +
                " ( " +
                "  SELECT IFNULL(SUM(DocServicePurch2Tabs.PriceCurrency), 0) FROM DocServicePurch2Tabs WHERE DocServicePurchID = " +
                "       ( " +
                "        SELECT DocServicePurch1Tabs.DocServicePurchID FROM DocServicePurch1Tabs WHERE DocServicePurch1TabID = NEW.DocServicePurch1TabID LIMIT 1 " +
                "       ) " +
                " ), " +

                " Sums = " +
                "  ( " +
                "   SELECT IFNULL(SUM(DocServicePurch1Tabs.PriceCurrency), 0) FROM DocServicePurch1Tabs WHERE DocServicePurchID = " +
                "       ( " +
                "        SELECT DocServicePurch1Tabs.DocServicePurchID FROM DocServicePurch1Tabs WHERE DocServicePurch1TabID = NEW.DocServicePurch1TabID LIMIT 1 " +
                "       ) " +
                "  )    " +
                " + " +
                " ( " +
                "  SELECT IFNULL(SUM(DocServicePurch2Tabs.PriceCurrency), 0) FROM DocServicePurch2Tabs WHERE DocServicePurchID = " +
                "       ( " +
                "        SELECT DocServicePurch1Tabs.DocServicePurchID FROM DocServicePurch1Tabs WHERE DocServicePurch1TabID = NEW.DocServicePurch1TabID LIMIT 1 " +
                "       ) " +
                " ) " +
                " - " +
                " IFNULL(DocServicePurches.PrepaymentSum, 0) " +

                " WHERE " +
                "  DocServicePurchID = (SELECT DocServicePurch1Tabs.DocServicePurchID FROM DocServicePurch1Tabs WHERE DocServicePurch1TabID = NEW.DocServicePurch1TabID LIMIT 1); " +

                "END; " +


                "CREATE TRIGGER[TG_INSERT_DocServicePurch1Tabs_DocServicePurches] " +
                "AFTER INSERT " +
                "ON[DocServicePurch1Tabs] " +
                "BEGIN " +

                " UPDATE DocServicePurches " +
                " SET " +

                " Sums1 = " +
                "  ( " +
                "   SELECT IFNULL(SUM(DocServicePurch1Tabs.PriceCurrency), 0) FROM DocServicePurch1Tabs WHERE DocServicePurchID = " +
                "       ( " +
                "        SELECT DocServicePurch1Tabs.DocServicePurchID FROM DocServicePurch1Tabs WHERE DocServicePurch1TabID = NEW.DocServicePurch1TabID LIMIT 1 " +
                "       ) " +
                "  ),  " +

                " Sums2 = " +
                " ( " +
                "  SELECT IFNULL(SUM(DocServicePurch2Tabs.PriceCurrency), 0) FROM DocServicePurch2Tabs WHERE DocServicePurchID = " +
                "       ( " +
                "        SELECT DocServicePurch1Tabs.DocServicePurchID FROM DocServicePurch1Tabs WHERE DocServicePurch1TabID = NEW.DocServicePurch1TabID LIMIT 1 " +
                "       ) " +
                " ), " +

                " Sums = " +
                "  ( " +
                "   SELECT IFNULL(SUM(DocServicePurch1Tabs.PriceCurrency), 0) FROM DocServicePurch1Tabs WHERE DocServicePurchID = " +
                "       ( " +
                "        SELECT DocServicePurch1Tabs.DocServicePurchID FROM DocServicePurch1Tabs WHERE DocServicePurch1TabID = NEW.DocServicePurch1TabID LIMIT 1 " +
                "       ) " +
                "  ) " +
                " + " +
                " ( " +
                "  SELECT IFNULL(SUM(DocServicePurch2Tabs.PriceCurrency), 0) FROM DocServicePurch2Tabs WHERE DocServicePurchID = " +
                "       ( " +
                "        SELECT DocServicePurch1Tabs.DocServicePurchID FROM DocServicePurch1Tabs WHERE DocServicePurch1TabID = NEW.DocServicePurch1TabID LIMIT 1 " +
                "       ) " +
                " ) " +
                " - " +
                " IFNULL(DocServicePurches.PrepaymentSum, 0) " +

                " WHERE " +
                "  DocServicePurchID = (SELECT DocServicePurch1Tabs.DocServicePurchID FROM DocServicePurch1Tabs WHERE DocServicePurch1TabID = NEW.DocServicePurch1TabID LIMIT 1); " +

                "END; " +


                "CREATE TRIGGER[TG_DELETE_DocServicePurch1Tabs_DocServicePurches] " +
                "BEFORE DELETE " +
                "ON[DocServicePurch1Tabs] " +
                "BEGIN " +

                " UPDATE DocServicePurches " +
                " SET " +

                " Sums1 = " +
                "  ( " +
                "   SELECT IFNULL(SUM(DocServicePurch1Tabs.PriceCurrency), 0) FROM DocServicePurch1Tabs WHERE DocServicePurchID = " +
                "       ( " +
                "        SELECT DocServicePurch1Tabs.DocServicePurchID FROM DocServicePurch1Tabs WHERE DocServicePurch1TabID = OLD.DocServicePurch1TabID LIMIT 1 " +
                "       ) " +
                "  ), " +

                " Sums2 = " +
                " ( " +
                "  SELECT IFNULL(SUM(DocServicePurch2Tabs.PriceCurrency), 0) FROM DocServicePurch2Tabs WHERE DocServicePurchID = " +
                "       ( " +
                "        SELECT DocServicePurch1Tabs.DocServicePurchID FROM DocServicePurch1Tabs WHERE DocServicePurch1TabID = OLD.DocServicePurch1TabID LIMIT 1 " +
                "       ) " +
                " ), " +

                " Sums = " +
                "  ( " +
                "   SELECT IFNULL(SUM(DocServicePurch1Tabs.PriceCurrency), 0) FROM DocServicePurch1Tabs WHERE DocServicePurchID = " +
                "       ( " +
                "        SELECT DocServicePurch1Tabs.DocServicePurchID FROM DocServicePurch1Tabs WHERE DocServicePurch1TabID = OLD.DocServicePurch1TabID LIMIT 1 " +
                "       ) " +
                "  ) " +
                " + " +
                " ( " +
                "  SELECT IFNULL(SUM(DocServicePurch2Tabs.PriceCurrency), 0) FROM DocServicePurch2Tabs WHERE DocServicePurchID = " +
                "       ( " +
                "        SELECT DocServicePurch1Tabs.DocServicePurchID FROM DocServicePurch1Tabs WHERE DocServicePurch1TabID = OLD.DocServicePurch1TabID LIMIT 1 " +
                "       ) " +
                " ) " +
                " - " +
                " IFNULL(DocServicePurches.PrepaymentSum, 0) " +

                " WHERE " +
                "  DocServicePurchID = (SELECT DocServicePurch1Tabs.DocServicePurchID FROM DocServicePurch1Tabs WHERE DocServicePurch1TabID = OLD.DocServicePurch1TabID LIMIT 1); " +

                "END; " +


                "CREATE TRIGGER[TG_INSERT_DocServicePurch2Tabs_DocServicePurches] " +
                "AFTER INSERT " +
                "ON[DocServicePurch2Tabs] " +
                "BEGIN " +

                " UPDATE DocServicePurches " +
                " SET " +

                " Sums1 = " +
                "  ( " +
                "   SELECT IFNULL(SUM(DocServicePurch1Tabs.PriceCurrency), 0) FROM DocServicePurch1Tabs WHERE DocServicePurchID = " +
                "       ( " +
                "        SELECT DocServicePurch2Tabs.DocServicePurchID FROM DocServicePurch2Tabs WHERE DocServicePurch2TabID = NEW.DocServicePurch2TabID LIMIT 1 " +
                "       ) " +
                "  ), " +

                " Sums2 = " +
                " ( " +
                "  SELECT IFNULL(SUM(DocServicePurch2Tabs.PriceCurrency), 0) FROM DocServicePurch2Tabs WHERE DocServicePurchID = " +
                "       ( " +
                "        SELECT DocServicePurch2Tabs.DocServicePurchID FROM DocServicePurch2Tabs WHERE DocServicePurch2TabID = NEW.DocServicePurch2TabID LIMIT 1 " +
                "       ) " +
                " ), " +

                " Sums = " +
                "  ( " +
                "   SELECT IFNULL(SUM(DocServicePurch1Tabs.PriceCurrency), 0) FROM DocServicePurch1Tabs WHERE DocServicePurchID = " +
                "       ( " +
                "        SELECT DocServicePurch2Tabs.DocServicePurchID FROM DocServicePurch2Tabs WHERE DocServicePurch2TabID = NEW.DocServicePurch2TabID LIMIT 1 " +
                "       ) " +
                "  ) " +
                " + " +
                " ( " +
                "  SELECT IFNULL(SUM(DocServicePurch2Tabs.PriceCurrency), 0) FROM DocServicePurch2Tabs WHERE DocServicePurchID = " +
                "       ( " +
                "        SELECT DocServicePurch2Tabs.DocServicePurchID FROM DocServicePurch2Tabs WHERE DocServicePurch2TabID = NEW.DocServicePurch2TabID LIMIT 1 " +
                "       ) " +
                " ) " +
                " - " +
                " IFNULL(DocServicePurches.PrepaymentSum, 0) " +

                " WHERE " +
                "  DocServicePurchID = (SELECT DocServicePurch2Tabs.DocServicePurchID FROM DocServicePurch2Tabs WHERE DocServicePurch2TabID = NEW.DocServicePurch2TabID LIMIT 1); " +

                "END; " +


                "CREATE TRIGGER[TG_UPDATE_DocServicePurch2Tabs_DocServicePurches] " +
                "AFTER UPDATE " +
                "ON[DocServicePurch2Tabs] " +
                "BEGIN " +

                " UPDATE DocServicePurches " +
                " SET " +

                " Sums1 = " +
                "  ( " +
                "   SELECT IFNULL(SUM(DocServicePurch1Tabs.PriceCurrency), 0) FROM DocServicePurch1Tabs WHERE DocServicePurchID = " +
                "       ( " +
                "        SELECT DocServicePurch2Tabs.DocServicePurchID FROM DocServicePurch2Tabs WHERE DocServicePurch2TabID = NEW.DocServicePurch2TabID LIMIT 1 " +
                "       ) " +
                "  ),  " +

                " Sums2 = " +
                " ( " +
                "  SELECT IFNULL(SUM(DocServicePurch2Tabs.PriceCurrency), 0) FROM DocServicePurch2Tabs WHERE DocServicePurchID = " +
                "       ( " +
                "        SELECT DocServicePurch2Tabs.DocServicePurchID FROM DocServicePurch2Tabs WHERE DocServicePurch2TabID = NEW.DocServicePurch2TabID LIMIT 1 " +
                "       ) " +
                " ), " +


                " Sums = " +
                "  ( " +
                "   SELECT IFNULL(SUM(DocServicePurch1Tabs.PriceCurrency), 0) FROM DocServicePurch1Tabs WHERE DocServicePurchID = " +
                "       ( " +
                "        SELECT DocServicePurch2Tabs.DocServicePurchID FROM DocServicePurch2Tabs WHERE DocServicePurch2TabID = NEW.DocServicePurch2TabID LIMIT 1 " +
                "       ) " +
                "  )  " +
                " + " +
                " ( " +
                "  SELECT IFNULL(SUM(DocServicePurch2Tabs.PriceCurrency), 0) FROM DocServicePurch2Tabs WHERE DocServicePurchID = " +
                "       ( " +
                "        SELECT DocServicePurch2Tabs.DocServicePurchID FROM DocServicePurch2Tabs WHERE DocServicePurch2TabID = NEW.DocServicePurch2TabID LIMIT 1 " +
                "       ) " +
                " ) " +
                " - " +
                " IFNULL(DocServicePurches.PrepaymentSum, 0) " +

                " WHERE " +
                "  DocServicePurchID = (SELECT DocServicePurch2Tabs.DocServicePurchID FROM DocServicePurch2Tabs WHERE DocServicePurch2TabID = NEW.DocServicePurch2TabID LIMIT 1); " +

                "END; " +


                "CREATE TRIGGER[TG_DELETE_DocServicePurch2Tabs_DocServicePurches] " +
                "BEFORE DELETE " +
                "ON[DocServicePurch2Tabs] " +
                "BEGIN " +

                " UPDATE DocServicePurches " +
                " SET " +

                " Sums1 = " +
                "  ( " +
                "   SELECT IFNULL(SUM(DocServicePurch1Tabs.PriceCurrency), 0) FROM DocServicePurch1Tabs WHERE DocServicePurchID = " +
                "       ( " +
                "        SELECT DocServicePurch2Tabs.DocServicePurchID FROM DocServicePurch2Tabs WHERE DocServicePurch2TabID = OLD.DocServicePurch2TabID LIMIT 1 " +
                "       ) " +
                "  ),  " +

                " Sums2 = " +
                " ( " +
                "  SELECT IFNULL(SUM(DocServicePurch2Tabs.PriceCurrency), 0) FROM DocServicePurch2Tabs WHERE DocServicePurchID = " +
                "       ( " +
                "        SELECT DocServicePurch2Tabs.DocServicePurchID FROM DocServicePurch2Tabs WHERE DocServicePurch2TabID = OLD.DocServicePurch2TabID LIMIT 1 " +
                "       ) " +
                " ), " +

                " Sums = " +
                "  ( " +
                "   SELECT IFNULL(SUM(DocServicePurch1Tabs.PriceCurrency), 0) FROM DocServicePurch1Tabs WHERE DocServicePurchID = " +
                "       ( " +
                "        SELECT DocServicePurch2Tabs.DocServicePurchID FROM DocServicePurch2Tabs WHERE DocServicePurch2TabID = OLD.DocServicePurch2TabID LIMIT 1 " +
                "       ) " +
                "  ) " +
                " + " +
                " ( " +
                "  SELECT IFNULL(SUM(DocServicePurch2Tabs.PriceCurrency), 0) FROM DocServicePurch2Tabs WHERE DocServicePurchID = " +
                "       ( " +
                "        SELECT DocServicePurch2Tabs.DocServicePurchID FROM DocServicePurch2Tabs WHERE DocServicePurch2TabID = OLD.DocServicePurch2TabID LIMIT 1 " +
                "       ) " +
                " ) " +
                " - " +
                " IFNULL(DocServicePurches.PrepaymentSum, 0) " +

                " WHERE " +
                "  DocServicePurchID = (SELECT DocServicePurch2Tabs.DocServicePurchID FROM DocServicePurch2Tabs WHERE DocServicePurch2TabID = OLD.DocServicePurch2TabID LIMIT 1); " +

                "END; ";


            await Task.Run(() => db.Database.ExecuteSqlCommandAsync(SQL));

            #endregion



            #region 4. Пересчитываем суммы Sums1 и Sums2

            SQL =
                "UPDATE DocServicePurches " +
                "SET " +
                " Sums1 = " +
                " (SELECT IFNULL(SUM(PriceCurrency), 0) FROM DocServicePurch1Tabs WHERE DocServicePurch1Tabs.DocServicePurchID = DocServicePurches.DocServicePurchID); " +

                "UPDATE DocServicePurches " +
                "SET " +
                " Sums2 = " +
                " (SELECT IFNULL(SUM(PriceCurrency), 0) FROM DocServicePurch2Tabs WHERE DocServicePurch2Tabs.DocServicePurchID = DocServicePurches.DocServicePurchID); ";

            await Task.Run(() => db.Database.ExecuteSqlCommandAsync(SQL));

            #endregion



            //n.
            Models.Sklad.Sys.SysVer sysVer = await Task.Run(() => db.SysVers.FindAsync(1));
            sysVer.SysVerNumber = 10;
            sysVer.SysVerDate = DateTime.Now;

            db.Entry(sysVer).State = System.Data.Entity.EntityState.Modified;
            await Task.Run(() => db.SaveChangesAsync());

            return true;
        }

        #endregion


        #region Update11 - Б/У - 1

        private async Task<bool> Update11(DbConnectionSklad db)
        {
            #region 1. Права

            string SQL =
                "ALTER TABLE DirEmployees ADD COLUMN [RightDocSecondHands0] BOOL DEFAULT 0; " +
                "UPDATE DirEmployees SET RightDocSecondHands0=1 WHERE DirEmployeeID=1; " +

                "ALTER TABLE DirEmployees ADD COLUMN [RightDocSecondHandPurches] INTEGER DEFAULT 3; " +
                "ALTER TABLE DirEmployees ADD COLUMN [RightDocSecondHandPurchesCheck] BOOL DEFAULT 0; " +
                "UPDATE DirEmployees SET RightDocSecondHandPurches=1, RightDocSecondHandPurchesCheck=1 WHERE DirEmployeeID=1; " +

                "ALTER TABLE DirEmployees ADD COLUMN [RightDocSecondHandRetails] INTEGER DEFAULT 3; " +
                "ALTER TABLE DirEmployees ADD COLUMN [RightDocSecondHandRetailsCheck] BOOL DEFAULT 0; " +
                "UPDATE DirEmployees SET RightDocSecondHandRetails=1, RightDocSecondHandRetailsCheck=1 WHERE DirEmployeeID=1; " +

                "ALTER TABLE DirEmployees ADD COLUMN [RightDocSecondHandsReport] INTEGER DEFAULT 3; " +
                "ALTER TABLE DirEmployees ADD COLUMN [RightDocSecondHandsReportCheck] BOOL DEFAULT 0; " +
                "UPDATE DirEmployees SET RightDocSecondHandsReport=1, RightDocSecondHandsReportCheck=1 WHERE DirEmployeeID=1; ";

            await Task.Run(() => db.Database.ExecuteSqlCommandAsync(SQL));

            #endregion


            #region 2. Документ "DocSecondHands"

            //...

            #endregion


            #region 3. ListObjects: 64, 3, DocSecondHands, Документ Б/У

            //...

            #endregion


            #region 4. ListObjectPFs

            //...

            #endregion




            //n.
            Models.Sklad.Sys.SysVer sysVer = await Task.Run(() => db.SysVers.FindAsync(1));
            sysVer.SysVerNumber = 11;
            sysVer.SysVerDate = DateTime.Now;

            db.Entry(sysVer).State = System.Data.Entity.EntityState.Modified;
            await Task.Run(() => db.SaveChangesAsync());

            return true;
        }

        #endregion


        #region Update12 - Б/У - 2

        private async Task<bool> Update12(DbConnectionSklad db)
        {
  
            #region 1. Права

            //Добавить права:
            //RightDocSecondHandWorkshops, RightDocSecondHandWorkshopsCheck
            //RightDocSecondHandPurch1Tabs, RightDocSecondHandPurch1TabsCheck
            //RightDocSecondHandPurch2Tabs, RightDocSecondHandPurch2TabsCheck
            //RightDocSecondHandRetailReturns, RightDocSecondHandRetailReturnsCheck
            //RightDocSecondHandRetailActWriteOffs, RightDocSecondHandRetailActWriteOffsCheck, 

            string SQL =
                "ALTER TABLE DirEmployees ADD COLUMN [RightDocSecondHandWorkshops] INTEGER DEFAULT 3; " +
                "ALTER TABLE DirEmployees ADD COLUMN [RightDocSecondHandWorkshopsCheck] BOOL DEFAULT 0; " +
                "UPDATE DirEmployees SET RightDocSecondHandWorkshops=1, RightDocSecondHandWorkshopsCheck=1 WHERE DirEmployeeID=1; " +

                "ALTER TABLE DirEmployees ADD COLUMN [RightDocSecondHandPurch1Tabs] INTEGER DEFAULT 3; " +
                "ALTER TABLE DirEmployees ADD COLUMN [RightDocSecondHandPurch1TabsCheck] BOOL DEFAULT 0; " +
                "UPDATE DirEmployees SET RightDocSecondHandPurch1Tabs=1, RightDocSecondHandPurch1TabsCheck=1 WHERE DirEmployeeID=1; " +

                "ALTER TABLE DirEmployees ADD COLUMN [RightDocSecondHandPurch2Tabs] INTEGER DEFAULT 3; " +
                "ALTER TABLE DirEmployees ADD COLUMN [RightDocSecondHandPurch2TabsCheck] BOOL DEFAULT 0; " +
                "UPDATE DirEmployees SET RightDocSecondHandPurch2Tabs=1, RightDocSecondHandPurch2TabsCheck=1 WHERE DirEmployeeID=1; " +

                "ALTER TABLE DirEmployees ADD COLUMN [RightDocSecondHandRetailReturns] INTEGER DEFAULT 3; " +
                "ALTER TABLE DirEmployees ADD COLUMN [RightDocSecondHandRetailReturnsCheck] BOOL DEFAULT 0; " +
                "UPDATE DirEmployees SET RightDocSecondHandRetailReturns=1, RightDocSecondHandRetailReturnsCheck=1 WHERE DirEmployeeID=1; " +


                "ALTER TABLE DirEmployees ADD COLUMN [RightDocSecondHandRetailActWriteOffs] INTEGER DEFAULT 3; " +
                "ALTER TABLE DirEmployees ADD COLUMN [RightDocSecondHandRetailActWriteOffsCheck] BOOL DEFAULT 0; " +
                "UPDATE DirEmployees SET RightDocSecondHandRetailActWriteOffs=1, RightDocSecondHandRetailActWriteOffsCheck=1 WHERE DirEmployeeID=1; ";

            await Task.Run(() => db.Database.ExecuteSqlCommandAsync(SQL));

            #endregion

            #region 2. DirServiceContractors

            //В таблицу "DirServiceContractors" добавить 2-а поля "PassportSeries" and "PassportNumber"
            SQL =
                "ALTER TABLE DirServiceContractors ADD COLUMN [PassportSeries] TEXT(32); " +
                "ALTER TABLE DirServiceContractors ADD COLUMN [PassportNumber] TEXT(32); ";

            await Task.Run(() => db.Database.ExecuteSqlCommandAsync(SQL));

            #endregion

            #region 3. DirSecondHandStatuses

            //1. Создать таблицу "DirSecondHandStatuses"
            //2. Заполнить статусами

            SQL =
                "CREATE TABLE[DirSecondHandStatuses]( " +
                "  [DirSecondHandStatusID] INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                "  [DirSecondHandStatusName] TEXT(256) NOT NULL, " +
                "  [SortNum] INTEGER NOT NULL); " +
                "CREATE UNIQUE INDEX[UDX_DirSecondHandStatuses_SortNum] ON[DirSecondHandStatuses]([SortNum]); " + 
            
                "INSERT INTO DirSecondHandStatuses (DirSecondHandStatusID, DirSecondHandStatusName, SortNum)values(1, 'Куплен', 1); " +
                "INSERT INTO DirSecondHandStatuses (DirSecondHandStatusID, DirSecondHandStatusName, SortNum)values(2, 'Предпродажа (мастерская)', 2); " +
                "INSERT INTO DirSecondHandStatuses (DirSecondHandStatusID, DirSecondHandStatusName, SortNum)values(3, 'На согласовании', 3); " +
                "INSERT INTO DirSecondHandStatuses (DirSecondHandStatusID, DirSecondHandStatusName, SortNum)values(4, 'Согласовано', 4); " +
                "INSERT INTO DirSecondHandStatuses (DirSecondHandStatusID, DirSecondHandStatusName, SortNum)values(5, 'Ожидает запчастей', 5); " +
                "INSERT INTO DirSecondHandStatuses (DirSecondHandStatusID, DirSecondHandStatusName, SortNum)values(7, 'Готов для продажи', 6); " +
                "INSERT INTO DirSecondHandStatuses (DirSecondHandStatusID, DirSecondHandStatusName, SortNum)values(8, 'На разбор', 7); " +
                "INSERT INTO DirSecondHandStatuses (DirSecondHandStatusID, DirSecondHandStatusName, SortNum)values(9, 'Выдан', 8); ";

            await Task.Run(() => db.Database.ExecuteSqlCommandAsync(SQL));

            #endregion

            #region 4. DocSecondHandPurches

            //Создать таблицу "DocSecondHandPurches"
            //В таблицу "ListObjects" добавить 1 запись: (65, 3, DocSecondHandPurches, Документ Б/У)

            SQL =
                "CREATE TABLE [DocSecondHandPurches] ( " +
                "  [DocSecondHandPurchID] INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,  " +
                "  [DocID] INTEGER NOT NULL CONSTRAINT [FK_DocSecondHandPurches_DocID] REFERENCES [Docs]([DocID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED,  " +
                "  [DirWarehouseID] INTEGER NOT NULL CONSTRAINT [FK_DocSecondHandPurches_DirWarehouseID] REFERENCES [DirWarehouses]([DirWarehouseID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED,  " +
                "  [DirServiceNomenID] INTEGER NOT NULL CONSTRAINT [FK_DocSecondHandPurches_DirServiceNomenID] REFERENCES [DirServiceNomens]([DirServiceNomenID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED,  " +
                "  [DirSecondHandStatusID] INTEGER NOT NULL CONSTRAINT [FK_DocSecondHandPurches_DirSecondHandStatusID] REFERENCES [DirSecondHandStatuses]([DirSecondHandStatusID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED,  " +
                "  [DirSecondHandStatusID_789] INTEGER CONSTRAINT [FK_DocSecondHandPurches_DirSecondHandStatusID_789] REFERENCES [DirSecondHandStatuses]([DirSecondHandStatusID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED,  " +
                "  [SerialNumberNo] BOOL DEFAULT 0,  " +
                "  [SerialNumber] TEXT(256),  " +
                "  [ComponentPasTextNo] BOOL DEFAULT 0,  " +
                "  [ComponentPasText] TEXT(256),  " +
                "  [ComponentOtherText] TEXT(256),  " +
                "  [ProblemClientWords] TEXT(2048), " +
                "  [Note] TEXT(2048),  " +
                "  [DirEmployeeIDMaster] INTEGER NOT NULL CONSTRAINT [FK_DocSecondHandPurches_DirEmployeeIDMaster] REFERENCES [DirEmployees]([DirEmployeeID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED,  " +
                "  [DirServiceContractorName] TEXT(256),  " +
                "  [DirServiceContractorRegular] BOOL DEFAULT 0,  " +
                "  [DirServiceContractorID] INTEGER CONSTRAINT [FK_DocSecondHandPurches_DirServiceContractorID] REFERENCES [DirServiceContractors]([DirServiceContractorID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED,  " +
                "  [DirServiceContractorAddress] TEXT,  " +
                "  [DirServiceContractorPhone] TEXT,  " +
                "  [DirServiceContractorEmail] TEXT,  " +
                "  [PassportSeries] TEXT,  " +
                "  [PassportNumber] TEXT,  " +
                "  [ServiceTypeRepair] INTEGER NOT NULL DEFAULT 1,  " +
                "  [PriceVAT] REAL NOT NULL,  " +
                "  [PriceCurrency] REAL NOT NULL,  " +
                "  [DirCurrencyID] INTEGER NOT NULL CONSTRAINT [FK_DocSecondHandPurches_DirCurrencyID] REFERENCES [DirCurrencies]([DirCurrencyID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED,  " +
                "  [DirCurrencyRate] REAL NOT NULL DEFAULT 1,  " +
                "  [DirCurrencyMultiplicity] INTEGER NOT NULL DEFAULT 1,  " +
                "  [PriceRetailVAT] REAL NOT NULL,  " +
                "  [PriceRetailCurrency] REAL NOT NULL,  " +
                "  [PriceWholesaleVAT] REAL NOT NULL,  " +
                "  [PriceWholesaleCurrency] REAL NOT NULL,  " +
                "  [PriceIMVAT] REAL NOT NULL,  " +
                "  [PriceIMCurrency] REAL NOT NULL,  " +
                "  [ComponentOther] BOOL DEFAULT 0,  " +
                "  [DateDone] DATE NOT NULL,  " +
                "  [IssuanceDate] DATE,  " +
                "  [Summ_NotPre] REAL,  " +
                "  [Sums] REAL,  " +
                "  [DocDate_First] DATE,  " +
                "  [DateStatusChange] DATE,  " +
                "  [Sums1] REAL NOT NULL DEFAULT 0,  " +
                "  [Sums2] REAL NOT NULL DEFAULT 0); " +

                "CREATE INDEX [IDX_DocSecondHandPurches_DateStatusChange] ON [DocSecondHandPurches] ([DateStatusChange]); " +

                "CREATE INDEX [IDX_DocSecondHandPurches_DirSecondHandStatusID] ON [DocSecondHandPurches] ([DirSecondHandStatusID]); " +

                "CREATE INDEX [IDX_DocSecondHandPurches_DirSecondHandStatusID_789] ON [DocSecondHandPurches] ([DirSecondHandStatusID_789]); " +

                "CREATE INDEX [IDX_DocSecondHandPurches_DirServiceContractorID] ON [DocSecondHandPurches] ([DirServiceContractorID]); " +

                "CREATE INDEX [IDX_DocSecondHandPurches_DirServiceNomenID] ON [DocSecondHandPurches] ([DirServiceNomenID]); " +

                "CREATE INDEX [IDX_DocSecondHandPurches_DirWarehouseID] ON [DocSecondHandPurches] ([DirWarehouseID]); " +

                "CREATE INDEX [IDX_DocSecondHandPurches_IssuanceDate] ON [DocSecondHandPurches] ([IssuanceDate]); " +

                "CREATE INDEX [IDX_DocSecondHandPurches_SerialNumber] ON [DocSecondHandPurches] ([SerialNumber] COLLATE NOCASE); " +


            "INSERT INTO ListObjects (ListObjectID, ListObjectTypeID, ListObjectNameSys, ListObjectNameRu)values(65, 3, 'DocSecondHandPurches', 'Документ Б/У'); ";


            await Task.Run(() => db.Database.ExecuteSqlCommandAsync(SQL));

            #endregion

            #region 4.1. DocSecondHandPurch1Tabs

            //Создать таблицу "DocSecondHandPurch1Tabs"

            SQL =
                "CREATE TABLE [DocSecondHandPurch1Tabs] ( " +
                "  [DocSecondHandPurch1TabID] INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,  " +
                "  [DocSecondHandPurchID] INTEGER NOT NULL CONSTRAINT [FK_DocSecondHandPurch1Tabs_DocSecondHandPurchID] REFERENCES [DocSecondHandPurches]([DocSecondHandPurchID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED,  " +
                "  [DirEmployeeID] INTEGER NOT NULL CONSTRAINT [FK_DocSecondHandPurch1Tabs_DirEmployeeID] REFERENCES [DirEmployees]([DirEmployeeID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED,  " +
                "  [DirServiceJobNomenID] INTEGER CONSTRAINT [FK_DocSecondHandPurch1Tabs_DirServiceJobNomenID] REFERENCES [DirServiceJobNomens]([DirServiceJobNomenID]) ON DELETE RESTRICT ON UPDATE RESTRICT MATCH FULL DEFERRABLE INITIALLY DEFERRED,  " +
                "  [DirServiceJobNomenName] TEXT(256),  " +
                "  [PriceVAT] REAL NOT NULL,  " +
                "  [PriceCurrency] REAL NOT NULL,  " +
                "  [DirCurrencyID] INTEGER NOT NULL CONSTRAINT [FK_DocSaleTab_DirCurrencyID] REFERENCES [DirCurrencies]([DirCurrencyID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED,  " +
                "  [DirCurrencyRate] REAL NOT NULL DEFAULT 1,  " +
                "  [DirCurrencyMultiplicity] INTEGER NOT NULL DEFAULT 1,  " +
                "  [DiagnosticRresults] TEXT(1024),  " +
                "  [TabDate] DATETIME,  " +
                "  [DirSecondHandStatusID] INTEGER); " +

                "CREATE INDEX [IDX_DocSecondHandPurch1Tabs_DirEmployeeID] ON [DocSecondHandPurch1Tabs] ([DirEmployeeID]); " +

                "CREATE TRIGGER [TG_UPDATE_DocSecondHandPurch1Tabs_DocSecondHandPurches] " +
                "AFTER UPDATE " +
                "ON [DocSecondHandPurch1Tabs] " +
                "BEGIN  UPDATE DocSecondHandPurches  SET  Sums1 =   (    SELECT IFNULL(SUM(DocSecondHandPurch1Tabs.PriceCurrency), 0) FROM DocSecondHandPurch1Tabs WHERE DocSecondHandPurchID =        (         SELECT DocSecondHandPurch1Tabs.DocSecondHandPurchID FROM DocSecondHandPurch1Tabs WHERE DocSecondHandPurch1TabID = NEW.DocSecondHandPurch1TabID LIMIT 1        )   ),  Sums2 =  (   SELECT IFNULL(SUM(DocSecondHandPurch2Tabs.PriceCurrency), 0) FROM DocSecondHandPurch2Tabs WHERE DocSecondHandPurchID =        (         SELECT DocSecondHandPurch1Tabs.DocSecondHandPurchID FROM DocSecondHandPurch1Tabs WHERE DocSecondHandPurch1TabID = NEW.DocSecondHandPurch1TabID LIMIT 1        )  ),  Sums =   (    SELECT IFNULL(SUM(DocSecondHandPurch1Tabs.PriceCurrency), 0) FROM DocSecondHandPurch1Tabs WHERE DocSecondHandPurchID =        (         SELECT DocSecondHandPurch1Tabs.DocSecondHandPurchID FROM DocSecondHandPurch1Tabs WHERE DocSecondHandPurch1TabID = NEW.DocSecondHandPurch1TabID LIMIT 1        )   )     +  (   SELECT IFNULL(SUM(DocSecondHandPurch2Tabs.PriceCurrency), 0) FROM DocSecondHandPurch2Tabs WHERE DocSecondHandPurchID =        (         SELECT DocSecondHandPurch1Tabs.DocSecondHandPurchID FROM DocSecondHandPurch1Tabs WHERE DocSecondHandPurch1TabID = NEW.DocSecondHandPurch1TabID LIMIT 1        )  )  WHERE   DocSecondHandPurchID = (SELECT DocSecondHandPurch1Tabs.DocSecondHandPurchID FROM DocSecondHandPurch1Tabs WHERE DocSecondHandPurch1TabID = NEW.DocSecondHandPurch1TabID LIMIT 1); END; " +

                "CREATE TRIGGER [TG_INSERT_DocSecondHandPurch1Tabs_DocSecondHandPurches] " +
                "AFTER INSERT " +
                "ON [DocSecondHandPurch1Tabs] " +
                "BEGIN  UPDATE DocSecondHandPurches  SET  Sums1 =   (    SELECT IFNULL(SUM(DocSecondHandPurch1Tabs.PriceCurrency), 0) FROM DocSecondHandPurch1Tabs WHERE DocSecondHandPurchID =        (         SELECT DocSecondHandPurch1Tabs.DocSecondHandPurchID FROM DocSecondHandPurch1Tabs WHERE DocSecondHandPurch1TabID = NEW.DocSecondHandPurch1TabID LIMIT 1        )   ),   Sums2 =  (   SELECT IFNULL(SUM(DocSecondHandPurch2Tabs.PriceCurrency), 0) FROM DocSecondHandPurch2Tabs WHERE DocSecondHandPurchID =        (         SELECT DocSecondHandPurch1Tabs.DocSecondHandPurchID FROM DocSecondHandPurch1Tabs WHERE DocSecondHandPurch1TabID = NEW.DocSecondHandPurch1TabID LIMIT 1        )  ),  Sums =   (    SELECT IFNULL(SUM(DocSecondHandPurch1Tabs.PriceCurrency), 0) FROM DocSecondHandPurch1Tabs WHERE DocSecondHandPurchID =        (         SELECT DocSecondHandPurch1Tabs.DocSecondHandPurchID FROM DocSecondHandPurch1Tabs WHERE DocSecondHandPurch1TabID = NEW.DocSecondHandPurch1TabID LIMIT 1        )   )  +  (   SELECT IFNULL(SUM(DocSecondHandPurch2Tabs.PriceCurrency), 0) FROM DocSecondHandPurch2Tabs WHERE DocSecondHandPurchID =        (         SELECT DocSecondHandPurch1Tabs.DocSecondHandPurchID FROM DocSecondHandPurch1Tabs WHERE DocSecondHandPurch1TabID = NEW.DocSecondHandPurch1TabID LIMIT 1        )  )  WHERE   DocSecondHandPurchID = (SELECT DocSecondHandPurch1Tabs.DocSecondHandPurchID FROM DocSecondHandPurch1Tabs WHERE DocSecondHandPurch1TabID = NEW.DocSecondHandPurch1TabID LIMIT 1); END; " +

                "CREATE TRIGGER [TG_DELETE_DocSecondHandPurch1Tabs_DocSecondHandPurches] " +
                "BEFORE DELETE " +
                "ON [DocSecondHandPurch1Tabs] " +

                "BEGIN " +


                " UPDATE DocSecondHandPurches " +
                " SET " +

                "  Sums1 = Sums1 - " +
                "  ( " +
                "    SELECT IFNULL(DocSecondHandPurch1Tabs.PriceCurrency, 0) " +
                "    FROM DocSecondHandPurch1Tabs " +
                "    WHERE DocSecondHandPurch1TabID = OLD.DocSecondHandPurch1TabID LIMIT 1  " +
                "  ),  " +

                "  Sums2 = " +
                "  ( " +
                "   SELECT IFNULL(SUM(DocSecondHandPurch2Tabs.PriceCurrency), 0) " +
                "   FROM DocSecondHandPurch2Tabs " +
                "   WHERE DocSecondHandPurchID = " +
                "   ( " +
                "    SELECT DocSecondHandPurch1Tabs.DocSecondHandPurchID " +
                "    FROM DocSecondHandPurch1Tabs " +
                "    WHERE DocSecondHandPurch1TabID = OLD.DocSecondHandPurch1TabID LIMIT 1 " +
                "   ) " +
                "  ), " +

                "  Sums = " +
                "  Sums1 - " +
                "  ( " +
                "    SELECT IFNULL(DocSecondHandPurch1Tabs.PriceCurrency, 0) " +
                "    FROM DocSecondHandPurch1Tabs " +
                "    WHERE DocSecondHandPurch1TabID = OLD.DocSecondHandPurch1TabID LIMIT 1 " +
                "  ) " +
                "  + " +
                "  ( " +
                "   SELECT IFNULL(SUM(DocSecondHandPurch2Tabs.PriceCurrency), 0) " +
                "   FROM DocSecondHandPurch2Tabs " +
                "   WHERE DocSecondHandPurchID =  " +
                "   ( " +
                "    SELECT DocSecondHandPurch1Tabs.DocSecondHandPurchID  " +
                "    FROM DocSecondHandPurch1Tabs  " +
                "    WHERE DocSecondHandPurch1TabID = OLD.DocSecondHandPurch1TabID LIMIT 1 " +
                "   ) " +
                "  ) " +

                " WHERE    " +
                "  DocSecondHandPurchID =  " +
                "  ( " +
                "   SELECT DocSecondHandPurch1Tabs.DocSecondHandPurchID  " +
                "   FROM DocSecondHandPurch1Tabs  " +
                "   WHERE DocSecondHandPurch1TabID = OLD.DocSecondHandPurch1TabID LIMIT 1 " +
                "  );  " +

                "END; ";


            await Task.Run(() => db.Database.ExecuteSqlCommandAsync(SQL));

            #endregion

            #region 4.2. DocSecondHandPurch2Tabs

            //Создать таблицу "DocSecondHandPurch1Tabs"

            SQL =
                "CREATE TABLE [DocSecondHandPurch2Tabs] ( " +
                "  [DocSecondHandPurch2TabID] INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                "  [DocSecondHandPurchID] INTEGER NOT NULL CONSTRAINT [FK_DocSecondHandPurch1Tabs_DocSecondHandPurchID] REFERENCES [DocSecondHandPurches]([DocSecondHandPurchID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED, " +
                "  [DirEmployeeID] INTEGER NOT NULL CONSTRAINT [FK_DocSecondHandPurch2Tabs_DirEmployeeID] REFERENCES [DirEmployees]([DirEmployeeID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED, " +
                "  [DirNomenID] INTEGER NOT NULL CONSTRAINT [FK_DocSecondHandPurch1Tabs_DirSecondHandJobNomenID] REFERENCES [DirNomens]([DirNomenID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED, " +
                "  [RemPartyID] INTEGER CONSTRAINT [FK_DocSecondHandPurch2Tabs_RemPartyID] REFERENCES [RemParties]([RemPartyID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED, " +
                "  [PriceVAT] REAL NOT NULL, " +
                "  [PriceCurrency] REAL NOT NULL, " +
                "  [DirCurrencyID] INTEGER NOT NULL CONSTRAINT [FK_DocSaleTab_DirCurrencyID] REFERENCES [DirCurrencies]([DirCurrencyID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED, " +
                "  [DirCurrencyRate] REAL NOT NULL DEFAULT 1, " +
                "  [DirCurrencyMultiplicity] INTEGER NOT NULL DEFAULT 1, " +
                "  [TabDate] DATETIME); " +

                "CREATE INDEX [IDX_DocSecondHandPurch2Tabs_DirEmployeeID] ON [DocSecondHandPurch2Tabs] ([DirEmployeeID]); " +

                "CREATE INDEX [IDX_DocSecondHandPurch2Tabs_RemPartyID] ON [DocSecondHandPurch2Tabs] ([RemPartyID]); " +

                "CREATE TRIGGER [TG_INSERT_DocSecondHandPurch2Tabs_DocSecondHandPurches] " +
                "AFTER INSERT " +
                "ON [DocSecondHandPurch2Tabs] " +
                "BEGIN  UPDATE DocSecondHandPurches  SET  Sums1 =   (    SELECT IFNULL(SUM(DocSecondHandPurch1Tabs.PriceCurrency), 0) FROM DocSecondHandPurch1Tabs WHERE DocSecondHandPurchID =        (         SELECT DocSecondHandPurch2Tabs.DocSecondHandPurchID FROM DocSecondHandPurch2Tabs WHERE DocSecondHandPurch2TabID = NEW.DocSecondHandPurch2TabID LIMIT 1        )   ),  Sums2 =  (   SELECT IFNULL(SUM(DocSecondHandPurch2Tabs.PriceCurrency), 0) FROM DocSecondHandPurch2Tabs WHERE DocSecondHandPurchID =        (         SELECT DocSecondHandPurch2Tabs.DocSecondHandPurchID FROM DocSecondHandPurch2Tabs WHERE DocSecondHandPurch2TabID = NEW.DocSecondHandPurch2TabID LIMIT 1        )  ),  Sums =   (    SELECT IFNULL(SUM(DocSecondHandPurch1Tabs.PriceCurrency), 0) FROM DocSecondHandPurch1Tabs WHERE DocSecondHandPurchID =        (         SELECT DocSecondHandPurch2Tabs.DocSecondHandPurchID FROM DocSecondHandPurch2Tabs WHERE DocSecondHandPurch2TabID = NEW.DocSecondHandPurch2TabID LIMIT 1        )   )  +  (   SELECT IFNULL(SUM(DocSecondHandPurch2Tabs.PriceCurrency), 0) FROM DocSecondHandPurch2Tabs WHERE DocSecondHandPurchID =        (         SELECT DocSecondHandPurch2Tabs.DocSecondHandPurchID FROM DocSecondHandPurch2Tabs WHERE DocSecondHandPurch2TabID = NEW.DocSecondHandPurch2TabID LIMIT 1        )  )  WHERE   DocSecondHandPurchID = (SELECT DocSecondHandPurch2Tabs.DocSecondHandPurchID FROM DocSecondHandPurch2Tabs WHERE DocSecondHandPurch2TabID = NEW.DocSecondHandPurch2TabID LIMIT 1); END; " +

                "CREATE TRIGGER [TG_UPDATE_DocSecondHandPurch2Tabs_DocSecondHandPurches] " +
                "AFTER UPDATE " +
                "ON [DocSecondHandPurch2Tabs] " +
                "BEGIN  UPDATE DocSecondHandPurches  SET  Sums1 =   (    SELECT IFNULL(SUM(DocSecondHandPurch1Tabs.PriceCurrency), 0) FROM DocSecondHandPurch1Tabs WHERE DocSecondHandPurchID =        (         SELECT DocSecondHandPurch2Tabs.DocSecondHandPurchID FROM DocSecondHandPurch2Tabs WHERE DocSecondHandPurch2TabID = NEW.DocSecondHandPurch2TabID LIMIT 1        )   ),   Sums2 =  (   SELECT IFNULL(SUM(DocSecondHandPurch2Tabs.PriceCurrency), 0) FROM DocSecondHandPurch2Tabs WHERE DocSecondHandPurchID =        (         SELECT DocSecondHandPurch2Tabs.DocSecondHandPurchID FROM DocSecondHandPurch2Tabs WHERE DocSecondHandPurch2TabID = NEW.DocSecondHandPurch2TabID LIMIT 1        )  ),  Sums =   (    SELECT IFNULL(SUM(DocSecondHandPurch1Tabs.PriceCurrency), 0) FROM DocSecondHandPurch1Tabs WHERE DocSecondHandPurchID =        (         SELECT DocSecondHandPurch2Tabs.DocSecondHandPurchID FROM DocSecondHandPurch2Tabs WHERE DocSecondHandPurch2TabID = NEW.DocSecondHandPurch2TabID LIMIT 1        )   )   +  (   SELECT IFNULL(SUM(DocSecondHandPurch2Tabs.PriceCurrency), 0) FROM DocSecondHandPurch2Tabs WHERE DocSecondHandPurchID =        (         SELECT DocSecondHandPurch2Tabs.DocSecondHandPurchID FROM DocSecondHandPurch2Tabs WHERE DocSecondHandPurch2TabID = NEW.DocSecondHandPurch2TabID LIMIT 1        )  )  WHERE   DocSecondHandPurchID = (SELECT DocSecondHandPurch2Tabs.DocSecondHandPurchID FROM DocSecondHandPurch2Tabs WHERE DocSecondHandPurch2TabID = NEW.DocSecondHandPurch2TabID LIMIT 1); END; " +

                "CREATE TRIGGER [TG_DELETE_DocSecondHandPurch2Tabs_DocSecondHandPurches] " +
                "BEFORE DELETE " +
                "ON [DocSecondHandPurch2Tabs] " +

                "BEGIN " +

                " UPDATE DocSecondHandPurches " +
                " SET " +

                "  Sums1 = " +
                "   ( " +
                "    SELECT IFNULL(SUM([DocSecondHandPurch1Tabs].PriceCurrency), 0) " +
                "    FROM DocSecondHandPurch1Tabs " +
                "    WHERE DocSecondHandPurchID = " +
                "    ( " +
                "     SELECT DocSecondHandPurch2Tabs.DocSecondHandPurchID " +
                "     FROM DocSecondHandPurch2Tabs " +
                "     WHERE DocSecondHandPurch2TabID = OLD.DocSecondHandPurch2TabID LIMIT 1 " +
                "    ) " +
                "   ), " +

                "  Sums2 = " +
                "   Sums2 - " +
                "   ( " +
                "     SELECT IFNULL(DocSecondHandPurch2Tabs.PriceCurrency, 0) " +
                "     FROM DocSecondHandPurch2Tabs " +
                "     WHERE DocSecondHandPurch2TabID = OLD.DocSecondHandPurch2TabID LIMIT 1 " +
                "   ), " +

                "  Sums = " +
                "   ( " +
                "    SELECT IFNULL(SUM(DocSecondHandPurch1Tabs.PriceCurrency), 0) " +
                "    FROM DocSecondHandPurch1Tabs " +
                "    WHERE DocSecondHandPurchID = " +
                "    ( " +
                "     SELECT DocSecondHandPurch2Tabs.DocSecondHandPurchID " +
                "     FROM DocSecondHandPurch2Tabs " +
                "     WHERE DocSecondHandPurch2TabID = OLD.DocSecondHandPurch2TabID LIMIT 1 " +
                "    ) " +
                "   ) " +
                "   + " +
                "   Sums2 - " +
                "   ( " +
                "     SELECT IFNULL(DocSecondHandPurch2Tabs.PriceCurrency, 0) " +
                "     FROM DocSecondHandPurch2Tabs " +
                "     WHERE DocSecondHandPurch2TabID = OLD.DocSecondHandPurch2TabID LIMIT 1 " +
                "   ) " +


                "  WHERE " +
                "   DocSecondHandPurchID = " +
                "   ( " +
                "    SELECT DocSecondHandPurch2Tabs.DocSecondHandPurchID " +
                "    FROM DocSecondHandPurch2Tabs " +
                "    WHERE DocSecondHandPurch2TabID = OLD.DocSecondHandPurch2TabID LIMIT 1 " +
                "   ); " +

                " END;";


            await Task.Run(() => db.Database.ExecuteSqlCommandAsync(SQL));

            #endregion
            
            #region 5. LogSecondHands and DirSecondHandLogTypes

            //Создать таблицу "DirSecondHandLogTypes"
            //Создать таблицу "LogSecondHands"

            SQL =
                "CREATE TABLE [DirSecondHandLogTypes] ( " +
                "  [DirSecondHandLogTypeID] INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                "  [Del] BOOL NOT NULL DEFAULT 0, " +
                "  [DirSecondHandLogTypeName] TEXT(256) NOT NULL); " +

                "INSERT INTO DirSecondHandLogTypes (DirSecondHandLogTypeID, DirSecondHandLogTypeName)values(1, 'Смена статуса'); " +
                "INSERT INTO DirSecondHandLogTypes (DirSecondHandLogTypeID, DirSecondHandLogTypeName)values(2, 'Смена инженера'); " +
                "INSERT INTO DirSecondHandLogTypes (DirSecondHandLogTypeID, DirSecondHandLogTypeName)values(3, 'Комментарии и заметки'); " +
                "INSERT INTO DirSecondHandLogTypes (DirSecondHandLogTypeID, DirSecondHandLogTypeName)values(4, 'Отправка СМС'); " +
                "INSERT INTO DirSecondHandLogTypes (DirSecondHandLogTypeID, DirSecondHandLogTypeName)values(5, 'Выполненная работа'); " +
                "INSERT INTO DirSecondHandLogTypes (DirSecondHandLogTypeID, DirSecondHandLogTypeName)values(6, 'Запчасть'); " +
                "INSERT INTO DirSecondHandLogTypes (DirSecondHandLogTypeID, DirSecondHandLogTypeName)values(7, 'Другое'); " +
                "INSERT INTO DirSecondHandLogTypes (DirSecondHandLogTypeID, DirSecondHandLogTypeName)values(8, 'Смена гарантии'); " +
                "INSERT INTO DirSecondHandLogTypes (DirSecondHandLogTypeID, DirSecondHandLogTypeName)values(9, 'Возврат по гарантии'); ";

            await Task.Run(() => db.Database.ExecuteSqlCommandAsync(SQL));


            SQL = 
                "CREATE TABLE [LogSecondHands] ( " +
                "  [LogSecondHandID] INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,  " +
                "  [DocSecondHandPurchID] INTEGER NOT NULL CONSTRAINT [FK_LogSecondHands_DocServicePurchID] REFERENCES [DocSecondHandPurches]([DocSecondHandPurchID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED,  " +
                "  [DirSecondHandLogTypeID] INTEGER NOT NULL CONSTRAINT [FK_LogSecondHands_DirServiceLogTypeID] REFERENCES [DirSecondHandLogTypes]([DirSecondHandLogTypeID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED,  " +
                "  [DirEmployeeID] INTEGER NOT NULL CONSTRAINT [FK_LogSecondHands_DirEmployees] REFERENCES [DirEmployees]([DirEmployeeID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED,  " +
                "  [DirSecondHandStatusID] INTEGER CONSTRAINT [FK_LogSecondHands_DirSecondHandStatusID] REFERENCES [DirSecondHandStatuses]([DirSecondHandStatusID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED,  " +
                "  [LogSecondHandDate] DATETIME DEFAULT (datetime(CURRENT_TIMESTAMP, 'localtime')),  " +
                "  [Msg] TEXT(1024)); " +

                "CREATE INDEX [IDX_LogSecondHands_DirEmployeeID] ON [LogSecondHands] ([DirEmployeeID]); " +

                "CREATE INDEX [IDX_LogSecondHands_DirSecondHandStatusID] ON [LogSecondHands] ([DirSecondHandStatusID]); " +

                "CREATE INDEX [IDX_LogSecondHands_DirServiceLogTypeID] ON [LogSecondHands] ([DirSecondHandLogTypeID]); " +

                "CREATE INDEX [IDX_LogSecondHands_DocServicePurchID] ON [LogSecondHands] ([DocSecondHandPurchID]);";

            await Task.Run(() => db.Database.ExecuteSqlCommandAsync(SQL));


            #endregion

            #region 6. LogSecondHands, DirSecondHandLogTypes and ListObjects

            //Создать таблицу "DocSecondHandRetails"
            //Создать таблицу "DocSecondHandRetailTabs"
            //Создать запсь в "ListObjects" (66, 3, "DocSecondHandRetails", "Документ Б/У Розница", ...)

            SQL =
                "CREATE TABLE [DocSecondHandRetails] ( " +
                "  [DocSecondHandRetailID] INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,  " +
                "  [DocID] INTEGER NOT NULL CONSTRAINT [FK_DocSecondHandRetails_DocID] REFERENCES [Docs]([DocID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED,  " +
                "  [DirWarehouseID] INTEGER NOT NULL CONSTRAINT [FK_DocSecondHandRetails_DirWarehouseID] REFERENCES [DirWarehouses]([DirWarehouseID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED,  " +
                "  [Reserve] BOOL NOT NULL DEFAULT 1,  " +
                "  [OnCredit] BOOL NOT NULL DEFAULT 0); " +
                "CREATE INDEX [IDX_DocSecondHandRetails_DirWarehouseID] ON [DocSecondHandRetails] ([DirWarehouseID]); " +
                "CREATE INDEX [IDX_DocSecondHandRetails_DocID] ON [DocSecondHandRetails] ([DocID]); " +
                "CREATE INDEX [IDX_DocSecondHandRetails_OnCredit] ON [DocSecondHandRetails] ([OnCredit]); " +

                "CREATE TABLE [DocSecondHandRetailTabs] ( " +
                "  [DocSecondHandRetailTabID] INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,  " +
                "  [DocSecondHandRetailID] INTEGER NOT NULL CONSTRAINT [FK_DocSecondHandRetailTab_DocSecondHandRetailsID] REFERENCES [DocSecondHandRetails]([DocSecondHandRetailID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED,  " +
                "  [DirServiceNomenID] INTEGER NOT NULL CONSTRAINT [FK_DocSecondHandRetailTab_DirServiceNomenID] REFERENCES [DirServiceNomens]([DirServiceNomenID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED,  " +
                "  [Rem2PartyID] INTEGER CONSTRAINT [FK_DocSecondHandRetailTabs_Rem2PartyID] REFERENCES [Rem2Parties]([Rem2PartyID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED,  " +
                "  [Quantity] REAL NOT NULL,  " +
                "  [DirPriceTypeID] INTEGER NOT NULL CONSTRAINT [FK_DocSecondHandRetailTabs_DirPriceTypeID] REFERENCES [DirPriceTypes]([DirPriceTypeID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED,  " +
                "  [PriceVAT] REAL NOT NULL,  " +
                "  [PriceCurrency] REAL NOT NULL,  " +
                "  [DirCurrencyID] INTEGER NOT NULL CONSTRAINT [FK_DocSecondHandRetailTab_DirCurrencyID] REFERENCES [DirCurrencies]([DirCurrencyID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED,  " +
                "  [DirCurrencyRate] REAL NOT NULL DEFAULT 1,  " +
                "  [DirCurrencyMultiplicity] INTEGER NOT NULL DEFAULT 1,  " +
                "  [DirReturnTypeID] INTEGER,  " +
                "  [DirDescriptionID] INTEGER " +
                "); " +
                "CREATE INDEX [IDX_DocSecondHandRetailTabs_DirServiceNomenID] ON [DocSecondHandRetailTabs] ([DirServiceNomenID]); " +
                "CREATE INDEX [IDX_DocSecondHandRetailTabs_DocSecondHandRetailID] ON [DocSecondHandRetailTabs] ([DocSecondHandRetailID]); " +
                "CREATE INDEX [IDX_DocSecondHandRetailTabs_Rem2PartyID] ON [DocSecondHandRetailTabs] ([Rem2PartyID]); " +

                "INSERT INTO ListObjects (ListObjectID, ListObjectTypeID, ListObjectNameSys, ListObjectNameRu)values(66, 3, 'DocSecondHandRetails', 'Документ Б/У Розница'); ";

            await Task.Run(() => db.Database.ExecuteSqlCommandAsync(SQL));



            //Создать таблицу "DocSecondHandRetailReturns"
            //Создать таблицу "DocSecondHandRetailReturnTabs"
            //Создать запсь в "ListObjects" (67, 3, "DocSecondHandRetailReturns", "Документ Б/У Возврат", ...)

            SQL =
                "CREATE TABLE [DocSecondHandRetailReturns] ( " +
                "  [DocSecondHandRetailReturnID] INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT CONSTRAINT [FK_DocSecondHandRetailReturns_DocID] REFERENCES [DocSecondHandRetailReturns]([DocSecondHandRetailReturnID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED,  " +
                "  [DocID] INTEGER NOT NULL,  " +
                "  [DocSecondHandRetailID] INTEGER CONSTRAINT [FK_DocSecondHandRetailReturns_DocSecondHandRetailID] REFERENCES [DocSecondHandRetails]([DocSecondHandRetailID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED,  " +
                "  [DirWarehouseID] INTEGER NOT NULL CONSTRAINT [FK_DocSecondHandRetailReturns_DirWarehouseID] REFERENCES [DirWarehouses]([DirWarehouseID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED,  " +
                "  [Reserve] BOOL NOT NULL DEFAULT 1,  " +
                "  [OnCredit] BOOL NOT NULL DEFAULT 0); " +
                "CREATE INDEX [IDX_DocSecondHandRetailReturns_DirWarehouseID] ON [DocSecondHandRetailReturns] ([DirWarehouseID]); " +
                "CREATE INDEX [IDX_DocSecondHandRetailReturns_DocID] ON [DocSecondHandRetailReturns] ([DocID]); " +
                "CREATE INDEX [IDX_DocSecondHandRetailReturns_OnCredit] ON [DocSecondHandRetailReturns] ([OnCredit]); " +

                "CREATE TABLE [DocSecondHandRetailReturnTabs] ( " +
                "  [DocSecondHandRetailReturnTabID] INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,  " +
                "  [DocSecondHandRetailReturnID] INTEGER NOT NULL CONSTRAINT [FK_DocSecondHandRetailReturnTabs_DocSecondHandRetailReturnID] REFERENCES [DocSecondHandRetailReturns]([DocSecondHandRetailReturnID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED,  " +
                "  [DirServiceNomenID] INTEGER NOT NULL CONSTRAINT [FK_DocSecondHandRetailReturnTabs_DirServiceNomenID] REFERENCES [DirServiceNomens]([DirServiceNomenID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED,  " +
                "  [Rem2PartyMinusID] INTEGER CONSTRAINT [FK_DocSecondHandRetailReturnTabs_Rem2PartyMinusID] REFERENCES [Rem2PartyMinuses]([Rem2PartyMinusID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED,  " +
                "  [Quantity] REAL NOT NULL,  " +
                "  [PriceVAT] REAL NOT NULL,  " +
                "  [PriceCurrency] REAL NOT NULL,  " +
                "  [DirCurrencyID] INTEGER NOT NULL CONSTRAINT [FK_DocSecondHandRetailReturnTabs_DirCurrencyID] REFERENCES [DirCurrencies]([DirCurrencyID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED,  " +
                "  [DirCurrencyRate] REAL NOT NULL,  " +
                "  [DirCurrencyMultiplicity] INTEGER NOT NULL,  " +
                "  [DirReturnTypeID] INTEGER CONSTRAINT [FK_DocSecondHandRetailReturnTabs_DirReturnTypeID] REFERENCES [DirPriceTypes]([DirPriceTypeID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED,  " +
                "  [DirDescriptionID] INTEGER CONSTRAINT [FK_DocSecondHandRetailReturnTabs_DirDescriptionID] REFERENCES [DirDescriptions]([DirDescriptionID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED); " +
                "CREATE INDEX [IDX_DocSecondHandRetailReturnTabs_DirServiceNomenID] ON [DocSecondHandRetailReturnTabs] ([DirServiceNomenID]); " +
                "CREATE INDEX [IDX_DocSecondHandRetailReturnTabs_DocSecondHandRetailReturnID] ON [DocSecondHandRetailReturnTabs] ([DocSecondHandRetailReturnID]); " +
                "CREATE INDEX [IDX_DocSecondHandRetailReturnTabs_Rem2PartyMinusID] ON [DocSecondHandRetailReturnTabs] ([Rem2PartyMinusID]); " +

                "INSERT INTO ListObjects (ListObjectID, ListObjectTypeID, ListObjectNameSys, ListObjectNameRu)values(67, 3, 'DocSecondHandRetailReturns', 'Документ Б/У Возврат'); ";

            await Task.Run(() => db.Database.ExecuteSqlCommandAsync(SQL));



            //Создать таблицу "DocSecondHandRetailActWriteOffs"
            //Создать таблицу "DocSecondHandRetailActWriteOffTabs"
            //Создать запсь в "ListObjects" (68, 3, "DocSecondHandRetailActWriteOffs", "Документ Б/У Списание", ...)

            SQL =
                "CREATE TABLE [DocSecondHandRetailActWriteOffs] ( " +
                "  [DocSecondHandRetailActWriteOffID] INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,  " +
                "  [DocID] INTEGER NOT NULL CONSTRAINT [FK_DocSecondHandRetailActWriteOffs_DocID] REFERENCES [Docs]([DocID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED,  " +
                "  [DirWarehouseID] INTEGER NOT NULL CONSTRAINT [FK_DocSecondHandRetailActWriteOffs_DirWarehouseID] REFERENCES [DirWarehouses]([DirWarehouseID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED,  " +
                "  [Reserve] BOOL NOT NULL DEFAULT 1 " +
                "); " +
                "CREATE INDEX [IDX_DocSecondHandRetailActWriteOffs_DirWarehouseID] ON [DocSecondHandRetailActWriteOffs] ([DirWarehouseID]); " +
                "CREATE INDEX [IDX_DocSecondHandRetailActWriteOffs_DocID] ON [DocSecondHandRetailActWriteOffs] ([DocID]); " +

                "CREATE TABLE [DocSecondHandRetailActWriteOffTabs] ( " +
                "  [DocSecondHandRetailActWriteOffTabID] INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,  " +
                "  [DocSecondHandRetailActWriteOffID] INTEGER NOT NULL CONSTRAINT [FK_DocSecondHandRetailActWriteOffTab_DocSecondHandRetailActWriteOffsID] REFERENCES [DocSecondHandRetailActWriteOffs]([DocSecondHandRetailActWriteOffID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED,  " +
                "  [DirServiceNomenID] INTEGER NOT NULL CONSTRAINT [FK_DocSecondHandRetailActWriteOffTab_DirServiceNomenID] REFERENCES [DirServiceNomens]([DirServiceNomenID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED,  " +
                "  [Rem2PartyID] INTEGER CONSTRAINT [FK_DocSecondHandRetailActWriteOffTabs_Rem2PartyID] REFERENCES [Rem2Parties]([Rem2PartyID]) ON DELETE RESTRICT ON UPDATE RESTRICT MATCH FULL DEFERRABLE INITIALLY DEFERRED,  " +
                "  [Quantity] REAL NOT NULL,  " +
                "  [DirPriceTypeID] INTEGER NOT NULL CONSTRAINT [FK_DocSecondHandRetailActWriteOffTabs_DirPriceTypeID] REFERENCES [DirPriceTypes]([DirPriceTypeID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED,  " +
                "  [PriceVAT] REAL NOT NULL,  " +
                "  [PriceCurrency] REAL NOT NULL,  " +
                "  [DirCurrencyID] INTEGER NOT NULL CONSTRAINT [FK_DocSecondHandRetailActWriteOffTab_DirCurrencyID] REFERENCES [DirCurrencies]([DirCurrencyID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED,  " +
                "  [DirCurrencyRate] REAL NOT NULL DEFAULT 1,  " +
                "  [DirCurrencyMultiplicity] INTEGER NOT NULL DEFAULT 1,  " +
                "  [DirReturnTypeID] INTEGER,  " +
                "  [DirDescriptionID] INTEGER); " +
                "CREATE INDEX [IDX_DocSecondHandRetailActWriteOffTabs_DirServiceNomenID] ON [DocSecondHandRetailActWriteOffTabs] ([DirServiceNomenID]); " +
                "CREATE INDEX [IDX_DocSecondHandRetailActWriteOffTabs_DocSecondHandRetailActWriteOffID] ON [DocSecondHandRetailActWriteOffTabs] ([DocSecondHandRetailActWriteOffID]); " +
                "CREATE INDEX [IDX_DocSecondHandRetailActWriteOffTabs_Rem2PartyID] ON [DocSecondHandRetailActWriteOffTabs] ([Rem2PartyID]); " +

                "INSERT INTO ListObjects (ListObjectID, ListObjectTypeID, ListObjectNameSys, ListObjectNameRu)values(68, 3, 'DocSecondHandRetailActWriteOffs', 'Документ Б/У Списание'); ";

            await Task.Run(() => db.Database.ExecuteSqlCommandAsync(SQL));


            #endregion

            #region 7. DirCashOfficeSumTypes and DirBankSumTypes

            //Создать запсь в "DirCashOfficeSumTypes": 20, 21, 22, 23
            SQL =
                "INSERT INTO DirCashOfficeSumTypes (DirCashOfficeSumTypeID, DirCashOfficeSumTypeName, Sign)values(21, 'Покупка Б/У №', -1); " +
                "INSERT INTO DirCashOfficeSumTypes (DirCashOfficeSumTypeID, DirCashOfficeSumTypeName, Sign)values(22, 'Отмены покупки Б/У №', 1); " +

                "INSERT INTO DirCashOfficeSumTypes (DirCashOfficeSumTypeID, DirCashOfficeSumTypeName, Sign)values(23, 'Магазин продажа Б/У №', 1); " +
                "INSERT INTO DirCashOfficeSumTypes (DirCashOfficeSumTypeID, DirCashOfficeSumTypeName, Sign)values(24, 'Отмены проведения продажи Б/У в магазине №', -1); " +
                "INSERT INTO DirCashOfficeSumTypes (DirCashOfficeSumTypeID, DirCashOfficeSumTypeName, Sign)values(25, 'Магазин возврат Б/У №', -1); " +
                "INSERT INTO DirCashOfficeSumTypes (DirCashOfficeSumTypeID, DirCashOfficeSumTypeName, Sign)values(26, 'Отмены проведения возврата Б/У в магазине №', 1); ";

            await Task.Run(() => db.Database.ExecuteSqlCommandAsync(SQL));

            //Создать запсь в "DirBankSumTypes": 19, 20, 21, 22
            SQL =
                "INSERT INTO DirBankSumTypes (DirBankSumTypeID, DirBankSumTypeName, Sign)values(19, 'Покупка Б/У №', -1); " +
                "INSERT INTO DirBankSumTypes (DirBankSumTypeID, DirBankSumTypeName, Sign)values(20, 'Отмены покупки Б/У №', 1); " +

                "INSERT INTO DirBankSumTypes (DirBankSumTypeID, DirBankSumTypeName, Sign)values(21, 'Магазин продажа Б/У №', 1); " +
                "INSERT INTO DirBankSumTypes (DirBankSumTypeID, DirBankSumTypeName, Sign)values(22, 'Отмены проведения продажи в магазине №', -1); " +
                "INSERT INTO DirBankSumTypes (DirBankSumTypeID, DirBankSumTypeName, Sign)values(23, 'Магазин возврат Б/У №', -1); " +
                "INSERT INTO DirBankSumTypes (DirBankSumTypeID, DirBankSumTypeName, Sign)values(24, 'Отмены проведения возврата Б/У в магазине №', 1); ";

            await Task.Run(() => db.Database.ExecuteSqlCommandAsync(SQL));

            #endregion

            #region 8. Rem2Parties and Rem2PartyMinuses

            SQL =
                "CREATE TABLE [Rem2Parties] ( " +
                "  [Rem2PartyID] INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,  " +
                "  [DocID] INTEGER NOT NULL CONSTRAINT [FK_Rem2Parties_DocID] REFERENCES [Docs]([DocID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED,  " +
                "  [DocDatePurches] DATE,  " +
                "  [DirContractorIDOrg] INTEGER NOT NULL CONSTRAINT [FK_Rem2Parties_DirContractorIDOrg] REFERENCES [DirContractors]([DirContractorID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED,  " +
                "  [DirServiceContractorID] INTEGER NOT NULL CONSTRAINT [FK_Rem2Parties_DirServiceContractorID] REFERENCES [DirServiceContractors]([DirServiceContractorID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED,  " +
                "  [DirWarehouseID] INTEGER NOT NULL CONSTRAINT [FK_Rem2Parties_DirWarehouseID] REFERENCES [DirWarehouses]([DirWarehouseID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED,  " +
                "  [DirWarehouseIDDebit] INTEGER NOT NULL CONSTRAINT [FK_Rem2Parties_DirWarehouseIDDebit] REFERENCES [DirWarehouses]([DirWarehouseID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED,  " +
                "  [DirWarehouseIDPurch] INTEGER NOT NULL CONSTRAINT [FK_Rem2Parties_DirWarehouseIDPurch] REFERENCES [DirWarehouses]([DirWarehouseID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED,  " +
                "  [DirServiceNomenID] INTEGER NOT NULL CONSTRAINT [FK_Rem2Parties_DirServiceNomenID] REFERENCES [DirServiceNomens]([DirServiceNomenID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED, " +
                "  [DirCharColourID] INTEGER CONSTRAINT [FK_Rem2Parties_DirCharColourID] REFERENCES [DirCharColours]([DirCharColourID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED,  " +
                "  [DirCharMaterialID] INTEGER CONSTRAINT [FK_Rem2Parties_DirCharMaterialID] REFERENCES [DirCharMaterials]([DirCharMaterialID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED,  " +
                "  [DirCharNameID] INTEGER CONSTRAINT [FK_Rem2Parties_DirCharNameID] REFERENCES [DirCharNames]([DirCharNameID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED,  " +
                "  [DirCharSeasonID] INTEGER CONSTRAINT [FK_Rem2Parties_DirCharSeasonID] REFERENCES [DirCharSeasons]([DirCharSeasonID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED,  " +
                "  [DirCharSexID] INTEGER CONSTRAINT [FK_Rem2Parties_DirCharSexID] REFERENCES [DirCharSexes]([DirCharSexID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED,  " +
                "  [DirCharSizeID] INTEGER CONSTRAINT [FK_Rem2Parties_DirCharSizeID] REFERENCES [DirCharSizes]([DirCharSizeID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED,  " +
                "  [DirCharStyleID] INTEGER CONSTRAINT [FK_Rem2Parties_DirCharStyleID] REFERENCES [DirCharStyles]([DirCharStyleID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED,  " +
                "  [DirCharTextureID] INTEGER CONSTRAINT [FK_Rem2Parties_DirCharTextureID] REFERENCES [DirCharTextures]([DirCharTextureID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED,  " +
                "  [SerialNumber] TEXT(256),  " +
                "  [Barcode] TEXT(256),  " +
                "  [Quantity] REAL NOT NULL,  " +
                "  [Remnant] REAL NOT NULL,  " +
                "  [PriceVAT] REAL NOT NULL,  " +
                "  [DirVatValue] REAL NOT NULL,  " +
                "  [PriceCurrency] REAL NOT NULL,  " +
                "  [DirCurrencyID] INTEGER NOT NULL CONSTRAINT [FK_Rem2Parties_DirCurrencyID] REFERENCES [DirCurrencies]([DirCurrencyID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED,  " +
                "  [PriceRetailVAT] REAL NOT NULL DEFAULT 0,  " +
                "  [PriceRetailCurrency] REAL NOT NULL DEFAULT 0,  " +
                "  [PriceWholesaleVAT] REAL NOT NULL DEFAULT 0,  " +
                "  [PriceWholesaleCurrency] REAL NOT NULL DEFAULT 0,  " +
                "  [PriceIMVAT] REAL NOT NULL DEFAULT 0,  " +
                "  [PriceIMCurrency] REAL NOT NULL DEFAULT 0,  " +
                "  [FieldID] INTEGER NOT NULL,  " +
                "  [Rem2PartyMinusID] INTEGER,  " +
                "  [DirNomenMinimumBalance] REAL DEFAULT 0,  " +
                "  [DirReturnTypeID] INTEGER CONSTRAINT [FK_Rem2Parties_DirReturnTypeID] REFERENCES [DirReturnTypes]([DirReturnTypeID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED,  " +
                "  [DirDescriptionID] INTEGER CONSTRAINT [FK_Rem2Parties_DirDescriptionID] REFERENCES [DirDescriptions]([DirDescriptionID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED,  " +
                "  [DirEmployeeID] INTEGER CONSTRAINT [FK_Rem2Parties_DirEmployeeID] REFERENCES [DirEmployees]([DirEmployeeID]) ON DELETE RESTRICT ON UPDATE RESTRICT MATCH FULL DEFERRABLE INITIALLY DEFERRED,  " +
                "  [DocDate] DATE); " +

                "CREATE INDEX [IDX_Rem2Parties_Barcode] ON [Rem2Parties] ([Barcode]); " +
                "CREATE INDEX [IDX_Rem2Parties_DirServiceContractorID] ON [Rem2Parties] ([DirServiceContractorID]); " +
                "CREATE INDEX [IDX_Rem2Parties_DirContractorIDOrg] ON [Rem2Parties] ([DirContractorIDOrg]); " +
                "CREATE INDEX [IDX_Rem2Parties_DirContractorIDOrg_DirWarehouseID_DirServiceNomenID] ON [Rem2Parties] ([DirContractorIDOrg], [DirWarehouseID], [DirServiceNomenID]); " +
                "CREATE INDEX [IDX_Rem2Parties_DirContractorIDOrg_DirWarehouseID_DirServiceNomenID_DirServiceContractorID] ON [Rem2Parties] ([DirContractorIDOrg], [DirWarehouseID], [DirServiceNomenID], [DirServiceContractorID]); " +
                "CREATE INDEX [IDX_Rem2Parties_DirContractorIDOrg_DirWarehouseID_DirServiceNomenID_Remnant] ON [Rem2Parties] ([DirContractorIDOrg], [DirWarehouseID], [DirServiceNomenID], [Remnant]); " +
                "CREATE INDEX [IDX_Rem2Parties_DirServiceNomenID] ON [Rem2Parties] ([DirServiceNomenID]); " +
                "CREATE INDEX [IDX_Rem2Parties_DirNomenMinimumBalance] ON [Rem2Parties] ([DirNomenMinimumBalance]); " +
                "CREATE INDEX [IDX_Rem2Parties_DirWarehouseID] ON [Rem2Parties] ([DirWarehouseID]); " +
                "CREATE INDEX [IDX_Rem2Parties_DirWarehouseIDDebit] ON [Rem2Parties] ([DirWarehouseIDDebit]); " +
                "CREATE INDEX [IDX_Rem2Parties_DirWarehouseIDPurch] ON [Rem2Parties] ([DirWarehouseIDPurch]); " +
                "CREATE INDEX [IDX_Rem2Parties_DocID] ON [Rem2Parties] ([DocID]); " +
                "CREATE INDEX [IDX_Rem2Parties_FieldID] ON [Rem2Parties] ([FieldID]); " +
                "CREATE INDEX [IDX_Rem2Parties_Remnant] ON [Rem2Parties] ([Remnant] ASC); " +
                "CREATE INDEX [IDX_Rem2Parties_Remnant_DirContractorIDOrg] ON [Rem2Parties] ([Remnant], [DirContractorIDOrg]); " +
                "CREATE INDEX [IDX_Rem2Parties_Rem2PartyMinusID] ON [Rem2Parties] ([Rem2PartyMinusID]); " +
                "CREATE INDEX [IDX_Rem2Parties_SerialNumber] ON [Rem2Parties] ([SerialNumber]); " +





                "CREATE TABLE [Rem2PartyMinuses] ( " +
                "  [Rem2PartyMinusID] INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,  " +
                "  [Rem2PartyID] INTEGER NOT NULL CONSTRAINT [FK_Rem2Parties_Rem2PartyID] REFERENCES [Rem2Parties]([Rem2PartyID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED,  " +
                "  [DocID] INTEGER NOT NULL CONSTRAINT [FK_Rem2Parties_DocID] REFERENCES [Docs]([DocID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED,  " +
                "  [DirServiceNomenID] INTEGER NOT NULL CONSTRAINT [FK_Rem2Parties_DirServiceNomenID] REFERENCES [DirServiceNomens]([DirServiceNomenID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED,  " +
                "  [DirWarehouseID] INTEGER NOT NULL CONSTRAINT [FK_Rem2Parties_DirWarehouseID] REFERENCES [DirWarehouses]([DirWarehouseID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED,  " +
                "  [DirContractorIDOrg] INTEGER NOT NULL CONSTRAINT [FK_Rem2PartyMinuses_DirContractorIDOrg] REFERENCES [DirContractors]([DirContractorID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED,  " +
                "  [DirServiceContractorID] INTEGER NOT NULL CONSTRAINT [FK_Rem2Parties_DirServiceContractorID] REFERENCES [DirServiceContractors]([DirServiceContractorID]) ON DELETE RESTRICT ON UPDATE RESTRICT MATCH FULL DEFERRABLE INITIALLY DEFERRED,  " +
                "  [Quantity] REAL NOT NULL,  " +
                "  [PriceVAT] REAL NOT NULL,  " +
                "  [DirVatValue] REAL NOT NULL,  " +
                "  [PriceCurrency] REAL NOT NULL,  " +
                "  [DirCurrencyID] INTEGER NOT NULL CONSTRAINT [FK_Rem2Parties_DirCurrencyID] REFERENCES [DirCurrencies]([DirCurrencyID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED,  " +
                "  [DirCurrencyRate] REAL NOT NULL,  " +
                "  [DirCurrencyMultiplicity] INTEGER NOT NULL,  " +
                "  [Reserve] BOOL NOT NULL DEFAULT 0,  " +
                "  [FieldID] INTEGER NOT NULL,  " +
                "  [DirEmployeeID] INTEGER CONSTRAINT [FK_Rem2PartyMinuses_DirEmployeeID] REFERENCES [DirEmployees]([DirEmployeeID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED,  " +
                "  [DocDate] DATE); " +

                "CREATE INDEX [IDX_Rem2PartyMinuses_DirContractorIDOrg] ON [Rem2PartyMinuses] ([DirContractorIDOrg]); " +
                "CREATE INDEX [IDX_Rem2PartyMinuses_Rem2PartyID] ON [Rem2PartyMinuses] ([Rem2PartyID]); " +
                "CREATE INDEX [IDX_Rem2PartySales_DirServiceContractorID] ON [Rem2PartyMinuses] ([DirServiceContractorID]); " +
                "CREATE INDEX [IDX_Rem2PartySales_DirServiceNomenID] ON [Rem2PartyMinuses] ([DirServiceNomenID]); " +
                "CREATE INDEX [IDX_Rem2PartySales_DirWarehouseID] ON [Rem2PartyMinuses] ([DirWarehouseID]); " +
                "CREATE INDEX [IDX_Rem2PartySales_DocID] ON [Rem2PartyMinuses] ([DocID]); " +
                "CREATE INDEX [IDX_Rem2PartySales_Reserve] ON [Rem2PartyMinuses] ([Reserve]); " +

                "CREATE TRIGGER [TG_DELETE_Rem2PartyMinuses_Rem2Parties] " +
                "BEFORE DELETE " +
                "ON [Rem2PartyMinuses] " +
                "BEGIN " +
                " UPDATE Rem2Parties " +
                " SET Remnant = Remnant + (SELECT Rem2PartyMinuses.Quantity FROM Rem2PartyMinuses WHERE Rem2PartyMinusID = OLD.Rem2PartyMinusID) " +
                " WHERE Rem2PartyID = (SELECT Rem2PartyID FROM Rem2PartyMinuses WHERE Rem2PartyMinuses.Rem2PartyMinusID = OLD.Rem2PartyMinusID); " +
                "END; " +

                "CREATE TRIGGER [TG_INSERT_Rem2PartyMinuses] " +
                "AFTER INSERT " +
                "ON [Rem2PartyMinuses] " +
                "BEGIN " +
                " UPDATE Rem2Parties " +
                " SET Remnant = Remnant - (SELECT Rem2PartyMinuses.Quantity FROM Rem2PartyMinuses WHERE Rem2PartyMinusID = NEW.Rem2PartyMinusID) " +
                " WHERE Rem2PartyID = (SELECT Rem2PartyID FROM Rem2PartyMinuses WHERE Rem2PartyMinuses.Rem2PartyMinusID = NEW.Rem2PartyMinusID); " +
                "END;";



            await Task.Run(() => db.Database.ExecuteSqlCommandAsync(SQL));

            #endregion



            //n.
            Models.Sklad.Sys.SysVer sysVer = await Task.Run(() => db.SysVers.FindAsync(1));
            sysVer.SysVerNumber = 12;
            sysVer.SysVerDate = DateTime.Now;

            db.Entry(sysVer).State = System.Data.Entity.EntityState.Modified;
            await Task.Run(() => db.SaveChangesAsync());

            return true;
        }

        #endregion


        #region Update13

        private async Task<bool> Update13(DbConnectionSklad db)
        {
            #region 1. DirWarehouses.Phone

            string SQL = "ALTER TABLE DirWarehouses ADD COLUMN [Phone] TEXT(128);";

            await Task.Run(() => db.Database.ExecuteSqlCommandAsync(SQL));

            #endregion

            #region 2. DirSmsTemplates.DirSmsTemplateMsg

            //Мало регистраций - исправлю вручную

            //1.Поменять в БД:
            //[[[АппаратИмя]]] => [[[ТоварНаименование]]]
            //[[[НомерПакета]]] => [[[ДокументНомер]]]
            //[[[Товар]]] => [[[ТоварНаименование]]]

            //2. Ваш [[[АппаратИмя]]] готов к выдаче (отремонтирован), стоимость [[[Сумма]]]
            //Ваш [[[ТоварНаименование]]] готов к выдаче (отремонтирован), стоимость [[[Сумма]]]

            //3. Ваш [[[АппаратИмя]]] готов к выдаче (без ремонта)
            //Ваш [[[ТоварНаименование]]] готов к выдаче (без ремонта)

            //4. Забрать пакет № [[[НомерПакета]]] с точки [[[ТочкаОт]]]
            //Забрать пакет № [[[ДокументНомер]]] с точки [[[ТочкаОт]]]

            //5. Ваш товар [[[Товар]]] доставлен на [[[ТочкаНа]]]
            //Ваш товар [[[ТоварНаименование]]] доставлен на [[[ТочкаНа]]]

            //6. Ваш [[[АппаратИмя]]] принят в ремонт, номер ремонта [[[ДокументНомер]]] Мастерская [[[Организация]]] тел: [[[ТочкаТелефон]]]
            //Ваш [[[ТоварНаименование]]] принят в ремонт, номер ремонта [[[ДокументНомер]]] Мастерская [[[Организация]]] тел: [[[ТочкаТелефон]]]

            #endregion



            //n.
            Models.Sklad.Sys.SysVer sysVer = await Task.Run(() => db.SysVers.FindAsync(1));
            sysVer.SysVerNumber = 13;
            sysVer.SysVerDate = DateTime.Now;

            db.Entry(sysVer).State = System.Data.Entity.EntityState.Modified;
            await Task.Run(() => db.SaveChangesAsync());

            return true;
        }

        #endregion


        #region Update14: ПФ "Б/У"

        private async Task<bool> Update14(DbConnectionSklad db)
        {
            #region 1. ListObjectFieldNames: 2-а поля

            string SQL =
                "INSERT INTO ListObjectFieldNames (ListObjectFieldNameID, ListObjectFieldNameReal, ListObjectFieldNameRu, ListObjectFieldNameUa)values(375, 'PassportSeries', 'Документ.ПаспортСерия', 'Документ.ПаспортСерія'); " +
                "INSERT INTO ListObjectFieldNames (ListObjectFieldNameID, ListObjectFieldNameReal, ListObjectFieldNameRu, ListObjectFieldNameUa)values(376, 'PassportNumber', 'Документ.ПаспортНомер', 'Документ.ПаспортНомер'); " +
                "INSERT INTO ListObjectFieldNames (ListObjectFieldNameID, ListObjectFieldNameReal, ListObjectFieldNameRu, ListObjectFieldNameUa)values(377, 'DocSecondHandPurchID', 'Документ.БУ.НомерВнутренний', 'Документ.БЖ.НомерВнутрішній'); ";

            await Task.Run(() => db.Database.ExecuteSqlCommandAsync(SQL));

            #endregion


            #region 2. ListObjectFields: копируем поля из Серв.Центра

            SQL =
                //Переносим всё кроме поля "DocServicePurchID"
                "INSERT INTO ListObjectFields (ListObjectID, ListObjectFieldHeaderShow, ListObjectFieldTabShow, ListObjectFieldFooterShow, ListObjectFieldNameID) " +
                "SELECT 65, lof.ListObjectFieldHeaderShow, lof.ListObjectFieldTabShow, lof.ListObjectFieldFooterShow, lof.ListObjectFieldNameID " +
                "FROM ListObjectFields lof, ListObjectFieldNames lofn " +
                "WHERE " +
                " lof.ListObjectID = 40 and " +
                " lof.ListObjectFieldNameID = lofn.ListObjectFieldNameID and " +
                " lofn.ListObjectFieldNameID <> 342; " +

                //DocSecondHandPurchID
                "INSERT INTO ListObjectFields (ListObjectID, ListObjectFieldHeaderShow, ListObjectFieldTabShow, ListObjectFieldFooterShow, ListObjectFieldNameID)" +
                "values" +
                "(65, 1, 0, 0, 377); " +
                //PassportSeries
                "INSERT INTO ListObjectFields (ListObjectID, ListObjectFieldHeaderShow, ListObjectFieldTabShow, ListObjectFieldFooterShow, ListObjectFieldNameID)" +
                "values" +
                "(65, 1, 0, 0, 375); " +
                //PassportNumber
                "INSERT INTO ListObjectFields (ListObjectID, ListObjectFieldHeaderShow, ListObjectFieldTabShow, ListObjectFieldFooterShow, ListObjectFieldNameID)" +
                "values" +
                "(65, 1, 0, 0, 376); " + 

                //370.SumTotal_InWords
                "INSERT INTO ListObjectFields (ListObjectID, ListObjectFieldHeaderShow, ListObjectFieldTabShow, ListObjectFieldFooterShow, ListObjectFieldNameID)" +
                "values" +
                "(65, 1, 0, 0, 370); ";

            await Task.Run(() => db.Database.ExecuteSqlCommandAsync(SQL));

            #endregion


            //!!! Внимание !!!
            //Создать ПФ - вручную!!!




            //n.
            Models.Sklad.Sys.SysVer sysVer = await Task.Run(() => db.SysVers.FindAsync(1));
            sysVer.SysVerNumber = 14;
            sysVer.SysVerDate = DateTime.Now;

            db.Entry(sysVer).State = System.Data.Entity.EntityState.Modified;
            await Task.Run(() => db.SaveChangesAsync());

            return true;
        }

        #endregion


        #region Update15: ПФ "Б/У"

        private async Task<bool> Update15(DbConnectionSklad db)
        {

            //66 - DocSecondHandRetails
            //67 - DocSecondHandRetailReturns
            //68 - DocSecondHandRetailActWriteOffs


            #region 1. ListObjectFieldNames: 2-а поля

            string SQL =
                //66 - 378
                "INSERT INTO ListObjectFieldNames (ListObjectFieldNameID, ListObjectFieldNameReal, ListObjectFieldNameRu, ListObjectFieldNameUa)values(378, 'DocSecondHandRetailID', 'Документ.БУРозница.НомерВнутренний', 'Документ.БЖРоздріб.НомерВнутрішній'); " +
                //67 - 379;
                "INSERT INTO ListObjectFieldNames (ListObjectFieldNameID, ListObjectFieldNameReal, ListObjectFieldNameRu, ListObjectFieldNameUa)values(379, 'DocSecondHandRetailReturnID', 'Документ.БУРозницаВозврат.НомерВнутренний', 'Документ.БЖРоздрібПовернення.НомерВнутрішній'); " +
                //68 - 380;
                "INSERT INTO ListObjectFieldNames (ListObjectFieldNameID, ListObjectFieldNameReal, ListObjectFieldNameRu, ListObjectFieldNameUa)values(380, 'DocSecondHandRetailActWriteOffID', 'Документ.БУРозницаСписание.НомерВнутренний', 'Документ.БЖРоздрібСписання.НомерВнутрішній'); ";

            await Task.Run(() => db.Database.ExecuteSqlCommandAsync(SQL));

            #endregion


            #region 2. ListObjectFields: копируем поля из Серв.Центра

            SQL =
                //66
                //Переносим всё кроме поля "DocServicePurchID"
                "INSERT INTO ListObjectFields (ListObjectID, ListObjectFieldHeaderShow, ListObjectFieldTabShow, ListObjectFieldFooterShow, ListObjectFieldNameID) " +
                "SELECT 66, lof.ListObjectFieldHeaderShow, lof.ListObjectFieldTabShow, lof.ListObjectFieldFooterShow, lof.ListObjectFieldNameID " +
                "FROM ListObjectFields lof, ListObjectFieldNames lofn " +
                "WHERE " +
                " lof.ListObjectID = 40 and " +
                " lof.ListObjectFieldNameID = lofn.ListObjectFieldNameID and " +
                " lofn.ListObjectFieldNameID <> 342; " +
                //DocSecondHandPurchID
                "INSERT INTO ListObjectFields (ListObjectID, ListObjectFieldHeaderShow, ListObjectFieldTabShow, ListObjectFieldFooterShow, ListObjectFieldNameID)" +
                "values" +
                "(66, 1, 0, 0, 378); " +
                //370.SumTotal_InWords
                "INSERT INTO ListObjectFields (ListObjectID, ListObjectFieldHeaderShow, ListObjectFieldTabShow, ListObjectFieldFooterShow, ListObjectFieldNameID)" +
                "values" +
                "(66, 1, 0, 0, 370); " +


                //67
                //Переносим всё кроме поля "DocServicePurchID"
                "INSERT INTO ListObjectFields (ListObjectID, ListObjectFieldHeaderShow, ListObjectFieldTabShow, ListObjectFieldFooterShow, ListObjectFieldNameID) " +
                "SELECT 67, lof.ListObjectFieldHeaderShow, lof.ListObjectFieldTabShow, lof.ListObjectFieldFooterShow, lof.ListObjectFieldNameID " +
                "FROM ListObjectFields lof, ListObjectFieldNames lofn " +
                "WHERE " +
                " lof.ListObjectID = 40 and " +
                " lof.ListObjectFieldNameID = lofn.ListObjectFieldNameID and " +
                " lofn.ListObjectFieldNameID <> 342; " +
                //DocSecondHandPurchID
                "INSERT INTO ListObjectFields (ListObjectID, ListObjectFieldHeaderShow, ListObjectFieldTabShow, ListObjectFieldFooterShow, ListObjectFieldNameID)" +
                "values" +
                "(67, 1, 0, 0, 379); " +
                //370.SumTotal_InWords
                "INSERT INTO ListObjectFields (ListObjectID, ListObjectFieldHeaderShow, ListObjectFieldTabShow, ListObjectFieldFooterShow, ListObjectFieldNameID)" +
                "values" +
                "(67, 1, 0, 0, 370); " +


                //68
                //Переносим всё кроме поля "DocServicePurchID"
                "INSERT INTO ListObjectFields (ListObjectID, ListObjectFieldHeaderShow, ListObjectFieldTabShow, ListObjectFieldFooterShow, ListObjectFieldNameID) " +
                "SELECT 68, lof.ListObjectFieldHeaderShow, lof.ListObjectFieldTabShow, lof.ListObjectFieldFooterShow, lof.ListObjectFieldNameID " +
                "FROM ListObjectFields lof, ListObjectFieldNames lofn " +
                "WHERE " +
                " lof.ListObjectID = 40 and " +
                " lof.ListObjectFieldNameID = lofn.ListObjectFieldNameID and " +
                " lofn.ListObjectFieldNameID <> 342; " +
                //DocSecondHandPurchID
                "INSERT INTO ListObjectFields (ListObjectID, ListObjectFieldHeaderShow, ListObjectFieldTabShow, ListObjectFieldFooterShow, ListObjectFieldNameID)" +
                "values" +
                "(68, 1, 0, 0, 380); " +
                //370.SumTotal_InWords
                "INSERT INTO ListObjectFields (ListObjectID, ListObjectFieldHeaderShow, ListObjectFieldTabShow, ListObjectFieldFooterShow, ListObjectFieldNameID)" +
                "values" +
                "(68, 1, 0, 0, 370); ";


            await Task.Run(() => db.Database.ExecuteSqlCommandAsync(SQL));

            #endregion


            //!!! Внимание !!!
            //Создать ПФ - вручную!!!
            //2-е ПФ
            //Сергею заменить ПФ на новую!




            //n.
            Models.Sklad.Sys.SysVer sysVer = await Task.Run(() => db.SysVers.FindAsync(1));
            sysVer.SysVerNumber = 15;
            sysVer.SysVerDate = DateTime.Now;

            db.Entry(sysVer).State = System.Data.Entity.EntityState.Modified;
            await Task.Run(() => db.SaveChangesAsync());

            return true;
        }

        #endregion


        #region Update16: ПФ "Б/У"

        private async Task<bool> Update16(DbConnectionSklad db)
        {

            //66 - DocSecondHandRetails
            //67 - DocSecondHandRetailReturns
            //68 - DocSecondHandRetailActWriteOffs


            #region 1. ListObjectFieldNames: 2-а поля

            string SQL =
                //66 - 378
                "INSERT INTO ListObjectFieldNames (ListObjectFieldNameID, ListObjectFieldNameReal, ListObjectFieldNameRu, ListObjectFieldNameUa)values(381, 'Phone', 'Точка.Телефон', 'Точка.Телефон'); ";

            await Task.Run(() => db.Database.ExecuteSqlCommandAsync(SQL));

            #endregion


            #region 2. ListObjectFields: копируем поля из Серв.Центра

            SQL =
                "INSERT INTO ListObjectFields (ListObjectID, ListObjectFieldHeaderShow, ListObjectFieldTabShow, ListObjectFieldFooterShow, ListObjectFieldNameID)" +
                "values" +
                "(65, 1, 0, 0, 381); " +
                "INSERT INTO ListObjectFields (ListObjectID, ListObjectFieldHeaderShow, ListObjectFieldTabShow, ListObjectFieldFooterShow, ListObjectFieldNameID)" +
                "values" +
                "(66, 1, 0, 0, 381); " +
                "INSERT INTO ListObjectFields (ListObjectID, ListObjectFieldHeaderShow, ListObjectFieldTabShow, ListObjectFieldFooterShow, ListObjectFieldNameID)" +
                "values" +
                "(67, 1, 0, 0, 381); " +
                "INSERT INTO ListObjectFields (ListObjectID, ListObjectFieldHeaderShow, ListObjectFieldTabShow, ListObjectFieldFooterShow, ListObjectFieldNameID)" +
                "values" +
                "(68, 1, 0, 0, 381); ";
                

            await Task.Run(() => db.Database.ExecuteSqlCommandAsync(SQL));

            #endregion



            //n.
            Models.Sklad.Sys.SysVer sysVer = await Task.Run(() => db.SysVers.FindAsync(1));
            sysVer.SysVerNumber = 16;
            sysVer.SysVerDate = DateTime.Now;

            db.Entry(sysVer).State = System.Data.Entity.EntityState.Modified;
            await Task.Run(() => db.SaveChangesAsync());

            return true;
        }

        #endregion


        #region Update17: ЗП: DirBonus2ID2, "Б/У"

        private async Task<bool> Update17(DbConnectionSklad db)
        {
            #region 1. ЗП: DirBonus2ID2, "Б/У"

            string SQL =
                //1. Серв.Цент: изменяем существующие записи
                "ALTER TABLE DirEmployees ADD COLUMN [DirBonus2ID] INTEGER CONSTRAINT [FK_DirEmployees_DirBonus2ID] REFERENCES [DirBonuses]([DirBonusID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED; " +
                "UPDATE DirEmployees SET DirBonus2ID=DirBonusID2; " +
                "UPDATE DirEmployees SET DirBonusID2=null; " +

                //3. Документ Зарплата: изменяем существующие записи
                //3.1. 
                "ALTER TABLE DocSalaryTabs ADD COLUMN [DirBonus2ID] INTEGER CONSTRAINT [FK_DocSalaryTabs_DirBonus2ID] REFERENCES [DirBonuses]([DirBonusID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED; " +
                "UPDATE DocSalaryTabs SET DirBonus2ID=DirBonusID2; " +
                "UPDATE DocSalaryTabs SET DirBonusID2=null; " +
                //3.1. 
                "ALTER TABLE DocSalaryTabs ADD COLUMN [DirBonus2IDSalary] REAL NOT NULL DEFAULT 0; " +
                "UPDATE DocSalaryTabs SET DirBonus2IDSalary=DirBonusID2Salary; " +
                "UPDATE DocSalaryTabs SET DirBonusID2Salary=0; " +

                //2. Б/У: добавляем поля
                //2.1. Ремонт
                "ALTER TABLE DirEmployees ADD COLUMN [SalarySecondHandWorkshopCheck] BOOL DEFAULT 0; " +
                "ALTER TABLE DirEmployees ADD COLUMN [DirBonus3ID] INTEGER CONSTRAINT [FK_DirEmployees_DirBonus3ID] REFERENCES [DirBonuses]([DirBonusID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED; " +
                "ALTER TABLE DirEmployees ADD COLUMN [SalaryFixedSecondHandWorkshopOne] REAL DEFAULT 0; " +
                //2.2. Продажа
                "ALTER TABLE DirEmployees ADD COLUMN [DirBonus4ID] INTEGER CONSTRAINT [FK_DirEmployees_DirBonus4ID] REFERENCES [DirBonuses]([DirBonusID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED; " +
                "ALTER TABLE DirEmployees ADD COLUMN [SalaryFixedSecondHandRetailOne] REAL DEFAULT 0; ";

            await Task.Run(() => db.Database.ExecuteSqlCommandAsync(SQL));

            #endregion



            //n.
            Models.Sklad.Sys.SysVer sysVer = await Task.Run(() => db.SysVers.FindAsync(1));
            sysVer.SysVerNumber = 17;
            sysVer.SysVerDate = DateTime.Now;

            db.Entry(sysVer).State = System.Data.Entity.EntityState.Modified;
            await Task.Run(() => db.SaveChangesAsync());

            return true;
        }

        #endregion


        #region Update18: Права: RightDocServiceWorkshopsOnlyUsers, Что бы Мастер видел только свои аппараты

        private async Task<bool> Update18(DbConnectionSklad db)
        {
            #region 1. Права: RightDocServiceWorkshopsOnlyUsers

            string SQL =
                //1. Серв.Цент: изменяем существующие записи
                "ALTER TABLE DirEmployees ADD COLUMN [RightDocServiceWorkshopsOnlyUsers] INTEGER DEFAULT 1; " +
                "ALTER TABLE DirEmployees ADD COLUMN [RightDocServiceWorkshopsOnlyUsersCheck] BOOL DEFAULT 1; " +
                "UPDATE DirEmployees SET RightDocServiceWorkshopsOnlyUsers=0; " +
                "UPDATE DirEmployees SET RightDocServiceWorkshopsOnlyUsersCheck=0; ";
                
            await Task.Run(() => db.Database.ExecuteSqlCommandAsync(SQL));

            #endregion



            //n.
            Models.Sklad.Sys.SysVer sysVer = await Task.Run(() => db.SysVers.FindAsync(1));
            sysVer.SysVerNumber = 18;
            sysVer.SysVerDate = DateTime.Now;

            db.Entry(sysVer).State = System.Data.Entity.EntityState.Modified;
            await Task.Run(() => db.SaveChangesAsync());

            return true;
        }

        #endregion


        #region Update19: Права: Admin, Что бы Мастер видел только свои аппараты

        private async Task<bool> Update19(DbConnectionSklad db)
        {
            #region 1. Права: Admin

            string SQL = "ALTER TABLE DirEmployeeWarehouses ADD COLUMN [IsAdmin] BOOL DEFAULT 0; ";

            await Task.Run(() => db.Database.ExecuteSqlCommandAsync(SQL));

            #endregion


            #region 1. Права: Admin

            SQL =
                "ALTER TABLE DirEmployees ADD COLUMN [RightReportSalaries] INTEGER DEFAULT 3; " +
                "ALTER TABLE DirEmployees ADD COLUMN [RightReportSalariesCheck] BOOL DEFAULT 0; " +
                "UPDATE DirEmployees SET RightReportSalaries=1, RightReportSalariesCheck=1 WHERE DirEmployeeID=1; " +

                "ALTER TABLE DirEmployees ADD COLUMN [RightReportSalariesWarehouses] INTEGER DEFAULT 3; " +
                "ALTER TABLE DirEmployees ADD COLUMN [RightReportSalariesWarehousesCheck] BOOL DEFAULT 0; " +
                "UPDATE DirEmployees SET RightReportSalariesWarehouses=1, RightReportSalariesWarehousesCheck=1 WHERE DirEmployeeID=1; ";

            await Task.Run(() => db.Database.ExecuteSqlCommandAsync(SQL));

            #endregion




            //n.
            Models.Sklad.Sys.SysVer sysVer = await Task.Run(() => db.SysVers.FindAsync(1));
            sysVer.SysVerNumber = 19;
            sysVer.SysVerDate = DateTime.Now;

            db.Entry(sysVer).State = System.Data.Entity.EntityState.Modified;
            await Task.Run(() => db.SaveChangesAsync());

            return true;
        }

        #endregion


        #region Update20: Права: Admin, Что бы Мастер видел только свои аппараты

        private async Task<bool> Update20(DbConnectionSklad db)
        {
            #region 1. Права: Admin

            string SQL =
                "ALTER TABLE DirWarehouses ADD COLUMN [SalaryPercentTrade] REAL NOT NULL DEFAULT 0; " +
                "ALTER TABLE DirWarehouses ADD COLUMN [SalaryPercentService1Tabs] REAL NOT NULL DEFAULT 0; " +
                "ALTER TABLE DirWarehouses ADD COLUMN [SalaryPercentService2Tabs] REAL NOT NULL DEFAULT 0; " +
                "ALTER TABLE DirWarehouses ADD COLUMN [SalaryPercentSecond] REAL NOT NULL DEFAULT 0; ";

            await Task.Run(() => db.Database.ExecuteSqlCommandAsync(SQL));

            #endregion
            



            //n.
            Models.Sklad.Sys.SysVer sysVer = await Task.Run(() => db.SysVers.FindAsync(1));
            sysVer.SysVerNumber = 20;
            sysVer.SysVerDate = DateTime.Now;

            db.Entry(sysVer).State = System.Data.Entity.EntityState.Modified;
            await Task.Run(() => db.SaveChangesAsync());

            return true;
        }

        #endregion


        #region Update21: Права: Admin, Что бы Мастер видел только свои аппараты

        private async Task<bool> Update21(DbConnectionSklad db)
        {
            #region 1. Права: Admin

            string SQL = "UPDATE Rem2Parties SET PriceCurrency=PriceVAT; ";

            await Task.Run(() => db.Database.ExecuteSqlCommandAsync(SQL));

            #endregion




            //n.
            Models.Sklad.Sys.SysVer sysVer = await Task.Run(() => db.SysVers.FindAsync(1));
            sysVer.SysVerNumber = 21;
            sysVer.SysVerDate = DateTime.Now;

            db.Entry(sysVer).State = System.Data.Entity.EntityState.Modified;
            await Task.Run(() => db.SaveChangesAsync());

            return true;
        }

        #endregion


        #region Update22: DocInventoryTabs

        private async Task<bool> Update22(DbConnectionSklad db)
        {
            #region 1. DocInventoryTabs

            string SQL =
                "PRAGMA foreign_keys = OFF; " +

                "DROP INDEX IDX_DocInventoryTabs_DirNomenID; " +
                "DROP INDEX IDX_DocInventoryTabs_DocInventoryID; " +
                "DROP TABLE DocInventoryTabs; " +

                "CREATE TABLE[DocInventoryTabs]( \n" +
                "  [DocInventoryTabID] INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, \n" +
                "  [DocInventoryID] INTEGER NOT NULL CONSTRAINT[FK_DocInventoryTabs_DocInventoryID] REFERENCES[DocInventories]([DocInventoryID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED, \n" +
                "  [DirNomenID] INTEGER NOT NULL CONSTRAINT[FK_DocInventoryTabs_DirNomenID] REFERENCES[DirNomens]([DirNomenID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED, \n" +
                "  [RemPartyID] INTEGER CONSTRAINT[FK_DocInventoryTabs_RemPartyID] REFERENCES[RemParties]([RemPartyID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED, \n" +
                "  [DirCharColourID] INTEGER CONSTRAINT[FK_DocInventoryTabs_DirCharColourID] REFERENCES[DirCharColours]([DirCharColourID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED, \n" +
                "  [DirCharMaterialID] INTEGER CONSTRAINT[FK_DocInventoryTabs_DirCharMaterialID] REFERENCES[DirCharMaterials]([DirCharMaterialID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED, \n" +
                "  [DirCharNameID] INTEGER CONSTRAINT[FK_DocInventoryTabs_DirCharNameID] REFERENCES[DirCharNames]([DirCharNameID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED, \n" +
                "  [DirCharSeasonID] INTEGER CONSTRAINT[FK_DocInventoryTabs_DirCharSeasonID] REFERENCES[DirCharSeasons]([DirCharSeasonID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED, \n" +
                "  [DirCharSexID] INTEGER CONSTRAINT[FK_DocInventoryTabs_DirCharSexID] REFERENCES[DirCharSexes]([DirCharSexID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED, \n" +
                "  [DirCharSizeID] INTEGER CONSTRAINT[FK_DocInventoryTabs_DirCharSizeID] REFERENCES[DirCharSizes]([DirCharSizeID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED, \n" +
                "  [DirCharStyleID] INTEGER CONSTRAINT[FK_DocInventoryTabs_DirCharStyleID] REFERENCES[DirCharStyles]([DirCharStyleID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED, \n" +
                "  [DirCharTextureID] INTEGER CONSTRAINT[FK_DocInventoryTabs_DirCharTextureID] REFERENCES[DirCharTextures]([DirCharTextureID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED, \n" +
                "  [SerialNumber] TEXT(256), \n" +
                "  [Barcode] TEXT(256), \n" +

                "  [DirCurrencyID] INTEGER NOT NULL CONSTRAINT[FK_DocInventoryTabs_DirCurrencyID] REFERENCES[DirCurrencies]([DirCurrencyID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED, \n" +
                "  [DirCurrencyRate] REAL NOT NULL, \n" +
                "  [DirCurrencyMultiplicity] INTEGER NOT NULL, \n" +

                //Списание
                "  [Quantity_WriteOff] REAL NOT NULL, \n" +
                "  [PriceVAT] REAL NOT NULL, \n" +
                "  [PriceCurrency] REAL NOT NULL, \n" +

                "  [DirContractorID] INTEGER CONSTRAINT[FK_DocInventoryTabs_DirContractorID] REFERENCES[DirContractors]([DirContractorID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED, \n" +

                //Приход
                "  [Quantity_Purch] REAL NOT NULL, \n" +
                "  [PriceRetailVAT] REAL NOT NULL, \n" +
                "  [PriceRetailCurrency] REAL NOT NULL, \n" +
                "  [PriceWholesaleVAT] REAL NOT NULL, \n" +
                "  [PriceWholesaleCurrency] REAL NOT NULL, \n" +
                "  [PriceIMVAT] REAL NOT NULL, \n" +
                "  [PriceIMCurrency] REAL NOT NULL, \n" +
                "  [DirNomenMinimumBalance] INTEGER NOT NULL); " + 

                "CREATE INDEX [IDX_DocInventoryTabs_DirNomenID] ON [DocInventoryTabs] ([DirNomenID]); " +
                "CREATE INDEX [IDX_DocInventoryTabs_DocInventoryID] ON [DocInventoryTabs] ([DocInventoryID]); " +

                "PRAGMA foreign_keys = ON; ";

            await Task.Run(() => db.Database.ExecuteSqlCommandAsync(SQL));

            #endregion




            //n.
            Models.Sklad.Sys.SysVer sysVer = await Task.Run(() => db.SysVers.FindAsync(1));
            sysVer.SysVerNumber = 22;
            sysVer.SysVerDate = DateTime.Now;

            db.Entry(sysVer).State = System.Data.Entity.EntityState.Modified;
            await Task.Run(() => db.SaveChangesAsync());

            return true;
        }

        #endregion


        #region Update23: Хоз.расходы (DomesticExpenses)

        private async Task<bool> Update23(DbConnectionSklad db)
        {
            #region 1. DocInventoryTabs

            string SQL =
                //Справочник Хоз.Расчёты
                "CREATE TABLE[DirDomesticExpenses]( \n" +
                "  [DirDomesticExpenseID] INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, \n" +
                "  [Del] BOOL NOT NULL DEFAULT 0, \n" +
                "  [DirDomesticExpenseName] TEXT NOT NULL DEFAULT 256, \n" +
                "  [DirDomesticExpenseType] INTEGER NOT NULL DEFAULT 0, \n" +
                "  [Sign] INTEGER NOT NULL DEFAULT -1); " +
                "INSERT INTO DirDomesticExpenses (DirDomesticExpenseID, DirDomesticExpenseName, DirDomesticExpenseType, Sign)values(1, 'Уборка', 1, -1); " +
                "INSERT INTO DirDomesticExpenses (DirDomesticExpenseID, DirDomesticExpenseName, DirDomesticExpenseType, Sign)values(2, 'Зарплата', 1, -1); " +
                "INSERT INTO DirDomesticExpenses (DirDomesticExpenseID, DirDomesticExpenseName, DirDomesticExpenseType, Sign)values(3, 'Инкасация', 1, -1); " +

                //Документ Хоз.Расчёты
                //1.
                "CREATE TABLE[DocDomesticExpenses]( " +
                "  [DocDomesticExpenseID] INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, \n" +
                "  [DocID] INTEGER NOT NULL CONSTRAINT[FK_DocDomesticExpenses_DocID] REFERENCES[Docs]([DocID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED, \n" +
                "  [DirWarehouseID] INTEGER NOT NULL CONSTRAINT[FK_DocDomesticExpenses_DirWarehouseID] REFERENCES[DirWarehouses]([DirWarehouseID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED); \n" +
                "CREATE INDEX[IDX_DocDomesticExpenses_DirWarehouseID] ON[DocDomesticExpenses] ([DirWarehouseID]); \n" +
                "CREATE INDEX[IDX_DocDomesticExpenses_DocID] ON[DocDomesticExpenses] ([DocID]); \n" +
                //2.
                "CREATE TABLE[DocDomesticExpenseTabs]( \n" +
                "  [DocDomesticExpenseTabID] INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, \n" +
                "  [DocDomesticExpenseID] INTEGER NOT NULL CONSTRAINT[FK_DocDomesticExpenseTab_DocDomesticExpensesID] REFERENCES[DocDomesticExpenses]([DocDomesticExpenseID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED, \n" +
                "  [DirDomesticExpenseID] INTEGER NOT NULL CONSTRAINT[FK_DocDomesticExpenseTab_DirNomenID] REFERENCES[DirDomesticExpenses]([DirDomesticExpenseID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED, \n" +
                "  [PriceVAT] REAL NOT NULL, \n" +
                "  [PriceCurrency] REAL NOT NULL, \n" +
                "  [DirCurrencyID] INTEGER NOT NULL CONSTRAINT[FK_DocDomesticExpenseTab_DirCurrencyID] REFERENCES[DirCurrencies]([DirCurrencyID]) ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED, \n" +
                "  [DirCurrencyRate] REAL NOT NULL DEFAULT 1, \n" +
                "[DirCurrencyMultiplicity] INTEGER NOT NULL DEFAULT 1); \n" +
                "CREATE INDEX[IDX_DocDomesticExpenseTabs_DirDomesticExpenseID] ON[DocDomesticExpenseTabs] ([DirDomesticExpenseID]); " +
                "CREATE INDEX[IDX_DocDomesticExpenseTabs_DocDomesticExpenseID] ON[DocDomesticExpenseTabs] ([DocDomesticExpenseID]); " + 

                //ListObjects
                "INSERT INTO ListObjects (ListObjectID, ListObjectTypeID, ListObjectNameSys, ListObjectNameRu)values(69, 2, 'DirDomesticExpenses', 'Справочник Хоз.расходы'); " +
                "INSERT INTO ListObjects (ListObjectID, ListObjectTypeID, ListObjectNameSys, ListObjectNameRu)values(70, 3, 'DocDomesticExpenses', 'Документ Хоз.расходы'); " +

                //DirCashOfficeSumTypes
                "INSERT INTO DirCashOfficeSumTypes (DirCashOfficeSumTypeID, DirCashOfficeSumTypeName, Sign)values(27, 'Хоз.Расходы №', -1); " +
                "INSERT INTO DirCashOfficeSumTypes (DirCashOfficeSumTypeID, DirCashOfficeSumTypeName, Sign)values(28, 'Отмены Хоз.Расходы №', 1); " +

                //DirBankSumTypes
                "INSERT INTO DirBankSumTypes (DirBankSumTypeID, DirBankSumTypeName, Sign)values(25, 'Хоз.Расходы №', -1); " +
                "INSERT INTO DirBankSumTypes (DirBankSumTypeID, DirBankSumTypeName, Sign)values(26, 'Отмены Хоз.Расходы №', 1); ";

            await Task.Run(() => db.Database.ExecuteSqlCommandAsync(SQL));

            #endregion




            //n.
            Models.Sklad.Sys.SysVer sysVer = await Task.Run(() => db.SysVers.FindAsync(1));
            sysVer.SysVerNumber = 23;
            sysVer.SysVerDate = DateTime.Now;

            db.Entry(sysVer).State = System.Data.Entity.EntityState.Modified;
            await Task.Run(() => db.SaveChangesAsync());

            return true;
        }

        #endregion


        #region Update24: Права: Хоз.Расчеты

        private async Task<bool> Update24(DbConnectionSklad db)
        {
            #region 1. Права: Хоз.Расчеты

            string SQL =
                "ALTER TABLE DirEmployees ADD COLUMN [RightDirDomesticExpenses] INTEGER DEFAULT 3; " +
                "ALTER TABLE DirEmployees ADD COLUMN [RightDirDomesticExpensesCheck] BOOL DEFAULT 0; " +
                "UPDATE DirEmployees SET RightDirDomesticExpenses=1, RightDirDomesticExpensesCheck=1 WHERE DirEmployeeID=1; " +

                "ALTER TABLE DirEmployees ADD COLUMN [RightDocDomesticExpenses] INTEGER DEFAULT 3; " +
                "ALTER TABLE DirEmployees ADD COLUMN [RightDocDomesticExpensesCheck] BOOL DEFAULT 0; " +
                "UPDATE DirEmployees SET RightDocDomesticExpenses=1, RightDocDomesticExpensesCheck=1 WHERE DirEmployeeID=1; ";

            await Task.Run(() => db.Database.ExecuteSqlCommandAsync(SQL));

            #endregion




            //n.
            Models.Sklad.Sys.SysVer sysVer = await Task.Run(() => db.SysVers.FindAsync(1));
            sysVer.SysVerNumber = 24;
            sysVer.SysVerDate = DateTime.Now;

            db.Entry(sysVer).State = System.Data.Entity.EntityState.Modified;
            await Task.Run(() => db.SaveChangesAsync());

            return true;
        }

        #endregion


        #region Update25: Индекс "IDX_DocServicePurches_DirServiceContractorPhone"

        private async Task<bool> Update25(DbConnectionSklad db)
        {
            #region 1. INDEX

            string SQL = "CREATE INDEX [IDX_DocServicePurches_DirServiceContractorPhone] ON [DocServicePurches] ([DirServiceContractorPhone]); ";

            await Task.Run(() => db.Database.ExecuteSqlCommandAsync(SQL));

            #endregion




            //n.
            Models.Sklad.Sys.SysVer sysVer = await Task.Run(() => db.SysVers.FindAsync(1));
            sysVer.SysVerNumber = 25;
            sysVer.SysVerDate = DateTime.Now;

            db.Entry(sysVer).State = System.Data.Entity.EntityState.Modified;
            await Task.Run(() => db.SaveChangesAsync());

            return true;
        }

        #endregion


        #region Update26: DirWarehouses

        private async Task<bool> Update26(DbConnectionSklad db)
        {
            #region 1. DirWarehouses

            string SQL =
                "ALTER TABLE DirWarehouses ADD COLUMN [SalaryPercentTradeType] INTEGER DEFAULT 1; " +
                "ALTER TABLE DirWarehouses ADD COLUMN [SalaryPercentService2TabsType] INTEGER DEFAULT 1; ";

            await Task.Run(() => db.Database.ExecuteSqlCommandAsync(SQL));

            #endregion




            //n.
            Models.Sklad.Sys.SysVer sysVer = await Task.Run(() => db.SysVers.FindAsync(1));
            sysVer.SysVerNumber = 26;
            sysVer.SysVerDate = DateTime.Now;

            db.Entry(sysVer).State = System.Data.Entity.EntityState.Modified;
            await Task.Run(() => db.SaveChangesAsync());

            return true;
        }

        #endregion


        #region Update27: TRIGGER DocServicePurchXTabs

        private async Task<bool> Update27(DbConnectionSklad db)
        {
            #region 1. TRIGGER DocServicePurchXTabs

            string SQL =

            "DROP TRIGGER TG_DELETE_DocServicePurch1Tabs_DocServicePurches; " +


            "CREATE TRIGGER[TG_DELETE_DocServicePurch1Tabs_DocServicePurches] \n" +
            "BEFORE DELETE ON[DocServicePurch1Tabs] \n" +
            "BEGIN \n" +

            "UPDATE DocServicePurches \n" +
            "SET \n" +

            "Sums1 = \n" +
            "( \n" +
            "   ( \n" +
            "      SELECT IFNULL(SUM(DocServicePurch1Tabs.PriceCurrency), 0) \n" +
            "      FROM DocServicePurch1Tabs \n" +
            "      WHERE DocServicePurchID = \n" +
            "      ( \n" +
            "         SELECT DocServicePurch1Tabs.DocServicePurchID \n" +
            "         FROM DocServicePurch1Tabs \n" +
            "         WHERE DocServicePurch1TabID = OLD.DocServicePurch1TabID LIMIT 1 \n" +
            "      ) \n" +
            "   ) \n" +
            "   - \n" +
            "   ( \n" +
            "      SELECT DocServicePurch1Tabs.PriceCurrency \n" +
            "      FROM DocServicePurch1Tabs \n" +
            "      WHERE DocServicePurch1TabID = OLD.DocServicePurch1TabID LIMIT 1 \n" +
            "   ) \n" +
            "), \n" +

            "Sums2 = (SELECT IFNULL(SUM(DocServicePurch2Tabs.PriceCurrency), 0) FROM DocServicePurch2Tabs WHERE DocServicePurchID = (SELECT DocServicePurch1Tabs.DocServicePurchID FROM DocServicePurch1Tabs WHERE DocServicePurch1TabID = OLD.DocServicePurch1TabID LIMIT 1        )  ), \n" +
            "Sums = (SELECT IFNULL(SUM(DocServicePurch1Tabs.PriceCurrency), 0) FROM DocServicePurch1Tabs WHERE DocServicePurchID = (SELECT DocServicePurch1Tabs.DocServicePurchID FROM DocServicePurch1Tabs WHERE DocServicePurch1TabID = OLD.DocServicePurch1TabID LIMIT 1        )   )  +(SELECT IFNULL(SUM(DocServicePurch2Tabs.PriceCurrency), 0) FROM DocServicePurch2Tabs WHERE DocServicePurchID = (SELECT DocServicePurch1Tabs.DocServicePurchID FROM DocServicePurch1Tabs WHERE DocServicePurch1TabID = OLD.DocServicePurch1TabID LIMIT 1        )  )  -IFNULL(DocServicePurches.PrepaymentSum, 0)  WHERE DocServicePurchID = (SELECT DocServicePurch1Tabs.DocServicePurchID FROM DocServicePurch1Tabs WHERE DocServicePurch1TabID = OLD.DocServicePurch1TabID LIMIT 1); \n" +

            "END; " +



            "DROP TRIGGER TG_DELETE_DocServicePurch2Tabs_DocServicePurches; " +

            "CREATE TRIGGER[TG_DELETE_DocServicePurch2Tabs_DocServicePurches] \n" +
            "BEFORE DELETE ON[DocServicePurch2Tabs] \n" +
            "BEGIN \n" +

            "UPDATE DocServicePurches \n" +
            "SET \n" +

            "Sums1 = (SELECT IFNULL(SUM(DocServicePurch1Tabs.PriceCurrency), 0) FROM DocServicePurch1Tabs WHERE DocServicePurchID = (SELECT DocServicePurch2Tabs.DocServicePurchID FROM DocServicePurch2Tabs WHERE DocServicePurch2TabID = OLD.DocServicePurch2TabID LIMIT 1        )   ),    \n" +

            "Sums2 = \n" +
            "( \n" +
            "   ( \n" +
            "      SELECT IFNULL(SUM(DocServicePurch2Tabs.PriceCurrency), 0) \n" +
            "      FROM DocServicePurch2Tabs \n" +
            "      WHERE DocServicePurchID = \n" +
            "      ( \n" +
            "         SELECT DocServicePurch2Tabs.DocServicePurchID \n" +
            "         FROM DocServicePurch2Tabs \n" +
            "         WHERE DocServicePurch2TabID = OLD.DocServicePurch2TabID LIMIT 1 \n" +
            "      ) \n" +
            "   ) \n" +
            "   - \n" +
            "   ( \n" +
            "      SELECT DocServicePurch2Tabs.PriceCurrency \n" +
            "      FROM DocServicePurch2Tabs \n" +
            "      WHERE DocServicePurch2TabID = OLD.DocServicePurch2TabID LIMIT 1 \n" +
            "   ) \n" +
            "), \n" +

            "Sums = (SELECT IFNULL(SUM(DocServicePurch1Tabs.PriceCurrency), 0) FROM DocServicePurch1Tabs WHERE DocServicePurchID = (SELECT DocServicePurch2Tabs.DocServicePurchID FROM DocServicePurch2Tabs WHERE DocServicePurch2TabID = OLD.DocServicePurch2TabID LIMIT 1        )   )  +(SELECT IFNULL(SUM(DocServicePurch2Tabs.PriceCurrency), 0) FROM DocServicePurch2Tabs WHERE DocServicePurchID = (SELECT DocServicePurch2Tabs.DocServicePurchID FROM DocServicePurch2Tabs WHERE DocServicePurch2TabID = OLD.DocServicePurch2TabID LIMIT 1        )  )  -IFNULL(DocServicePurches.PrepaymentSum, 0)  WHERE DocServicePurchID = (SELECT DocServicePurch2Tabs.DocServicePurchID FROM DocServicePurch2Tabs WHERE DocServicePurch2TabID = OLD.DocServicePurch2TabID LIMIT 1); \n" +

            "END; ";




            await Task.Run(() => db.Database.ExecuteSqlCommandAsync(SQL));

            #endregion



            #region 4. Пересчитываем суммы Sums1 и Sums2

            SQL =
                "UPDATE DocServicePurches " +
                "SET " +
                " Sums1 = " +
                " (SELECT IFNULL(SUM(PriceCurrency), 0) FROM DocServicePurch1Tabs WHERE DocServicePurch1Tabs.DocServicePurchID = DocServicePurches.DocServicePurchID); " +

                "UPDATE DocServicePurches " +
                "SET " +
                " Sums2 = " +
                " (SELECT IFNULL(SUM(PriceCurrency), 0) FROM DocServicePurch2Tabs WHERE DocServicePurch2Tabs.DocServicePurchID = DocServicePurches.DocServicePurchID); ";

            await Task.Run(() => db.Database.ExecuteSqlCommandAsync(SQL));

            #endregion




            //n.
            Models.Sklad.Sys.SysVer sysVer = await Task.Run(() => db.SysVers.FindAsync(1));
            sysVer.SysVerNumber = 27;
            sysVer.SysVerDate = DateTime.Now;

            db.Entry(sysVer).State = System.Data.Entity.EntityState.Modified;
            await Task.Run(() => db.SaveChangesAsync());

            return true;
        }

        #endregion


        #region Update28: TRIGGER DocSecondHandPurchXTabs

        private async Task<bool> Update28(DbConnectionSklad db)
        {
            #region 1. TRIGGER DocSecondHandPurchXTabs

            string SQL =

            "DROP TRIGGER TG_DELETE_DocSecondHandPurch1Tabs_DocSecondHandPurches; " +


            "CREATE TRIGGER[TG_DELETE_DocSecondHandPurch1Tabs_DocSecondHandPurches] \n" +
            "BEFORE DELETE ON[DocSecondHandPurch1Tabs] \n" +
            "BEGIN \n" +

            "UPDATE DocSecondHandPurches \n" +
            "SET \n" +

            "Sums1 = \n" +
            "( \n" +
            "   ( \n" +
            "      SELECT IFNULL(SUM(DocSecondHandPurch1Tabs.PriceCurrency), 0) \n" +
            "      FROM DocSecondHandPurch1Tabs \n" +
            "      WHERE DocSecondHandPurchID = \n" +
            "      ( \n" +
            "         SELECT DocSecondHandPurch1Tabs.DocSecondHandPurchID \n" +
            "         FROM DocSecondHandPurch1Tabs \n" +
            "         WHERE DocSecondHandPurch1TabID = OLD.DocSecondHandPurch1TabID LIMIT 1 \n" +
            "      ) \n" +
            "   ) \n" +
            "   - \n" +
            "   ( \n" +
            "      SELECT DocSecondHandPurch1Tabs.PriceCurrency \n" +
            "      FROM DocSecondHandPurch1Tabs \n" +
            "      WHERE DocSecondHandPurch1TabID = OLD.DocSecondHandPurch1TabID LIMIT 1 \n" +
            "   ) \n" +
            "), \n" +

            "Sums2 = (SELECT IFNULL(SUM(DocSecondHandPurch2Tabs.PriceCurrency), 0) FROM DocSecondHandPurch2Tabs WHERE DocSecondHandPurchID = (SELECT DocSecondHandPurch1Tabs.DocSecondHandPurchID FROM DocSecondHandPurch1Tabs WHERE DocSecondHandPurch1TabID = OLD.DocSecondHandPurch1TabID LIMIT 1        )  ), \n" +
            "Sums = (SELECT IFNULL(SUM(DocSecondHandPurch1Tabs.PriceCurrency), 0) FROM DocSecondHandPurch1Tabs WHERE DocSecondHandPurchID = (SELECT DocSecondHandPurch1Tabs.DocSecondHandPurchID FROM DocSecondHandPurch1Tabs WHERE DocSecondHandPurch1TabID = OLD.DocSecondHandPurch1TabID LIMIT 1        )   )  +(SELECT IFNULL(SUM(DocSecondHandPurch2Tabs.PriceCurrency), 0) FROM DocSecondHandPurch2Tabs WHERE DocSecondHandPurchID = (SELECT DocSecondHandPurch1Tabs.DocSecondHandPurchID FROM DocSecondHandPurch1Tabs WHERE DocSecondHandPurch1TabID = OLD.DocSecondHandPurch1TabID LIMIT 1        )  ) WHERE DocSecondHandPurchID = (SELECT DocSecondHandPurch1Tabs.DocSecondHandPurchID FROM DocSecondHandPurch1Tabs WHERE DocSecondHandPurch1TabID = OLD.DocSecondHandPurch1TabID LIMIT 1); \n" +

            "END; " +



            "DROP TRIGGER TG_DELETE_DocSecondHandPurch2Tabs_DocSecondHandPurches; " +

            "CREATE TRIGGER[TG_DELETE_DocSecondHandPurch2Tabs_DocSecondHandPurches] \n" +
            "BEFORE DELETE ON[DocSecondHandPurch2Tabs] \n" +
            "BEGIN \n" +

            "UPDATE DocSecondHandPurches \n" +
            "SET \n" +

            "Sums1 = (SELECT IFNULL(SUM(DocSecondHandPurch1Tabs.PriceCurrency), 0) FROM DocSecondHandPurch1Tabs WHERE DocSecondHandPurchID = (SELECT DocSecondHandPurch2Tabs.DocSecondHandPurchID FROM DocSecondHandPurch2Tabs WHERE DocSecondHandPurch2TabID = OLD.DocSecondHandPurch2TabID LIMIT 1        )   ),    \n" +

            "Sums2 = \n" +
            "( \n" +
            "   ( \n" +
            "      SELECT IFNULL(SUM(DocSecondHandPurch2Tabs.PriceCurrency), 0) \n" +
            "      FROM DocSecondHandPurch2Tabs \n" +
            "      WHERE DocSecondHandPurchID = \n" +
            "      ( \n" +
            "         SELECT DocSecondHandPurch2Tabs.DocSecondHandPurchID \n" +
            "         FROM DocSecondHandPurch2Tabs \n" +
            "         WHERE DocSecondHandPurch2TabID = OLD.DocSecondHandPurch2TabID LIMIT 1 \n" +
            "      ) \n" +
            "   ) \n" +
            "   - \n" +
            "   ( \n" +
            "      SELECT DocSecondHandPurch2Tabs.PriceCurrency \n" +
            "      FROM DocSecondHandPurch2Tabs \n" +
            "      WHERE DocSecondHandPurch2TabID = OLD.DocSecondHandPurch2TabID LIMIT 1 \n" +
            "   ) \n" +
            "), \n" +

            "Sums = (SELECT IFNULL(SUM(DocSecondHandPurch1Tabs.PriceCurrency), 0) FROM DocSecondHandPurch1Tabs WHERE DocSecondHandPurchID = (SELECT DocSecondHandPurch2Tabs.DocSecondHandPurchID FROM DocSecondHandPurch2Tabs WHERE DocSecondHandPurch2TabID = OLD.DocSecondHandPurch2TabID LIMIT 1        )   )  +(SELECT IFNULL(SUM(DocSecondHandPurch2Tabs.PriceCurrency), 0) FROM DocSecondHandPurch2Tabs WHERE DocSecondHandPurchID = (SELECT DocSecondHandPurch2Tabs.DocSecondHandPurchID FROM DocSecondHandPurch2Tabs WHERE DocSecondHandPurch2TabID = OLD.DocSecondHandPurch2TabID LIMIT 1        )  ) WHERE DocSecondHandPurchID = (SELECT DocSecondHandPurch2Tabs.DocSecondHandPurchID FROM DocSecondHandPurch2Tabs WHERE DocSecondHandPurch2TabID = OLD.DocSecondHandPurch2TabID LIMIT 1); \n" +

            "END; ";




            await Task.Run(() => db.Database.ExecuteSqlCommandAsync(SQL));

            #endregion



            #region 4. Пересчитываем суммы Sums1 и Sums2

            SQL =
                "UPDATE DocSecondHandPurches " +
                "SET " +
                " Sums1 = " +
                " (SELECT IFNULL(SUM(PriceCurrency), 0) FROM DocSecondHandPurch1Tabs WHERE DocSecondHandPurch1Tabs.DocSecondHandPurchID = DocSecondHandPurches.DocSecondHandPurchID); " +

                "UPDATE DocSecondHandPurches " +
                "SET " +
                " Sums2 = " +
                " (SELECT IFNULL(SUM(PriceCurrency), 0) FROM DocSecondHandPurch2Tabs WHERE DocSecondHandPurch2Tabs.DocSecondHandPurchID = DocSecondHandPurches.DocSecondHandPurchID); ";

            await Task.Run(() => db.Database.ExecuteSqlCommandAsync(SQL));

            #endregion




            //n.
            Models.Sklad.Sys.SysVer sysVer = await Task.Run(() => db.SysVers.FindAsync(1));
            sysVer.SysVerNumber = 28;
            sysVer.SysVerDate = DateTime.Now;

            db.Entry(sysVer).State = System.Data.Entity.EntityState.Modified;
            await Task.Run(() => db.SaveChangesAsync());

            return true;
        }

        #endregion


        #region Update29: TRIGGER DocSecondHandPurchXTabs

        private async Task<bool> Update29(DbConnectionSklad db)
        {
            string SQL =
                "ALTER TABLE SysSettings ADD COLUMN [SmsAutoShow9] BOOL NOT NULL DEFAULT 0; " +
                "INSERT INTO DirSmsTemplates (DirSmsTemplateID, DirSmsTemplateName, DirSmsTemplateMsg, DirSmsTemplateType, MenuID)values(7, 'Выдан аппарат', 'Ваш [[[ТоварНаименование]]] выдан, гарантия до [[[Гарантия]]]', 4, 1); ";
            await Task.Run(() => db.Database.ExecuteSqlCommandAsync(SQL));



            //n.
            Models.Sklad.Sys.SysVer sysVer = await Task.Run(() => db.SysVers.FindAsync(1));
            sysVer.SysVerNumber = 29;
            sysVer.SysVerDate = DateTime.Now;

            db.Entry(sysVer).State = System.Data.Entity.EntityState.Modified;
            await Task.Run(() => db.SaveChangesAsync());

            return true;
        }

        #endregion


        #region Update30: DirWarehouses

        private async Task<bool> Update30(DbConnectionSklad db)
        {
            #region 1. DirWarehouses

            string SQL =
                "ALTER TABLE DirWarehouses ADD COLUMN [SalaryPercentService1TabsType] INTEGER DEFAULT 1; ";

            await Task.Run(() => db.Database.ExecuteSqlCommandAsync(SQL));

            #endregion




            //n.
            Models.Sklad.Sys.SysVer sysVer = await Task.Run(() => db.SysVers.FindAsync(1));
            sysVer.SysVerNumber = 30;
            sysVer.SysVerDate = DateTime.Now;

            db.Entry(sysVer).State = System.Data.Entity.EntityState.Modified;
            await Task.Run(() => db.SaveChangesAsync());

            return true;
        }

        #endregion


        #region Update31: DirServiceJobNomens.DirServiceJobNomenType

        private async Task<bool> Update31(DbConnectionSklad db)
        {
            #region 1. DirWarehouses

            string SQL =
                //1 - СЦ
                //2 - БУ
                "ALTER TABLE DirServiceJobNomens ADD COLUMN [DirServiceJobNomenType] INTEGER DEFAULT 1; ";


            await Task.Run(() => db.Database.ExecuteSqlCommandAsync(SQL));

            #endregion




            //n.
            Models.Sklad.Sys.SysVer sysVer = await Task.Run(() => db.SysVers.FindAsync(1));
            sysVer.SysVerNumber = 31;
            sysVer.SysVerDate = DateTime.Now;

            db.Entry(sysVer).State = System.Data.Entity.EntityState.Modified;
            await Task.Run(() => db.SaveChangesAsync());

            return true;
        }

        #endregion


        #region Update32: DocServicePurch1Tabs (PayDate, DocCashOfficeSumID, DocBankSumID)

        private async Task<bool> Update32(DbConnectionSklad db)
        {
            #region 1. ===

            string SQL =
                //1 - СЦ
                //1.1. DocServicePurch1Tabs
                "ALTER TABLE DocServicePurch1Tabs ADD COLUMN [PayDate] DATETIME; " +
                "ALTER TABLE DocServicePurch1Tabs ADD COLUMN [DocCashOfficeSumID] INTEGER; " +
                "ALTER TABLE DocServicePurch1Tabs ADD COLUMN [DocBankSumID] INTEGER; " +
                //1.1. DocServicePurch1Tabs
                "ALTER TABLE DocServicePurch2Tabs ADD COLUMN [PayDate] DATETIME; " +
                "ALTER TABLE DocServicePurch2Tabs ADD COLUMN [DocCashOfficeSumID] INTEGER; " +
                "ALTER TABLE DocServicePurch2Tabs ADD COLUMN [DocBankSumID] INTEGER; " +

                //2 - БУ
                //1.1. DocServicePurch1Tabs
                "ALTER TABLE DocSecondHandPurch1Tabs ADD COLUMN [PayDate] DATETIME; " +
                //1.1. DocServicePurch1Tabs
                "ALTER TABLE DocSecondHandPurch2Tabs ADD COLUMN [PayDate] DATETIME; ";

            await Task.Run(() => db.Database.ExecuteSqlCommandAsync(SQL));




            //1.1. DocServicePurch1Tabs.DocCashOfficeSums
            SQL =
                "UPDATE DocServicePurch1Tabs SET " +
                "PayDate=" +
                "(" +
                " SELECT MAX(DocCashOfficeSumDate) " +
                " FROM DocCashOfficeSums c, DocServicePurches p, DocServicePurch1Tabs x " +
                " WHERE c.DocID=p.DocID and p.DocServicePurchID=x.DocServicePurchID and c.DocCashOfficeSumDate>x.TabDate and x.DocServicePurch1TabID=DocServicePurch1Tabs.DocServicePurch1TabID" +
                "), " +
                "DocCashOfficeSumID=" +
                "(" +
                " SELECT MAX(c.DocCashOfficeSumID) " +
                " FROM DocCashOfficeSums c, DocServicePurches p, DocServicePurch1Tabs x " +
                " WHERE c.DocID=p.DocID and p.DocServicePurchID=x.DocServicePurchID and c.DocCashOfficeSumDate>x.TabDate and x.DocServicePurch1TabID=DocServicePurch1Tabs.DocServicePurch1TabID" +
                "); ";
            await Task.Run(() => db.Database.ExecuteSqlCommandAsync(SQL));

            //1.2. DocServicePurch1Tabs.DocBankSums
            SQL =
                "UPDATE DocServicePurch1Tabs SET " +
                "PayDate=" +
                "(" +
                " SELECT MAX(DocBankSumDate) " +
                " FROM DocBankSums c, DocServicePurches p, DocServicePurch1Tabs x " +
                " WHERE c.DocID=p.DocID and p.DocServicePurchID=x.DocServicePurchID and c.DocBankSumDate>x.TabDate and x.DocServicePurch1TabID=DocServicePurch1Tabs.DocServicePurch1TabID" +
                "), " +
                "DocBankSumID=" +
                "(" +
                " SELECT MAX(c.DocBankSumID) " +
                " FROM DocBankSums c, DocServicePurches p, DocServicePurch1Tabs x " +
                " WHERE c.DocID=p.DocID and p.DocServicePurchID=x.DocServicePurchID and c.DocBankSumDate>x.TabDate and x.DocServicePurch1TabID=DocServicePurch1Tabs.DocServicePurch1TabID" +
                ") " +
                "WHERE PayDate IS NULL; ";
            await Task.Run(() => db.Database.ExecuteSqlCommandAsync(SQL));



            //2.1. DocServicePurch2Tabs.DocCashOfficeSums
            SQL =
                "UPDATE DocServicePurch2Tabs SET " +
                "PayDate=" +
                "(" +
                " SELECT MAX(DocCashOfficeSumDate) " +
                " FROM DocCashOfficeSums c, DocServicePurches p, DocServicePurch2Tabs x " +
                " WHERE c.DocID=p.DocID and p.DocServicePurchID=x.DocServicePurchID and c.DocCashOfficeSumDate>x.TabDate and x.DocServicePurch2TabID=DocServicePurch2Tabs.DocServicePurch2TabID" +
                "), " +
                "DocCashOfficeSumID=" +
                "(" +
                " SELECT MAX(c.DocCashOfficeSumID) " +
                " FROM DocCashOfficeSums c, DocServicePurches p, DocServicePurch2Tabs x " +
                " WHERE c.DocID=p.DocID and p.DocServicePurchID=x.DocServicePurchID and c.DocCashOfficeSumDate>x.TabDate and x.DocServicePurch2TabID=DocServicePurch2Tabs.DocServicePurch2TabID" +
                "); ";
            await Task.Run(() => db.Database.ExecuteSqlCommandAsync(SQL));

            //2.2. DocServicePurch2Tabs.DocBankSums
            SQL =
                "UPDATE DocServicePurch2Tabs SET " +
                "PayDate=" +
                "(" +
                " SELECT MAX(DocBankSumDate) " +
                " FROM DocBankSums c, DocServicePurches p, DocServicePurch2Tabs x " +
                " WHERE c.DocID=p.DocID and p.DocServicePurchID=x.DocServicePurchID and c.DocBankSumDate>x.TabDate and x.DocServicePurch2TabID=DocServicePurch2Tabs.DocServicePurch2TabID" +
                "), " +
                "DocBankSumID=" +
                "(" +
                " SELECT MAX(c.DocBankSumID) " +
                " FROM DocBankSums c, DocServicePurches p, DocServicePurch2Tabs x " +
                " WHERE c.DocID=p.DocID and p.DocServicePurchID=x.DocServicePurchID and c.DocBankSumDate>x.TabDate and x.DocServicePurch2TabID=DocServicePurch2Tabs.DocServicePurch2TabID" +
                ") " +
                "WHERE PayDate IS NULL; ";
            await Task.Run(() => db.Database.ExecuteSqlCommandAsync(SQL));


            #endregion




            //n.
            Models.Sklad.Sys.SysVer sysVer = await Task.Run(() => db.SysVers.FindAsync(1));
            sysVer.SysVerNumber = 32;
            sysVer.SysVerDate = DateTime.Now;

            db.Entry(sysVer).State = System.Data.Entity.EntityState.Modified;
            await Task.Run(() => db.SaveChangesAsync());

            return true;
        }

        #endregion


        #region Update33: DocServicePurch1Tabs (PayDate, DocCashOfficeSumID, DocBankSumID)

        private async Task<bool> Update33(DbConnectionSklad db)
        {
            #region 1. ===

            string SQL = "";

            //1.1. DocServicePurch1Tabs.DocCashOfficeSums
            SQL =
                "UPDATE DocServicePurch1Tabs SET " +
                "PayDate=" +
                "(" +
                " SELECT MAX(DocCashOfficeSumDate) " +
                " FROM DocCashOfficeSums c, DocServicePurches p, DocServicePurch1Tabs x " +
                " WHERE c.DocID=p.DocID and p.DocServicePurchID=x.DocServicePurchID and c.DocCashOfficeSumDate>x.TabDate and x.DocServicePurch1TabID=DocServicePurch1Tabs.DocServicePurch1TabID" +
                "), " +
                "DocCashOfficeSumID=" +
                "(" +
                " SELECT MAX(c.DocCashOfficeSumID) " +
                " FROM DocCashOfficeSums c, DocServicePurches p, DocServicePurch1Tabs x " +
                " WHERE c.DocID=p.DocID and p.DocServicePurchID=x.DocServicePurchID and c.DocCashOfficeSumDate>x.TabDate and x.DocServicePurch1TabID=DocServicePurch1Tabs.DocServicePurch1TabID" +
                "); ";
            await Task.Run(() => db.Database.ExecuteSqlCommandAsync(SQL));

            //1.2. DocServicePurch1Tabs.DocBankSums
            SQL =
                "UPDATE DocServicePurch1Tabs SET " +
                "PayDate=" +
                "(" +
                " SELECT MAX(DocBankSumDate) " +
                " FROM DocBankSums c, DocServicePurches p, DocServicePurch1Tabs x " +
                " WHERE c.DocID=p.DocID and p.DocServicePurchID=x.DocServicePurchID and c.DocBankSumDate>x.TabDate and x.DocServicePurch1TabID=DocServicePurch1Tabs.DocServicePurch1TabID" +
                "), " +
                "DocBankSumID=" +
                "(" +
                " SELECT MAX(c.DocBankSumID) " +
                " FROM DocBankSums c, DocServicePurches p, DocServicePurch1Tabs x " +
                " WHERE c.DocID=p.DocID and p.DocServicePurchID=x.DocServicePurchID and c.DocBankSumDate>x.TabDate and x.DocServicePurch1TabID=DocServicePurch1Tabs.DocServicePurch1TabID" +
                ") " +
                "WHERE PayDate IS NULL; ";
            await Task.Run(() => db.Database.ExecuteSqlCommandAsync(SQL));



            //2.1. DocServicePurch2Tabs.DocCashOfficeSums
            SQL =
                "UPDATE DocServicePurch2Tabs SET " +
                "PayDate=" +
                "(" +
                " SELECT MAX(DocCashOfficeSumDate) " +
                " FROM DocCashOfficeSums c, DocServicePurches p, DocServicePurch2Tabs x " +
                " WHERE c.DocID=p.DocID and p.DocServicePurchID=x.DocServicePurchID and c.DocCashOfficeSumDate>x.TabDate and x.DocServicePurch2TabID=DocServicePurch2Tabs.DocServicePurch2TabID" +
                "), " +
                "DocCashOfficeSumID=" +
                "(" +
                " SELECT MAX(c.DocCashOfficeSumID) " +
                " FROM DocCashOfficeSums c, DocServicePurches p, DocServicePurch2Tabs x " +
                " WHERE c.DocID=p.DocID and p.DocServicePurchID=x.DocServicePurchID and c.DocCashOfficeSumDate>x.TabDate and x.DocServicePurch2TabID=DocServicePurch2Tabs.DocServicePurch2TabID" +
                "); ";
            await Task.Run(() => db.Database.ExecuteSqlCommandAsync(SQL));

            //2.2. DocServicePurch2Tabs.DocBankSums
            SQL =
                "UPDATE DocServicePurch2Tabs SET " +
                "PayDate=" +
                "(" +
                " SELECT MAX(DocBankSumDate) " +
                " FROM DocBankSums c, DocServicePurches p, DocServicePurch2Tabs x " +
                " WHERE c.DocID=p.DocID and p.DocServicePurchID=x.DocServicePurchID and c.DocBankSumDate>x.TabDate and x.DocServicePurch2TabID=DocServicePurch2Tabs.DocServicePurch2TabID" +
                "), " +
                "DocBankSumID=" +
                "(" +
                " SELECT MAX(c.DocBankSumID) " +
                " FROM DocBankSums c, DocServicePurches p, DocServicePurch2Tabs x " +
                " WHERE c.DocID=p.DocID and p.DocServicePurchID=x.DocServicePurchID and c.DocBankSumDate>x.TabDate and x.DocServicePurch2TabID=DocServicePurch2Tabs.DocServicePurch2TabID" +
                ") " +
                "WHERE PayDate IS NULL; ";
            await Task.Run(() => db.Database.ExecuteSqlCommandAsync(SQL));


            #endregion




            //n.
            Models.Sklad.Sys.SysVer sysVer = await Task.Run(() => db.SysVers.FindAsync(1));
            sysVer.SysVerNumber = 33;
            sysVer.SysVerDate = DateTime.Now;

            db.Entry(sysVer).State = System.Data.Entity.EntityState.Modified;
            await Task.Run(() => db.SaveChangesAsync());

            return true;
        }

        #endregion


        #region Update34: DocServicePurch1Tabs (PayDate, DocCashOfficeSumID, DocBankSumID)

        private async Task<bool> Update34(DbConnectionSklad db)
        {
            #region 1. ===

            string SQL = "";

            //1.1. DocServicePurch1Tabs.DocCashOfficeSums
            SQL =
                "ALTER TABLE DirWarehouses ADD COLUMN [SalaryPercent2Second] REAL NOT NULL DEFAULT 0; " +
                "ALTER TABLE DirWarehouses ADD COLUMN [SalaryPercent3Second] REAL NOT NULL DEFAULT 0; ";

            await Task.Run(() => db.Database.ExecuteSqlCommandAsync(SQL));


            #endregion




            //n.
            Models.Sklad.Sys.SysVer sysVer = await Task.Run(() => db.SysVers.FindAsync(1));
            sysVer.SysVerNumber = 34;
            sysVer.SysVerDate = DateTime.Now;

            db.Entry(sysVer).State = System.Data.Entity.EntityState.Modified;
            await Task.Run(() => db.SaveChangesAsync());

            return true;
        }

        #endregion


        #endregion



        #region DbMigration - не используется

        //Для наследования "DbMigration"
        public override void Up()
        {
            //Example:
            //AddColumn("DirEmployee", "Address", c => c.String(nullable: true));
        }
        public override void Down()
        {

        }

        #endregion

    }
}