﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PartionnyAccount.Models.Sklad.Dir
{
    [Table("DirServiceNomens")]
    public class DirServiceNomen
    {
        [Key]
        public int? DirServiceNomenID { get; set; }
        public int? Sub { get; set; }
        public bool Del { get; set; }
        
        [Display(Name = "Имя")]
        public string DirServiceNomenName { get; set; }
        [Display(Name = "Для поиска")]
        public string NameLower { get; set; }
        [Display(Name = "Имя Полное")]
        public string DirServiceNomenNameFull { get; set; }
        [Display(Name = "Полное наименование")]
        public string NameFullLower { get; set; }
        public string Description { get; set; }
        public string DescriptionFull { get; set; }

        [Display(Name = "Дата последнего изменения записи")]
        public DateTime? DateTimeUpdate { get; set; }


        //DirServiceNomenPrice
        [NotMapped]
        public string recordsDirServiceNomenPriceGrid { get; set; }


        //Заполняем пустные поля (которые не должны быть пустыми)
        public void Substitute()
        {
            if (DirServiceNomenNameFull == null) DirServiceNomenNameFull = DirServiceNomenName;
        }
    }

    [Table("DirServiceNomenPrices")]
    public class DirServiceNomenPrice
    {
        [Key]
        public int? DirServiceNomenPriceID { get; set; }

        [Display(Name = "Устройства")]
        [Required]
        public int DirServiceNomenID { get; set; }
        [ForeignKey("DirServiceNomenID")]
        public virtual Dir.DirServiceNomen dirServiceNomen { get; set; }

        [Display(Name = "Шаблон наименовния Типовой Неисправности")]
        [Required]
        public int DirServiceNomenTypicalFaultID { get; set; }
        [ForeignKey("DirServiceNomenTypicalFaultID")]
        public virtual Dir.DirServiceNomenTypicalFault dirServiceNomenTypicalFault { get; set; }

        [Display(Name = "Валюта")]
        [Required]
        public int DirCurrencyID { get; set; }
        [ForeignKey("DirCurrencyID")]
        public virtual Dir.DirCurrency dirCurrency { get; set; }

        public double PriceVAT { get; set; }
    }

    //Шаблон наименовния Типовой Неисправности
    [Table("DirServiceNomenTypicalFaults")]
    public class DirServiceNomenTypicalFault
    {
        [Key]
        public int? DirServiceNomenTypicalFaultID { get; set; }
        [Required]
        public string DirServiceNomenTypicalFaultName { get; set; }
    }
}