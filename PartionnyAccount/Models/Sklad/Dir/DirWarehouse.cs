﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PartionnyAccount.Models.Sklad.Dir
{
    [Table("DirWarehouses")]
    public class DirWarehouse
    {
        [Key]
        public int? DirWarehouseID { get; set; }
        public bool Del { get; set; }
        public bool SysRecord { get; set; }

        [Display(Name = "Под группа")]
        public int? Sub { get; set; }
        [ForeignKey("Sub")]
        public virtual Dir.DirWarehouse dirWarehouseSub { get; set; }

        [Required]
        public string DirWarehouseName { get; set; }
        public string DirWarehouseAddress { get; set; }
        public string Phone { get; set; }

        [Display(Name = "Касса")]
        [Required]
        public int DirCashOfficeID { get; set; }
        [ForeignKey("DirCashOfficeID")]
        public virtual Dir.DirCashOffice dirCashOffice { get; set; }

        [Display(Name = "Банк")]
        [Required]
        public int DirBankID { get; set; }
        [ForeignKey("DirBankID")]
        public virtual Dir.DirBank dirBank { get; set; }

        public int? DirWarehouseLoc { get; set; }


        //Зарплата
        [Display(Name = "ЗП: торговля.Тип")]
        public int SalaryPercentTradeType { get; set; }
        public double SalaryPercentTrade { get; set; }

        [Display(Name = "ЗП: Работы.Тип")]
        public int SalaryPercentService1TabsType { get; set; }
        public double SalaryPercentService1Tabs { get; set; }

        [Display(Name = "ЗП: Запчасти.Тип")]
        public int SalaryPercentService2TabsType { get; set; }
        public double SalaryPercentService2Tabs { get; set; }

        [Display(Name = "ЗП: БУ")]
        //% с прибыли
        public double SalaryPercentSecond { get; set; }
        //Фикс. с каждого проданной единиц
        public double SalaryPercent2Second { get; set; }
        //Фикс. за отремонтированную единицу
        public double SalaryPercent3Second { get; set; }
    }
}