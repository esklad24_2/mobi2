﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PartionnyAccount.Models.Sklad.Doc
{
    [Table("DocBankSums")]
    public class DocBankSum
    {
        [Key]
        public int? DocBankSumID { get; set; }

        [Display(Name = "Банк")]
        //[Required]
        public int DirBankID { get; set; }
        [ForeignKey("DirBankID")]
        public virtual Dir.DirBank dirBank { get; set; }

        [Display(Name = "Сотрудник, который создал запись")]
        //[Required]
        public int? DirEmployeeID { get; set; }
        [ForeignKey("DirEmployeeID")]
        public virtual Dir.DirEmployee dirEmployee { get; set; }

        [Display(Name = "Тип операции")]
        //[Required]
        public int DirBankSumTypeID { get; set; }
        [ForeignKey("DirBankSumTypeID")]
        public virtual Dir.DirBankSumType dirBankSumType { get; set; }

        [Display(Name = "Дата операции")]
        //[Required]
        public DateTime? DocBankSumDate { get; set; }

        [Display(Name = "Документ - на основании кого документа создана запись")]
        public int? DocID { get; set; }
        [ForeignKey("DocID")]
        public virtual Doc doc { get; set; }

        [Display(Name = "Документ - ID-шник документа: DocPurches, DocSales, ...")]
        public int? DocXID { get; set; }

        [Display(Name = "Сумма документа")]
        public double DocBankSumSum { get; set; }

        [Display(Name = "Валюта")]
        //[Required]
        public int? DirCurrencyID { get; set; }
        [ForeignKey("DirCurrencyID")]
        public virtual Dir.DirCurrency dirCurrency { get; set; }

        public double? DirCurrencyRate { get; set; }
        public int? DirCurrencyMultiplicity { get; set; }

        public string Base { get; set; }
        public string Description { get; set; }
    }
}