﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PartionnyAccount.Models.Sklad.Doc
{
    [Table("DocSecondHandPurches")]
    public class DocSecondHandPurch
    {
        [Key]
        public int? DocSecondHandPurchID { get; set; }

        [Display(Name = "Документ")]
        //[Required]
        public int? DocID { get; set; }
        [ForeignKey("DocID")]
        public virtual Models.Sklad.Doc.Doc doc { get; set; }

        [Display(Name = "Склад")]
        [Required]
        public int DirWarehouseID { get; set; }
        [ForeignKey("DirWarehouseID")]
        public virtual Dir.DirWarehouse dirWarehouse { get; set; }

        [Display(Name = "Склад")]
        [Required]
        public int DirServiceNomenID { get; set; }
        [ForeignKey("DirServiceNomenID")]
        public virtual Dir.DirServiceNomen dirServiceNomen { get; set; }

        [Display(Name = "Статус")]
        //[Required]
        public int? DirSecondHandStatusID { get; set; }
        [ForeignKey("DirSecondHandStatusID")]
        public virtual Dir.DirSecondHandStatus dirSecondHandStatus { get; set; }

        [Display(Name = "Статус: Готов или Отказ (заполняется при выдаче)")]
        //[Required]
        public int? DirSecondHandStatusID_789 { get; set; }
        [ForeignKey("DirSecondHandStatusID_789")]
        public virtual Dir.DirSecondHandStatus dirSecondHandStatus_789 { get; set; }

        [Display(Name = "Серийный номер")]
        public bool? SerialNumberNo { get; set; }
        public string SerialNumber { get; set; }
        [Display(Name = "Пароль на устройстве")]
        public bool? ComponentPasTextNo { get; set; }
        [Display(Name = "Пароль на устройстве: поле для ввода пароля")]
        public string ComponentPasText { get; set; }
        [Display(Name = "Другое: Поле для ввода текста")]
        public string ComponentOtherText { get; set; }
        [Display(Name = "Неисправность со слов клиента (большое поле для ввода)")]
        public string ProblemClientWords { get; set; }
        [Display(Name = "Примечание")]
        public string Note { get; set; }
        [Display(Name = "Поле для ввода имени Клиента (он же Контрагент)")]
        public string DirServiceContractorName { get; set; }

        //Клиент
        [Display(Name = "Постоянный Клиент")]
        public bool? DirServiceContractorRegular { get; set; }

        [Display(Name = "Выбор из справочника Контрагентов сервиса")]
        //[Required]
        public int? DirServiceContractorID { get; set; }
        [ForeignKey("DirServiceContractorID")]
        public virtual Dir.DirServiceContractor dirServiceContractor { get; set; }

        [Display(Name = "Адрес (из справочника, если постоянный)")]
        public string DirServiceContractorAddress { get; set; }
        [Display(Name = "Телефон (из справочника, если постоянный)")]
        public string DirServiceContractorPhone { get; set; }
        [Display(Name = "Мейл (из справочника, если постоянный)")]
        public string DirServiceContractorEmail { get; set; }
        [Display(Name = "Паспорт: Серия")]
        public string PassportSeries { get; set; }
        [Display(Name = "Паспорт: Номер")]
        public string PassportNumber { get; set; }

        //Цены
        [Display(Name = "Сумма сделки")]
        public double PriceVAT { get; set; }
        [Display(Name = "Сумма сделки в тек.валюте")]
        public double? PriceCurrency { get; set; }

        [Display(Name = "Валюта")]
        [Required]
        public int DirCurrencyID { get; set; }
        [ForeignKey("DirCurrencyID")]
        public virtual Dir.DirCurrency dirCurrency { get; set; }

        public double DirCurrencyRate { get; set; }
        public int DirCurrencyMultiplicity { get; set; }

        //Цены
        [Display(Name = "Розница")]
        public double? PriceRetailVAT { get; set; }
        public double? PriceRetailCurrency { get; set; }
        //Цены
        [Display(Name = "Опт")]
        public double? PriceWholesaleVAT { get; set; }
        public double? PriceWholesaleCurrency { get; set; }
        //Цены
        [Display(Name = "И-М")]
        public double? PriceIMVAT { get; set; }
        public double? PriceIMCurrency { get; set; }

        //Устанавливается при приёмке Аппарата
        [Display(Name = "Дата готовности")]
        [Required]
        public DateTime DateDone { get; set; }

        [Display(Name = "Мастер")]
        //[Required]
        public int? DirEmployeeIDMaster { get; set; }
        [ForeignKey("DirEmployeeIDMaster")]
        public virtual PartionnyAccount.Models.Sklad.Dir.DirEmployee dirEmployee { get; set; }


        public int ServiceTypeRepair { get; set; }

        [Display(Name = "Сумма: что бы не подсчитывать каждый раз сумму")]
        public double? Summ_NotPre { get; set; }

        [Display(Name = "Дата, когда аппарат переместили на выдачу")]
        public DateTime? IssuanceDate { get; set; }

        //До 04.06.2017 == IssuanceDate
        [Display(Name = "Дата смены статуса")]
        public DateTime? DateStatusChange { get; set; }

        [Display(Name = "Сумма - считается тригером")]
        public double? Sums { get; set; }
        [Display(Name = "Сумма - выполненных работ")]
        public double? Sums1 { get; set; }
        [Display(Name = "Сумма - запчастей")]
        public double? Sums2 { get; set; }

        [Display(Name = "Если вернули на доработку, то запомнить первичную дату поступления аппарта")]
        public DateTime? DocDate_First { get; set; }







        //Таблица Doc *** *** *** *** *** *** *** *** *** ***
        [NotMapped]
        public bool? Del { get; set; }
        [NotMapped]
        public int ListObjectID { get; set; }
        [NotMapped]
        public string NumberInt { get; set; }
        [NotMapped]
        public DateTime? DocDate { get; set; }
        [NotMapped]
        public DateTime? DocDateCreate { get; set; }
        [NotMapped]
        public bool? Held { get; set; }
        [NotMapped]
        public double Discount { get; set; }
        [NotMapped]
        [Display(Name = "На основании какого документа создан данный")]
        public int? DocIDBase { get; set; }
        [NotMapped]
        public string Base { get; set; }
        [Display(Name = "Организация")]
        [NotMapped]
        public int DirContractorIDOrg { get; set; }
        [Display(Name = "Контрагент")]
        [NotMapped]
        public int DirContractorID { get; set; }
        [Display(Name = "Кто создал документ. Используется для начислении премии сотруднику")]
        [NotMapped]
        public int DirEmployeeID { get; set; }
        [NotMapped]
        public bool? IsImport { get; set; }
        [NotMapped]
        public string Description { get; set; }
        [Display(Name = "Тип оплаты: Касса или Банк.")]
        [NotMapped]
        public int DirPaymentTypeID { get; set; }
        [Display(Name = "Сумма оплаты")]
        [NotMapped]
        public double Payment { get; set; }
        [Display(Name = "НДС")]
        [NotMapped]
        public double DirVatValue { get; set; }






        //Табличные данные *** *** *** *** *** *** *** *** *** ***
        [NotMapped]
        public string recordsDocSecondHandPurch1Tab { get; set; }
        [NotMapped]
        public string recordsDocSecondHandPurch2Tab { get; set; }



        //Заполняем пустные поля (которые не должны быть пустыми)
        public void Substitute()
        {
            //Заполняем пустные поля
            if (ComponentPasTextNo == true) { ComponentPasText = null; }
            if (DirSecondHandStatusID == null) DirSecondHandStatusID = 1;

            if (DirPaymentTypeID == null) DirPaymentTypeID = 1;
            if (Payment == null) Payment = 0;
            ListObjectID = 65;
        }
    }

    [Table("DocSecondHandPurch1Tabs")]
    public class DocSecondHandPurch1Tab
    {
        [Key]
        public int? DocSecondHandPurch1TabID { get; set; }

        [Display(Name = "Документ")]
        //[Required]
        public int DocSecondHandPurchID { get; set; }
        [ForeignKey("DocSecondHandPurchID")]
        public virtual Models.Sklad.Doc.DocSecondHandPurch docSecondHandPurch { get; set; }

        [Display(Name = "Мастер")]
        //[Required]
        public int DirEmployeeID { get; set; }
        [ForeignKey("DirEmployeeID")]
        public virtual Models.Sklad.Dir.DirEmployee dirEmployee { get; set; }

        [Display(Name = "Выполненная работа")]
        //[Required]
        public int? DirServiceJobNomenID { get; set; }
        [ForeignKey("DirServiceJobNomenID")]
        public virtual Models.Sklad.Dir.DirServiceJobNomen dirServiceJobNomen { get; set; }

        [Display(Name = "Выполненная работа: Наименование, если DirServiceJobNomenID = null")]
        public string DirServiceJobNomenName { get; set; }


        [Display(Name = "Цена")]
        [Required]
        public double PriceVAT { get; set; }
        [Required]
        public double PriceCurrency { get; set; }

        [Display(Name = "Валюта")]
        [Required]
        public int DirCurrencyID { get; set; }
        [ForeignKey("DirCurrencyID")]
        public virtual Dir.DirCurrency dirCurrency { get; set; }

        public double DirCurrencyRate { get; set; }
        public int DirCurrencyMultiplicity { get; set; }

        [Display(Name = "Результат Диагностики")]
        public string DiagnosticRresults { get; set; }

        [Display(Name = "Дата создания записи")]
        public DateTime? TabDate { get; set; }

        [Display(Name = "Статус для результата диагностики")]
        public int? DirSecondHandStatusID { get; set; }



        //Оплата за эту работу
        [Display(Name = "Дата оплата за эту работу")]
        public DateTime? PayDate { get; set; }


        //Табличные данные *** *** *** *** *** *** *** *** *** ***
        [NotMapped]
        public string recordsDataX { get; set; }
    }

    [Table("DocSecondHandPurch2Tabs")]
    public class DocSecondHandPurch2Tab
    {
        [Key]
        public int? DocSecondHandPurch2TabID { get; set; }

        [Display(Name = "Документ")]
        //[Required]
        public int DocSecondHandPurchID { get; set; }
        [ForeignKey("DocSecondHandPurchID")]
        public virtual Models.Sklad.Doc.DocSecondHandPurch docSecondHandPurch { get; set; }

        [Display(Name = "Мастер")]
        //[Required]
        public int DirEmployeeID { get; set; }
        [ForeignKey("DirEmployeeID")]
        public virtual Models.Sklad.Dir.DirEmployee dirEmployee { get; set; }

        [Display(Name = "Выполненная работа")]
        //[Required]
        public int DirNomenID { get; set; }
        [ForeignKey("DirNomenID")]
        public virtual Models.Sklad.Dir.DirNomen dirNomen { get; set; }

        [NotMapped]
        public string DirNomenName { get; set; }

        [Display(Name = "Партия -  с какой партии списываем товар")]
        [Required]
        public int RemPartyID { get; set; }
        [ForeignKey("RemPartyID")]
        public virtual Rem.RemParty remParty { get; set; }

        [Display(Name = "Цена")]
        [Required]
        public double PriceVAT { get; set; }
        [Required]
        public double PriceCurrency { get; set; }

        [Display(Name = "Валюта")]
        [Required]
        public int DirCurrencyID { get; set; }
        [ForeignKey("DirCurrencyID")]
        public virtual Dir.DirCurrency dirCurrency { get; set; }

        public double DirCurrencyRate { get; set; }
        public int DirCurrencyMultiplicity { get; set; }

        [Display(Name = "Дата создания записи")]
        public DateTime? TabDate { get; set; }



        //Оплата за эту запчасть
        [Display(Name = "Дата оплата за эту работу")]
        public DateTime? PayDate { get; set; }
        //[Display(Name = "ID оплаты в Кассе за эту работу")]
        //public int? DocCashOfficeSumID { get; set; }
        //[Display(Name = "ID оплаты в Банке за эту работу")]
        //public int? DocBankSumID { get; set; }



        //Табличные данные *** *** *** *** *** *** *** *** *** ***
        [NotMapped]
        public string recordsDataX { get; set; }
    }



    [Table("DocSecondHandPurches")]
    public class DocSecondHandPurchSQL
    {
        public int? CountX { get; set; }
    }



    [Table("DocSecondHandRetails")]
    public class DocSecondHandRetail
    {
        [Key]
        public int? DocSecondHandRetailID { get; set; }

        [Display(Name = "Документ")]
        //[Required]
        public int? DocID { get; set; }
        [ForeignKey("DocID")]
        public virtual Models.Sklad.Doc.Doc doc { get; set; }

        [Display(Name = "Склад")]
        [Required]
        public int DirWarehouseID { get; set; }
        [ForeignKey("DirWarehouseID")]
        public virtual Dir.DirWarehouse dirWarehouse { get; set; }

        public bool Reserve { get; set; }
        public bool OnCredit { get; set; }



        #region Таблица Doc *** *** *** *** *** *** *** *** *** ***

        [NotMapped]
        public bool? Del { get; set; }
        [NotMapped]
        public int ListObjectID { get; set; }
        [NotMapped]
        public string NumberInt { get; set; }
        [NotMapped]
        public DateTime? DocDate { get; set; }
        [NotMapped]
        public DateTime? DocDateCreate { get; set; }
        [NotMapped]
        public bool? Held { get; set; }
        [NotMapped]
        public double Discount { get; set; }
        [NotMapped]
        [Display(Name = "На основании какого документа создан данный")]
        public int? DocIDBase { get; set; }
        [NotMapped]
        public string Base { get; set; }
        [Display(Name = "Организация")]
        [NotMapped]
        public int DirContractorIDOrg { get; set; }
        [Display(Name = "Контрагент")]
        [NotMapped]
        public int? DirContractorID { get; set; }
        [Display(Name = "Кто создал документ. Используется для начислении премии сотруднику")]
        [NotMapped]
        public int DirEmployeeID { get; set; }
        [NotMapped]
        public bool? IsImport { get; set; }
        [NotMapped]
        public string Description { get; set; }
        [Display(Name = "Тип оплаты: Касса или Банк.")]
        [NotMapped]
        public int DirPaymentTypeID { get; set; }
        [Display(Name = "Сумма оплаты")]
        [NotMapped]
        public double Payment { get; set; }
        [Display(Name = "НДС")]
        [NotMapped]
        public double DirVatValue { get; set; }

        #endregion



        #region Rem2PartyMinus

        /*
        [Display(Name = "Контрагент")]
        [NotMapped]
        public int? DirServiceContractorID { get; set; }
        */

        #endregion



        //Табличные данные *** *** *** *** *** *** *** *** *** ***
        [NotMapped]
        public string recordsDocSecondHandRetailTab { get; set; }



        //Заполняем пустные поля (которые не должны быть пустыми)
        public void Substitute()
        {
            //Заполняем пустные поля
            //if (String.IsNullOrEmpty(DirContractorLegalName)) DirContractorLegalName = DirContractorName;
            if (DirPaymentTypeID == null) DirPaymentTypeID = 1;
            if (Payment == null) Payment = 0;
            ListObjectID = 66;
        }
    }

    [Table("DocSecondHandRetailTabs")]
    public class DocSecondHandRetailTab
    {
        [Key]
        public int? DocSecondHandRetailTabID { get; set; }

        [Display(Name = "Документ")]
        [Required]
        public int? DocSecondHandRetailID { get; set; }
        [ForeignKey("DocSecondHandRetailID")]
        public virtual DocSecondHandRetail docSecondHandRetail { get; set; }

        [Display(Name = "Товар")]
        [Required]
        public int DirServiceNomenID { get; set; }
        [ForeignKey("DirServiceNomenID")]
        public virtual Dir.DirServiceNomen dirServiceNomen { get; set; }

        [Display(Name = "Партия -  с какой партии списываем товар")]
        [Required]
        public int? Rem2PartyID { get; set; }
        [ForeignKey("Rem2PartyID")]
        public virtual Rem.Rem2Party rem2Party { get; set; }

        [Required]
        public double Quantity { get; set; }

        [Display(Name = "Тип цены")]
        [Required]
        public int DirPriceTypeID { get; set; }
        [ForeignKey("DirPriceTypeID")]
        public virtual Dir.DirPriceType dirPriceType { get; set; }

        [Display(Name = "Продажная цена")]
        [Required]
        public double PriceVAT { get; set; }
        [Required]
        public double PriceCurrency { get; set; }

        [Display(Name = "Валюта")]
        [Required]
        public int DirCurrencyID { get; set; }
        [ForeignKey("DirCurrencyID")]
        public virtual Dir.DirCurrency dirCurrency { get; set; }

        public double DirCurrencyRate { get; set; }
        public int DirCurrencyMultiplicity { get; set; }

        [Display(Name = "Тип возврата - НЕ НУЖНОЕ ПОЛЕ, нужно только для UNION")]
        public int? DirReturnTypeID { get; set; }
        [Display(Name = "Комментарий: причина возврата - НЕ НУЖНОЕ ПОЛЕ, нужно только для UNION")]
        public int? DirDescriptionID { get; set; }
    }



    [Table("DocSecondHandRetailReturns")]
    public class DocSecondHandRetailReturn
    {
        [Key]
        public int? DocSecondHandRetailReturnID { get; set; }

        [Display(Name = "Документ")]
        //[Required]
        public int? DocID { get; set; }
        [ForeignKey("DocID")]
        public virtual Models.Sklad.Doc.Doc doc { get; set; }

        [Display(Name = "Партии. А это то что вернули")]
        //[Required]
        public int? DocSecondHandRetailID { get; set; }
        [ForeignKey("DocSecondHandRetailID")]
        public virtual Models.Sklad.Doc.DocSecondHandRetail docRetail { get; set; }

        [Display(Name = "Склад")]
        [Required]
        public int DirWarehouseID { get; set; }
        [ForeignKey("DirWarehouseID")]
        public virtual Dir.DirWarehouse dirWarehouse { get; set; }

        public bool Reserve { get; set; }
        public bool OnCredit { get; set; }



        #region Таблица Doc *** *** *** *** *** *** *** *** *** ***

        [NotMapped]
        public bool? Del { get; set; }
        [NotMapped]
        public int ListObjectID { get; set; }
        [NotMapped]
        public string NumberInt { get; set; }
        [NotMapped]
        public DateTime? DocDate { get; set; }
        [NotMapped]
        public DateTime? DocDateCreate { get; set; }
        [NotMapped]
        public bool? Held { get; set; }
        [NotMapped]
        public double Discount { get; set; }
        [NotMapped]
        [Display(Name = "На основании какого документа создан данный")]
        public int? DocIDBase { get; set; }
        [NotMapped]
        public string Base { get; set; }
        [Display(Name = "Организация")]
        [NotMapped]
        public int DirContractorIDOrg { get; set; }
        [Display(Name = "Контрагент")]
        [NotMapped]
        public int DirContractorID { get; set; }
        [Display(Name = "Кто создал документ. Используется для начислении премии сотруднику")]
        [NotMapped]
        public int DirEmployeeID { get; set; }
        [NotMapped]
        public bool? IsImport { get; set; }
        [NotMapped]
        public string Description { get; set; }
        [Display(Name = "Тип оплаты: Касса или Банк.")]
        [NotMapped]
        public int DirPaymentTypeID { get; set; }
        [Display(Name = "Сумма оплаты")]
        [NotMapped]
        public double Payment { get; set; }
        [Display(Name = "НДС")]
        [NotMapped]
        public double DirVatValue { get; set; }

        #endregion



        //Табличные данные *** *** *** *** *** *** *** *** *** ***
        [NotMapped]
        public string recordsDocSecondHandRetailReturnTab { get; set; }



        //Заполняем пустные поля (которые не должны быть пустыми)
        public void Substitute()
        {
            //Заполняем пустные поля
            //if (String.IsNullOrEmpty(DirContractorLegalName)) DirContractorLegalName = DirContractorName;
            if (DirPaymentTypeID == null) DirPaymentTypeID = 1;
            if (Payment == null) Payment = 0;
            ListObjectID = 67;
        }
    }

    [Table("DocSecondHandRetailReturnTabs")]
    public class DocSecondHandRetailReturnTab
    {
        [Key]
        public int? DocSecondHandRetailReturnTabID { get; set; }

        [Display(Name = "Документ")]
        [Required]
        public int DocSecondHandRetailReturnID { get; set; }
        [ForeignKey("DocSecondHandRetailReturnID")]
        public virtual DocSecondHandRetailReturn docSecondHandRetailReturn { get; set; }

        [Display(Name = "Товар")]
        [Required]
        public int DirServiceNomenID { get; set; }
        [ForeignKey("DirServiceNomenID")]
        public virtual Dir.DirServiceNomen dirServiceNomen { get; set; }

        [Required]
        public double Quantity { get; set; }
        [Display(Name = "Приходная цена")]
        [Required]
        public double PriceVAT { get; set; }
        [Required]
        public double PriceCurrency { get; set; }

        [Display(Name = "Валюта")]
        [Required]
        public int DirCurrencyID { get; set; }
        [ForeignKey("DirCurrencyID")]
        public virtual Dir.DirCurrency dirCurrency { get; set; }

        public double DirCurrencyRate { get; set; }
        public int DirCurrencyMultiplicity { get; set; }


        [Display(Name = "Партии списание. Тоесть возвращаем списанный товар на склад из ПартииМинус")]
        [Required]
        public int? Rem2PartyMinusID { get; set; }
        [ForeignKey("Rem2PartyMinusID")]
        public virtual Rem.Rem2PartyMinus rem2PartyMinus { get; set; }


        [Display(Name = "Тип возврата")]
        public int? DirReturnTypeID { get; set; }
        [ForeignKey("DirReturnTypeID")]
        public virtual Dir.DirReturnType dirReturnType { get; set; }

        [Display(Name = "Комментарий: причина возврата")]
        public int? DirDescriptionID { get; set; }
        [ForeignKey("DirDescriptionID")]
        public virtual Dir.DirDescription dirDescription { get; set; }

        [NotMapped]
        public string Description { get; set; }

    }



    [Table("DocSecondHandRetailActWriteOffs")]
    public class DocSecondHandRetailActWriteOff
    {
        [Key]
        public int? DocSecondHandRetailActWriteOffID { get; set; }

        [Display(Name = "Документ")]
        //[Required]
        public int? DocID { get; set; }
        [ForeignKey("DocID")]
        public virtual Models.Sklad.Doc.Doc doc { get; set; }

        [Display(Name = "Склад")]
        [Required]
        public int DirWarehouseID { get; set; }
        [ForeignKey("DirWarehouseID")]
        public virtual Dir.DirWarehouse dirWarehouse { get; set; }

        public bool Reserve { get; set; }



        #region Таблица Doc *** *** *** *** *** *** *** *** *** ***

        [NotMapped]
        public bool? Del { get; set; }
        [NotMapped]
        public int ListObjectID { get; set; }
        [NotMapped]
        public string NumberInt { get; set; }
        [NotMapped]
        public DateTime? DocDate { get; set; }
        [NotMapped]
        public DateTime? DocDateCreate { get; set; }
        [NotMapped]
        public bool? Held { get; set; }
        [NotMapped]
        public double Discount { get; set; }
        [NotMapped]
        [Display(Name = "На основании какого документа создан данный")]
        public int? DocIDBase { get; set; }
        [NotMapped]
        public string Base { get; set; }
        [Display(Name = "Организация")]
        [NotMapped]
        public int DirContractorIDOrg { get; set; }
        [Display(Name = "Контрагент")]
        [NotMapped]
        public int? DirContractorID { get; set; }
        [Display(Name = "Кто создал документ. Используется для начислении премии сотруднику")]
        [NotMapped]
        public int DirEmployeeID { get; set; }
        [NotMapped]
        public bool? IsImport { get; set; }
        [NotMapped]
        public string Description { get; set; }
        [Display(Name = "Тип оплаты: Касса или Банк.")]
        [NotMapped]
        public int DirPaymentTypeID { get; set; }
        [Display(Name = "Сумма оплаты")]
        [NotMapped]
        public double Payment { get; set; }
        [Display(Name = "НДС")]
        [NotMapped]
        public double DirVatValue { get; set; }

        #endregion


        #region Rem2PartyMinus

        /*
        [Display(Name = "Контрагент")]
        [NotMapped]
        public int? DirServiceContractorID { get; set; }
        */

        #endregion


        //Табличные данные *** *** *** *** *** *** *** *** *** ***
        [NotMapped]
        public string recordsDocSecondHandRetailActWriteOffTab { get; set; }



        //Заполняем пустные поля (которые не должны быть пустыми)
        public void Substitute()
        {
            //Заполняем пустные поля
            //if (String.IsNullOrEmpty(DirContractorLegalName)) DirContractorLegalName = DirContractorName;
            if (DirPaymentTypeID == null) DirPaymentTypeID = 1;
            if (Payment == null) Payment = 0;
            ListObjectID = 68;
        }
    }

    [Table("DocSecondHandRetailActWriteOffTabs")]
    public class DocSecondHandRetailActWriteOffTab
    {
        [Key]
        public int? DocSecondHandRetailActWriteOffTabID { get; set; }

        [Display(Name = "Документ")]
        [Required]
        public int? DocSecondHandRetailActWriteOffID { get; set; }
        [ForeignKey("DocSecondHandRetailActWriteOffID")]
        public virtual DocSecondHandRetailActWriteOff docSecondHandRetailActWriteOff { get; set; }

        [Display(Name = "Товар")]
        [Required]
        public int DirServiceNomenID { get; set; }
        [ForeignKey("DirServiceNomenID")]
        public virtual Dir.DirServiceNomen dirServiceNomen { get; set; }

        [Display(Name = "Партия -  с какой партии списываем товар")]
        [Required]
        public int? Rem2PartyID { get; set; }
        [ForeignKey("Rem2PartyID")]
        public virtual Rem.Rem2Party rem2Party { get; set; }

        [Required]
        public double Quantity { get; set; }

        [Display(Name = "Тип цены")]
        [Required]
        public int DirPriceTypeID { get; set; }
        [ForeignKey("DirPriceTypeID")]
        public virtual Dir.DirPriceType dirPriceType { get; set; }

        [Display(Name = "Продажная цена")]
        [Required]
        public double PriceVAT { get; set; }
        [Required]
        public double PriceCurrency { get; set; }

        [Display(Name = "Валюта")]
        [Required]
        public int DirCurrencyID { get; set; }
        [ForeignKey("DirCurrencyID")]
        public virtual Dir.DirCurrency dirCurrency { get; set; }

        public double DirCurrencyRate { get; set; }
        public int DirCurrencyMultiplicity { get; set; }

        [Display(Name = "Тип возврата")]
        public int? DirReturnTypeID { get; set; }
        [ForeignKey("DirReturnTypeID")]
        public virtual Dir.DirReturnType dirReturnType { get; set; }

        [Display(Name = "Комментарий: причина возврата")]
        public int? DirDescriptionID { get; set; }
        [ForeignKey("DirDescriptionID")]
        public virtual Dir.DirDescription dirDescription { get; set; }

        [NotMapped]
        public string Description { get; set; }
    }

}