﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PartionnyAccount.Models.Sklad.Doc
{
    [Table("DocCashOfficeSums")]
    public class DocCashOfficeSum
    {
        [Key]
        public int? DocCashOfficeSumID { get; set; }

        [Display(Name = "Касса")]
        [Required]
        public int DirCashOfficeID { get; set; }
        [ForeignKey("DirCashOfficeID")]
        public virtual Dir.DirCashOffice dirCashOffice { get; set; }

        [Display(Name = "Сотрудник, который создал запись")]
        //[Required]
        public int? DirEmployeeID { get; set; }
        [ForeignKey("DirEmployeeID")]
        public virtual Dir.DirEmployee dirEmployee { get; set; }

        [Display(Name = "Тип кассовой операции")]
        //[Required]
        public int DirCashOfficeSumTypeID { get; set; }
        [ForeignKey("DirCashOfficeSumTypeID")]
        public virtual Dir.DirCashOfficeSumType dirCashOfficeSumType { get; set; }

        [Display(Name = "дата операции по кассе")]
        //[Required]
        public DateTime? DocCashOfficeSumDate { get; set; }

        [Display(Name = "Документ - на основании кого документа создана запись")]
        public int? DocID { get; set; }
        [ForeignKey("DocID")]
        public virtual Doc doc { get; set; }

        [Display(Name = "Документ - ID-шник документа: DocPurches, DocSales, ...")]
        public int? DocXID { get; set; }

        [Display(Name = "Сумма документа")]
        public double DocCashOfficeSumSum { get; set; }

        [Display(Name = "Валюта")]
        //[Required]
        public int? DirCurrencyID { get; set; }
        [ForeignKey("DirCurrencyID")]
        public virtual Dir.DirCurrency dirCurrency { get; set; }

        public double? DirCurrencyRate { get; set; }
        public int? DirCurrencyMultiplicity { get; set; }

        public string Base { get; set; }
        public int? DirEmployeeIDMoney { get; set; }
        public string Description { get; set; }
    }
}