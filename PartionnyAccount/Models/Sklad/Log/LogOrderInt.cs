﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PartionnyAccount.Models.Sklad.Log
{
    [Table("LogOrderInts")]
    public class LogOrderInt
    {
        [Key]
        public int? LogOrderIntID { get; set; }

        [Display(Name = "Аппарат")]
        [Required]
        public int? DocOrderIntID { get; set; }
        [ForeignKey("DocOrderIntID")]
        public virtual Doc.DocOrderInt docOrderInt { get; set; }

        [Display(Name = "Тип Лога")]
        [Required]
        public int? DirOrderIntLogTypeID { get; set; }
        [ForeignKey("DirOrderIntLogTypeID")]
        public virtual Dir.DirOrderIntLogType dirOrderIntLogType { get; set; }

        [Display(Name = "Сотрудник")]
        //[Required]
        public int? DirEmployeeID { get; set; }
        [ForeignKey("DirEmployeeID")]
        public virtual Dir.DirEmployee dirEmployee { get; set; }

        [Display(Name = "Статус (смена статуса, но может быть == null, в зависимости от DirOrderIntLogType)")]
        public int? DirOrderIntStatusID { get; set; }

        //[NotMapped]
        public DateTime LogOrderIntDate { get; set; }

        public string Msg { get; set; }
    }
}