﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PartionnyAccount.Models.Sklad.Log
{
    [Table("LogSecondHands")]
    public class LogSecondHand
    {
        [Key]
        public int? LogSecondHandID { get; set; }

        [Display(Name = "Аппарат")]
        [Required]
        public int? DocSecondHandPurchID { get; set; }
        [ForeignKey("DocSecondHandPurchID")]
        public virtual Doc.DocSecondHandPurch docSecondHandPurch { get; set; }

        [Display(Name = "Тип Лога")]
        [Required]
        public int? DirSecondHandLogTypeID { get; set; }
        [ForeignKey("DirSecondHandLogTypeID")]
        public virtual Dir.DirSecondHandLogType dirSecondHandLogType { get; set; }

        [Display(Name = "Сотрудник")]
        //[Required]
        public int? DirEmployeeID { get; set; }
        [ForeignKey("DirEmployeeID")]
        public virtual Dir.DirEmployee dirEmployee { get; set; }

        [Display(Name = "Статус (смена статуса, но может быть == null, в зависимости от DirSecondHandLogType)")]
        public int? DirSecondHandStatusID { get; set; }

        //[NotMapped]
        public DateTime LogSecondHandDate { get; set; }

        public string Msg { get; set; }
    }
}