﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PartionnyAccount.Models.Sklad.Log
{
    [Table("LogServices")]
    public class LogService
    {
        [Key]
        public int? LogServiceID { get; set; }

        [Display(Name = "Аппарат")]
        [Required]
        public int? DocServicePurchID { get; set; }
        [ForeignKey("DocServicePurchID")]
        public virtual Doc.DocServicePurch docServicePurch { get; set; }

        [Display(Name = "Тип Лога")]
        [Required]
        public int? DirServiceLogTypeID { get; set; }
        [ForeignKey("DirServiceLogTypeID")]
        public virtual Dir.DirServiceLogType dirServiceLogType { get; set; }

        [Display(Name = "Сотрудник")]
        //[Required]
        public int? DirEmployeeID { get; set; }
        [ForeignKey("DirEmployeeID")]
        public virtual Dir.DirEmployee dirEmployee { get; set; }

        [Display(Name = "Статус (смена статуса, но может быть == null, в зависимости от DirServiceLogType)")]
        public int? DirServiceStatusID { get; set; }

        //[NotMapped]
        public DateTime LogServiceDate { get; set; }

        public string Msg { get; set; }
    }
}