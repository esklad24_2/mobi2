﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PartionnyAccount.Models.Sklad.Log
{
    [Table("LogMovements")]
    public class LogMovement
    {
        [Key]
        public int? LogMovementID { get; set; }

        [Display(Name = "Документ Перемещение")]
        [Required]
        public int? DocMovementID { get; set; }
        [ForeignKey("DocMovementID")]
        public virtual Doc.DocMovement docMovement { get; set; }

        [Display(Name = "Тип Лога")]
        [Required]
        public int? DirMovementLogTypeID { get; set; }
        [ForeignKey("DirMovementLogTypeID")]
        public virtual Dir.DirMovementLogType dirMovementLogType { get; set; }

        [Display(Name = "Сотрудник")]
        //[Required]
        public int? DirEmployeeID { get; set; }
        [ForeignKey("DirEmployeeID")]
        public virtual Dir.DirEmployee dirEmployee { get; set; }

        [Display(Name = "Статус (смена статуса, но может быть == null, в зависимости от DirMovementLogType)")]
        public int? DirMovementStatusID { get; set; }

        //[NotMapped]
        public DateTime LogMovementDate { get; set; }

        public string Msg { get; set; }
    }
}