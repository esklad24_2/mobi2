﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using PartionnyAccount.Models;
using PartionnyAccount.Models.Sklad.Log;

namespace PartionnyAccount.Controllers.Sklad.Log
{
    public class LogOrderIntsController : ApiController
    {
        #region Classes

        Classes.Function.Exceptions.ExceptionEntry exceptionEntry = new Classes.Function.Exceptions.ExceptionEntry();
        Classes.Function.Variables.ConnectionString connectionString = new Classes.Function.Variables.ConnectionString();
        Classes.Account.Login login = new Classes.Account.Login();
        Classes.Account.AccessRight accessRight = new Classes.Account.AccessRight();
        Classes.Function.ReturnServer returnServer = new Classes.Function.ReturnServer();
        Classes.Function.Function function = new Classes.Function.Function();
        Models.Sklad.Sys.SysJourDisp sysJourDisp = new Models.Sklad.Sys.SysJourDisp(); Controllers.Sklad.Sys.SysJourDispsController sysJourDispsController = new Sys.SysJourDispsController();
        private DbConnectionSklad db = new DbConnectionSklad();
        private DbConnectionSklad dbRead = new DbConnectionSklad();

        int ListObjectID = 59;

        #endregion


        #region SELECT

        class Params
        {
            //Parameters
            public int limit = 11;
            public int page = 1;
            public int Skip = 0;
            public int GroupID = 0;
            public int DocOrderIntID = 0;
            public int DirOrderIntLogTypeID = 0;
            public string parSearch = "";
            public DateTime? DateS;
            public DateTime? DatePo;
            public int FilterType;

            public int? DirOrderIntLogTypeIDS;
            public int? DirOrderIntLogTypeIDPo;
        }
        // GET: api/LogOrderInts
        public async Task<IHttpActionResult> GetLogOrderInts(HttpRequestMessage request)
        {
            try
            {
                #region Проверяем Логин и Пароль + Изменяем строку соединения + Права + Разные Функции

                //Получаем Куку
                System.Web.HttpCookie authCookie = System.Web.HttpContext.Current.Request.Cookies["CookieIPOL"];

                // Проверяем Логин и Пароль
                Classes.Account.Login.Field field = await Task.Run(() => login.Return(authCookie, true));
                if (!field.Access) return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg10));

                //Изменяем строку соединения
                db = new DbConnectionSklad(connectionString.Return(field.DirCustomersID, null, true));
                dbRead = new DbConnectionSklad(connectionString.Return(field.DirCustomersID, null, true));

                //Права (1 - Write, 2 - Read, 3 - No Access)
                //int iRight = await Task.Run(() => accessRight.Access(connectionString.Return(field.DirCustomersID, null, true), field.DirEmployeeID, "RightDocSales"));
                //if (iRight == 3) return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg57(0)));

                //Разные Функции
                function.NumberDecimalSeparator();

                //Получам настройки
                Models.Sklad.Sys.SysSetting sysSetting = await db.SysSettings.FindAsync(1);

                #endregion


                #region Параметры

                Params _params = new Params();

                //paramList -список параметров
                var paramList = request.GetQueryNameValuePairs();
                //Параметры
                _params.limit = Convert.ToInt32(paramList.FirstOrDefault(kv => string.Compare(kv.Key, "limit", true) == 0).Value); //Записей на страницу
                _params.page = Convert.ToInt32(paramList.FirstOrDefault(kv => string.Compare(kv.Key, "page", true) == 0).Value);   //Номер страницы
                _params.Skip = _params.limit * (_params.page - 1);
                _params.GroupID = Convert.ToInt32(paramList.FirstOrDefault(kv => string.Compare(kv.Key, "pGroupID", true) == 0).Value); //Кликнули по группе
                _params.DocOrderIntID = Convert.ToInt32(paramList.FirstOrDefault(kv => string.Compare(kv.Key, "DocOrderIntID", true) == 0).Value); //Кликнули по группе
                _params.DirOrderIntLogTypeID = Convert.ToInt32(paramList.FirstOrDefault(kv => string.Compare(kv.Key, "DirOrderIntLogTypeID", true) == 0).Value); //Кликнули по группе
                _params.parSearch = paramList.FirstOrDefault(kv => string.Compare(kv.Key, "parSearch", true) == 0).Value; if (_params.parSearch != null) _params.parSearch = _params.parSearch.ToLower(); //Поиск
                _params.FilterType = Convert.ToInt32(paramList.FirstOrDefault(kv => string.Compare(kv.Key, "FilterType", true) == 0).Value);

                _params.DirOrderIntLogTypeIDS = Convert.ToInt32(paramList.FirstOrDefault(kv => string.Compare(kv.Key, "DirOrderIntLogTypeIDS", true) == 0).Value);
                _params.DirOrderIntLogTypeIDPo = Convert.ToInt32(paramList.FirstOrDefault(kv => string.Compare(kv.Key, "DirOrderIntLogTypeIDPo", true) == 0).Value);

                #endregion



                #region Основной запрос *** *** ***

                var query =
                    (
                        from x in db.LogOrderInts

                        join dirOrderIntStatus1 in db.DirOrderIntStatuses on x.DirOrderIntStatusID equals dirOrderIntStatus1.DirOrderIntStatusID into dirOrderIntStatus2
                        from dirOrderIntStatus in dirOrderIntStatus2.DefaultIfEmpty()

                        where x.DocOrderIntID == _params.DocOrderIntID

                        select new
                        {

                            LogOrderIntID = x.LogOrderIntID,
                            DocOrderIntID = x.DocOrderIntID,

                            DirOrderIntLogTypeID = x.DirOrderIntLogTypeID,
                            DirOrderIntLogTypeName = x.dirOrderIntLogType.DirOrderIntLogTypeName,

                            DirOrderIntStatusID = x.DirOrderIntStatusID,
                            DirOrderIntStatusName = dirOrderIntStatus.DirOrderIntStatusName,

                            DirEmployeeID = x.DirEmployeeID,
                            DirEmployeeName = x.dirEmployee.DirEmployeeName,

                            LogOrderIntDate = x.LogOrderIntDate.ToString(),
                            Msg = x.Msg,


                            Field1 = ""

                            //Field1 = Дата, Тип, Сотрудник
                            /*
                            Field1 =
                            "Дата: " + x.LogOrderIntDate + "<br />" + 
                            "Тип: " + x.dirOrderIntLogType.DirOrderIntLogTypeName + "<br />" +
                            x.dirEmployee.DirEmployeeName + "<br />", //"Сотрудник: " + x.dirEmployee.DirEmployeeName + "<br />",

                            //Field2 = Статус + Сообщение
                            Field2 =
                            x.Msg == null ? dirOrderIntStatus.DirOrderIntStatusName
                            :
                            dirOrderIntStatus.DirOrderIntStatusName + "<br />" + 
                            x.Msg
                            */


                            /*
                            Field1 =

                            x.LogOrderIntDate + " - " +
                            x.dirOrderIntLogType.DirOrderIntLogTypeName + " - " +

                            (
                            x.Msg == null ? dirOrderIntStatus.DirOrderIntStatusName
                            :
                            dirOrderIntStatus.DirOrderIntStatusName + " " +
                            x.Msg + " - "
                            ) + 

                            x.dirEmployee.DirEmployeeName
                            */
                        }
                    );

                #endregion


                #region Условия (параметры) *** *** ***


                #region DirOrderIntLogTypeID

                if (_params.DirOrderIntLogTypeID > 0)
                {
                    query = query.Where(x => x.DirOrderIntLogTypeID == _params.DirOrderIntLogTypeID);
                }

                #endregion


                #region Фильтр DirOrderIntLogTypeID S и Po

                if (_params.DirOrderIntLogTypeIDS != _params.DirOrderIntLogTypeIDPo)
                {
                    if (_params.DirOrderIntLogTypeIDS > 0) query = query.Where(x => x.DirOrderIntLogTypeID >= _params.DirOrderIntLogTypeIDS);
                    if (_params.DirOrderIntLogTypeIDPo > 0) query = query.Where(x => x.DirOrderIntLogTypeID <= _params.DirOrderIntLogTypeIDPo);
                }
                else
                {
                    if (_params.DirOrderIntLogTypeIDS > 0) query = query.Where(x => x.DirOrderIntLogTypeID == _params.DirOrderIntLogTypeIDS);
                }


                #endregion


                #region OrderBy и Лимит

                query = query.OrderByDescending(x => x.LogOrderIntID);

                #endregion


                #endregion


                #region Отправка JSON

                //К-во Номенклатуры
                //int dirCount = await Task.Run(() => db.LogOrderInts.Where(x => x.doc.DocDate >= _params.DateS && x.doc.DocDate <= _params.DatePo).Count());

                //А вдруг к-во меньше Лимита, тогда показать не общее к-во, а реальное!
                int dirCount = await query.CountAsync();
                //if (dirCount2 < _params.limit) dirCount = _params.limit * (_params.page - 1) + dirCount2;

                dynamic collectionWrapper = new
                {
                    sucess = true,
                    total = dirCount,
                    LogOrderInt = query
                };
                return await Task.Run(() => Ok(collectionWrapper));

                #endregion

            }
            catch (Exception ex)
            {
                return Ok(returnServer.Return(false, exceptionEntry.Return(ex)));
            }
        }

        // GET: api/LogOrderInts/5
        [ResponseType(typeof(LogOrderInt))]
        public async Task<IHttpActionResult> GetLogOrderInt(int id)
        {
            return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg57(0)));
        }

        #endregion


        #region UPDATE

        // PUT: api/LogOrderInts/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutLogOrderInt(int id, LogOrderInt logOrderInt)
        {
            #region Проверяем Логин и Пароль + Изменяем строку соединения + Права + Разные Функции

            //Получаем Куку
            System.Web.HttpCookie authCookie = System.Web.HttpContext.Current.Request.Cookies["CookieIPOL"];

            // Проверяем Логин и Пароль
            Classes.Account.Login.Field field = await Task.Run(() => login.Return(authCookie, true));
            if (!field.Access) return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg10));

            //Изменяем строку соединения
            db = new DbConnectionSklad(connectionString.Return(field.DirCustomersID, null, true));

            //Права (1 - Write, 2 - Read, 3 - No Access)
            //int iRight = await Task.Run(() => accessRight.Access(connectionString.Return(field.DirCustomersID, null, true), field.DirEmployeeID, "RightLogOrderInts"));
            //if (iRight != 1) return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg57(0)));

            //Разные Функции
            function.NumberDecimalSeparator();

            //Получам настройки
            Models.Sklad.Sys.SysSetting sysSetting = await db.SysSettings.FindAsync(1);

            #endregion

            #region Проверки

            if (!ModelState.IsValid) return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg91)); //return BadRequest(ModelState);
            if (id != logOrderInt.LogOrderIntID) return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg8)); //return BadRequest();

            //Подстановки - некоторые поля надо заполнить, если они не заполены
            //logOrderInt.Substitute();

            #endregion


            #region Сохранение

            try
            {
                logOrderInt.DirEmployeeID = field.DirEmployeeID;
                logOrderInt = await mPutPostLogOrderInts(db, logOrderInt, EntityState.Modified);


                #region 6. JourDisp *** *** *** *** *** *** *** *** *** *

                Models.Sklad.Sys.SysJourDisp sysJourDisp = new Models.Sklad.Sys.SysJourDisp();
                sysJourDisp.DirDispOperationID = 4; //Изменение записи
                sysJourDisp.DirEmployeeID = field.DirEmployeeID;
                sysJourDisp.ListObjectID = ListObjectID;
                sysJourDisp.TableFieldID = logOrderInt.LogOrderIntID;
                sysJourDisp.Description = "";
                try { sysJourDispsController.mPutPostSysJourDisps(db, sysJourDisp, EntityState.Added); } catch (Exception ex) { }

                #endregion


                dynamic collectionWrapper = new
                {
                    ID = logOrderInt.LogOrderIntID
                };
                return Ok(returnServer.Return(true, collectionWrapper)); //return Ok(returnServer.Return(true, ""));
            }
            catch (Exception ex)
            {
                return Ok(returnServer.Return(false, exceptionEntry.Return(ex)));
            }

            #endregion
        }

        // POST: api/LogOrderInts
        [ResponseType(typeof(LogOrderInt))]
        public async Task<IHttpActionResult> PostLogOrderInt(LogOrderInt logOrderInt)
        {
            #region Проверяем Логин и Пароль + Изменяем строку соединения + Права + Разные Функции

            //Получаем Куку
            System.Web.HttpCookie authCookie = System.Web.HttpContext.Current.Request.Cookies["CookieIPOL"];

            // Проверяем Логин и Пароль
            Classes.Account.Login.Field field = await Task.Run(() => login.Return(authCookie, true));
            if (!field.Access) return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg10));

            //Изменяем строку соединения
            db = new DbConnectionSklad(connectionString.Return(field.DirCustomersID, null, true));

            //Права (1 - Write, 2 - Read, 3 - No Access)
            //int iRight = await Task.Run(() => accessRight.Access(connectionString.Return(field.DirCustomersID, null, true), field.DirEmployeeID, "RightLogOrderInts"));
            //if (iRight != 1) return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg57(0)));

            //Разные Функции
            function.NumberDecimalSeparator();

            //Получам настройки
            Models.Sklad.Sys.SysSetting sysSetting = await db.SysSettings.FindAsync(1);

            #endregion

            #region Проверки

            if (!ModelState.IsValid) return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg91)); //return BadRequest(ModelState);

            //Подстановки - некоторые поля надо заполнить, если они не заполены
            //logOrderInt.Substitute();

            #endregion


            #region Сохранение

            try
            {
                logOrderInt.DirEmployeeID = field.DirEmployeeID;
                logOrderInt = await mPutPostLogOrderInts(db, logOrderInt, EntityState.Added);


                #region 6. JourDisp *** *** *** *** *** *** *** *** *** *

                Models.Sklad.Sys.SysJourDisp sysJourDisp = new Models.Sklad.Sys.SysJourDisp();
                sysJourDisp.DirDispOperationID = 3; //Добавление записи
                sysJourDisp.DirEmployeeID = field.DirEmployeeID;
                sysJourDisp.ListObjectID = ListObjectID;
                sysJourDisp.TableFieldID = logOrderInt.LogOrderIntID;
                sysJourDisp.Description = "";
                try { sysJourDispsController.mPutPostSysJourDisps(db, sysJourDisp, EntityState.Added); } catch (Exception ex) { }

                #endregion


                dynamic collectionWrapper = new
                {
                    ID = logOrderInt.LogOrderIntID
                };
                return Ok(returnServer.Return(true, collectionWrapper)); //return Ok(returnServer.Return(true, ""));
            }
            catch (Exception ex)
            {
                return Ok(returnServer.Return(false, exceptionEntry.Return(ex)));
            }

            #endregion
        }

        // DELETE: api/LogOrderInts/5
        [ResponseType(typeof(LogOrderInt))]
        public async Task<IHttpActionResult> DeleteLogOrderInt(int id)
        {
            #region Проверяем Логин и Пароль + Изменяем строку соединения + Права + Разные Функции

            //Получаем Куку
            System.Web.HttpCookie authCookie = System.Web.HttpContext.Current.Request.Cookies["CookieIPOL"];

            // Проверяем Логин и Пароль
            Classes.Account.Login.Field field = await Task.Run(() => login.Return(authCookie, true));
            if (!field.Access) return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg10));

            //Изменяем строку соединения
            db = new DbConnectionSklad(connectionString.Return(field.DirCustomersID, null, true));

            //Права (1 - Write, 2 - Read, 3 - No Access)
            //int iRight = await Task.Run(() => accessRight.Access(connectionString.Return(field.DirCustomersID, null, true), field.DirEmployeeID, "RightLogOrderInts"));
            //if (iRight != 1) return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg57(0)));

            //Разные Функции
            function.NumberDecimalSeparator();

            //Получам настройки
            Models.Sklad.Sys.SysSetting sysSetting = await db.SysSettings.FindAsync(1);

            #endregion

            #region Удаление

            try
            {
                LogOrderInt logOrderInt = await db.LogOrderInts.FindAsync(id);
                if (logOrderInt == null) return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg99));

                db.LogOrderInts.Remove(logOrderInt);
                await db.SaveChangesAsync();


                #region 6. JourDisp *** *** *** *** *** *** *** *** *** *

                Models.Sklad.Sys.SysJourDisp sysJourDisp = new Models.Sklad.Sys.SysJourDisp();
                sysJourDisp.DirDispOperationID = 5; //Удаление записи
                sysJourDisp.DirEmployeeID = field.DirEmployeeID;
                sysJourDisp.ListObjectID = ListObjectID;
                sysJourDisp.TableFieldID = logOrderInt.LogOrderIntID;
                sysJourDisp.Description = "";
                try { sysJourDispsController.mPutPostSysJourDisps(db, sysJourDisp, EntityState.Added); } catch (Exception ex) { }

                #endregion


                dynamic collectionWrapper = new
                {
                    ID = logOrderInt.LogOrderIntID,
                    Msg = Classes.Language.Sklad.Language.msg19
                };
                return Ok(returnServer.Return(true, collectionWrapper)); //return Ok(returnServer.Return(true, "")
            }
            catch (Exception ex)
            {
                return Ok(returnServer.Return(false, exceptionEntry.Return(ex)));
            }

            #endregion
        }

        #endregion


        #region Mthods

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool LogOrderIntExists(int id)
        {
            return db.LogOrderInts.Count(e => e.LogOrderIntID == id) > 0;
        }


        internal async Task<LogOrderInt> mPutPostLogOrderInts(
                    DbConnectionSklad db,
                    LogOrderInt logOrderInt,
                    EntityState entityState //EntityState.Added, Modified
                    )
        {
            //logOrderInt.LogOrderIntDate = Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd h:mm tt"));
            logOrderInt.LogOrderIntDate = Convert.ToDateTime(DateTime.Now); //.ToString("yyyy-MM-dd hh:mm")
            db.Entry(logOrderInt).State = entityState;
            await db.SaveChangesAsync();

            //if (dirNomen.DirNomenArticle == null) dirNomen.DirNomenArticle = dirNomen.DirNomenID.ToString();

            return logOrderInt;
        }

        #endregion

    }
}