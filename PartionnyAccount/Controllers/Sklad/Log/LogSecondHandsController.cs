﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using PartionnyAccount.Models;
using PartionnyAccount.Models.Sklad.Log;

namespace PartionnyAccount.Controllers.Sklad.Log
{
    public class LogSecondHandsController : ApiController
    {
        #region Classes

        Classes.Function.Exceptions.ExceptionEntry exceptionEntry = new Classes.Function.Exceptions.ExceptionEntry();
        Classes.Function.Variables.ConnectionString connectionString = new Classes.Function.Variables.ConnectionString();
        Classes.Account.Login login = new Classes.Account.Login();
        Classes.Account.AccessRight accessRight = new Classes.Account.AccessRight();
        Classes.Function.ReturnServer returnServer = new Classes.Function.ReturnServer();
        Classes.Function.Function function = new Classes.Function.Function();
        Models.Sklad.Sys.SysJourDisp sysJourDisp = new Models.Sklad.Sys.SysJourDisp(); Controllers.Sklad.Sys.SysJourDispsController sysJourDispsController = new Sys.SysJourDispsController();
        private DbConnectionSklad db = new DbConnectionSklad();
        private DbConnectionSklad dbRead = new DbConnectionSklad();

        int ListObjectID = 40;

        #endregion


        #region SELECT

        class Params
        {
            //Parameters
            public int limit = 11;
            public int page = 1;
            public int Skip = 0;
            public int GroupID = 0;
            public int DocSecondHandPurchID = 0;
            public int DirSecondHandLogTypeID = 0;
            public string parSearch = "";
            public DateTime? DateS;
            public DateTime? DatePo;
            public int FilterType;

            public int? DirSecondHandLogTypeIDS;
            public int? DirSecondHandLogTypeIDPo;
        }
        // GET: api/LogSecondHands
        public async Task<IHttpActionResult> GetLogSecondHands(HttpRequestMessage request)
        {
            try
            {
                #region Проверяем Логин и Пароль + Изменяем строку соединения + Права + Разные Функции

                //Получаем Куку
                System.Web.HttpCookie authCookie = System.Web.HttpContext.Current.Request.Cookies["CookieIPOL"];

                // Проверяем Логин и Пароль
                Classes.Account.Login.Field field = await Task.Run(() => login.Return(authCookie, true));
                if (!field.Access) return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg10));

                //Изменяем строку соединения
                db = new DbConnectionSklad(connectionString.Return(field.DirCustomersID, null, true));
                dbRead = new DbConnectionSklad(connectionString.Return(field.DirCustomersID, null, true));

                //Права (1 - Write, 2 - Read, 3 - No Access)
                //int iRight = await Task.Run(() => accessRight.Access(connectionString.Return(field.DirCustomersID, null, true), field.DirEmployeeID, "RightDocSales"));
                //if (iRight == 3) return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg57(0)));

                //Разные Функции
                function.NumberDecimalSeparator();

                //Получам настройки
                Models.Sklad.Sys.SysSetting sysSetting = await db.SysSettings.FindAsync(1);

                #endregion


                #region Параметры

                Params _params = new Params();

                //paramList -список параметров
                var paramList = request.GetQueryNameValuePairs();
                //Параметры
                _params.limit = Convert.ToInt32(paramList.FirstOrDefault(kv => string.Compare(kv.Key, "limit", true) == 0).Value); //Записей на страницу
                _params.page = Convert.ToInt32(paramList.FirstOrDefault(kv => string.Compare(kv.Key, "page", true) == 0).Value);   //Номер страницы
                _params.Skip = _params.limit * (_params.page - 1);
                _params.GroupID = Convert.ToInt32(paramList.FirstOrDefault(kv => string.Compare(kv.Key, "pGroupID", true) == 0).Value); //Кликнули по группе
                _params.DocSecondHandPurchID = Convert.ToInt32(paramList.FirstOrDefault(kv => string.Compare(kv.Key, "DocSecondHandPurchID", true) == 0).Value); //Кликнули по группе
                _params.DirSecondHandLogTypeID = Convert.ToInt32(paramList.FirstOrDefault(kv => string.Compare(kv.Key, "DirSecondHandLogTypeID", true) == 0).Value); //Кликнули по группе
                _params.parSearch = paramList.FirstOrDefault(kv => string.Compare(kv.Key, "parSearch", true) == 0).Value; if (_params.parSearch != null) _params.parSearch = _params.parSearch.ToLower(); //Поиск
                _params.FilterType = Convert.ToInt32(paramList.FirstOrDefault(kv => string.Compare(kv.Key, "FilterType", true) == 0).Value);

                _params.DirSecondHandLogTypeIDS = Convert.ToInt32(paramList.FirstOrDefault(kv => string.Compare(kv.Key, "DirSecondHandLogTypeIDS", true) == 0).Value);
                _params.DirSecondHandLogTypeIDPo = Convert.ToInt32(paramList.FirstOrDefault(kv => string.Compare(kv.Key, "DirSecondHandLogTypeIDPo", true) == 0).Value);

                #endregion



                #region Основной запрос *** *** ***

                var query =
                    (
                        from x in db.LogSecondHands

                        join dirSecondHandStatus1 in db.DirSecondHandStatuses on x.DirSecondHandStatusID equals dirSecondHandStatus1.DirSecondHandStatusID into dirSecondHandStatus2
                        from dirSecondHandStatus in dirSecondHandStatus2.DefaultIfEmpty()

                        where x.DocSecondHandPurchID == _params.DocSecondHandPurchID

                        select new
                        {

                            LogSecondHandID = x.LogSecondHandID,
                            DocSecondHandPurchID = x.DocSecondHandPurchID,

                            DirSecondHandLogTypeID = x.DirSecondHandLogTypeID,
                            DirSecondHandLogTypeName = x.dirSecondHandLogType.DirSecondHandLogTypeName,

                            DirSecondHandStatusID = x.DirSecondHandStatusID,
                            DirSecondHandStatusName = dirSecondHandStatus.DirSecondHandStatusName,

                            DirEmployeeID = x.DirEmployeeID,
                            DirEmployeeName = x.dirEmployee.DirEmployeeName,

                            LogSecondHandDate = x.LogSecondHandDate.ToString(),
                            Msg = x.Msg,


                            Field1 = ""

                            //Field1 = Дата, Тип, Сотрудник
                            /*
                            Field1 =
                            "Дата: " + x.LogSecondHandDate + "<br />" + 
                            "Тип: " + x.dirSecondHandLogType.DirSecondHandLogTypeName + "<br />" +
                            x.dirEmployee.DirEmployeeName + "<br />", //"Сотрудник: " + x.dirEmployee.DirEmployeeName + "<br />",

                            //Field2 = Статус + Сообщение
                            Field2 =
                            x.Msg == null ? dirSecondHandStatus.DirSecondHandStatusName
                            :
                            dirSecondHandStatus.DirSecondHandStatusName + "<br />" + 
                            x.Msg
                            */


                            /*
                            Field1 =

                            x.LogSecondHandDate + " - " +
                            x.dirSecondHandLogType.DirSecondHandLogTypeName + " - " +

                            (
                            x.Msg == null ? dirSecondHandStatus.DirSecondHandStatusName
                            :
                            dirSecondHandStatus.DirSecondHandStatusName + " " +
                            x.Msg + " - "
                            ) + 

                            x.dirEmployee.DirEmployeeName
                            */
                        }
                    );

                #endregion


                #region Условия (параметры) *** *** ***


                #region DirSecondHandLogTypeID

                if (_params.DirSecondHandLogTypeID > 0)
                {
                    query = query.Where(x => x.DirSecondHandLogTypeID == _params.DirSecondHandLogTypeID);
                }

                #endregion


                #region Фильтр DirSecondHandLogTypeID S и Po

                if (_params.DirSecondHandLogTypeIDS != _params.DirSecondHandLogTypeIDPo)
                {
                    if (_params.DirSecondHandLogTypeIDS > 0) query = query.Where(x => x.DirSecondHandLogTypeID >= _params.DirSecondHandLogTypeIDS);
                    if (_params.DirSecondHandLogTypeIDPo > 0) query = query.Where(x => x.DirSecondHandLogTypeID <= _params.DirSecondHandLogTypeIDPo);
                }
                else
                {
                    if (_params.DirSecondHandLogTypeIDS > 0) query = query.Where(x => x.DirSecondHandLogTypeID == _params.DirSecondHandLogTypeIDS);
                }


                #endregion


                #region OrderBy и Лимит

                query = query.OrderByDescending(x => x.LogSecondHandID);

                #endregion


                #endregion


                #region Отправка JSON

                //К-во Номенклатуры
                //int dirCount = await Task.Run(() => db.LogSecondHands.Where(x => x.doc.DocDate >= _params.DateS && x.doc.DocDate <= _params.DatePo).Count());

                //А вдруг к-во меньше Лимита, тогда показать не общее к-во, а реальное!
                int dirCount = await query.CountAsync();
                //if (dirCount2 < _params.limit) dirCount = _params.limit * (_params.page - 1) + dirCount2;

                dynamic collectionWrapper = new
                {
                    sucess = true,
                    total = dirCount,
                    LogSecondHand = query
                };
                return await Task.Run(() => Ok(collectionWrapper));

                #endregion

            }
            catch (Exception ex)
            {
                return Ok(returnServer.Return(false, exceptionEntry.Return(ex)));
            }
        }

        // GET: api/LogSecondHands/5
        [ResponseType(typeof(LogSecondHand))]
        public async Task<IHttpActionResult> GetLogSecondHand(int id, HttpRequestMessage request)
        {
            return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg57(0)));
        }

        #endregion


        #region UPDATE

        // PUT: api/LogSecondHands/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutLogSecondHand(int id, LogSecondHand logSecondHand)
        {
            #region Проверяем Логин и Пароль + Изменяем строку соединения + Права + Разные Функции

            //Получаем Куку
            System.Web.HttpCookie authCookie = System.Web.HttpContext.Current.Request.Cookies["CookieIPOL"];

            // Проверяем Логин и Пароль
            Classes.Account.Login.Field field = await Task.Run(() => login.Return(authCookie, true));
            if (!field.Access) return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg10));

            //Изменяем строку соединения
            db = new DbConnectionSklad(connectionString.Return(field.DirCustomersID, null, true));

            //Права (1 - Write, 2 - Read, 3 - No Access)
            //int iRight = await Task.Run(() => accessRight.Access(connectionString.Return(field.DirCustomersID, null, true), field.DirEmployeeID, "RightLogSecondHands"));
            //if (iRight != 1) return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg57(0)));

            //Разные Функции
            function.NumberDecimalSeparator();

            //Получам настройки
            Models.Sklad.Sys.SysSetting sysSetting = await db.SysSettings.FindAsync(1);

            #endregion

            #region Проверки

            if (!ModelState.IsValid) return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg91)); //return BadRequest(ModelState);
            if (id != logSecondHand.LogSecondHandID) return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg8)); //return BadRequest();

            //Подстановки - некоторые поля надо заполнить, если они не заполены
            //logSecondHand.Substitute();

            #endregion


            #region Сохранение

            try
            {
                logSecondHand.DirEmployeeID = field.DirEmployeeID;
                logSecondHand = await mPutPostLogSecondHands(db, logSecondHand, EntityState.Modified);


                #region 6. JourDisp *** *** *** *** *** *** *** *** *** *

                Models.Sklad.Sys.SysJourDisp sysJourDisp = new Models.Sklad.Sys.SysJourDisp();
                sysJourDisp.DirDispOperationID = 4; //Изменение записи
                sysJourDisp.DirEmployeeID = field.DirEmployeeID;
                sysJourDisp.ListObjectID = ListObjectID;
                sysJourDisp.TableFieldID = logSecondHand.LogSecondHandID;
                sysJourDisp.Description = "";
                try { sysJourDispsController.mPutPostSysJourDisps(db, sysJourDisp, EntityState.Added); } catch (Exception ex) { }

                #endregion


                dynamic collectionWrapper = new
                {
                    ID = logSecondHand.LogSecondHandID
                };
                return Ok(returnServer.Return(true, collectionWrapper)); //return Ok(returnServer.Return(true, ""));
            }
            catch (Exception ex)
            {
                return Ok(returnServer.Return(false, exceptionEntry.Return(ex)));
            }

            #endregion
        }

        // POST: api/LogSecondHands
        [ResponseType(typeof(LogSecondHand))]
        public async Task<IHttpActionResult> PostLogSecondHand(LogSecondHand logSecondHand)
        {
            #region Проверяем Логин и Пароль + Изменяем строку соединения + Права + Разные Функции

            //Получаем Куку
            System.Web.HttpCookie authCookie = System.Web.HttpContext.Current.Request.Cookies["CookieIPOL"];

            // Проверяем Логин и Пароль
            Classes.Account.Login.Field field = await Task.Run(() => login.Return(authCookie, true));
            if (!field.Access) return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg10));

            //Изменяем строку соединения
            db = new DbConnectionSklad(connectionString.Return(field.DirCustomersID, null, true));

            //Права (1 - Write, 2 - Read, 3 - No Access)
            //int iRight = await Task.Run(() => accessRight.Access(connectionString.Return(field.DirCustomersID, null, true), field.DirEmployeeID, "RightLogSecondHands"));
            //if (iRight != 1) return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg57(0)));

            //Разные Функции
            function.NumberDecimalSeparator();

            //Получам настройки
            Models.Sklad.Sys.SysSetting sysSetting = await db.SysSettings.FindAsync(1);

            #endregion

            #region Проверки

            if (!ModelState.IsValid) return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg91)); //return BadRequest(ModelState);

            //Подстановки - некоторые поля надо заполнить, если они не заполены
            //logSecondHand.Substitute();

            #endregion


            #region Сохранение

            try
            {
                logSecondHand.DirEmployeeID = field.DirEmployeeID;
                logSecondHand = await mPutPostLogSecondHands(db, logSecondHand, EntityState.Added);


                #region 6. JourDisp *** *** *** *** *** *** *** *** *** *

                Models.Sklad.Sys.SysJourDisp sysJourDisp = new Models.Sklad.Sys.SysJourDisp();
                sysJourDisp.DirDispOperationID = 3; //Добавление записи
                sysJourDisp.DirEmployeeID = field.DirEmployeeID;
                sysJourDisp.ListObjectID = ListObjectID;
                sysJourDisp.TableFieldID = logSecondHand.LogSecondHandID;
                sysJourDisp.Description = "";
                try { sysJourDispsController.mPutPostSysJourDisps(db, sysJourDisp, EntityState.Added); } catch (Exception ex) { }

                #endregion


                dynamic collectionWrapper = new
                {
                    ID = logSecondHand.LogSecondHandID
                };
                return Ok(returnServer.Return(true, collectionWrapper)); //return Ok(returnServer.Return(true, ""));
            }
            catch (Exception ex)
            {
                return Ok(returnServer.Return(false, exceptionEntry.Return(ex)));
            }

            #endregion
        }

        // DELETE: api/LogSecondHands/5
        [ResponseType(typeof(LogSecondHand))]
        public async Task<IHttpActionResult> DeleteLogSecondHand(int id)
        {
            #region Проверяем Логин и Пароль + Изменяем строку соединения + Права + Разные Функции

            //Получаем Куку
            System.Web.HttpCookie authCookie = System.Web.HttpContext.Current.Request.Cookies["CookieIPOL"];

            // Проверяем Логин и Пароль
            Classes.Account.Login.Field field = await Task.Run(() => login.Return(authCookie, true));
            if (!field.Access) return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg10));

            //Изменяем строку соединения
            db = new DbConnectionSklad(connectionString.Return(field.DirCustomersID, null, true));

            //Права (1 - Write, 2 - Read, 3 - No Access)
            //int iRight = await Task.Run(() => accessRight.Access(connectionString.Return(field.DirCustomersID, null, true), field.DirEmployeeID, "RightLogSecondHands"));
            //if (iRight != 1) return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg57(0)));

            //Разные Функции
            function.NumberDecimalSeparator();

            //Получам настройки
            Models.Sklad.Sys.SysSetting sysSetting = await db.SysSettings.FindAsync(1);

            #endregion

            #region Удаление

            try
            {
                LogSecondHand logSecondHand = await db.LogSecondHands.FindAsync(id);
                if (logSecondHand == null) return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg99));

                db.LogSecondHands.Remove(logSecondHand);
                await db.SaveChangesAsync();


                #region 6. JourDisp *** *** *** *** *** *** *** *** *** *

                Models.Sklad.Sys.SysJourDisp sysJourDisp = new Models.Sklad.Sys.SysJourDisp();
                sysJourDisp.DirDispOperationID = 5; //Удаление записи
                sysJourDisp.DirEmployeeID = field.DirEmployeeID;
                sysJourDisp.ListObjectID = ListObjectID;
                sysJourDisp.TableFieldID = logSecondHand.LogSecondHandID;
                sysJourDisp.Description = "";
                try { sysJourDispsController.mPutPostSysJourDisps(db, sysJourDisp, EntityState.Added); } catch (Exception ex) { }

                #endregion


                dynamic collectionWrapper = new
                {
                    ID = logSecondHand.LogSecondHandID,
                    Msg = Classes.Language.Sklad.Language.msg19
                };
                return Ok(returnServer.Return(true, collectionWrapper)); //return Ok(returnServer.Return(true, "")
            }
            catch (Exception ex)
            {
                return Ok(returnServer.Return(false, exceptionEntry.Return(ex)));
            }

            #endregion
        }

        #endregion


        #region Mthods

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool LogSecondHandExists(int id)
        {
            return db.LogSecondHands.Count(e => e.LogSecondHandID == id) > 0;
        }


        internal async Task<LogSecondHand> mPutPostLogSecondHands(
                    DbConnectionSklad db,
                    LogSecondHand logSecondHand,
                    EntityState entityState //EntityState.Added, Modified
                    )
        {
            //logSecondHand.LogSecondHandDate = Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd h:mm tt"));
            logSecondHand.LogSecondHandDate = Convert.ToDateTime(DateTime.Now); //.ToString("yyyy-MM-dd hh:mm")
            db.Entry(logSecondHand).State = entityState;
            await db.SaveChangesAsync();

            //if (dirNomen.DirNomenArticle == null) dirNomen.DirNomenArticle = dirNomen.DirNomenID.ToString();

            return logSecondHand;
        }

        #endregion

    }
}