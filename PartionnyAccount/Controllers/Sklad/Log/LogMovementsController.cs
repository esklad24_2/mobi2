﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using PartionnyAccount.Models;
using PartionnyAccount.Models.Sklad.Log;

namespace PartionnyAccount.Controllers.Sklad.Log
{
    public class LogMovementsController : ApiController
    {
        #region Classes

        Classes.Function.Exceptions.ExceptionEntry exceptionEntry = new Classes.Function.Exceptions.ExceptionEntry();
        Classes.Function.Variables.ConnectionString connectionString = new Classes.Function.Variables.ConnectionString();
        Classes.Account.Login login = new Classes.Account.Login();
        Classes.Account.AccessRight accessRight = new Classes.Account.AccessRight();
        Classes.Function.ReturnServer returnServer = new Classes.Function.ReturnServer();
        Classes.Function.Function function = new Classes.Function.Function();
        Models.Sklad.Sys.SysJourDisp sysJourDisp = new Models.Sklad.Sys.SysJourDisp(); Controllers.Sklad.Sys.SysJourDispsController sysJourDispsController = new Sys.SysJourDispsController();
        private DbConnectionSklad db = new DbConnectionSklad();
        private DbConnectionSklad dbRead = new DbConnectionSklad();

        int ListObjectID = 33;

        #endregion


        #region SELECT

        class Params
        {
            //Parameters
            public int limit = 11;
            public int page = 1;
            public int Skip = 0;
            public int GroupID = 0;
            public int DocMovementID = 0;
            public int DirMovementLogTypeID = 0;
            public string parSearch = "";
            public DateTime? DateS;
            public DateTime? DatePo;
            public int FilterType;

            public int? DirMovementLogTypeIDS;
            public int? DirMovementLogTypeIDPo;
        }
        // GET: api/LogMovements
        public async Task<IHttpActionResult> GetLogMovements(HttpRequestMessage request)
        {
            try
            {
                #region Проверяем Логин и Пароль + Изменяем строку соединения + Права + Разные Функции

                //Получаем Куку
                System.Web.HttpCookie authCookie = System.Web.HttpContext.Current.Request.Cookies["CookieIPOL"];

                // Проверяем Логин и Пароль
                Classes.Account.Login.Field field = await Task.Run(() => login.Return(authCookie, true));
                if (!field.Access) return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg10));

                //Изменяем строку соединения
                db = new DbConnectionSklad(connectionString.Return(field.DirCustomersID, null, true));
                dbRead = new DbConnectionSklad(connectionString.Return(field.DirCustomersID, null, true));

                //Права (1 - Write, 2 - Read, 3 - No Access)
                //int iRight = await Task.Run(() => accessRight.Access(connectionString.Return(field.DirCustomersID, null, true), field.DirEmployeeID, "RightDocSales"));
                //if (iRight == 3) return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg57(0)));

                //Разные Функции
                function.NumberDecimalSeparator();

                //Получам настройки
                Models.Sklad.Sys.SysSetting sysSetting = await db.SysSettings.FindAsync(1);

                #endregion


                #region Параметры

                Params _params = new Params();

                //paramList -список параметров
                var paramList = request.GetQueryNameValuePairs();
                //Параметры
                _params.limit = Convert.ToInt32(paramList.FirstOrDefault(kv => string.Compare(kv.Key, "limit", true) == 0).Value); //Записей на страницу
                _params.page = Convert.ToInt32(paramList.FirstOrDefault(kv => string.Compare(kv.Key, "page", true) == 0).Value);   //Номер страницы
                _params.Skip = _params.limit * (_params.page - 1);
                _params.GroupID = Convert.ToInt32(paramList.FirstOrDefault(kv => string.Compare(kv.Key, "pGroupID", true) == 0).Value); //Кликнули по группе
                _params.DocMovementID = Convert.ToInt32(paramList.FirstOrDefault(kv => string.Compare(kv.Key, "DocMovementID", true) == 0).Value); //Кликнули по группе
                _params.DirMovementLogTypeID = Convert.ToInt32(paramList.FirstOrDefault(kv => string.Compare(kv.Key, "DirMovementLogTypeID", true) == 0).Value); //Кликнули по группе
                _params.parSearch = paramList.FirstOrDefault(kv => string.Compare(kv.Key, "parSearch", true) == 0).Value; if (_params.parSearch != null) _params.parSearch = _params.parSearch.ToLower(); //Поиск
                _params.FilterType = Convert.ToInt32(paramList.FirstOrDefault(kv => string.Compare(kv.Key, "FilterType", true) == 0).Value);

                _params.DirMovementLogTypeIDS = Convert.ToInt32(paramList.FirstOrDefault(kv => string.Compare(kv.Key, "DirMovementLogTypeIDS", true) == 0).Value);
                _params.DirMovementLogTypeIDPo = Convert.ToInt32(paramList.FirstOrDefault(kv => string.Compare(kv.Key, "DirMovementLogTypeIDPo", true) == 0).Value);

                #endregion



                #region Основной запрос *** *** ***

                var query =
                    (
                        from x in db.LogMovements

                        join dirMovementStatus1 in db.DirMovementStatuses on x.DirMovementStatusID equals dirMovementStatus1.DirMovementStatusID into dirMovementStatus2
                        from dirMovementStatus in dirMovementStatus2.DefaultIfEmpty()

                        where x.DocMovementID == _params.DocMovementID

                        select new
                        {

                            LogMovementID = x.LogMovementID,
                            DocMovementID = x.DocMovementID,

                            DirMovementLogTypeID = x.DirMovementLogTypeID,
                            DirMovementLogTypeName = x.dirMovementLogType.DirMovementLogTypeName,

                            DirMovementStatusID = x.DirMovementStatusID,
                            DirMovementStatusName = dirMovementStatus.DirMovementStatusName,

                            DirEmployeeID = x.DirEmployeeID,
                            DirEmployeeName = x.dirEmployee.DirEmployeeName,

                            LogMovementDate = x.LogMovementDate.ToString(),
                            Msg = x.Msg,


                            Field1 = ""

                            //Field1 = Дата, Тип, Сотрудник
                            /*
                            Field1 =
                            "Дата: " + x.LogMovementDate + "<br />" + 
                            "Тип: " + x.dirMovementLogType.DirMovementLogTypeName + "<br />" +
                            x.dirEmployee.DirEmployeeName + "<br />", //"Сотрудник: " + x.dirEmployee.DirEmployeeName + "<br />",

                            //Field2 = Статус + Сообщение
                            Field2 =
                            x.Msg == null ? dirMovementStatus.DirMovementStatusName
                            :
                            dirMovementStatus.DirMovementStatusName + "<br />" + 
                            x.Msg
                            */


                            /*
                            Field1 =

                            x.LogMovementDate + " - " +
                            x.dirMovementLogType.DirMovementLogTypeName + " - " +

                            (
                            x.Msg == null ? dirMovementStatus.DirMovementStatusName
                            :
                            dirMovementStatus.DirMovementStatusName + " " +
                            x.Msg + " - "
                            ) + 

                            x.dirEmployee.DirEmployeeName
                            */
                        }
                    );

                #endregion


                #region Условия (параметры) *** *** ***


                #region DirMovementLogTypeID

                if (_params.DirMovementLogTypeID > 0)
                {
                    query = query.Where(x => x.DirMovementLogTypeID == _params.DirMovementLogTypeID);
                }

                #endregion


                #region Фильтр DirMovementLogTypeID S и Po

                if (_params.DirMovementLogTypeIDS != _params.DirMovementLogTypeIDPo)
                {
                    if (_params.DirMovementLogTypeIDS > 0) query = query.Where(x => x.DirMovementLogTypeID >= _params.DirMovementLogTypeIDS);
                    if (_params.DirMovementLogTypeIDPo > 0) query = query.Where(x => x.DirMovementLogTypeID <= _params.DirMovementLogTypeIDPo);
                }
                else
                {
                    if (_params.DirMovementLogTypeIDS > 0) query = query.Where(x => x.DirMovementLogTypeID == _params.DirMovementLogTypeIDS);
                }


                #endregion


                #region OrderBy и Лимит

                query = query.OrderByDescending(x => x.LogMovementID);

                #endregion


                #endregion


                #region Отправка JSON

                //К-во Номенклатуры
                //int dirCount = await Task.Run(() => db.LogMovements.Where(x => x.doc.DocDate >= _params.DateS && x.doc.DocDate <= _params.DatePo).Count());

                //А вдруг к-во меньше Лимита, тогда показать не общее к-во, а реальное!
                int dirCount = await query.CountAsync();
                //if (dirCount2 < _params.limit) dirCount = _params.limit * (_params.page - 1) + dirCount2;

                dynamic collectionWrapper = new
                {
                    sucess = true,
                    total = dirCount,
                    LogMovement = query
                };
                return await Task.Run(() => Ok(collectionWrapper));

                #endregion

            }
            catch (Exception ex)
            {
                return Ok(returnServer.Return(false, exceptionEntry.Return(ex)));
            }
        }

        // GET: api/LogMovements/5
        [ResponseType(typeof(LogMovement))]
        public async Task<IHttpActionResult> GetLogMovement(int id)
        {
            return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg57(0)));
        }

        #endregion


        #region UPDATE

        // PUT: api/LogMovements/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutLogMovement(int id, LogMovement logMovement)
        {
            #region Проверяем Логин и Пароль + Изменяем строку соединения + Права + Разные Функции

            //Получаем Куку
            System.Web.HttpCookie authCookie = System.Web.HttpContext.Current.Request.Cookies["CookieIPOL"];

            // Проверяем Логин и Пароль
            Classes.Account.Login.Field field = await Task.Run(() => login.Return(authCookie, true));
            if (!field.Access) return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg10));

            //Изменяем строку соединения
            db = new DbConnectionSklad(connectionString.Return(field.DirCustomersID, null, true));

            //Права (1 - Write, 2 - Read, 3 - No Access)
            //int iRight = await Task.Run(() => accessRight.Access(connectionString.Return(field.DirCustomersID, null, true), field.DirEmployeeID, "RightLogMovements"));
            //if (iRight != 1) return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg57(0)));

            //Разные Функции
            function.NumberDecimalSeparator();

            //Получам настройки
            Models.Sklad.Sys.SysSetting sysSetting = await db.SysSettings.FindAsync(1);

            #endregion

            #region Проверки

            if (!ModelState.IsValid) return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg91)); //return BadRequest(ModelState);
            if (id != logMovement.LogMovementID) return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg8)); //return BadRequest();

            //Подстановки - некоторые поля надо заполнить, если они не заполены
            //logMovement.Substitute();

            #endregion


            #region Сохранение

            try
            {
                logMovement.DirEmployeeID = field.DirEmployeeID;
                logMovement = await mPutPostLogMovements(db, logMovement, EntityState.Modified);


                #region 6. JourDisp *** *** *** *** *** *** *** *** *** *

                Models.Sklad.Sys.SysJourDisp sysJourDisp = new Models.Sklad.Sys.SysJourDisp();
                sysJourDisp.DirDispOperationID = 4; //Изменение записи
                sysJourDisp.DirEmployeeID = field.DirEmployeeID;
                sysJourDisp.ListObjectID = ListObjectID;
                sysJourDisp.TableFieldID = logMovement.LogMovementID;
                sysJourDisp.Description = "";
                try { sysJourDispsController.mPutPostSysJourDisps(db, sysJourDisp, EntityState.Added); } catch (Exception ex) { }

                #endregion


                dynamic collectionWrapper = new
                {
                    ID = logMovement.LogMovementID
                };
                return Ok(returnServer.Return(true, collectionWrapper)); //return Ok(returnServer.Return(true, ""));
            }
            catch (Exception ex)
            {
                return Ok(returnServer.Return(false, exceptionEntry.Return(ex)));
            }

            #endregion
        }

        // POST: api/LogMovements
        [ResponseType(typeof(LogMovement))]
        public async Task<IHttpActionResult> PostLogMovement(LogMovement logMovement)
        {
            #region Проверяем Логин и Пароль + Изменяем строку соединения + Права + Разные Функции

            //Получаем Куку
            System.Web.HttpCookie authCookie = System.Web.HttpContext.Current.Request.Cookies["CookieIPOL"];

            // Проверяем Логин и Пароль
            Classes.Account.Login.Field field = await Task.Run(() => login.Return(authCookie, true));
            if (!field.Access) return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg10));

            //Изменяем строку соединения
            db = new DbConnectionSklad(connectionString.Return(field.DirCustomersID, null, true));

            //Права (1 - Write, 2 - Read, 3 - No Access)
            //int iRight = await Task.Run(() => accessRight.Access(connectionString.Return(field.DirCustomersID, null, true), field.DirEmployeeID, "RightLogMovements"));
            //if (iRight != 1) return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg57(0)));

            //Разные Функции
            function.NumberDecimalSeparator();

            //Получам настройки
            Models.Sklad.Sys.SysSetting sysSetting = await db.SysSettings.FindAsync(1);

            #endregion

            #region Проверки

            if (!ModelState.IsValid) return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg91)); //return BadRequest(ModelState);

            //Подстановки - некоторые поля надо заполнить, если они не заполены
            //logMovement.Substitute();

            #endregion


            #region Сохранение

            try
            {
                logMovement.DirEmployeeID = field.DirEmployeeID;
                logMovement = await mPutPostLogMovements(db, logMovement, EntityState.Added);


                #region 6. JourDisp *** *** *** *** *** *** *** *** *** *

                Models.Sklad.Sys.SysJourDisp sysJourDisp = new Models.Sklad.Sys.SysJourDisp();
                sysJourDisp.DirDispOperationID = 3; //Добавление записи
                sysJourDisp.DirEmployeeID = field.DirEmployeeID;
                sysJourDisp.ListObjectID = ListObjectID;
                sysJourDisp.TableFieldID = logMovement.LogMovementID;
                sysJourDisp.Description = "";
                try { sysJourDispsController.mPutPostSysJourDisps(db, sysJourDisp, EntityState.Added); } catch (Exception ex) { }

                #endregion


                dynamic collectionWrapper = new
                {
                    ID = logMovement.LogMovementID
                };
                return Ok(returnServer.Return(true, collectionWrapper)); //return Ok(returnServer.Return(true, ""));
            }
            catch (Exception ex)
            {
                return Ok(returnServer.Return(false, exceptionEntry.Return(ex)));
            }

            #endregion
        }

        // DELETE: api/LogMovements/5
        [ResponseType(typeof(LogMovement))]
        public async Task<IHttpActionResult> DeleteLogMovement(int id)
        {
            #region Проверяем Логин и Пароль + Изменяем строку соединения + Права + Разные Функции

            //Получаем Куку
            System.Web.HttpCookie authCookie = System.Web.HttpContext.Current.Request.Cookies["CookieIPOL"];

            // Проверяем Логин и Пароль
            Classes.Account.Login.Field field = await Task.Run(() => login.Return(authCookie, true));
            if (!field.Access) return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg10));

            //Изменяем строку соединения
            db = new DbConnectionSklad(connectionString.Return(field.DirCustomersID, null, true));

            //Права (1 - Write, 2 - Read, 3 - No Access)
            //int iRight = await Task.Run(() => accessRight.Access(connectionString.Return(field.DirCustomersID, null, true), field.DirEmployeeID, "RightLogMovements"));
            //if (iRight != 1) return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg57(0)));

            //Разные Функции
            function.NumberDecimalSeparator();

            //Получам настройки
            Models.Sklad.Sys.SysSetting sysSetting = await db.SysSettings.FindAsync(1);

            #endregion

            #region Удаление

            try
            {
                LogMovement logMovement = await db.LogMovements.FindAsync(id);
                if (logMovement == null) return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg99));

                db.LogMovements.Remove(logMovement);
                await db.SaveChangesAsync();


                #region 6. JourDisp *** *** *** *** *** *** *** *** *** *

                Models.Sklad.Sys.SysJourDisp sysJourDisp = new Models.Sklad.Sys.SysJourDisp();
                sysJourDisp.DirDispOperationID = 5; //Удаление записи
                sysJourDisp.DirEmployeeID = field.DirEmployeeID;
                sysJourDisp.ListObjectID = ListObjectID;
                sysJourDisp.TableFieldID = logMovement.LogMovementID;
                sysJourDisp.Description = "";
                try { sysJourDispsController.mPutPostSysJourDisps(db, sysJourDisp, EntityState.Added); } catch (Exception ex) { }

                #endregion


                dynamic collectionWrapper = new
                {
                    ID = logMovement.LogMovementID,
                    Msg = Classes.Language.Sklad.Language.msg19
                };
                return Ok(returnServer.Return(true, collectionWrapper)); //return Ok(returnServer.Return(true, "")
            }
            catch (Exception ex)
            {
                return Ok(returnServer.Return(false, exceptionEntry.Return(ex)));
            }

            #endregion
        }

        #endregion


        #region Mthods

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool LogMovementExists(int id)
        {
            return db.LogMovements.Count(e => e.LogMovementID == id) > 0;
        }


        internal async Task<LogMovement> mPutPostLogMovements(
                    DbConnectionSklad db,
                    LogMovement logMovement,
                    EntityState entityState //EntityState.Added, Modified
                    )
        {
            //logMovement.LogMovementDate = Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd h:mm tt"));
            logMovement.LogMovementDate = Convert.ToDateTime(DateTime.Now); //.ToString("yyyy-MM-dd hh:mm")
            db.Entry(logMovement).State = entityState;
            await db.SaveChangesAsync();

            //if (dirNomen.DirNomenArticle == null) dirNomen.DirNomenArticle = dirNomen.DirNomenID.ToString();

            return logMovement;
        }

        #endregion
    }
}