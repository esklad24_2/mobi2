﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using PartionnyAccount.Models;
using PartionnyAccount.Models.Sklad.Doc;
using System.Data.SQLite;
using System.Web.Script.Serialization;

namespace PartionnyAccount.Controllers.Sklad.Doc.DocOrderInts
{
    public class DocOrderIntsController : ApiController
    {
        #region Classes

        Classes.Function.Exceptions.ExceptionEntry exceptionEntry = new Classes.Function.Exceptions.ExceptionEntry();
        Classes.Function.Variables.ConnectionString connectionString = new Classes.Function.Variables.ConnectionString();
        Classes.Account.Login login = new Classes.Account.Login();
        Classes.Account.AccessRight accessRight = new Classes.Account.AccessRight();
        Classes.Function.ReturnServer returnServer = new Classes.Function.ReturnServer();
        Classes.Function.Function function = new Classes.Function.Function();
        Models.Sklad.Sys.SysJourDisp sysJourDisp = new Models.Sklad.Sys.SysJourDisp(); Controllers.Sklad.Sys.SysJourDispsController sysJourDispsController = new Sys.SysJourDispsController();
        Models.Sklad.Sys.SysSetting sysSetting = new Models.Sklad.Sys.SysSetting();
        Models.Sklad.Log.LogOrderInt logOrderInt = new Models.Sklad.Log.LogOrderInt(); Controllers.Sklad.Log.LogOrderIntsController logOrderIntsController = new Log.LogOrderIntsController();
        private DbConnectionSklad db = new DbConnectionSklad();
        private DbConnectionSklad dbRead = new DbConnectionSklad();

        int ListObjectID = 59;

        #endregion


        #region SELECT

        class Params
        {
            //Parameters
            public int limit = 11;
            public int page = 1;
            public int Skip = 0;
            public int GroupID = 0;
            public string parSearch = "";
            public int FilterType; // == DirOrderIntStatusID
            public int? DirWarehouseID;

            public DateTime? DateS;
            public DateTime? DatePo;

            public int DirOrderIntStatusIDS;
            public int DirOrderIntStatusIDPo;

            public int DocOrderIntTypeS;
            public int DocOrderIntTypePo;

            public int DirEmployeeID;
        }
        // GET: api/DocOrderInts
        public async Task<IHttpActionResult> GetDocOrderInts(HttpRequestMessage request)
        {
            try
            {
                #region Проверяем Логин и Пароль + Изменяем строку соединения + Права + Разные Функции

                //Получаем Куку
                System.Web.HttpCookie authCookie = System.Web.HttpContext.Current.Request.Cookies["CookieIPOL"];

                // Проверяем Логин и Пароль
                Classes.Account.Login.Field field = await Task.Run(() => login.Return(authCookie, true));
                if (!field.Access) return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg10));

                //Изменяем строку соединения
                db = new DbConnectionSklad(connectionString.Return(field.DirCustomersID, null, true));

                //Права (1 - Write, 2 - Read, 3 - No Access)
                /*
                int iRight = await Task.Run(() => accessRight.Access(connectionString.Return(field.DirCustomersID, null, true), field.DirEmployeeID, "RightDocOrderInts"));
                if (iRight == 3) return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg57(0)));
                */

                //Разные Функции
                function.NumberDecimalSeparator();

                //Получам настройки
                Models.Sklad.Sys.SysSetting sysSetting = await db.SysSettings.FindAsync(1);

                //Получаем сотрудника: если к нему привязан Склад и/или Организация, то выбираем документы только по этим характеристикам
                Models.Sklad.Dir.DirEmployee dirEmployee = await db.DirEmployees.FindAsync(field.DirEmployeeID);

                #endregion


                #region Параметры

                Params _params = new Params();

                //paramList -список параметров
                var paramList = request.GetQueryNameValuePairs();
                //Параметры
                _params.limit = sysSetting.PageSizeJurn; //Convert.ToInt32(paramList.FirstOrDefault(kv => string.Compare(kv.Key, "limit", true) == 0).Value); //Записей на страницу
                _params.page = Convert.ToInt32(paramList.FirstOrDefault(kv => string.Compare(kv.Key, "page", true) == 0).Value);   //Номер страницы
                _params.Skip = _params.limit * (_params.page - 1);
                _params.GroupID = Convert.ToInt32(paramList.FirstOrDefault(kv => string.Compare(kv.Key, "pGroupID", true) == 0).Value); //Кликнули по группе
                _params.parSearch = paramList.FirstOrDefault(kv => string.Compare(kv.Key, "parSearch", true) == 0).Value; if (_params.parSearch != null) _params.parSearch = _params.parSearch.ToLower(); //Поиск
                _params.FilterType = Convert.ToInt32(paramList.FirstOrDefault(kv => string.Compare(kv.Key, "FilterType", true) == 0).Value);
                _params.DirOrderIntStatusIDS = Convert.ToInt32(paramList.FirstOrDefault(kv => string.Compare(kv.Key, "DirOrderIntStatusIDS", true) == 0).Value);
                _params.DirOrderIntStatusIDPo = Convert.ToInt32(paramList.FirstOrDefault(kv => string.Compare(kv.Key, "DirOrderIntStatusIDPo", true) == 0).Value);
                _params.DocOrderIntTypeS = Convert.ToInt32(paramList.FirstOrDefault(kv => string.Compare(kv.Key, "DocOrderIntTypeS", true) == 0).Value);
                _params.DocOrderIntTypePo = Convert.ToInt32(paramList.FirstOrDefault(kv => string.Compare(kv.Key, "DocOrderIntTypePo", true) == 0).Value);
                _params.DirEmployeeID = Convert.ToInt32(paramList.FirstOrDefault(kv => string.Compare(kv.Key, "DirEmployeeID", true) == 0).Value);
                _params.DirWarehouseID = Convert.ToInt32(paramList.FirstOrDefault(kv => string.Compare(kv.Key, "DirWarehouseID", true) == 0).Value);

                string sDate = paramList.FirstOrDefault(kv => string.Compare(kv.Key, "DateS", true) == 0).Value;
                if (!String.IsNullOrEmpty(sDate))
                {
                    _params.DateS = Convert.ToDateTime(Convert.ToDateTime(sDate).ToString("yyyy-MM-dd 00:00:01"));
                    if (_params.DateS < Convert.ToDateTime("01.01.1800")) _params.DateS = Convert.ToDateTime(sysSetting.JurDateS.ToString("yyyy-MM-dd 00:00:00")).AddDays(-1);
                    else _params.DateS = _params.DateS.Value.AddDays(-1);
                }

                sDate = paramList.FirstOrDefault(kv => string.Compare(kv.Key, "DatePo", true) == 0).Value;
                if (!String.IsNullOrEmpty(sDate))
                {
                    _params.DatePo = Convert.ToDateTime(Convert.ToDateTime(sDate).ToString("yyyy-MM-dd 23:59:59"));
                    if (_params.DatePo < Convert.ToDateTime("01.01.1800")) _params.DatePo = Convert.ToDateTime(sysSetting.JurDatePo.ToString("yyyy-MM-dd 23:59:59"));
                }

                #endregion



                #region Основной запрос *** *** ***

                var query =
                    (
                        from x in db.DocOrderInts

                        select new
                        {
                            DocOrderIntID = x.DocOrderIntID,
                            DocDate = x.doc.DocDate,
                            DirContractorIDOrg = x.doc.DirContractorIDOrg,

                            DirEmployeeID = x.doc.DirEmployeeID,
                            DirEmployeeName = x.doc.dirEmployee.DirEmployeeName,

                            DirOrderIntStatusID = x.DirOrderIntStatusID,
                            DirOrderIntStatusName = x.dirOrderIntStatus.DirOrderIntStatusName,

                            DirOrderIntTypeID = x.DirOrderIntTypeID,
                            DirOrderIntTypeName = x.dirOrderIntType.DirOrderIntTypeName,

                            DirWarehouseID = x.DirWarehouseID,
                            DirWarehouseName = x.dirWarehouse.DirWarehouseName,

                            Description = x.doc.Description,


                            //Новый товар или существующий
                            NomenExist = x.DirNomenID == null ? "<b>Новый</b>"
                            :
                            "Существующий",


                            //DirNomenName = x.DirNomenName,

                            //Полное наименование товара/запчасти
                            /*
                            DirNomenXName6 =
                            x.DirNomen6Name == null ?
                            (
                                x.DirNomen5Name == null ?
                                (
                                    x.DirNomen4Name == null ?
                                    (
                                        x.DirNomen3Name == null ? 
                                        (
                                            x.DirNomen2Name == null ? x.DirNomen1Name
                                            :
                                            x.DirNomen1Name + " / " + x.DirNomen2Name
                                        ) 
                                        :
                                        x.DirNomen1Name + " / " + x.DirNomen2Name + " / " + x.DirNomen3Name
                                    )
                                    :
                                    x.DirNomen1Name + " / " + x.DirNomen2Name + " / " + x.DirNomen3Name + " / " + x.DirNomen4Name
                                )
                                :
                                x.DirNomen1Name + " / " + x.DirNomen2Name + " / " + x.DirNomen3Name + " / " + x.DirNomen4Name + " / " + x.DirNomen5Name
                            )
                            :
                            x.DirNomen1Name + " / " + x.DirNomen2Name + " / " + x.DirNomen3Name + " / " + x.DirNomen4Name + " / " + x.DirNomen5Name + " / " + x.DirNomen6Name,
                            */

                            DirNomenXName6 = x.dirNomen1.DirNomenName + " / " + x.dirNomen2.DirNomenName + "/" + x.dirNomenCategory.DirNomenCategoryName,


                            //Клиент
                            DirOrderIntContractorPhone = x.DirOrderIntContractorPhone,
                            DirOrderIntContractorName = x.DirOrderIntContractorName,

                            PrepaymentSum = x.PrepaymentSum,

                            //Дата.исполнения
                            DateDone = x.DateDone,

                        }
                    );

                #endregion


                #region Условия (параметры) *** *** ***


                #region dirEmployee: dirEmployee.DirWarehouseID and/or dirEmployee.DirContractorIDOrg

                if (_params.DirWarehouseID != null && _params.DirWarehouseID > 0)
                {
                    query = query.Where(x => x.DirWarehouseID == _params.DirWarehouseID);
                }

                if (dirEmployee.DirContractorIDOrg != null && dirEmployee.DirContractorIDOrg > 0)
                {
                    query = query.Where(x => x.DirContractorIDOrg == dirEmployee.DirContractorIDOrg);
                }

                #endregion


                #region Не показывать удалённые

                if (!Convert.ToBoolean(sysSetting.DeletedRecordsShow))
                {
                    //query = query.Where(x => x.Del == sysSetting.DeletedRecordsShow);
                }

                #endregion


                #region Фильтр "FilterType"

                if (_params.FilterType > 0)
                {
                    query = query.Where(x => x.DirOrderIntStatusID == _params.FilterType);
                }

                #endregion

                #region Фильтр "DocOrderIntType"

                //DocOrderIntType
                if (_params.DocOrderIntTypeS != _params.DocOrderIntTypePo)
                {
                    if (_params.DocOrderIntTypeS > 0) query = query.Where(x => x.DirOrderIntTypeID >= _params.DocOrderIntTypeS);
                    if (_params.DocOrderIntTypePo > 0) query = query.Where(x => x.DirOrderIntTypeID <= _params.DocOrderIntTypePo);
                }
                else
                {
                    if (_params.DocOrderIntTypeS > 0) query = query.Where(x => x.DirOrderIntTypeID == _params.DocOrderIntTypeS);
                }

                //DirOrderIntStatusID
                if (_params.DirOrderIntStatusIDS != _params.DirOrderIntStatusIDPo)
                {
                    if (_params.DirOrderIntStatusIDS > 0) query = query.Where(x => x.DirOrderIntStatusID >= _params.DirOrderIntStatusIDS);
                    if (_params.DirOrderIntStatusIDPo > 0) query = query.Where(x => x.DirOrderIntStatusID <= _params.DirOrderIntStatusIDPo);
                }
                else
                {
                    if (_params.DirOrderIntStatusIDS > 0) query = query.Where(x => x.DirOrderIntStatusID == _params.DirOrderIntStatusIDS);

                    //Кроме Архива
                    if (_params.DirOrderIntStatusIDS <= 0 && _params.DirOrderIntStatusIDPo <= 0)
                    {
                        query = query.Where(x => x.DirOrderIntStatusID != 4 && x.DirOrderIntStatusID != 5);
                    }
                }

                //DirEmployeeID
                if (_params.DirEmployeeID > 0)
                {
                    query = query.Where(x => x.DirEmployeeID <= _params.DirEmployeeID); // && x.DirOrderIntStatusID < 7
                }

                #endregion


                #region Поиск

                if (!String.IsNullOrEmpty(_params.parSearch))
                {
                    //Проверяем число ли это
                    Int32 iNumber32;
                    bool bResult32 = Int32.TryParse(_params.parSearch, out iNumber32);
                    DateTime dDateTime;
                    bool bDateTime = DateTime.TryParse(_params.parSearch, out dDateTime);


                    //Если число, то задействуем в поиске и числовые поля (_params.parSearch == iNumber)
                    if (bResult32)
                    {
                        query = query.Where(x => x.DocOrderIntID == iNumber32 || x.DirWarehouseName.Contains(_params.parSearch));
                    }
                    //Если Дата
                    else if (bDateTime)
                    {
                        query = query.Where(x => x.DocDate == dDateTime);
                    }
                    //Иначе, только текстовые поля
                    else
                    {
                        query = query.Where(x => x.DirWarehouseName.Contains(_params.parSearch));
                    }
                }

                #endregion


                #region OrderBy и Лимит

                //query = query.OrderByDescending(x => x.DocDate).Skip(_params.Skip).Take(_params.limit);
                query = query.OrderByDescending(x => x.DocOrderIntID); //.Skip(_params.Skip).Take(_params.limit);

                #endregion


                #endregion


                #region Отправка JSON

                //К-во Номенклатуры
                //int dirCount = await Task.Run(() => db.DocOrderInts.Count());

                //А вдруг к-во меньше Лимита, тогда показать не общее к-во, а реальное!
                int dirCount = query.Count();
                //if (dirCount2 < _params.limit) dirCount = _params.limit * (_params.page - 1) + dirCount2;

                dynamic collectionWrapper = new
                {
                    sucess = true,
                    total = dirCount,
                    DocOrderInt = query
                };
                return await Task.Run(() => Ok(collectionWrapper));

                #endregion

            }
            catch (Exception ex)
            {
                return Ok(returnServer.Return(false, exceptionEntry.Return(ex)));
            }
        }

        // GET: api/DocOrderInts/5
        [ResponseType(typeof(DocOrderInt))]
        public async Task<IHttpActionResult> GetDocOrderInt(int id, HttpRequestMessage request)
        {
            try
            {
                #region Проверяем Логин и Пароль + Изменяем строку соединения + Права + Разные Функции

                //Получаем Куку
                System.Web.HttpCookie authCookie = System.Web.HttpContext.Current.Request.Cookies["CookieIPOL"];

                // Проверяем Логин и Пароль
                Classes.Account.Login.Field field = await Task.Run(() => login.Return(authCookie, true));
                if (!field.Access) return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg10));

                //Изменяем строку соединения
                db = new DbConnectionSklad(connectionString.Return(field.DirCustomersID, null, true));

                //Права
                /*int Status = await Task.Run(() => accessRight.Access(db, field.DirEmployeeID, "DocOrderInt"));
                if (Status >= 3) return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg57(0)));*/

                //Права (1 - Write, 2 - Read, 3 - No Access)
                int iRight = await Task.Run(() => accessRight.Access(connectionString.Return(field.DirCustomersID, null, true), field.DirEmployeeID, "RightDocOrderInts"));
                if (iRight == 3) return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg57(0)));

                //Разные Функции
                function.NumberDecimalSeparator();

                //Получам настройки
                Models.Sklad.Sys.SysSetting sysSetting = await db.SysSettings.FindAsync(1);

                #endregion


                #region Параметры

                Params _params = new Params();

                //paramList -список параметров
                var paramList = request.GetQueryNameValuePairs();
                //Параметры
                _params.parSearch = paramList.FirstOrDefault(kv => string.Compare(kv.Key, "parSearch", true) == 0).Value; if (_params.parSearch != null) _params.parSearch = _params.parSearch.ToLower(); //Поиск

                #endregion


                #region Отправка JSON

                var query = await Task.Run(() =>
                    (

                        from x in db.DocOrderInts
                        where x.DocOrderIntID == id
                        select new
                        {
                            DocOrderIntID = x.DocOrderIntID,
                            DocDate = x.doc.DocDate,

                            DirEmployeeName = x.doc.dirEmployee.DirEmployeeName,
                            DirOrderIntStatusName = x.dirOrderIntStatus.DirOrderIntStatusName,
                            Description = x.doc.Description,
                            DirWarehouseName = x.dirWarehouse.DirWarehouseName,


                            DocID = x.DocID,
                            DirOrderIntTypeID = x.DirOrderIntTypeID,

                            //Документ который создал этот Заказ
                            DocID2 = x.DocID2,

                            DirWarehouseID = x.DirWarehouseID,
                            DirOrderIntStatusID = x.DirOrderIntStatusID,

                            //Товар
                            DirNomenID = x.DirNomenID,

                            //Группы товара и чсам товар
                            DirNomen1ID = x.DirNomen1ID,
                            DirNomen2ID = x.DirNomen2ID,
                            DirNomenCategoryID = x.DirNomenCategoryID,
                            //DirNomen3Name = x.DirNomen3Name,
                            //DirNomen4ID = x.DirNomen4ID,
                            //DirNomen4Name = x.DirNomen4Name,
                            //DirNomen5ID = x.DirNomen5ID,
                            //DirNomen5Name = x.DirNomen5Name,
                            //DirNomen6ID = x.DirNomen6ID,
                            //DirNomen6Name = x.DirNomen6Name,

                            PriceVAT = x.PriceVAT,
                            PriceCurrency = x.PriceCurrency,
                            DirCurrencyID = x.DirCurrencyID,
                            DirCurrencyRate = x.DirCurrencyRate,
                            DirCurrencyMultiplicity = x.DirCurrencyMultiplicity,
                            Quantity = x.Quantity,

                            //Характеристики
                            DirCharColourID = x.DirCharColourID,
                            DirCharMaterialID = x.DirCharMaterialID,
                            DirCharNameID = x.DirCharNameID,
                            DirCharSeasonID = x.DirCharSeasonID,
                            DirCharSexID = x.DirCharSexID,
                            DirCharSizeID = x.DirCharSizeID,
                            DirCharStyleID = x.DirCharStyleID,
                            DirCharTextureID = x.DirCharTextureID,


                            //Полное наименование товара/запчасти
                            /*
                            DirNomenXName6 =
                            x.DirNomen6Name == null ?
                            (
                                x.DirNomen5Name == null ?
                                (
                                    x.DirNomen4Name == null ?
                                    (
                                        x.DirNomen3Name == null ?
                                        (
                                            x.DirNomen2Name == null ? x.DirNomen1Name
                                            :
                                            x.DirNomen1Name + " / " + x.DirNomen2Name
                                        )
                                        :
                                        x.DirNomen1Name + " / " + x.DirNomen2Name + " / " + x.DirNomen3Name
                                    )
                                    :
                                    x.DirNomen1Name + " / " + x.DirNomen2Name + " / " + x.DirNomen3Name + " / " + x.DirNomen4Name
                                )
                                :
                                x.DirNomen1Name + " / " + x.DirNomen2Name + " / " + x.DirNomen3Name + " / " + x.DirNomen4Name + " / " + x.DirNomen5Name
                            )
                            :
                            x.DirNomen1Name + " / " + x.DirNomen2Name + " / " + x.DirNomen3Name + " / " + x.DirNomen4Name + " / " + x.DirNomen5Name + " / " + x.DirNomen6Name,
                            */

                            DirNomenXName6 = x.dirNomen1.DirNomenName + " / " + x.dirNomen1.DirNomenName + "/" + x.dirNomenCategory.DirNomenCategoryName,


                            //Клиент
                            DirOrderIntContractorPhone = x.DirOrderIntContractorPhone,
                            DirOrderIntContractorName = x.DirOrderIntContractorName,

                            PrepaymentSum = x.PrepaymentSum,

                            //Дата.исполнения
                            DateDone = x.DateDone,

                        }

                    ).ToListAsync());


                if (query.Count() > 0)
                {
                    return Ok(returnServer.Return(true, query[0]));
                }
                else
                {
                    return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg89));
                }

                //return Ok(returnServer.Return(false, Classes.Language.Language.msg89));

                #endregion

            }
            catch (Exception ex)
            {
                return Ok(returnServer.Return(false, exceptionEntry.Return(ex)));
            }
        }

        #endregion


        #region UPDATE

        // PUT: api/DocOrderInts/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutDocOrderInt(int id, DocOrderInt docOrderInt, HttpRequestMessage request)
        {
            try
            {
                #region Проверяем Логин и Пароль + Изменяем строку соединения + Права + Разные Функции

                //Получаем Куку
                System.Web.HttpCookie authCookie = System.Web.HttpContext.Current.Request.Cookies["CookieIPOL"];

                // Проверяем Логин и Пароль
                Classes.Account.Login.Field field = await Task.Run(() => login.Return(authCookie, true));
                if (!field.Access) return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg10));

                //Изменяем строку соединения
                db = new DbConnectionSklad(connectionString.Return(field.DirCustomersID, null, true));
                dbRead = new DbConnectionSklad(connectionString.Return(field.DirCustomersID, null, true));

                //Права (1 - Write, 2 - Read, 3 - No Access)
                int iRight = await Task.Run(() => accessRight.Access(connectionString.Return(field.DirCustomersID, null, true), field.DirEmployeeID, "RightDocOrderInts"));
                if (iRight != 1) return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg57(0)));

                //Разные Функции
                function.NumberDecimalSeparator();

                //Получам настройки
                sysSetting = await db.SysSettings.FindAsync(1);

                #endregion

                #region Параметры

                var paramList = request.GetQueryNameValuePairs();


                docOrderInt.DocIDBase = docOrderInt.DocID2;


                #endregion


                #region Сохранение

                try
                {
                    using (System.Data.Entity.DbContextTransaction ts = db.Database.BeginTransaction())
                    {
                        db.Configuration.AutoDetectChangesEnabled = false;

                        try
                        {
                            //Используем метод, что бы было всё в одном потоке
                            docOrderInt = await Task.Run(() => mPutPostdocOrderInt(db, ts, dbRead, docOrderInt, EntityState.Modified, field)); //sysSetting
                            ts.Commit(); //.Complete();
                        }
                        catch (Exception ex)
                        {
                            try { ts.Rollback(); ts.Dispose(); } catch { }
                            try { db.Database.Connection.Close(); db.Database.Connection.Dispose(); } catch { }

                            return Ok(returnServer.Return(false, exceptionEntry.Return(ex)));
                        }
                    }


                    #region 6. JourDisp *** *** *** *** *** *** *** *** *** *

                    Models.Sklad.Sys.SysJourDisp sysJourDisp = new Models.Sklad.Sys.SysJourDisp();
                    sysJourDisp.DirDispOperationID = 4; //Изменение записи
                    sysJourDisp.DirEmployeeID = field.DirEmployeeID;
                    sysJourDisp.ListObjectID = ListObjectID;
                    sysJourDisp.TableFieldID = docOrderInt.DocOrderIntID;
                    sysJourDisp.Description = "";
                    try { sysJourDispsController.mPutPostSysJourDisps(db, sysJourDisp, EntityState.Added); } catch (Exception ex) { }

                    #endregion


                    dynamic collectionWrapper = new
                    {
                        DocID = docOrderInt.DocID,
                        DocOrderIntID = docOrderInt.DocOrderIntID
                    };
                    return Ok(returnServer.Return(true, collectionWrapper)); //return Ok(returnServer.Return(true, ""));
                }
                catch (Exception ex)
                {
                    return Ok(returnServer.Return(false, exceptionEntry.Return(ex)));
                }

                #endregion
            }
            catch (Exception ex)
            {
                return Ok(returnServer.Return(false, exceptionEntry.Return(ex)));
            }
        }

        //Смена статуса
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutDocOrderInt(int id, int DirStatusID, HttpRequestMessage request)
        {
            try
            {
                #region Проверяем Логин и Пароль + Изменяем строку соединения + Права + Разные Функции

                //Получаем Куку
                System.Web.HttpCookie authCookie = System.Web.HttpContext.Current.Request.Cookies["CookieIPOL"];

                // Проверяем Логин и Пароль
                Classes.Account.Login.Field field = await Task.Run(() => login.Return(authCookie, true));
                if (!field.Access) return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg10));

                //Изменяем строку соединения
                db = new DbConnectionSklad(connectionString.Return(field.DirCustomersID, null, true));
                dbRead = new DbConnectionSklad(connectionString.Return(field.DirCustomersID, null, true));

                //Права (1 - Write, 2 - Read, 3 - No Access)
                int iRight = await Task.Run(() => accessRight.Access(connectionString.Return(field.DirCustomersID, null, true), field.DirEmployeeID, "RightDocOrderInts"));
                if (iRight != 1) return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg57(0)));

                //Разные Функции
                function.NumberDecimalSeparator();

                //Получам настройки
                sysSetting = await db.SysSettings.FindAsync(1);

                #endregion

                #region Параметры

                Params _params = new Params();

                //paramList -список параметров
                var paramList = request.GetQueryNameValuePairs();
                //Параметры
                int DirPaymentTypeID = Convert.ToInt32(paramList.FirstOrDefault(kv => string.Compare(kv.Key, "DirPaymentTypeID", true) == 0).Value);

                #endregion

                #region Проверки
                
                //...
                
                #endregion


                #region Сохранение

                try
                {
                    Models.Sklad.Doc.DocOrderInt docOrderInt = new DocOrderInt();

                    using (System.Data.Entity.DbContextTransaction ts = db.Database.BeginTransaction())
                    {
                        db.Configuration.AutoDetectChangesEnabled = false;

                        try
                        {
                            await mStatusChange(db, ts, docOrderInt, id, DirStatusID, DirPaymentTypeID, field);

                            ts.Commit();
                        }
                        catch (Exception ex)
                        {
                            try { ts.Rollback(); ts.Dispose(); } catch { }
                            try { db.Database.Connection.Close(); db.Database.Connection.Dispose(); } catch { }

                            return Ok(returnServer.Return(false, exceptionEntry.Return(ex)));
                        }
                    }

                    dynamic collectionWrapper = new
                    {
                        DocID = docOrderInt.DocID,
                        DocOrderIntID = docOrderInt.DocOrderIntID
                    };
                    return Ok(returnServer.Return(true, collectionWrapper));
                }
                catch (Exception ex)
                {
                    return Ok(returnServer.Return(false, exceptionEntry.Return(ex)));
                }

                #endregion
            }
            catch (Exception ex)
            {
                return Ok(returnServer.Return(false, exceptionEntry.Return(ex)));
            }
        }


        // POST: api/DocOrderInts
        [ResponseType(typeof(DocOrderInt))]
        public async Task<IHttpActionResult> PostDocOrderInt(DocOrderInt docOrderInt, HttpRequestMessage request)
        {
            #region Проверяем Логин и Пароль + Изменяем строку соединения + Права + Разные Функции

            //Получаем Куку
            System.Web.HttpCookie authCookie = System.Web.HttpContext.Current.Request.Cookies["CookieIPOL"];

            // Проверяем Логин и Пароль
            Classes.Account.Login.Field field = await Task.Run(() => login.Return(authCookie, true));
            if (!field.Access) return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg10));

            //Изменяем строку соединения
            db = new DbConnectionSklad(connectionString.Return(field.DirCustomersID, null, true));
            dbRead = new DbConnectionSklad(connectionString.Return(field.DirCustomersID, null, true));

            //Права (1 - Write, 2 - Read, 3 - No Access)
            int iRight = await Task.Run(() => accessRight.Access(connectionString.Return(field.DirCustomersID, null, true), field.DirEmployeeID, "RightDocOrderInts"));
            if (iRight != 1) return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg57(0)));

            //Разные Функции
            function.NumberDecimalSeparator();

            //Получам настройки
            sysSetting = await db.SysSettings.FindAsync(1);

            #endregion

            #region Параметры

            //var paramList = request.GetQueryNameValuePairs();

            //Docs.DocIDBase = DocOrderInt.DocID2
            docOrderInt.DocIDBase = docOrderInt.DocID2;
            docOrderInt.DirOrderIntStatusID = 1;

            #endregion

            #region Проверки

            if (!ModelState.IsValid) return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg91)); //return BadRequest(ModelState);

            //Подстановки - некоторые поля надо заполнить, если они не заполены
            docOrderInt.Substitute();

            #endregion


            #region Сохранение

            try
            {
                using (System.Data.Entity.DbContextTransaction ts = db.Database.BeginTransaction())
                {
                    db.Configuration.AutoDetectChangesEnabled = false;

                    try
                    {
                        //Используем метод, что бы было всё в одном потоке
                        docOrderInt = await Task.Run(() => mPutPostdocOrderInt(db, ts, dbRead, docOrderInt, EntityState.Added, field)); //sysSetting
                        ts.Commit(); //.Complete();
                    }
                    catch (Exception ex)
                    {
                        try { ts.Rollback(); ts.Dispose(); } catch { }
                        try { db.Database.Connection.Close(); db.Database.Connection.Dispose(); } catch { }

                        return Ok(returnServer.Return(false, exceptionEntry.Return(ex)));
                    }
                }


                #region 6. JourDisp *** *** *** *** *** *** *** *** *** *

                Models.Sklad.Sys.SysJourDisp sysJourDisp = new Models.Sklad.Sys.SysJourDisp();
                sysJourDisp.DirDispOperationID = 3; //Добавление записи
                sysJourDisp.DirEmployeeID = field.DirEmployeeID;
                sysJourDisp.ListObjectID = ListObjectID;
                sysJourDisp.TableFieldID = docOrderInt.DocOrderIntID;
                sysJourDisp.Description = "";
                try { sysJourDispsController.mPutPostSysJourDisps(db, sysJourDisp, EntityState.Added); } catch (Exception ex) { }

                #endregion


                dynamic collectionWrapper = new
                {
                    DocID = docOrderInt.DocID,
                    DocOrderIntID = docOrderInt.DocOrderIntID
                };
                return Ok(returnServer.Return(true, collectionWrapper)); //return Ok(returnServer.Return(true, ""));
            }
            catch (Exception ex)
            {
                return Ok(returnServer.Return(false, exceptionEntry.Return(ex)));
            }

            #endregion
        }

        // DELETE: api/DocOrderInts/5
        [ResponseType(typeof(DocOrderInt))]
        public async Task<IHttpActionResult> DeleteDocOrderInt(int id)
        {
            #region Проверяем Логин и Пароль + Изменяем строку соединения + Права + Разные Функции

            //Получаем Куку
            System.Web.HttpCookie authCookie = System.Web.HttpContext.Current.Request.Cookies["CookieIPOL"];

            // Проверяем Логин и Пароль
            Classes.Account.Login.Field field = await Task.Run(() => login.Return(authCookie, true));
            if (!field.Access) return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg10));

            //Изменяем строку соединения
            db = new DbConnectionSklad(connectionString.Return(field.DirCustomersID, null, true));

            //Права
            /*int Status = await Task.Run(() => accessRight.Access(db, field.DirEmployeeID, "DocOrderInt"));
            if (Status >= 3) return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg57(0)));*/

            //Права (1 - Write, 2 - Read, 3 - No Access)
            int iRight = await Task.Run(() => accessRight.Access(connectionString.Return(field.DirCustomersID, null, true), field.DirEmployeeID, "RightDocOrderInts"));
            if (iRight != 1) return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg57(0)));

            //Разные Функции
            function.NumberDecimalSeparator();

            //Получам настройки
            Models.Sklad.Sys.SysSetting sysSetting = await db.SysSettings.FindAsync(1);

            #endregion

            #region Удаление

            try
            {
                DocOrderInt docOrderInt = await db.DocOrderInts.FindAsync(id);
                if (docOrderInt == null) return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg99));


                db.DocOrderInts.Remove(docOrderInt);
                await db.SaveChangesAsync();


                #region 6. JourDisp *** *** *** *** *** *** *** *** *** *

                Models.Sklad.Sys.SysJourDisp sysJourDisp = new Models.Sklad.Sys.SysJourDisp();
                sysJourDisp.DirDispOperationID = 5; //Удаление записи
                sysJourDisp.DirEmployeeID = field.DirEmployeeID;
                sysJourDisp.ListObjectID = ListObjectID;
                sysJourDisp.TableFieldID = id;
                sysJourDisp.Description = "";
                try { sysJourDispsController.mPutPostSysJourDisps(db, sysJourDisp, EntityState.Added); } catch (Exception ex) { }

                #endregion


                dynamic collectionWrapper = new
                {
                    ID = docOrderInt.DocOrderIntID,
                    Msg = Classes.Language.Sklad.Language.msg19
                };
                return Ok(returnServer.Return(true, collectionWrapper)); //return Ok(returnServer.Return(true, "")
            }
            catch (Exception ex)
            {
                return Ok(returnServer.Return(false, exceptionEntry.Return(ex)));
            }

            #endregion

        }

        #endregion


        #region Mthods

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool DocOrderIntExists(int id)
        {
            return db.DocOrderInts.Count(e => e.DocOrderIntID == id) > 0;
        }


        internal async Task<DocOrderInt> mPutPostdocOrderInt(
            DbConnectionSklad db,
            System.Data.Entity.DbContextTransaction ts,
            DbConnectionSklad dbRead,
            DocOrderInt docOrderInt,
            EntityState entityState, //EntityState.Added, Modified

            Classes.Account.Login.Field field //Дополнительные данные о сотруднике
            )
        {

            #region 0. Заполняем DirOrderIntContractors
            // - не находим - создаём новую
            // - находим - обновляем

            Models.Sklad.Dir.DirOrderIntContractor dirOrderIntContractor = new Models.Sklad.Dir.DirOrderIntContractor();

            string DirOrderIntContractorPhone = docOrderInt.DirOrderIntContractorPhone.Replace("+", "").ToLower();
            if (docOrderInt.DirOrderIntContractorName != null)
            {
                if (!String.IsNullOrEmpty(DirOrderIntContractorPhone))
                {
                    var queryDirOrderIntContractors = await
                        (
                            from x in db.DirOrderIntContractors
                            where x.DirOrderIntContractorPhone == DirOrderIntContractorPhone
                            select x
                        ).ToListAsync();
                    if (queryDirOrderIntContractors.Count() == 0)
                    {
                        dirOrderIntContractor = new Models.Sklad.Dir.DirOrderIntContractor();
                        dirOrderIntContractor.DirOrderIntContractorPhone = DirOrderIntContractorPhone;
                        dirOrderIntContractor.DirOrderIntContractorName = docOrderInt.DirOrderIntContractorName;
                        dirOrderIntContractor.QuantityOk = 0;
                        dirOrderIntContractor.QuantityFail = 0;
                        dirOrderIntContractor.QuantityCount = 0;

                        db.Entry(dirOrderIntContractor).State = EntityState.Added;
                        await db.SaveChangesAsync();
                    }
                    else
                    {
                        dirOrderIntContractor = await db.DirOrderIntContractors.FindAsync(queryDirOrderIntContractors[0].DirOrderIntContractorID);
                        dirOrderIntContractor.DirOrderIntContractorName = docOrderInt.DirOrderIntContractorName;

                        db.Entry(dirOrderIntContractor).State = EntityState.Modified;
                        await db.SaveChangesAsync();
                    }
                }
            }

            #endregion


            #region 1. Doc

            //Модель
            Models.Sklad.Doc.Doc doc = new Models.Sklad.Doc.Doc();
            //Присваиваем значения
            doc.ListObjectID = ListObjectID;
            doc.IsImport = false;
            doc.NumberInt = docOrderInt.NumberInt;
            doc.NumberReal = docOrderInt.DocOrderIntID;
            doc.DirEmployeeID = field.DirEmployeeID;
            if (docOrderInt.PrepaymentSum > 0) doc.DirPaymentTypeID = docOrderInt.DirPaymentTypeID;
            else doc.DirPaymentTypeID = 1;
            doc.Payment = docOrderInt.Payment;
            doc.DirContractorID = docOrderInt.DirContractorIDOrg;  //docOrderInt.DirContractorID;
            doc.DirContractorIDOrg = docOrderInt.DirContractorIDOrg;
            doc.Discount = docOrderInt.Discount;
            doc.DirVatValue = docOrderInt.DirVatValue;
            doc.Base = docOrderInt.Base;
            doc.Description = docOrderInt.Description;
            doc.DocDate = docOrderInt.DocDate;
            //doc.DocDisc = docOrderInt.DocDisc;
            doc.Held = false;
            doc.DocID = docOrderInt.DocID;
            doc.DocIDBase = docOrderInt.DocIDBase;

            //Класс
            Docs.Docs docs = new Docs.Docs(db, dbRead, doc, entityState);
            //doc = await docs.Save();
            await Task.Run(() => docs.Save());

            //Нужно вернуть "docOrderInt" со всем полями!
            docOrderInt.DocID = doc.DocID;

            #endregion

            #region 2. docOrderInt


            #region  Если Новый товар. Проверить наименование, если отличается, то: docOrderInt.DirNomenID = null;
            //Алгоритм:
            //Ищим во 2-й группе товара "DirNomen2ID" - Катеририю "DirNomenCategoryID"
            //Если есть то "DirNomenID" присваиваем найденный "DirNomenID"
            //Если нет, то DirNomenID=null

            /*
            //1. Получаем старое наименование
            Models.Sklad.Dir.DirNomen dirNomen = await db.DirNomens.FindAsync(docOrderInt.DirNomenID);

            //2. Проверяем наименование
            if (dirNomen.DirNomenName.ToLower() != docOrderInt.DirNomenName.ToLower())
            {
                docOrderInt.DirNomenID = null;
            }
            */

            var queryDirNomenCat = await db.DirNomens.Where(x => x.Sub == docOrderInt.DirNomen2ID && x.DirNomenCategoryID == docOrderInt.DirNomenCategoryID).ToListAsync();
            if (queryDirNomenCat.Count() > 0) { docOrderInt.DirNomenID = queryDirNomenCat[0].DirNomenID; }
            else { docOrderInt.DirNomenID = null; }

            #endregion


            docOrderInt.DocID = doc.DocID;
            docOrderInt.DirOrderIntContractorID = dirOrderIntContractor.DirOrderIntContractorID;

            db.Entry(docOrderInt).State = entityState;
            await db.SaveChangesAsync();

            #region 2.1. UpdateNumberInt, если INSERT

            if (entityState == EntityState.Added && (docOrderInt.doc.NumberInt == null || docOrderInt.doc.NumberInt.Length == 0))
            {
                doc.NumberInt = docOrderInt.DocOrderIntID.ToString();
                doc.NumberReal = docOrderInt.DocOrderIntID;
                docs = new Docs.Docs(db, dbRead, doc, EntityState.Modified);
                await Task.Run(() => docs.Save());
            }
            else if (entityState == EntityState.Added)
            {
                doc.NumberReal = docOrderInt.DocOrderIntID;
                docs = new Docs.Docs(db, dbRead, doc, EntityState.Modified);
                await Task.Run(() => docs.Save());
            }

            #endregion


            #endregion

            #region 3. Касса или Банк

            //Только, если сумма больше 0
            if (docOrderInt.PrepaymentSum > 0)  //if (doc.Payment > 0)
            {
                //Касса
                if (doc.DirPaymentTypeID == 1)
                {
                    #region Касса

                    //1. По складу находим привязанную к нему Кассу
                    Models.Sklad.Dir.DirWarehouse dirWarehouse = db.DirWarehouses.Find(docOrderInt.DirWarehouseID);
                    int iDirCashOfficeID = dirWarehouse.DirCashOfficeID;

                    //2. Заполняем модель "DocCashOfficeSum"
                    Models.Sklad.Doc.DocCashOfficeSum docCashOfficeSum = new Models.Sklad.Doc.DocCashOfficeSum();
                    docCashOfficeSum.DirCashOfficeID = iDirCashOfficeID;
                    docCashOfficeSum.DirCashOfficeSumTypeID = 14;
                    docCashOfficeSum.DocCashOfficeSumDate = DateTime.Now;
                    docCashOfficeSum.DocID = doc.DocID;
                    docCashOfficeSum.DocXID = docOrderInt.DocOrderIntID;
                    docCashOfficeSum.DocCashOfficeSumSum = docOrderInt.PrepaymentSum; //doc.Payment;
                    docCashOfficeSum.Description = "";
                    docCashOfficeSum.DirEmployeeID = field.DirEmployeeID;
                    docCashOfficeSum.Base = "Предоплата";

                    //3. Пишем в Кассу
                    Doc.DocCashOfficeSums.DocCashOfficeSumsController docCashOfficeSumsController = new Doc.DocCashOfficeSums.DocCashOfficeSumsController();
                    docCashOfficeSum = await Task.Run(() => docCashOfficeSumsController.mPutPostDocCashOfficeSum(db, docCashOfficeSum, EntityState.Added));

                    #endregion
                }
                //Банк
                else if (doc.DirPaymentTypeID == 2)
                {
                    #region Банк

                    //1. По складу находим привязанную к нему Кассу
                    Models.Sklad.Dir.DirWarehouse dirWarehouse = db.DirWarehouses.Find(docOrderInt.DirWarehouseID);
                    int iDirBankID = dirWarehouse.DirBankID;

                    //2. Заполняем модель "DocBankSum"
                    Models.Sklad.Doc.DocBankSum docBankSum = new Models.Sklad.Doc.DocBankSum();
                    docBankSum.DirBankID = iDirBankID;
                    docBankSum.DirBankSumTypeID = 13; //Изъятие из кассы на основании проведения приходной накладной №
                    docBankSum.DocBankSumDate = DateTime.Now;
                    docBankSum.DocID = doc.DocID;
                    docBankSum.DocXID = docOrderInt.DocOrderIntID;
                    docBankSum.DocBankSumSum = docOrderInt.PrepaymentSum; //doc.Payment;
                    docBankSum.Description = "";
                    docBankSum.DirEmployeeID = field.DirEmployeeID;
                    docBankSum.Base = "Предоплата";

                    //3. Пишем в Банк
                    Doc.DocBankSums.DocBankSumsController docBankSumsController = new Doc.DocBankSums.DocBankSumsController();
                    docBankSum = await Task.Run(() => docBankSumsController.mPutPostDocBankSum(db, docBankSum, EntityState.Added));

                    #endregion
                }
            }

            #endregion

            #region 4. Log

            logOrderInt.DocOrderIntID = docOrderInt.DocOrderIntID;
            logOrderInt.DirOrderIntLogTypeID = 1;
            logOrderInt.DirEmployeeID = field.DirEmployeeID;
            logOrderInt.DirOrderIntStatusID = docOrderInt.DirOrderIntStatusID;

            await logOrderIntsController.mPutPostLogOrderInts(db, logOrderInt, EntityState.Added);

            #endregion




            #region n. Подтверждение транзакции *** *** *** *** *** *

            //ts.Commit(); //.Complete();

            #endregion


            return docOrderInt;
        }



        internal async Task<bool> mStatusChange(
            DbConnectionSklad db,
            System.Data.Entity.DbContextTransaction ts,
            Models.Sklad.Doc.DocOrderInt docOrderInt,
            int id,
            int DirOrderIntStatusID,
            int DirPaymentTypeID,

            Classes.Account.Login.Field field //Дополнительные данные о сотруднике
            )
        {

            #region Проверка, если предыдущий статус такой же на который меняем, то не писать в Лог

            //Исключение, т.к. если в Логе нет записей с сменой статуса получим Ошибку из-за "FirstAsync()"
            try
            {
                var query = await
                    (
                        from x in db.LogOrderInts
                        where x.DocOrderIntID == id && x.DirOrderIntStatusID != null
                        select new
                        {
                            LogOrderIntID = x.LogOrderIntID,
                            DirOrderIntStatusID = x.DirOrderIntStatusID
                        }
                    ).OrderByDescending(x => x.LogOrderIntID).FirstAsync();

                if (query.DirOrderIntStatusID == DirOrderIntStatusID)
                {
                    return false;
                }
            }
            catch (Exception ex) { }

            #endregion


            #region 1. Сохранение статуса в БД

            docOrderInt = await db.DocOrderInts.FindAsync(id);
            docOrderInt.DirOrderIntStatusID = DirOrderIntStatusID;

            db.Entry(docOrderInt).State = EntityState.Modified;
            await Task.Run(() => db.SaveChangesAsync());

            #endregion


            #region 2. Касса или Банк - !!! не используется !!!

            //Только, если сумма больше 0
            /*
            if (DirOrderIntStatusID == 9 && SumTotal2a > 0)  //if (doc.Payment > 0)
            {
                //Касса
                if (DirPaymentTypeID == 1)
                {
                    #region Касса

                    //1. По складу находим привязанную к нему Кассу
                    Models.Sklad.Dir.DirWarehouse dirWarehouse = db.DirWarehouses.Find(docOrderInt.DirWarehouseID);
                    int iDirCashOfficeID = dirWarehouse.DirCashOfficeID;

                    //2. Заполняем модель "DocCashOfficeSum"
                    Models.Sklad.Doc.DocCashOfficeSum docCashOfficeSum = new Models.Sklad.Doc.DocCashOfficeSum();
                    docCashOfficeSum.DirCashOfficeID = iDirCashOfficeID;
                    docCashOfficeSum.DirCashOfficeSumTypeID = 14;
                    docCashOfficeSum.DocCashOfficeSumDate = DateTime.Now;
                    docCashOfficeSum.DocID = docOrderInt.doc.DocID; //doc.DocID;
                    docCashOfficeSum.DocXID = docOrderInt.DocOrderIntID;
                    docCashOfficeSum.DocCashOfficeSumSum = SumTotal2a; //docOrderInt.PrepaymentSum; //doc.Payment;
                    docCashOfficeSum.Description = "";
                    docCashOfficeSum.DirEmployeeID = field.DirEmployeeID;
                    docCashOfficeSum.Base = "Оплата";

                    //3. Пишем в Кассу
                    Doc.DocCashOfficeSums.DocCashOfficeSumsController docCashOfficeSumsController = new Doc.DocCashOfficeSums.DocCashOfficeSumsController();
                    docCashOfficeSum = await Task.Run(() => docCashOfficeSumsController.mPutPostDocCashOfficeSum(db, docCashOfficeSum, EntityState.Added));

                    #endregion
                }
                //Банк
                else if (DirPaymentTypeID == 2)
                {
                    #region Банк

                    //1. По складу находим привязанную к нему Кассу
                    Models.Sklad.Dir.DirWarehouse dirWarehouse = db.DirWarehouses.Find(docOrderInt.DirWarehouseID);
                    int iDirBankID = dirWarehouse.DirBankID;

                    //2. Заполняем модель "DocBankSum"
                    Models.Sklad.Doc.DocBankSum docBankSum = new Models.Sklad.Doc.DocBankSum();
                    docBankSum.DirBankID = iDirBankID;
                    docBankSum.DirBankSumTypeID = 13; //Изъятие из кассы на основании проведения приходной накладной №
                    docBankSum.DocBankSumDate = DateTime.Now;
                    docBankSum.DocID = docOrderInt.doc.DocID; //doc.DocID;
                    docBankSum.DocXID = docOrderInt.DocOrderIntID;
                    docBankSum.DocBankSumSum = SumTotal2a; //docOrderInt.PrepaymentSum; //doc.Payment;
                    docBankSum.Description = "";
                    docBankSum.DirEmployeeID = field.DirEmployeeID;
                    docBankSum.Base = "Оплата";

                    //3. Пишем в Банк
                    Doc.DocBankSums.DocBankSumsController docBankSumsController = new Doc.DocBankSums.DocBankSumsController();
                    docBankSum = await Task.Run(() => docBankSumsController.mPutPostDocBankSum(db, docBankSum, EntityState.Added));

                    #endregion
                }
            }
            */

            #endregion


            #region 3. Лог


            #region Пишем в Лог о смене статуса и мастера, если такое было

            logOrderInt.DocOrderIntID = id;
            logOrderInt.DirOrderIntLogTypeID = 1; //Смена статуса
            //if (!bDirOrderIntLogTypeID9) logOrderInt.DirOrderIntLogTypeID = 1; //Смена статуса
            //else logOrderInt.DirOrderIntLogTypeID = 9; //Возврат по гарантии
            logOrderInt.DirEmployeeID = field.DirEmployeeID;
            logOrderInt.DirOrderIntStatusID = DirOrderIntStatusID;
            //logOrderInt.Msg = sLogMsg;

            await logOrderIntsController.mPutPostLogOrderInts(db, logOrderInt, EntityState.Added);

            #endregion


            #endregion


            #region 4. Заполняем DirOrderIntContractors - !!! не используется !!!
            //Надо ввести доп.поле статуса в "DocOrderIntes" с предыдущим статусом: "Готов" или "Отказ"
            //Когда нажимаем выдан, то заполнять это поле.
            //Нужно для:
            // - Справочника "DirOrderIntContractor" поля: QuantityOk и QuantityFail
            // - Для статистики сколько Готовых, сколько Отказных

            /*
            //Если в Логе НЕТ записей, что вернут на доработку
            var queryLogCount = await
                (
                    from x in db.LogOrderInts
                    where x.DirOrderIntLogTypeID == 9 && x.DocOrderIntID == id
                    select x
                ).CountAsync();


            //1. Находим Клиента по 
            if (
                queryLogCount == 0 &&
                DirOrderIntStatusID == 9 &&
                docOrderInt.DirOrderIntContractorID != null &&
                docOrderInt.DirOrderIntContractorID > 0
               )
            {
                Models.Sklad.Dir.DirOrderIntContractor dirOrderIntContractor = await db.DirOrderIntContractors.FindAsync(docOrderInt.DirOrderIntContractorID);

                //2. К-во (3 шт)
                if (DirOrderIntStatusID_OLD == 7)
                {
                    //Выдан
                    dirOrderIntContractor.QuantityOk = dirOrderIntContractor.QuantityOk + 1;
                }
                else if (DirOrderIntStatusID_OLD == 8)
                {
                    //Отказ
                    dirOrderIntContractor.QuantityFail = dirOrderIntContractor.QuantityFail + 1;
                }
                dirOrderIntContractor.QuantityCount = dirOrderIntContractor.QuantityCount + 1;

                //3. Сохранение
                db.Entry(dirOrderIntContractor).State = EntityState.Modified;
                await Task.Run(() => db.SaveChangesAsync());
            }
            */
            #endregion


            return true;
        }



        #endregion
    }
}