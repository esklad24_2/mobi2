﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using PartionnyAccount.Models;
using PartionnyAccount.Models.Sklad.Doc;
using System.Collections;
using System.Data.SQLite;
using System.Web.Script.Serialization;

namespace PartionnyAccount.Controllers.Sklad.Doc.DocServicePurches
{
    public class DocServicePurchesController : ApiController
    {
        #region Classes

        Classes.Function.Exceptions.ExceptionEntry exceptionEntry = new Classes.Function.Exceptions.ExceptionEntry();
        Classes.Function.Variables.ConnectionString connectionString = new Classes.Function.Variables.ConnectionString();
        Classes.Account.Login login = new Classes.Account.Login();
        Classes.Account.AccessRight accessRight = new Classes.Account.AccessRight();
        Classes.Function.ReturnServer returnServer = new Classes.Function.ReturnServer();
        Classes.Function.Function function = new Classes.Function.Function();
        Models.Sklad.Sys.SysJourDisp sysJourDisp = new Models.Sklad.Sys.SysJourDisp(); Controllers.Sklad.Sys.SysJourDispsController sysJourDispsController = new Sys.SysJourDispsController();
        Models.Sklad.Sys.SysSetting sysSetting = new Models.Sklad.Sys.SysSetting();
        Models.Sklad.Log.LogService logService = new Models.Sklad.Log.LogService(); Controllers.Sklad.Log.LogServicesController logServicesController = new Log.LogServicesController();
        private DbConnectionSklad db = new DbConnectionSklad();
        private DbConnectionSklad dbRead = new DbConnectionSklad();

        int ListObjectID = 40;

        #endregion


        #region SELECT

        class Params
        {
            //Parameters
            public int limit = 11;
            public int page = 1;
            public int Skip = 0;
            public int? GroupID = 0;
            public string parSearch = "";
            public int? FilterType; // == DirServiceStatusID
            public int? DirWarehouseID;
            public DateTime? DateS;
            public DateTime? DatePo;
            public int? DirServiceStatusIDS;
            public int? DirServiceStatusIDPo;
            public int iTypeService; //1 - Приёмка, 2 - Мастерская, 3 - Выдача
            public int? DirEmployeeID;

            //Отобразить "Архив"
            public int? DocServicePurchID;
        }
        // GET: api/DocServicePurches
        public async Task<IHttpActionResult> GetDocServicePurches(HttpRequestMessage request)
        {
            try
            {
                #region Проверяем Логин и Пароль + Изменяем строку соединения + Права + Разные Функции

                //Получаем Куку
                System.Web.HttpCookie authCookie = System.Web.HttpContext.Current.Request.Cookies["CookieIPOL"];

                // Проверяем Логин и Пароль
                Classes.Account.Login.Field field = await Task.Run(() => login.Return(authCookie, true));
                if (!field.Access) return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg10));

                //Изменяем строку соединения
                db = new DbConnectionSklad(connectionString.Return(field.DirCustomersID, null, true));
                dbRead = new DbConnectionSklad(connectionString.Return(field.DirCustomersID, null, true));


                //1. Права (1 - Write, 2 - Read, 3 - No Access)
                int iRight = await Task.Run(() => accessRight.Access(connectionString.Return(field.DirCustomersID, null, true), field.DirEmployeeID, "RightDocServicePurches"));
                if (iRight == 3) return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg57(0)));
                //2. Получаем "RightDocServiceWorkshopsOnlyUsers"
                //bool iRightCheck = await Task.Run(() => accessRight.AccessCheck(connectionString.Return(field.DirCustomersID, null, true), field.DirEmployeeID, "RightDocServiceWorkshopsOnlyUsersCheck"));
                //2.1. Админ точки?
                //... ниже, т.к. нам нужен склад ...



                //Разные Функции
                function.NumberDecimalSeparator();

                //Получам настройки
                Models.Sklad.Sys.SysSetting sysSetting = await db.SysSettings.FindAsync(1);

                //Получаем сотрудника: если к нему привязан Склад и/или Организация, то выбираем документы только по этим характеристикам
                Models.Sklad.Dir.DirEmployee dirEmployee = await db.DirEmployees.FindAsync(field.DirEmployeeID);

                #endregion


                #region Параметры

                Params _params = new Params();

                //paramList -список параметров
                var paramList = request.GetQueryNameValuePairs();
                //Параметры
                _params.limit = Convert.ToInt32(paramList.FirstOrDefault(kv => string.Compare(kv.Key, "limit", true) == 0).Value); //Записей на страницу
                _params.page = Convert.ToInt32(paramList.FirstOrDefault(kv => string.Compare(kv.Key, "page", true) == 0).Value);   //Номер страницы
                _params.Skip = _params.limit * (_params.page - 1);
                _params.GroupID = Convert.ToInt32(paramList.FirstOrDefault(kv => string.Compare(kv.Key, "pGroupID", true) == 0).Value); //Кликнули по группе
                _params.parSearch = paramList.FirstOrDefault(kv => string.Compare(kv.Key, "parSearch", true) == 0).Value; if (_params.parSearch != null) _params.parSearch = _params.parSearch.ToLower(); //Поиск
                _params.FilterType = Convert.ToInt32(paramList.FirstOrDefault(kv => string.Compare(kv.Key, "FilterType", true) == 0).Value);
                _params.DirServiceStatusIDS = Convert.ToInt32(paramList.FirstOrDefault(kv => string.Compare(kv.Key, "DirServiceStatusIDS", true) == 0).Value);
                _params.DirServiceStatusIDPo = Convert.ToInt32(paramList.FirstOrDefault(kv => string.Compare(kv.Key, "DirServiceStatusIDPo", true) == 0).Value);
                _params.DirEmployeeID = Convert.ToInt32(paramList.FirstOrDefault(kv => string.Compare(kv.Key, "DirEmployeeID", true) == 0).Value);
                _params.DirWarehouseID = Convert.ToInt32(paramList.FirstOrDefault(kv => string.Compare(kv.Key, "DirWarehouseID", true) == 0).Value);

                
                //*** *** ***
                //2.1. Админ точки?
                bool iRightCheck = await Task.Run(() => accessRight.AccessIsAdmin(connectionString.Return(field.DirCustomersID, null, true), field.DirEmployeeID, _params.DirWarehouseID));
                //*** *** ***


                //Архив
                _params.DocServicePurchID = Convert.ToInt32(paramList.FirstOrDefault(kv => string.Compare(kv.Key, "DocServicePurchID", true) == 0).Value);

                string sDate = paramList.FirstOrDefault(kv => string.Compare(kv.Key, "DateS", true) == 0).Value;
                if (!String.IsNullOrEmpty(sDate))
                {
                    _params.DateS = Convert.ToDateTime(Convert.ToDateTime(sDate).ToString("yyyy-MM-dd 00:00:01"));
                    if (_params.DateS < Convert.ToDateTime("01.01.1800")) _params.DateS = Convert.ToDateTime(sysSetting.JurDateS.ToString("yyyy-MM-dd 00:00:00")).AddDays(-1);
                    else _params.DateS = _params.DateS.Value.AddDays(-1);
                }

                sDate = paramList.FirstOrDefault(kv => string.Compare(kv.Key, "DatePo", true) == 0).Value;
                if (!String.IsNullOrEmpty(sDate))
                {
                    _params.DatePo = Convert.ToDateTime(Convert.ToDateTime(sDate).ToString("yyyy-MM-dd 23:59:59"));
                    if (_params.DatePo < Convert.ToDateTime("01.01.1800")) _params.DatePo = Convert.ToDateTime(sysSetting.JurDatePo.ToString("yyyy-MM-dd 23:59:59"));
                }

                #endregion



                #region Основной запрос *** *** ***

                var query =
                    (
                        from x in db.DocServicePurches

                        join dirServiceNomens11 in db.DirServiceNomens on x.dirServiceNomen.Sub equals dirServiceNomens11.DirServiceNomenID into dirServiceNomens12
                        from dirServiceNomensSubGroup in dirServiceNomens12.DefaultIfEmpty()

                        join dirServiceNomens21 in db.DirServiceNomens on dirServiceNomensSubGroup.Sub equals dirServiceNomens21.DirServiceNomenID into dirServiceNomens22
                        from dirServiceNomensGroup in dirServiceNomens22.DefaultIfEmpty()

                        select new
                        {
                            DirServiceNomenID = x.DirServiceNomenID,

                            DirServiceNomenName =
                            dirServiceNomensSubGroup.DirServiceNomenName == null ? x.dirServiceNomen.DirServiceNomenName :
                            dirServiceNomensGroup.DirServiceNomenName == null ? dirServiceNomensSubGroup.DirServiceNomenName + " / " + x.dirServiceNomen.DirServiceNomenName :
                            dirServiceNomensGroup.DirServiceNomenName + " / " + dirServiceNomensSubGroup.DirServiceNomenName + " / " + x.dirServiceNomen.DirServiceNomenName,

                            DocID = x.DocID,
                            DocDate = x.doc.DocDate,
                            Base = x.doc.Base,
                            Held = x.doc.Held,
                            Discount = x.doc.Discount,
                            Del = x.doc.Del,
                            Description = x.doc.Description,
                            IsImport = x.doc.IsImport,
                            DirVatValue = x.doc.DirVatValue,

                            DirEmployeeID = x.doc.DirEmployeeID,

                            DocServicePurchID = x.DocServicePurchID,
                            DirContractorName = x.doc.dirContractor.DirContractorName,
                            DirContractorIDOrg = x.doc.dirContractorOrg.DirContractorID,
                            //DirContractorNameOrg = x.doc.dirContractorOrg.DirContractorName,
                            DirWarehouseID = x.dirWarehouse.DirWarehouseID,
                            DirWarehouseName = x.dirWarehouse.DirWarehouseName,
                            ProblemClientWords = x.ProblemClientWords,
                            SerialNumber = x.SerialNumber,
                            DirServiceStatusID = x.DirServiceStatusID, Status = x.DirServiceStatusID,
                            DirServiceStatusName = x.dirServiceStatus.DirServiceStatusName,
                            DirServiceContractorName = x.DirServiceContractorName,
                            DirServiceContractorPhone = x.DirServiceContractorPhone,
                            UrgentRepairs = x.UrgentRepairs,
                            PriceVAT = x.PriceVAT, //PriceCurrency = x.PriceCurrency,
                            DateDone = x.DateDone.ToString(),
                            PrepaymentSum = x.PrepaymentSum == null ? 0 : x.PrepaymentSum,
                            //Оплата
                            Payment = x.doc.Payment,
                            //Мастер
                            DirEmployeeIDMaster = x.DirEmployeeIDMaster,
                            DirEmployeeNameMaster = x.dirEmployee.DirEmployeeName,
                            FromGuarantee = x.FromGuarantee,
                            ServiceTypeRepair = x.ServiceTypeRepair,

                            DirServiceContractorID = x.DirServiceContractorID,
                            //QuantityOk = x.dirServiceContractor.QuantityOk,
                            //QuantityFail = x.dirServiceContractor.QuantityFail,
                            //QuantityCount = x.dirServiceContractor.QuantityCount

                            IssuanceDate = x.IssuanceDate.ToString(),
                            Sums = x.Sums
                        }
                        
                    );

                #endregion


                #region Условия (параметры) *** *** ***


                #region Отобразить "Предыдущие ремонты"

                if (_params.DocServicePurchID != null && _params.DocServicePurchID > 0)
                {
                    //1. Получаем "DirServiceContractorID" по "DocServicePurchID"
                    //2.Добавляем условие в запрос

                    Models.Sklad.Doc.DocServicePurch docServicePurch = await db.DocServicePurches.FindAsync(_params.DocServicePurchID);
                    int? DirServiceContractorID = docServicePurch.DirServiceContractorID;
                    if (DirServiceContractorID == null) { DirServiceContractorID = 0; }

                    query = query.Where(x => x.DirServiceContractorID == DirServiceContractorID && x.DocServicePurchID != _params.DocServicePurchID);
                }

                #endregion



                #region dirEmployee: dirEmployee.DirWarehouseID and/or dirEmployee.DirContractorIDOrg

                if (_params.DirWarehouseID != null && _params.DirWarehouseID > 0)
                {
                    query = query.Where(x => x.DirWarehouseID == _params.DirWarehouseID);
                }

                if (dirEmployee.DirContractorIDOrg != null && dirEmployee.DirContractorIDOrg > 0)
                {
                    query = query.Where(x => x.DirContractorIDOrg == dirEmployee.DirContractorIDOrg);
                }

                #endregion


                #region Не показывать удалённые

                if (!Convert.ToBoolean(sysSetting.DeletedRecordsShow))
                {
                    query = query.Where(x => x.Del == sysSetting.DeletedRecordsShow);
                }

                #endregion


                #region Фильтр FilterType

                if (_params.FilterType > 0)
                {
                    query = query.Where(x => x.DirServiceStatusID == _params.FilterType);
                }

                #endregion

                #region Фильтр DirServiceStatusID S и Po

                if (_params.DirServiceStatusIDS != _params.DirServiceStatusIDPo)
                {
                    if (_params.DirServiceStatusIDS > 0 && _params.DirServiceStatusIDPo > 0) { query = query.Where(x => x.DirServiceStatusID >= _params.DirServiceStatusIDS && x.DirServiceStatusID <= _params.DirServiceStatusIDPo); }
                    else
                    {
                        if (_params.DirServiceStatusIDS > 0) query = query.Where(x => x.DirServiceStatusID >= _params.DirServiceStatusIDS);
                        if (_params.DirServiceStatusIDPo > 0) query = query.Where(x => x.DirServiceStatusID <= _params.DirServiceStatusIDPo);
                    }
                }
                else
                {
                    if (_params.DirServiceStatusIDS > 0) query = query.Where(x => x.DirServiceStatusID == _params.DirServiceStatusIDS);
                }

                if (_params.DirEmployeeID > 0)
                {
                    query = query.Where(x => x.DirEmployeeID <= _params.DirEmployeeID && x.DirServiceStatusID < 7);
                }

                #endregion

                #region Фильтр Date S и Po

                if (_params.DateS != null)
                {
                    query = query.Where(x => x.DocDate >= _params.DateS);
                }

                if (_params.DatePo != null)
                {
                    query = query.Where(x => x.DocDate <= _params.DatePo);
                }

                #endregion


                //!!! Новое !!!
                #region Видит только свои ремонты (Админ Точки видит все ремонты точки, мастер только свои)

                if (!iRightCheck && _params.DirServiceStatusIDPo != 9)
                {
                    query = query.Where(x => x.DirEmployeeIDMaster == field.DirEmployeeID);
                }

                #endregion


                #region Поиск

                if (!String.IsNullOrEmpty(_params.parSearch))
                {
                    //Проверяем число ли это
                    Int32 iNumber32;
                    bool bResult32 = Int32.TryParse(_params.parSearch, out iNumber32);
                    Int64 iNumber64;
                    bool bResult64 = Int64.TryParse(_params.parSearch, out iNumber64);


                    //Если число, то задействуем в поиске и числовые поля (_params.parSearch == iNumber)
                    if (bResult32 && _params.parSearch.IndexOf("+") == -1)
                    {
                        query = query.Where(x => x.DocServicePurchID == iNumber32);
                    }
                    else
                    {
                        //Ищим в справочнике "DirServiceContractors"

                        if (bResult64)
                        {
                            //query = query.Where(x => x.DirServiceContractorPhone == _params.parSearch);

                            //Ищим в справочнике "DirServiceContractors"
                            int DirServiceContractorID = 0;
                            var queryDirServiceContractors = await
                                (
                                    from x in db.DirServiceContractors
                                    where x.DirServiceContractorPhone == _params.parSearch
                                    select new
                                    {
                                        x.DirServiceContractorID
                                    }
                                ).ToListAsync();
                            if (queryDirServiceContractors.Count() > 0)
                            {
                                DirServiceContractorID = Convert.ToInt32(queryDirServiceContractors[0].DirServiceContractorID);
                            }

                            query = query.Where(x => x.DirServiceContractorID == DirServiceContractorID);
                        }
                        //Иначе, только текстовые поля
                        else
                        {
                            _params.parSearch = _params.parSearch.Replace("+", "");
                            //query = query.Where(x => x.DirServiceContractorName.Contains(_params.parSearch));

                            //Ищим в справочнике "DirServiceContractors"
                            //int DirServiceContractorID = 0;
                            var queryDirServiceContractors = await
                                (
                                    from x in db.DirServiceContractors
                                    where x.DirServiceContractorName.Contains(_params.parSearch)
                                    select x.DirServiceContractorID
                                ).ToListAsync();
                            /*if (queryDirServiceContractors.Count() > 0)
                            {
                                DirServiceContractorID = Convert.ToInt32(queryDirServiceContractors[0].DirServiceContractorID);
                            }*/

                            query = query.Where(x => queryDirServiceContractors.Contains(x.DirServiceContractorID));
                        }
                    }
                    
                }

                #endregion


                #region OrderBy и Лимит

                //query = query.OrderByDescending(x => x.DocDate).Skip(_params.Skip).Take(_params.limit);
                query = query.OrderByDescending(x => x.DocServicePurchID); //.Skip(_params.Skip).Take(_params.limit);

                #endregion


                #endregion


                #region Отправка JSON

                //К-во Номенклатуры
                int dirCount = await Task.Run(() => db.DocServicePurches.Where(x => x.doc.DocDate >= _params.DateS && x.doc.DocDate <= _params.DatePo).Count());

                //А вдруг к-во меньше Лимита, тогда показать не общее к-во, а реальное!
                int dirCount2 = query.Count();
                if (dirCount2 < _params.limit) dirCount = _params.limit * (_params.page - 1) + dirCount2;

                dynamic collectionWrapper = new
                {
                    sucess = true,
                    total = dirCount,
                    DocServicePurch = query
                };
                return await Task.Run(() => Ok(collectionWrapper));

                #endregion

            }
            catch (Exception ex)
            {
                return Ok(returnServer.Return(false, exceptionEntry.Return(ex)));
            }
        }

        // GET: api/DocServicePurches/5
        [ResponseType(typeof(DocServicePurch))]
        public async Task<IHttpActionResult> GetDocServicePurch(int id, HttpRequestMessage request)
        {
            try
            {
                #region Проверяем Логин и Пароль + Изменяем строку соединения + Права + Разные Функции

                //Получаем Куку
                System.Web.HttpCookie authCookie = System.Web.HttpContext.Current.Request.Cookies["CookieIPOL"];

                // Проверяем Логин и Пароль
                Classes.Account.Login.Field field = await Task.Run(() => login.Return(authCookie, true));
                if (!field.Access) return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg10));

                //Изменяем строку соединения
                db = new DbConnectionSklad(connectionString.Return(field.DirCustomersID, null, true));
                dbRead = new DbConnectionSklad(connectionString.Return(field.DirCustomersID, null, true));

                //Права (1 - Write, 2 - Read, 3 - No Access)
                int iRight = await Task.Run(() => accessRight.Access(connectionString.Return(field.DirCustomersID, null, true), field.DirEmployeeID, "RightDocServicePurches"));
                if (iRight == 3) return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg57(0)));

                //Разные Функции
                function.NumberDecimalSeparator();

                //Получам настройки
                Models.Sklad.Sys.SysSetting sysSetting = await db.SysSettings.FindAsync(1);

                #endregion


                #region Параметры

                Params _params = new Params();

                //paramList -список параметров
                var paramList = request.GetQueryNameValuePairs();
                //Параметры
                int DocID = Convert.ToInt32(paramList.FirstOrDefault(kv => string.Compare(kv.Key, "DocID", true) == 0).Value); //Кликнули по группе

                //Если не пришёл параметр "DocID", то получаем его из БД (что бы SQlServer не перебирал все оплаты)
                if (DocID == 0)
                {
                    var queryDocID = await Task.Run(() =>
                    (
                        from docServicePurches in db.DocServicePurches
                        where docServicePurches.DocServicePurchID == id
                        select docServicePurches
                    ).ToListAsync());

                    if (queryDocID.Count() > 0) DocID = Convert.ToInt32(queryDocID[0].DocID);
                }

                #endregion


                #region Отправка JSON


                #region Полный путь Аппарата

                //1. Получаем Sub аппарата по "DocServicePurchID" (id)
                /*
                string DirServiceNomenPatchFull = null;
                var querySub = await Task.Run(() =>
                     (
                        from x in db.DocServicePurches
                        where x.DocServicePurchID == id
                        select new
                        {
                            Sub = x.dirServiceNomen.Sub
                        }
                    ).ToArrayAsync());

                if (querySub.Count() > 0)
                {
                    int? iSub = querySub[0].Sub;

                    Controllers.Sklad.Dir.DirServiceNomens.DirServiceNomensController dirServiceNomensController = new Dir.DirServiceNomens.DirServiceNomensController();
                    DirServiceNomenPatchFull = await Task.Run(() => dirServiceNomensController.DirServiceNomenSubFind2(db, iSub));
                }
                */

                string[] ret = await Task.Run(() => mPatchFull(db, id));
                string DirServiceNomenPatchFull = ret[0];

                #endregion


                #region Суммы Услуг и Запчастей

               //double dSumDocServicePurch1Tabs = await db.DocServicePurch1Tabs.Where(x => x.DocServicePurchID == id).Select(x=>x.PriceCurrency).DefaultIfEmpty(0).SumAsync();

                //double dSumDocServicePurch2Tabs = await db.DocServicePurch2Tabs.Where(x => x.DocServicePurchID == id).Select(x => x.PriceCurrency).DefaultIfEmpty(0).SumAsync();

                #endregion



                #region QUERY


                var query = await Task.Run(() =>
                    (
                        #region from

                        from docServicePurches in db.DocServicePurches

                        join docServicePurch1Tabs1 in db.DocServicePurch1Tabs on docServicePurches.DocServicePurchID equals docServicePurch1Tabs1.DocServicePurchID into docServicePurch1Tabs2
                        from docServicePurch1Tabs in docServicePurch1Tabs2.DefaultIfEmpty()

                        join docServicePurch2Tabs1 in db.DocServicePurch2Tabs on docServicePurches.DocServicePurchID equals docServicePurch2Tabs1.DocServicePurchID into docServicePurch2Tabs2
                        from docServicePurch2Tabs in docServicePurch2Tabs2.DefaultIfEmpty()

                        #endregion

                        where docServicePurches.DocServicePurchID == id

                        #region select

                        select new
                        {
                            DocID = docServicePurches.DocID,
                            DocDate = docServicePurches.doc.DocDate,
                            Base = docServicePurches.doc.Base,
                            Held = docServicePurches.doc.Held,
                            Discount = docServicePurches.doc.Discount,
                            Del = docServicePurches.doc.Del,
                            Description = docServicePurches.doc.Description,
                            IsImport = docServicePurches.doc.IsImport,
                            DirVatValue = docServicePurches.doc.DirVatValue,
                            //DirPaymentTypeID = docServicePurches.doc.DirPaymentTypeID,

                            DirServiceNomenID = docServicePurches.DirServiceNomenID,

                            DirServiceNomenNameLittle = docServicePurches.dirServiceNomen.DirServiceNomenName,

                            DirServiceNomenName =
                            DirServiceNomenPatchFull == null ? docServicePurches.dirServiceNomen.DirServiceNomenName
                            :
                            DirServiceNomenPatchFull + docServicePurches.dirServiceNomen.DirServiceNomenName,

                            DocServicePurchID = docServicePurches.DocServicePurchID,
                            DirContractorName = docServicePurches.doc.dirContractor.DirContractorName,
                            DirContractorIDOrg = docServicePurches.doc.dirContractorOrg.DirContractorID,
                            DirContractorNameOrg = docServicePurches.doc.dirContractorOrg.DirContractorName,
                            DirWarehouseID = docServicePurches.dirWarehouse.DirWarehouseID,
                            DirWarehouseName = docServicePurches.dirWarehouse.DirWarehouseName,

                            DirServiceStatusID = docServicePurches.DirServiceStatusID,
                            SerialNumberNo = docServicePurches.SerialNumberNo,
                            SerialNumber = docServicePurches.SerialNumber,
                            TypeRepair = docServicePurches.TypeRepair,
                            ComponentDevice = docServicePurches.ComponentDevice,

                            ComponentBattery = docServicePurches.ComponentBattery,
                            ComponentBatterySerial = docServicePurches.ComponentBatterySerial,
                            ComponentBackCover = docServicePurches.ComponentBackCover,
                            ComponentPasTextNo = docServicePurches.ComponentPasTextNo,
                            ComponentPasText = docServicePurches.ComponentPasText,
                            ComponentOtherText = docServicePurches.ComponentOtherText,
                            ProblemClientWords = docServicePurches.ProblemClientWords,
                            Note = docServicePurches.Note,
                            DirServiceContractorName = docServicePurches.DirServiceContractorName,
                            DirServiceContractorRegular = docServicePurches.DirServiceContractorRegular,
                            DirServiceContractorID = docServicePurches.DirServiceContractorID,
                            DirServiceContractorAddress = docServicePurches.DirServiceContractorAddress,
                            DirServiceContractorPhone = docServicePurches.DirServiceContractorPhone,
                            DirServiceContractorEmail = docServicePurches.DirServiceContractorEmail,

                            PriceVAT = docServicePurches.PriceVAT,
                            //PriceCurrency = docServicePurches.PriceCurrency,

                            DirCurrencyID = docServicePurches.DirCurrencyID,
                            DirCurrencyRate = docServicePurches.DirCurrencyRate,
                            DirCurrencyMultiplicity = docServicePurches.DirCurrencyMultiplicity,
                            DirCurrencyName = docServicePurches.dirCurrency.DirCurrencyName + " (" + docServicePurches.DirCurrencyRate + ", " + docServicePurches.DirCurrencyMultiplicity + ")",

                            DateDone = docServicePurches.DateDone,
                            UrgentRepairs = docServicePurches.UrgentRepairs,
                            Prepayment = docServicePurches.Prepayment,
                            PrepaymentSum = docServicePurches.PrepaymentSum == null ? 0 : docServicePurches.PrepaymentSum,

                            //Оплата
                            Payment = docServicePurches.doc.Payment,
                            //Мастер
                            DirEmployeeIDMaster = docServicePurches.DirEmployeeIDMaster,
                            DirEmployeeNameMaster = docServicePurches.dirEmployee.DirEmployeeName,

                            ServiceTypeRepair = docServicePurches.ServiceTypeRepair,

                            Phone = docServicePurches.dirWarehouse.Phone,
                            DirWarehouseAddress = docServicePurches.dirWarehouse.DirWarehouseAddress,

                            //К-во раз Клиент обращался в сервис
                            QuantityOk = docServicePurches.dirServiceContractor.QuantityOk,
                            QuantityFail = docServicePurches.dirServiceContractor.QuantityFail,
                            QuantityCount = docServicePurches.dirServiceContractor.QuantityCount,


                            // *** СУММЫ *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** ***

                            //1. Подсчет табличной части Работы "SumDocServicePurch1Tabs"
                            SumDocServicePurch1Tabs = docServicePurches.Sums1, SumDocServicePurch1Tabs2 = docServicePurches.Sums1, //SumDocServicePurch1Tabs = dSumDocServicePurch1Tabs, SumDocServicePurch1Tabs2 = dSumDocServicePurch1Tabs,
                            //2. Подсчет табличной части Работы "SumDocServicePurch2Tabs"
                            SumDocServicePurch2Tabs = docServicePurches.Sums2, SumDocServicePurch2Tabs2 = docServicePurches.Sums2, //SumDocServicePurch2Tabs = dSumDocServicePurch2Tabs, SumDocServicePurch2Tabs2 = dSumDocServicePurch2Tabs,
                            //3. Сумма 1+2 "SumTotal"
                            SumTotal = docServicePurches.Sums1 + docServicePurches.Sums2, //SumTotal = dSumDocServicePurch1Tabs + dSumDocServicePurch2Tabs, SumTotal2 = dSumDocServicePurch1Tabs + dSumDocServicePurch2Tabs,
                            //5. 3 - 4 "SumTotal2"
                            SumTotal2a = docServicePurches.Sums1 + docServicePurches.Sums2 - docServicePurches.PrepaymentSum, //SumTotal2a = dSumDocServicePurch1Tabs + dSumDocServicePurch2Tabs - docServicePurches.PrepaymentSum,


                            Alerted = docServicePurches.AlertedCount == null ? "Не оповещён" : "Оповещён (" + docServicePurches.AlertedCount + ") " + docServicePurches.AlertedDateTxt,


                            //Для отправки СМС, что бы знать были ли предыдущие ремонты
                            PrepaymentSum_1 = docServicePurches.PrepaymentSum_1
                        }

                        #endregion

                    ).ToListAsync());


                #endregion
                


                if (query.Count() > 0)
                {

                    #region Смена статуса и запис в Лог
                    //1. Изменять статус, если бы "Принят" на "В диагностике" + писать в Лог изменение статуса
                    //2. Изменять инженера, который открыл + писать в Лог изменение инженера

                    //1. Проверяем статус, если == 1, то меняем на 2
                    if (query[0].DirServiceStatusID == 1)
                    {
                        //Меняем статус + меняем мастера
                        Models.Sklad.Doc.DocServicePurch docServicePurch = await dbRead.DocServicePurches.FindAsync(id);
                        //docServicePurch.DirEmployeeIDMaster = field.DirEmployeeID;
                        docServicePurch.DirServiceStatusID = 2;
                        dbRead.Entry(docServicePurch).State = EntityState.Modified;
                        await dbRead.SaveChangesAsync();

                        //Пишем в Лог о смене статуса и мастера, если такое было
                        logService.DocServicePurchID = id;
                        logService.DirServiceLogTypeID = 1;
                        logService.DirEmployeeID = field.DirEmployeeID;
                        logService.DirServiceStatusID = docServicePurch.DirServiceStatusID;
                        //if (query[0].DirEmployeeIDMaster != field.DirEmployeeID) logService.Msg = "Смена мастера " + query[0].DirEmployeeNameMaster;

                        await logServicesController.mPutPostLogServices(db, logService, EntityState.Added);
                    }


                    //??? Пишем в Лог о смене мастера, если такое было ???
                    /*
                    if (query[0].DirEmployeeIDMaster != field.DirEmployeeID)
                    {
                        Models.Sklad.Doc.DocServicePurch docServicePurch = await dbRead.DocServicePurches.FindAsync(id);
                        //docServicePurch.DirEmployeeIDMaster = field.DirEmployeeID;
                        //docServicePurch.DirServiceStatusID = 2;
                        dbRead.Entry(docServicePurch).State = EntityState.Modified;
                        await dbRead.SaveChangesAsync();
                    }
                    */

                    #endregion

                    return Ok(returnServer.Return(true, query[0]));
                }
                else
                {
                    return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg89));
                }

                #endregion
            }
            catch (Exception ex)
            {
                return Ok(returnServer.Return(false, exceptionEntry.Return(ex)));
            }
        }

        #endregion


        #region UPDATE

        // PUT: api/DocServicePurches/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutDocServicePurch(int id, DocServicePurch docServicePurch, HttpRequestMessage request)
        {
            try
            {
                #region Проверяем Логин и Пароль + Изменяем строку соединения + Права + Разные Функции

                //Получаем Куку
                System.Web.HttpCookie authCookie = System.Web.HttpContext.Current.Request.Cookies["CookieIPOL"];

                // Проверяем Логин и Пароль
                Classes.Account.Login.Field field = await Task.Run(() => login.Return(authCookie, true));
                if (!field.Access) return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg10));

                //Изменяем строку соединения
                db = new DbConnectionSklad(connectionString.Return(field.DirCustomersID, null, true));
                dbRead = new DbConnectionSklad(connectionString.Return(field.DirCustomersID, null, true));

                //Права (1 - Write, 2 - Read, 3 - No Access)
                int iRight = await Task.Run(() => accessRight.Access(connectionString.Return(field.DirCustomersID, null, true), field.DirEmployeeID, "RightDocServicePurches"));
                if (iRight != 1) return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg57(0)));

                //Разные Функции
                function.NumberDecimalSeparator();

                //Получам настройки
                sysSetting = await db.SysSettings.FindAsync(1);

                #endregion

                #region Параметры

                var paramList = request.GetQueryNameValuePairs();

                //1 - Изменили "Дату Готовности"
                //Params _params = new Params();
                //_params.iTypeService = Convert.ToInt32(paramList.FirstOrDefault(kv => string.Compare(kv.Key, "iTypeService", true) == 0).Value);

                DateTime DateDone = Convert.ToDateTime(Convert.ToDateTime(paramList.FirstOrDefault(kv => string.Compare(kv.Key, "DateDone", true) == 0).Value).ToString("yyyy-MM-dd 00:00:00"));
                if (DateDone < Convert.ToDateTime("01.01.1800"))
                {
                    //...
                }


                #endregion


                #region Сохранение

                try
                {
                    //Находим Аппарат и меняем дату готовности
                    docServicePurch = await db.DocServicePurches.FindAsync(id);
                    DateTime DateDoneOLD = docServicePurch.DateDone;
                    docServicePurch.DateDone = DateDone;

                    using (System.Data.Entity.DbContextTransaction ts = db.Database.BeginTransaction())
                    {
                        db.Configuration.AutoDetectChangesEnabled = false;

                        try
                        {
                            db.Entry(docServicePurch).State = EntityState.Modified;
                            await db.SaveChangesAsync();


                            #region 4. Log

                            logService.DocServicePurchID = docServicePurch.DocServicePurchID;
                            logService.DirServiceLogTypeID = 7;
                            logService.DirEmployeeID = field.DirEmployeeID;
                            //logService.DirServiceStatusID = docServicePurch.DirServiceStatusID;
                            logService.Msg = "Смена даты готовности с " + DateDoneOLD.ToString("yyyy-MM-dd") + " на " + DateDone.ToString("yyyy-MM-dd");

                            await logServicesController.mPutPostLogServices(db, logService, EntityState.Added);

                            #endregion


                            ts.Commit();
                        }
                        catch (Exception ex)
                        {
                            try { ts.Rollback(); ts.Dispose(); } catch { }
                            try { db.Database.Connection.Close(); db.Database.Connection.Dispose(); } catch { }

                            return Ok(returnServer.Return(false, exceptionEntry.Return(ex)));
                        }
                    }


                    #region 6. JourDisp *** *** *** *** *** *** *** *** *** *

                    Models.Sklad.Sys.SysJourDisp sysJourDisp = new Models.Sklad.Sys.SysJourDisp();
                    sysJourDisp.DirDispOperationID = 4; //Изменение записи
                    sysJourDisp.DirEmployeeID = field.DirEmployeeID;
                    sysJourDisp.ListObjectID = ListObjectID;
                    sysJourDisp.TableFieldID = docServicePurch.DocServicePurchID;
                    sysJourDisp.Description = "";
                    try { sysJourDispsController.mPutPostSysJourDisps(db, sysJourDisp, EntityState.Added); } catch (Exception ex) { }

                    #endregion


                    dynamic collectionWrapper = new
                    {
                        DocID = docServicePurch.DocID,
                        DocServicePurchID = docServicePurch.DocServicePurchID
                    };
                    return Ok(returnServer.Return(true, collectionWrapper)); //return Ok(returnServer.Return(true, ""));
                }
                catch (Exception ex)
                {
                    return Ok(returnServer.Return(false, exceptionEntry.Return(ex)));
                }

                #endregion
            }
            catch (Exception ex)
            {
                return Ok(returnServer.Return(false, exceptionEntry.Return(ex)));
            }
        }

        //Смена статуса
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutDocServicePurch(int id, int DirStatusID, HttpRequestMessage request)
        {
            try
            {
                #region Проверяем Логин и Пароль + Изменяем строку соединения + Права + Разные Функции

                //Получаем Куку
                System.Web.HttpCookie authCookie = System.Web.HttpContext.Current.Request.Cookies["CookieIPOL"];

                // Проверяем Логин и Пароль
                Classes.Account.Login.Field field = await Task.Run(() => login.Return(authCookie, true));
                if (!field.Access) return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg10));

                //Изменяем строку соединения
                db = new DbConnectionSklad(connectionString.Return(field.DirCustomersID, null, true));
                dbRead = new DbConnectionSklad(connectionString.Return(field.DirCustomersID, null, true));

                //Права (1 - Write, 2 - Read, 3 - No Access)
                int iRight = await Task.Run(() => accessRight.Access(connectionString.Return(field.DirCustomersID, null, true), field.DirEmployeeID, "RightDocServicePurches"));
                if (iRight != 1) return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg57(0)));

                //Разные Функции
                function.NumberDecimalSeparator();

                //Получам настройки
                sysSetting = await db.SysSettings.FindAsync(1);

                #endregion

                #region Параметры

                Params _params = new Params();

                //paramList -список параметров
                var paramList = request.GetQueryNameValuePairs();
                //Параметры
                int DirPaymentTypeID = Convert.ToInt32(paramList.FirstOrDefault(kv => string.Compare(kv.Key, "DirPaymentTypeID", true) == 0).Value);
                double SumTotal2a = Convert.ToDouble(paramList.FirstOrDefault(kv => string.Compare(kv.Key, "SumTotal2a", true) == 0).Value.Replace(".", ","));
                string sReturnRresults = paramList.FirstOrDefault(kv => string.Compare(kv.Key, "sReturnRresults", true) == 0).Value;

                #endregion

                #region Проверки
                //Если Статус "7", то проверить Таб часть-1
                // Если DirStatusID = 7 и нет ни одной выполненной работы, то не пускать сохранять и выдать эксепшн
                //if (iTypeService > 1 && docServicePurch.DirStatusID == 7 && docServicePurch1TabCollection.Length == 0) { throw new System.InvalidOperationException(Classes.Language.Sklad.Language.msg114); }

                var queryCount = await
                    (
                        from x in db.DocServicePurch1Tabs
                        where x.DocServicePurchID == id
                        select x
                    ).CountAsync();
                if (queryCount == 0 && DirStatusID == 7)
                {
                    throw new System.InvalidOperationException(Classes.Language.Sklad.Language.msg114);
                }

                #endregion


                #region Сохранение

                try
                {
                    Models.Sklad.Doc.DocServicePurch docServicePurch = new DocServicePurch();

                    using (System.Data.Entity.DbContextTransaction ts = db.Database.BeginTransaction())
                    {
                        db.Configuration.AutoDetectChangesEnabled = false;

                        try
                        {
                            await mStatusChange(db, ts, docServicePurch, id, DirStatusID, DirPaymentTypeID, SumTotal2a, sReturnRresults, field);

                            ts.Commit();
                        }
                        catch (Exception ex)
                        {
                            try { ts.Rollback(); ts.Dispose(); } catch { }
                            try { db.Database.Connection.Close(); db.Database.Connection.Dispose(); } catch { }

                            return Ok(returnServer.Return(false, exceptionEntry.Return(ex)));
                        }
                    }

                    dynamic collectionWrapper = new
                    {
                        DocID = docServicePurch.DocID,
                        DocServicePurchID = docServicePurch.DocServicePurchID
                    };
                    return Ok(returnServer.Return(true, collectionWrapper));
                }
                catch (Exception ex)
                {
                    return Ok(returnServer.Return(false, exceptionEntry.Return(ex)));
                }

                #endregion
            }
            catch (Exception ex)
            {
                return Ok(returnServer.Return(false, exceptionEntry.Return(ex)));
            }
        }

        //Смена гарантии
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutDocServicePurch(int id, int ServiceTypeRepair, int iTrash, HttpRequestMessage request)
        {
            try
            {
                #region Проверяем Логин и Пароль + Изменяем строку соединения + Права + Разные Функции

                //Получаем Куку
                System.Web.HttpCookie authCookie = System.Web.HttpContext.Current.Request.Cookies["CookieIPOL"];

                // Проверяем Логин и Пароль
                Classes.Account.Login.Field field = await Task.Run(() => login.Return(authCookie, true));
                if (!field.Access) return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg10));

                //Изменяем строку соединения
                db = new DbConnectionSklad(connectionString.Return(field.DirCustomersID, null, true));
                dbRead = new DbConnectionSklad(connectionString.Return(field.DirCustomersID, null, true));

                //Права (1 - Write, 2 - Read, 3 - No Access)
                int iRight = await Task.Run(() => accessRight.Access(connectionString.Return(field.DirCustomersID, null, true), field.DirEmployeeID, "RightDocServicePurches"));
                if (iRight != 1) return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg57(0)));

                //Разные Функции
                function.NumberDecimalSeparator();

                //Получам настройки
                sysSetting = await db.SysSettings.FindAsync(1);

                #endregion

                #region Параметры

                Params _params = new Params();

                //paramList -список параметров
                var paramList = request.GetQueryNameValuePairs();
                //Параметры
                //int DirPaymentTypeID = Convert.ToInt32(paramList.FirstOrDefault(kv => string.Compare(kv.Key, "DirPaymentTypeID", true) == 0).Value);
                //double SumTotal2a = Convert.ToDouble(paramList.FirstOrDefault(kv => string.Compare(kv.Key, "SumTotal2a", true) == 0).Value.Replace(".", ","));

                #endregion

                #region Проверки

                //...

                #endregion


                #region Сохранение

                try
                {
                    Models.Sklad.Doc.DocServicePurch docServicePurch = new DocServicePurch();

                    using (System.Data.Entity.DbContextTransaction ts = db.Database.BeginTransaction())
                    {
                        db.Configuration.AutoDetectChangesEnabled = false;

                        try
                        {
                            await mRepairChange(db, ts, docServicePurch, id, ServiceTypeRepair, field);

                            ts.Commit();
                        }
                        catch (Exception ex)
                        {
                            try { ts.Rollback(); ts.Dispose(); } catch { }
                            try { db.Database.Connection.Close(); db.Database.Connection.Dispose(); } catch { }

                            return Ok(returnServer.Return(false, exceptionEntry.Return(ex)));
                        }
                    }

                    dynamic collectionWrapper = new
                    {
                        DocID = docServicePurch.DocID,
                        DocServicePurchID = docServicePurch.DocServicePurchID
                    };
                    return Ok(returnServer.Return(true, collectionWrapper));
                }
                catch (Exception ex)
                {
                    return Ok(returnServer.Return(false, exceptionEntry.Return(ex)));
                }

                #endregion
            }
            catch (Exception ex)
            {
                return Ok(returnServer.Return(false, exceptionEntry.Return(ex)));
            }
        }

        //Смена мастера
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutDocServicePurch(int id, int DirEmployeeID, int iTrash, int iTrash2, HttpRequestMessage request)
        {
            try
            {
                #region Проверяем Логин и Пароль + Изменяем строку соединения + Права + Разные Функции

                //Получаем Куку
                System.Web.HttpCookie authCookie = System.Web.HttpContext.Current.Request.Cookies["CookieIPOL"];

                // Проверяем Логин и Пароль
                Classes.Account.Login.Field field = await Task.Run(() => login.Return(authCookie, true));
                if (!field.Access) return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg10));

                //Изменяем строку соединения
                db = new DbConnectionSklad(connectionString.Return(field.DirCustomersID, null, true));
                dbRead = new DbConnectionSklad(connectionString.Return(field.DirCustomersID, null, true));


                //Права (1 - Write, 2 - Read, 3 - No Access)
                int iRight = await Task.Run(() => accessRight.Access(connectionString.Return(field.DirCustomersID, null, true), field.DirEmployeeID, "RightDocServicePurches"));
                if (iRight != 1) return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg57(0)));
                //2. Получаем "RightDocServiceWorkshopsOnlyUsers"
                //bool iRightCheck = await Task.Run(() => accessRight.AccessCheck(connectionString.Return(field.DirCustomersID, null, true), field.DirEmployeeID, "RightDocServiceWorkshopsOnlyUsersCheck"));
                //2.1. Админ точки?
                //... ниже, т.к. нам нужен склад ...


                //Разные Функции
                function.NumberDecimalSeparator();

                //Получам настройки
                sysSetting = await db.SysSettings.FindAsync(1);

                #endregion

                #region Параметры

                Params _params = new Params();

                //paramList -список параметров
                var paramList = request.GetQueryNameValuePairs();
                //Параметры
                //int DirPaymentTypeID = Convert.ToInt32(paramList.FirstOrDefault(kv => string.Compare(kv.Key, "DirPaymentTypeID", true) == 0).Value);
                //double SumTotal2a = Convert.ToDouble(paramList.FirstOrDefault(kv => string.Compare(kv.Key, "SumTotal2a", true) == 0).Value.Replace(".", ","));

                #endregion

                #region Проверки

                //Надо получить склад "docServicePurch.DirWarehouseID"
                Models.Sklad.Doc.DocServicePurch docServicePurch = await db.DocServicePurches.FindAsync(id);


                //*** *** ***
                //2.1. Админ точки?
                bool iRightCheck = await Task.Run(() => accessRight.AccessIsAdmin(connectionString.Return(field.DirCustomersID, null, true), field.DirEmployeeID, docServicePurch.DirWarehouseID));
                //*** *** ***


                //Только если Админ точки
                //Админ точки != RightDocServiceWorkshopsOnlyUsersCheck
                if (!iRightCheck) return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg57(0)));

                #endregion


                #region Сохранение

                try
                {
                    using (System.Data.Entity.DbContextTransaction ts = db.Database.BeginTransaction())
                    {
                        db.Configuration.AutoDetectChangesEnabled = false;

                        try
                        {
                            await mDirEmployeeIDChange(db, ts, docServicePurch, id, DirEmployeeID, field);

                            ts.Commit();
                        }
                        catch (Exception ex)
                        {
                            try { ts.Rollback(); ts.Dispose(); } catch { }
                            try { db.Database.Connection.Close(); db.Database.Connection.Dispose(); } catch { }

                            return Ok(returnServer.Return(false, exceptionEntry.Return(ex)));
                        }
                    }

                    dynamic collectionWrapper = new
                    {
                        DocID = docServicePurch.DocID,
                        DocServicePurchID = docServicePurch.DocServicePurchID
                    };
                    return Ok(returnServer.Return(true, collectionWrapper));
                }
                catch (Exception ex)
                {
                    return Ok(returnServer.Return(false, exceptionEntry.Return(ex)));
                }

                #endregion
            }
            catch (Exception ex)
            {
                return Ok(returnServer.Return(false, exceptionEntry.Return(ex)));
            }
        }




        // POST: api/DocServicePurches
        [ResponseType(typeof(DocServicePurch))]
        public async Task<IHttpActionResult> PostDocServicePurch(DocServicePurch docServicePurch, HttpRequestMessage request)
        {
            #region Проверяем Логин и Пароль + Изменяем строку соединения + Права + Разные Функции

            //Получаем Куку
            System.Web.HttpCookie authCookie = System.Web.HttpContext.Current.Request.Cookies["CookieIPOL"];

            // Проверяем Логин и Пароль
            Classes.Account.Login.Field field = await Task.Run(() => login.Return(authCookie, true));
            if (!field.Access) return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg10));

            //Изменяем строку соединения
            db = new DbConnectionSklad(connectionString.Return(field.DirCustomersID, null, true));
            dbRead = new DbConnectionSklad(connectionString.Return(field.DirCustomersID, null, true));

            //Права (1 - Write, 2 - Read, 3 - No Access)
            int iRight = await Task.Run(() => accessRight.Access(connectionString.Return(field.DirCustomersID, null, true), field.DirEmployeeID, "RightDocServicePurches"));
            if (iRight != 1) return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg57(0)));

            //Разные Функции
            function.NumberDecimalSeparator();

            //Получам настройки
            sysSetting = await db.SysSettings.FindAsync(1);

            #endregion

            #region Параметры

            //save, save_close, held, held_cancel
            var paramList = request.GetQueryNameValuePairs();

            //1 - Приёмка, 2 - Мастерская, 3 - Выдача
            Params _params = new Params();
            _params.iTypeService = Convert.ToInt32(paramList.FirstOrDefault(kv => string.Compare(kv.Key, "iTypeService", true) == 0).Value); //Записей на страницу

            string UO_Action = paramList.FirstOrDefault(kv => string.Compare(kv.Key, "UO_Action", true) == 0).Value;
            if (_params.iTypeService == 3 && UO_Action == null) return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg101));
            UO_Action = UO_Action.ToLower();

            //Получаем колекцию "Спецификации"

            Models.Sklad.Doc.DocServicePurch1Tab[] docServicePurch1TabCollection = null;
            if (!String.IsNullOrEmpty(docServicePurch.recordsDocServicePurch1Tab))
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                docServicePurch1TabCollection = serializer.Deserialize<Models.Sklad.Doc.DocServicePurch1Tab[]>(docServicePurch.recordsDocServicePurch1Tab);
            }

            Models.Sklad.Doc.DocServicePurch2Tab[] docServicePurch2TabCollection = null;
            if (!String.IsNullOrEmpty(docServicePurch.recordsDocServicePurch2Tab))
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                docServicePurch2TabCollection = serializer.Deserialize<Models.Sklad.Doc.DocServicePurch2Tab[]>(docServicePurch.recordsDocServicePurch2Tab);
            }

            #endregion

            #region Проверки

            if (!ModelState.IsValid && _params.iTypeService != 3) return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg91)); //return BadRequest(ModelState);

            //Подстановки - некоторые поля надо заполнить, если они не заполены
            docServicePurch.Substitute();

            #endregion


            #region Сохранение

            string res = "";

            try
            {
                using (System.Data.Entity.DbContextTransaction ts = db.Database.BeginTransaction())
                {
                    db.Configuration.AutoDetectChangesEnabled = false;

                    try
                    {
                        //Используем метод, что бы было всё в одном потоке
                        docServicePurch = await Task.Run(() => mPutPostDocServicePurch(db, dbRead, UO_Action, docServicePurch, docServicePurch1TabCollection, docServicePurch2TabCollection, EntityState.Added, _params.iTypeService, field)); //sysSetting
                        ts.Commit(); //.Complete();


                        try
                        {
                            #region 5. Sms

                            if (
                                docServicePurch.DirServiceContractorPhone != null && docServicePurch.DirServiceContractorPhone.Length > 7
                                &&
                                sysSetting.DocServicePurchSmsAutoShow
                               )
                            {

                                //string res = "";

                                Models.Sklad.Dir.DirSmsTemplate dirSmsTemplate = await db.DirSmsTemplates.FindAsync(6);

                                string DirSmsTemplateMsg = await function.mSms_DocServicePurches(db, dirSmsTemplate, docServicePurch, sysSetting);
                                
                                PartionnyAccount.Controllers.Sklad.SMS.SmsController smsController = new SMS.SmsController();
                                res = await smsController.SenSms(
                                    //res,
                                    sysSetting,
                                    40,
                                    6,
                                    docServicePurch.DirServiceContractorPhone,
                                    DirSmsTemplateMsg, //dirSmsTemplate.DirSmsTemplateMsg,
                                    field,
                                    db,
                                    Convert.ToInt32(docServicePurch.DocServicePurchID)
                                    );
                                
                            }

                            #endregion
                        }
                        catch (Exception ex7) { }
                    }
                    catch (Exception ex)
                    {
                        try { ts.Rollback(); ts.Dispose(); } catch { }
                        try { db.Database.Connection.Close(); db.Database.Connection.Dispose(); } catch { }

                        return Ok(returnServer.Return(false, exceptionEntry.Return(ex)));
                    }
                }


                #region 6. JourDisp *** *** *** *** *** *** *** *** *** *

                Models.Sklad.Sys.SysJourDisp sysJourDisp = new Models.Sklad.Sys.SysJourDisp();
                sysJourDisp.DirDispOperationID = 3; //Добавление записи
                sysJourDisp.DirEmployeeID = field.DirEmployeeID;
                sysJourDisp.ListObjectID = ListObjectID;
                sysJourDisp.TableFieldID = docServicePurch.DocServicePurchID;
                sysJourDisp.Description = "";
                try { sysJourDispsController.mPutPostSysJourDisps(db, sysJourDisp, EntityState.Added); } catch (Exception ex) { }

                #endregion


                dynamic collectionWrapper = new
                {
                    DocID = docServicePurch.DocID,
                    DocServicePurchID = docServicePurch.DocServicePurchID,
                    Msg = res
                };
                return Ok(returnServer.Return(true, collectionWrapper)); //return Ok(returnServer.Return(true, ""));
            }
            catch (Exception ex)
            {
                return Ok(returnServer.Return(false, exceptionEntry.Return(ex)));
            }

            #endregion
        }

        // DELETE: api/DocServicePurches/5
        [ResponseType(typeof(DocServicePurch))]
        public async Task<IHttpActionResult> DeleteDocServicePurch(int id)
        {
            #region Проверяем Логин и Пароль + Изменяем строку соединения + Права + Разные Функции

            //Получаем Куку
            System.Web.HttpCookie authCookie = System.Web.HttpContext.Current.Request.Cookies["CookieIPOL"];

            // Проверяем Логин и Пароль
            Classes.Account.Login.Field field = await Task.Run(() => login.Return(authCookie, true));
            if (!field.Access) return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg10));

            //Изменяем строку соединения
            db = new DbConnectionSklad(connectionString.Return(field.DirCustomersID, null, true));
            dbRead = new DbConnectionSklad(connectionString.Return(field.DirCustomersID, null, true));

            //Права (1 - Write, 2 - Read, 3 - No Access)
            int iRight = await Task.Run(() => accessRight.Access(connectionString.Return(field.DirCustomersID, null, true), field.DirEmployeeID, "RightDocServicePurches"));
            if (iRight != 1) return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg57(0)));

            //Разные Функции
            function.NumberDecimalSeparator();

            //Получам настройки
            Models.Sklad.Sys.SysSetting sysSetting = await db.SysSettings.FindAsync(1);

            #endregion


            #region Удаление

            //Алгоритм.
            //Удаляем по порядку:
            //1. RemParties
            //2. DocServicePurchTabs
            //3. DocServicePurches
            //4. Docs


            //Сотрудник
            Models.Sklad.Doc.DocServicePurch docServicePurch = await db.DocServicePurches.FindAsync(id);
            if (docServicePurch == null) return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg99));


            using (System.Data.Entity.DbContextTransaction ts = db.Database.BeginTransaction())
            {
                try
                {
                    #region 1. Ищим DocID *** *** *** *** ***

                    //1.1. Ищим DocID
                    int iDocID = 0;
                    var queryDocs1 = await
                        (
                            from x in db.DocServicePurches
                            where x.DocServicePurchID == id
                            select x
                        ).ToListAsync();
                    if (queryDocs1.Count() > 0) iDocID = Convert.ToInt32(queryDocs1[0].DocID);
                    else return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg99));

                    #endregion


                    #region 1. Ищим в Возврате покупателя, если нет, то удаляем в RemPartyMinuses *** *** *** *** ***

                    //1.1. Удаляем "RemPartyMinuses"
                    var queryRemPartyMinuses = await
                        (
                            from x in db.RemPartyMinuses
                            where x.DocID == iDocID
                            select x
                        ).ToListAsync();

                    for (int i = 0; i < queryRemPartyMinuses.Count(); i++)
                    {
                        int iRemPartyMinusID = Convert.ToInt32(queryRemPartyMinuses[i].RemPartyMinusID);

                        var queryDocReturnsCustomerTab = await
                            (
                                from x in db.DocReturnsCustomerTabs
                                where x.RemPartyMinusID == iRemPartyMinusID
                                select x
                            ).ToListAsync();

                        if (queryDocReturnsCustomerTab.Count() > 0)
                        {
                            throw new System.InvalidOperationException(
                                Classes.Language.Sklad.Language.msg112 +

                                "<tr>" +
                                "<td>" + queryDocReturnsCustomerTab[0].RemPartyMinusID + "</td>" +                           //партия списания
                                "<td>" + queryDocReturnsCustomerTab[0].DocReturnsCustomerID + "</td>" +                      //№ д-та
                                "<td>" + queryDocReturnsCustomerTab[0].DirNomenID + "</td>" +                                //Код товара
                                "<td>" + queryDocReturnsCustomerTab[0].Quantity + "</td>" +                                  //списуемое к-во
                                "</tr>" +
                                "</table>" +

                                Classes.Language.Sklad.Language.msg112_1
                                );
                        }

                        Models.Sklad.Rem.RemPartyMinus remPartyMinus = await db.RemPartyMinuses.FindAsync(iRemPartyMinusID);
                        db.RemPartyMinuses.Remove(remPartyMinus);
                        await db.SaveChangesAsync();
                    }

                    #endregion


                    #region 2. DocServicePurch1Tabs *** *** *** *** ***

                    var queryDocServicePurch1Tabs = await
                        (
                            from x in db.DocServicePurch1Tabs
                            where x.DocServicePurchID == id
                            select x
                        ).ToListAsync();
                    for (int i = 0; i < queryDocServicePurch1Tabs.Count(); i++)
                    {
                        Models.Sklad.Doc.DocServicePurch1Tab docServicePurch1Tab = await db.DocServicePurch1Tabs.FindAsync(queryDocServicePurch1Tabs[i].DocServicePurch1TabID);
                        db.DocServicePurch1Tabs.Remove(docServicePurch1Tab);
                        await db.SaveChangesAsync();
                    }

                    #endregion

                    #region 2. DocServicePurch2Tabs *** *** *** *** ***

                    var queryDocServicePurch2Tabs = await
                        (
                            from x in db.DocServicePurch2Tabs
                            where x.DocServicePurchID == id
                            select x
                        ).ToListAsync();
                    for (int i = 0; i < queryDocServicePurch2Tabs.Count(); i++)
                    {
                        Models.Sklad.Doc.DocServicePurch2Tab docServicePurch2Tab = await db.DocServicePurch2Tabs.FindAsync(queryDocServicePurch2Tabs[i].DocServicePurch2TabID);
                        db.DocServicePurch2Tabs.Remove(docServicePurch2Tab);
                        await db.SaveChangesAsync();
                    }

                    #endregion


                    #region 3. DocServicePurches *** *** *** *** ***

                    var queryDocServicePurches = await
                        (
                            from x in db.DocServicePurches
                            where x.DocServicePurchID == id
                            select x
                        ).ToListAsync();
                    for (int i = 0; i < queryDocServicePurches.Count(); i++)
                    {
                        Models.Sklad.Doc.DocServicePurch docServicePurch1 = await db.DocServicePurches.FindAsync(queryDocServicePurches[i].DocServicePurchID);
                        db.DocServicePurches.Remove(docServicePurch1);
                        await db.SaveChangesAsync();
                    }

                    #endregion


                    #region 4. Doc *** *** *** *** ***

                    var queryDocs2 = await
                        (
                            from x in db.Docs
                            where x.DocID == iDocID
                            select x
                        ).ToListAsync();
                    for (int i = 0; i < queryDocs2.Count(); i++)
                    {
                        Models.Sklad.Doc.Doc doc = await db.Docs.FindAsync(queryDocs2[i].DocID);
                        db.Docs.Remove(doc);
                        await db.SaveChangesAsync();
                    }

                    #endregion


                    ts.Commit();


                    #region 6. JourDisp *** *** *** *** *** *** *** *** *** *

                    Models.Sklad.Sys.SysJourDisp sysJourDisp = new Models.Sklad.Sys.SysJourDisp();
                    sysJourDisp.DirDispOperationID = 5; //Удаление записи
                    sysJourDisp.DirEmployeeID = field.DirEmployeeID;
                    sysJourDisp.ListObjectID = ListObjectID;
                    sysJourDisp.TableFieldID = id;
                    sysJourDisp.Description = "";
                    try { sysJourDispsController.mPutPostSysJourDisps(db, sysJourDisp, EntityState.Added); } catch (Exception ex) { }

                    #endregion


                    dynamic collectionWrapper = new
                    {
                        ID = id,
                        Msg = Classes.Language.Sklad.Language.msg19
                    };
                    return Ok(returnServer.Return(true, collectionWrapper)); //return Ok(returnServer.Return(true, ""));
                }
                catch (Exception ex)
                {
                    try { ts.Rollback(); ts.Dispose(); } catch { }
                    try { db.Database.Connection.Close(); db.Database.Connection.Dispose(); } catch { }

                    return Ok(returnServer.Return(false, exceptionEntry.Return(ex)));
                } //catch

            } //DbContextTransaction

            #endregion

        }

        #endregion


        #region Mthods

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool DocServicePurchExists(int id)
        {
            return db.DocServicePurches.Count(e => e.DocServicePurchID == id) > 0;
        }


        internal async Task<DocServicePurch> mPutPostDocServicePurch(
            DbConnectionSklad db,
            DbConnectionSklad dbRead,
            string UO_Action,
            DocServicePurch docServicePurch,
            Models.Sklad.Doc.DocServicePurch1Tab[] docServicePurch1TabCollection,
            Models.Sklad.Doc.DocServicePurch2Tab[] docServicePurch2TabCollection,
            EntityState entityState, //EntityState.Added, Modified
            int iTypeService,

            Classes.Account.Login.Field field //Дополнительные данные о сотруднике
            )
        {
            
            // Если DirServiceStatusID > 1, то не сохранять, а выводить сообщение!
            if (iTypeService == 1 && docServicePurch.DirServiceStatusID > 1) { throw new System.InvalidOperationException(Classes.Language.Sklad.Language.msg113); }
            // Если DirServiceStatusID = 7 и нет ни одной выполненной работы, то не пускать сохранять и выдать эксепшн
            if (iTypeService > 1 && docServicePurch.DirServiceStatusID == 7 && docServicePurch1TabCollection.Length == 0) { throw new System.InvalidOperationException(Classes.Language.Sklad.Language.msg114); }




            //Единственный рабочий вариант
            if (iTypeService == 1)
            {
                #region 1


                #region 0. Заполняем DirServiceContractors
                // - не находим - создаём новую
                // - находим - обновляем

                Models.Sklad.Dir.DirServiceContractor dirServiceContractor = new Models.Sklad.Dir.DirServiceContractor();
                if (!Convert.ToBoolean(docServicePurch.UrgentRepairs))
                {

                    string DirServiceContractorPhone = docServicePurch.DirServiceContractorPhone.Replace("+", "").ToLower();

                    if (!String.IsNullOrEmpty(DirServiceContractorPhone))
                    {
                        var queryDirServiceContractors = await
                            (
                                from x in db.DirServiceContractors
                                where x.DirServiceContractorPhone == DirServiceContractorPhone
                                select x
                            ).ToListAsync();
                        if (queryDirServiceContractors.Count() == 0)
                        {
                            dirServiceContractor = new Models.Sklad.Dir.DirServiceContractor();
                            dirServiceContractor.DirServiceContractorPhone = DirServiceContractorPhone;
                            dirServiceContractor.DirServiceContractorName = docServicePurch.DirServiceContractorName;
                            dirServiceContractor.QuantityOk = 0;
                            dirServiceContractor.QuantityFail = 0;
                            dirServiceContractor.QuantityCount = 0;

                            db.Entry(dirServiceContractor).State = EntityState.Added;
                            await db.SaveChangesAsync();
                        }
                        else
                        {
                            dirServiceContractor = await db.DirServiceContractors.FindAsync(queryDirServiceContractors[0].DirServiceContractorID);
                            dirServiceContractor.DirServiceContractorName = docServicePurch.DirServiceContractorName;

                            db.Entry(dirServiceContractor).State = EntityState.Modified;
                            await db.SaveChangesAsync();
                        }
                    }
                }

                #endregion


                //Сохраняем Шапку, только, если это Приёмка

                #region 1. Doc *** *** *** *** *** *** *** *** *** ***

                //Модель
                Models.Sklad.Doc.Doc doc = new Models.Sklad.Doc.Doc();
                //Присваиваем значения
                doc.ListObjectID = ListObjectID;
                doc.IsImport = false;
                doc.NumberInt = docServicePurch.NumberInt;
                doc.NumberReal = docServicePurch.DocServicePurchID;
                doc.DirEmployeeID = field.DirEmployeeID;
                doc.DirPaymentTypeID = docServicePurch.DirPaymentTypeID;
                doc.Payment = docServicePurch.Payment;
                doc.DirContractorID = docServicePurch.DirContractorIDOrg;
                doc.DirContractorIDOrg = docServicePurch.DirContractorIDOrg;
                doc.Discount = docServicePurch.Discount;
                doc.DirVatValue = docServicePurch.DirVatValue;
                doc.Base = docServicePurch.Base;
                doc.Description = docServicePurch.Description;
                doc.DocDate = docServicePurch.DocDate;
                //doc.DocDisc = docServicePurch.DocDisc;
                doc.Held = false;  //if (UO_Action == "held") doc.Held = false; //else doc.Held = false;
                doc.DocID = docServicePurch.DocID;
                doc.DocIDBase = docServicePurch.DocIDBase;

                //Класс
                Docs.Docs docs = new Docs.Docs(db, dbRead, doc, entityState);
                //doc = await docs.Save();
                await Task.Run(() => docs.Save());

                //Нужно вернуть "docServicePurch" со всем полями!
                docServicePurch.DocID = doc.DocID;

                #endregion

                #region 2. DocServicePurch *** *** *** *** *** *** *** *** *** ***


                #region Если выбрана хоть одна типовая несисправность - статус "Согласован" (3)
                if (
                    Convert.ToBoolean(docServicePurch.DirServiceNomenTypicalFaultID1) || Convert.ToBoolean(docServicePurch.DirServiceNomenTypicalFaultID2) || Convert.ToBoolean(docServicePurch.DirServiceNomenTypicalFaultID3) ||
                    Convert.ToBoolean(docServicePurch.DirServiceNomenTypicalFaultID4) || Convert.ToBoolean(docServicePurch.DirServiceNomenTypicalFaultID5) || Convert.ToBoolean(docServicePurch.DirServiceNomenTypicalFaultID4) ||
                    Convert.ToBoolean(docServicePurch.DirServiceNomenTypicalFaultID7)
                  )
                {
                    docServicePurch.DirServiceStatusID = 4;
                }

                #endregion


                #region Сохранение

                docServicePurch.DocID = doc.DocID;
                //docServicePurch.DirEmployeeIDMaster = field.DirEmployeeID; //Это мастер, его пока нет в форме.
                docServicePurch.ServiceTypeRepair = sysSetting.ServiceTypeRepair;
                docServicePurch.DirServiceContractorID = dirServiceContractor.DirServiceContractorID;
                if (docServicePurch.Sums == null) docServicePurch.Sums = 0;
                if (docServicePurch.Sums1 == null) docServicePurch.Sums1 = 0;
                if (docServicePurch.Sums2 == null) docServicePurch.Sums2 = 0;

                db.Entry(docServicePurch).State = entityState;
                await db.SaveChangesAsync();

                #endregion


                #region UpdateNumberInt, если INSERT *** *** *** *** ***

                if (entityState == EntityState.Added && (docServicePurch.doc.NumberInt == null || docServicePurch.doc.NumberInt.Length == 0))
                {
                    doc.NumberInt = docServicePurch.DocServicePurchID.ToString();
                    doc.NumberReal = docServicePurch.DocServicePurchID;
                    docs = new Docs.Docs(db, dbRead, doc, EntityState.Modified);
                    await Task.Run(() => docs.Save());
                }
                else if (entityState == EntityState.Added)
                {
                    doc.NumberReal = docServicePurch.DocServicePurchID;
                    docs = new Docs.Docs(db, dbRead, doc, EntityState.Modified);
                    await Task.Run(() => docs.Save());
                }

                #endregion


                #endregion

                #region 3. Касса или Банк

                //Только, если сумма больше 0
                if (docServicePurch.PrepaymentSum > 0)  //if (doc.Payment > 0)
                {
                    //Получаем наименование аппарата
                    Models.Sklad.Dir.DirServiceNomen dirServiceNomen = await db.DirServiceNomens.FindAsync(docServicePurch.DirServiceNomenID);

                    //Касса
                    if (doc.DirPaymentTypeID == 1)
                    {
                        #region Касса

                        //1. По складу находим привязанную к нему Кассу
                        Models.Sklad.Dir.DirWarehouse dirWarehouse = db.DirWarehouses.Find(docServicePurch.DirWarehouseID);
                        int iDirCashOfficeID = dirWarehouse.DirCashOfficeID;

                        //2. Заполняем модель "DocCashOfficeSum"
                        Models.Sklad.Doc.DocCashOfficeSum docCashOfficeSum = new Models.Sklad.Doc.DocCashOfficeSum();
                        docCashOfficeSum.DirCashOfficeID = iDirCashOfficeID;
                        docCashOfficeSum.DirCashOfficeSumTypeID = 14;
                        docCashOfficeSum.DocCashOfficeSumDate = DateTime.Now;
                        docCashOfficeSum.DocID = doc.DocID;
                        docCashOfficeSum.DocXID = docServicePurch.DocServicePurchID;
                        docCashOfficeSum.DocCashOfficeSumSum = docServicePurch.PrepaymentSum; //doc.Payment;
                        docCashOfficeSum.Description = "";
                        docCashOfficeSum.DirEmployeeID = field.DirEmployeeID;
                        docCashOfficeSum.Base = "Предоплата за аппарат: " + dirServiceNomen.DirServiceNomenName;

                        //3. Пишем в Кассу
                        Doc.DocCashOfficeSums.DocCashOfficeSumsController docCashOfficeSumsController = new Doc.DocCashOfficeSums.DocCashOfficeSumsController();
                        docCashOfficeSum = await Task.Run(() => docCashOfficeSumsController.mPutPostDocCashOfficeSum(db, docCashOfficeSum, EntityState.Added));

                        #endregion
                    }
                    //Банк
                    else if (doc.DirPaymentTypeID == 2)
                    {
                        #region Банк

                        //1. По складу находим привязанную к нему Кассу
                        Models.Sklad.Dir.DirWarehouse dirWarehouse = db.DirWarehouses.Find(docServicePurch.DirWarehouseID);
                        int iDirBankID = dirWarehouse.DirBankID;

                        //2. Заполняем модель "DocBankSum"
                        Models.Sklad.Doc.DocBankSum docBankSum = new Models.Sklad.Doc.DocBankSum();
                        docBankSum.DirBankID = iDirBankID;
                        docBankSum.DirBankSumTypeID = 13; //Изъятие из кассы на основании проведения приходной накладной №
                        docBankSum.DocBankSumDate = DateTime.Now;
                        docBankSum.DocID = doc.DocID;
                        docBankSum.DocXID = docServicePurch.DocServicePurchID;
                        docBankSum.DocBankSumSum = docServicePurch.PrepaymentSum; //doc.Payment;
                        docBankSum.Description = "";
                        docBankSum.DirEmployeeID = field.DirEmployeeID;
                        docBankSum.Base = "Предоплата за аппарат: " + dirServiceNomen.DirServiceNomenName;

                        //3. Пишем в Банк
                        Doc.DocBankSums.DocBankSumsController docBankSumsController = new Doc.DocBankSums.DocBankSumsController();
                        docBankSum = await Task.Run(() => docBankSumsController.mPutPostDocBankSum(db, docBankSum, EntityState.Added));

                        #endregion
                    }
                }
                
                #endregion

                #region 4. Log

                logService.DocServicePurchID = docServicePurch.DocServicePurchID;
                logService.DirServiceLogTypeID = 1;
                logService.DirEmployeeID = field.DirEmployeeID;
                logService.DirServiceStatusID = docServicePurch.DirServiceStatusID;

                await logServicesController.mPutPostLogServices(db, logService, EntityState.Added);

                #endregion

                #region 5. Если заполненно поле "ComponentOtherText", то ищим похожую запись в таблице "DirServiceComplects.DirServiceComplectName"
                // - не находим - создаём новую
                // - находим - ничего не делаем

                if (!String.IsNullOrEmpty(docServicePurch.ComponentOtherText))
                {
                    var queryDirServiceComplects = await
                        (
                            from x in db.DirServiceComplects
                            where x.DirServiceComplectName.ToLower() == docServicePurch.ComponentOtherText.ToLower()
                            select x
                        ).ToListAsync();
                    if (queryDirServiceComplects.Count() == 0)
                    {
                        Models.Sklad.Dir.DirServiceComplect dirServiceComplect = new Models.Sklad.Dir.DirServiceComplect();
                        dirServiceComplect.DirServiceComplectName = docServicePurch.ComponentOtherText;

                        db.Entry(dirServiceComplect).State = EntityState.Added;
                        await db.SaveChangesAsync();
                    }
                }

                #endregion

                #endregion
            }



            #region OLD - НЕ используется
            /*
            else if (iTypeService == 2)
            {
                #region 2 - НЕ используется

                //Сохраняем в Шапке, только Статус - не получилось, глючит: обнуляет значения для модели "Docs" (она содержится в моделе DocServicePurches)

                #region 1. DocServicePurch *** *** *** *** *** *** *** *** *** ***

                //Сохраняем
                db.Entry(docServicePurch).State = entityState;
                await db.SaveChangesAsync();

                #endregion


                //Спецификация

                #region 3. DocServicePurch1Tab *** *** *** *** *** *** *** *** ***

                //2.1. Удаляем записи в БД, если UPDATE
                if (entityState == EntityState.Modified)
                {
                    SQLiteParameter parDocServicePurchID = new SQLiteParameter("@DocServicePurchID", System.Data.DbType.Int32) { Value = docServicePurch.DocServicePurchID };
                    db.Database.ExecuteSqlCommand("DELETE FROM DocServicePurch1Tabs WHERE DocServicePurchID=@DocServicePurchID;", parDocServicePurchID);
                }

                //2.2. Проставляем ID-шник "DocServicePurchID" для всех позиций спецификации
                for (int i = 0; i < docServicePurch1TabCollection.Count(); i++)
                {
                    docServicePurch1TabCollection[i].DocServicePurch1TabID = null;
                    docServicePurch1TabCollection[i].DocServicePurchID = Convert.ToInt32(docServicePurch.DocServicePurchID);
                    db.Entry(docServicePurch1TabCollection[i]).State = EntityState.Added;
                }
                await db.SaveChangesAsync();

                #endregion


                #region 4. DocServicePurch2Tab *** *** *** *** *** *** *** *** ***

                //2.1. Удаляем записи в БД, если UPDATE
                if (entityState == EntityState.Modified)
                {
                    SQLiteParameter parDocServicePurchID = new SQLiteParameter("@DocServicePurchID", System.Data.DbType.Int32) { Value = docServicePurch.DocServicePurchID };
                    db.Database.ExecuteSqlCommand("DELETE FROM DocServicePurch2Tabs WHERE DocServicePurchID=@DocServicePurchID;", parDocServicePurchID);
                }

                //2.2. Проставляем ID-шник "DocServicePurchID" для всех позиций спецификации
                for (int i = 0; i < docServicePurch2TabCollection.Count(); i++)
                {
                    docServicePurch2TabCollection[i].DocServicePurch2TabID = null;
                    docServicePurch2TabCollection[i].DocServicePurchID = Convert.ToInt32(docServicePurch.DocServicePurchID);
                    db.Entry(docServicePurch2TabCollection[i]).State = EntityState.Added;
                }
                await db.SaveChangesAsync();

                #endregion


                //Списание Партий

                Controllers.Sklad.Rem.RemPartyMinusesController remPartyMinuses = new Rem.RemPartyMinusesController();


                #region Удаляем все записи из таблицы "RemPartyMinuses"
                //Удаляем все записи из таблицы "RemPartyMinuses"
                //Что бы правильно Проверяло на Остаток.
                //А то, товар уже списан, а я проверяю на остаток!

                await Task.Run(() => remPartyMinuses.Delete(db, Convert.ToInt32(docServicePurch.DocID))); //doc.DocID

                #endregion


                #region Проверки и Списание с партий (RemPartyMinuses)

                for (int i = 0; i < docServicePurch2TabCollection.Count(); i++)
                {
                    #region Проверка

                    //Переменные
                    int iRemPartyID = docServicePurch2TabCollection[i].RemPartyID;
                    double dQuantity = 1; // docServicePurch2TabCollection[i].Quantity;
                    //Находим партию
                    Models.Sklad.Rem.RemParty remParty = await db.RemParties.FindAsync(iRemPartyID);
                    db.Entry(remParty).Reload(); // - Это Важно! Триггер изменил значения, то они НЕ видны в проекте, надо обновить значения!!!

                    #region 1. Есть ли остаток в партии с которой списываем!
                    if (remParty.Remnant < dQuantity)
                    {
                        throw new System.InvalidOperationException(
                            Classes.Language.Sklad.Language.msg104 +

                            "<tr>" +
                            "<td>" + docServicePurch2TabCollection[i].RemPartyID + "</td>" +    //Партия
                            "<td>" + docServicePurch2TabCollection[i].DirNomenID + "</td>" +    //Код товара
                            "<td>" + 1 + "</td>" +                                              //списуемое к-во (docServicePurch2TabCollection[i].Quantity)
                            "<td>" + remParty.Remnant + "</td>" +                               //остаток партии
                            "<td>" + (1 - remParty.Remnant).ToString() + "</td>" +              //недостающее к-во (docServicePurch2TabCollection[i].Quantity - ...)
                            "</tr>" +
                            "</table>" +

                            Classes.Language.Sklad.Language.msg104_1
                        );
                    }
                    #endregion

                    #region 2. Склад: склад документа должен соответствовать каждой списуемой партии!
                    if (remParty.DirWarehouseID != docServicePurch.DirWarehouseID)
                    {
                        //Это нужно, т.к. к нам от клиента не пришли все значения модели: "docServicePurch.dirWarehouse.DirWarehouseName"
                        Models.Sklad.Dir.DirWarehouse dirWarehouse = await db.DirWarehouses.FindAsync(docServicePurch.DirWarehouseID);

                        throw new System.InvalidOperationException(
                            Classes.Language.Sklad.Language.msg105 +

                            "<tr>" +
                            "<td>" + docServicePurch2TabCollection[i].RemPartyID + "</td>" +           //партия
                            "<td>" + docServicePurch2TabCollection[i].DirNomenID + "</td>" +           //Код товара
                            "<td>" + dirWarehouse.DirWarehouseName + "</td>" +                //склад документа
                            "<td>" + remParty.dirWarehouse.DirWarehouseName + "</td>" +       //склад партии
                            "</tr>" +
                            "</table>" +

                            Classes.Language.Sklad.Language.msg105_1
                        );
                    }
                    #endregion

                    #region 3. Организация: организация документа должен соответствовать каждой списуемой партии!
                    if (remParty.DirContractorIDOrg != docServicePurch.DirContractorIDOrg)
                    {
                        //Это нужно, т.к. к нам от клиента не пришли все значения модели: "docServicePurch.dirWarehouse.DirWarehouseName"
                        int iDirContractorIDOrg = docServicePurch.DirContractorIDOrg;
                        int iDirCurrencyID = docServicePurch.DirCurrencyID;
                        Models.Sklad.Dir.DirContractor dirContractor = await db.DirContractors.FindAsync(iDirContractorIDOrg);

                        throw new System.InvalidOperationException(
                            Classes.Language.Sklad.Language.msg106 +

                            "<tr>" +
                            "<td>" + docServicePurch2TabCollection[i].RemPartyID + "</td>" +           //партия
                            "<td>" + docServicePurch2TabCollection[i].DirNomenID + "</td>" +           //Код товара
                            "<td>" + dirContractor.DirContractorName + "</td>" +              //организация спецификации
                            "<td>" + remParty.dirContractorOrg.DirContractorName + "</td>" +  //организация партии
                            "</tr>" +
                            "</table>" +

                            Classes.Language.Sklad.Language.msg106_1
                        );
                    }
                    #endregion

                    #endregion


                    #region Сохранение

                    Models.Sklad.Rem.RemPartyMinus remPartyMinus = new Models.Sklad.Rem.RemPartyMinus();
                    remPartyMinus.RemPartyMinusID = null;
                    remPartyMinus.RemPartyID = docServicePurch2TabCollection[i].RemPartyID;
                    remPartyMinus.DirNomenID = docServicePurch2TabCollection[i].DirNomenID;
                    remPartyMinus.Quantity = 1; // docServicePurch2TabCollection[i].Quantity;
                    remPartyMinus.DirCurrencyID = docServicePurch2TabCollection[i].DirCurrencyID;
                    remPartyMinus.DirCurrencyMultiplicity = docServicePurch2TabCollection[i].DirCurrencyMultiplicity;
                    remPartyMinus.DirCurrencyRate = docServicePurch2TabCollection[i].DirCurrencyRate;
                    remPartyMinus.DirVatValue = docServicePurch.DirVatValue;
                    remPartyMinus.DirWarehouseID = docServicePurch.DirWarehouseID;
                    remPartyMinus.DirContractorIDOrg = docServicePurch.DirContractorIDOrg;
                    if (docServicePurch.DirContractorID > 0) remPartyMinus.DirContractorID = docServicePurch.DirContractorID;
                    else remPartyMinus.DirContractorID = docServicePurch.DirContractorIDOrg;
                    remPartyMinus.DocID = Convert.ToInt32(docServicePurch.DocID);
                    remPartyMinus.PriceCurrency = docServicePurch2TabCollection[i].PriceCurrency;
                    remPartyMinus.PriceVAT = docServicePurch2TabCollection[i].PriceVAT;
                    remPartyMinus.FieldID = Convert.ToInt32(docServicePurch2TabCollection[i].DocServicePurch2TabID);
                    remPartyMinus.Reserve = false; // docServicePurch.Reserve;

                    db.Entry(remPartyMinus).State = EntityState.Added;
                    await db.SaveChangesAsync();

                    #endregion
                }

                #endregion

                #endregion
            }
            else if (iTypeService == 3)
            {
                #region 3 - НЕ используется

                //Сохраняем в Документе только поле Held = true

                var query = await
                    (
                        from x in db.DocServicePurches
                        where x.DocServicePurchID == docServicePurch.DocServicePurchID
                        select new
                        {
                            DirServiceStatusName = x.dirServiceStatus.DirServiceStatusName
                        }
                    ).ToListAsync();

                string DirServiceStatusName = "";
                if (query.Count() > 0) DirServiceStatusName = query[0].DirServiceStatusName;


                #region 2. DocServicePurch *** *** *** *** *** *** *** *** *** ***

                //Т.к. после сохранения это поле будет == 0
                double SumTotal2 = Convert.ToDouble(docServicePurch.SumTotal2);

                //Сохраняем
                docServicePurch = await db.DocServicePurches.FindAsync(docServicePurch.DocServicePurchID);
                docServicePurch.DirServiceStatusID = 9;
                db.Entry(docServicePurch).State = entityState;
                //db.Entry(docServicePurch).Reload();
                await db.SaveChangesAsync();

                #endregion


                #region 1. Doc *** *** *** *** *** *** *** *** *** ***

                //Модель
                Models.Sklad.Doc.Doc doc = await db.Docs.FindAsync(docServicePurch.DocID);
                doc.Held = true;
                doc.Payment = SumTotal2; // Convert.ToDouble(docServicePurch.SumTotal2);
                doc.Description = DirServiceStatusName;

                Docs.Docs docs = new Docs.Docs(db, dbRead, doc, entityState);
                await Task.Run(() => docs.Save());

                //Нужно вернуть "docServicePurch" со всем полями!
                docServicePurch.DocID = doc.DocID;

                #endregion

                
                #endregion
            }
            else if (iTypeService == 4)
            {
                #region 4 - НЕ используется
                //На доработку

                #region 2. DocServicePurch *** *** *** *** *** *** *** *** *** ***

                //Сохраняем
                docServicePurch = await db.DocServicePurches.FindAsync(docServicePurch.DocServicePurchID);
                docServicePurch.DirServiceStatusID = 2;
                db.Entry(docServicePurch).State = entityState;
                await db.SaveChangesAsync();

                #endregion

                #endregion
            }
            */
            #endregion



            #region n. Подтверждение транзакции - НЕ используется

            //ts.Commit(); //.Complete();

            #endregion


            return docServicePurch;
        }


        internal async Task<bool> mStatusChange(
            DbConnectionSklad db,
            System.Data.Entity.DbContextTransaction ts,
            Models.Sklad.Doc.DocServicePurch docServicePurch,
            int id,
            int DirServiceStatusID,
            int DirPaymentTypeID,
            double SumTotal2a,
            string sReturnRresults,

            Classes.Account.Login.Field field //Дополнительные данные о сотруднике
            )
        {

            DateTime dtNow = DateTime.Now;

            #region Проверка, если предыдущий статус такой же на который меняем, то не писать в Лог

            //Исключение, т.к. если в Логе нет записей с сменой статуса получим Ошибку из-за "FirstAsync()"
            try
            {
                var query = await
                    (
                        from x in db.LogServices
                        where x.DocServicePurchID == id && x.DirServiceStatusID != null
                        select new
                        {
                            LogServiceID = x.LogServiceID,
                            DirServiceStatusID = x.DirServiceStatusID
                        }
                    ).OrderByDescending(x => x.LogServiceID).FirstAsync();

                if (query.DirServiceStatusID == DirServiceStatusID)
                {
                    return false;
                }
            }
            catch (Exception ex) { }

            #endregion


            #region 1. Сохранение статуса в БД

            docServicePurch = await db.DocServicePurches.FindAsync(id);

            //Сохраняем старый статус, ниже - нужен
            int? DirServiceStatusID_OLD = docServicePurch.DirServiceStatusID;

            //Если Статус == 9 (Выдан), то менять "DateDone" на текущую
            if (DirServiceStatusID == 9)
            {
                docServicePurch.DateDone = dtNow;
                docServicePurch.FromGuaranteeCount = Convert.ToInt32(docServicePurch.FromGuaranteeCount) + 1;
                docServicePurch.Summ_NotPre = SumTotal2a;
                //docServicePurch.DirServiceStatusID_789 = DirServiceStatusID_OLD; //Статус аппарата сохранить: Готов или Отказ
            }

            //Сохранить статус аппарата: Готов (7) или Отказ (8)
            if (DirServiceStatusID == 7 || DirServiceStatusID == 8) // || DirServiceStatusID == 9
            {
                docServicePurch.DirServiceStatusID_789 = DirServiceStatusID; //Статус аппарата сохранить: Готов или Отказ
            }

            //Если был Статус == 9 (Выдан) и сменили на "В диагностике", то менять "DateDone" на текущую + 7 дней (из настроек)
            bool bDirServiceLogTypeID9 = false;
            if (docServicePurch.DirServiceStatusID == 9 && DirServiceStatusID == 2)
            {
                //1. Проверяем есть ли ещё Гарантия
                if (docServicePurch.DateDone.AddMonths(docServicePurch.ServiceTypeRepair) <= dtNow && sysSetting.WarrantyPeriodPassed)
                {
                    //Исключение
                    throw new System.InvalidOperationException("Срок гарантии прошёл (до "+ docServicePurch.DateDone.AddMonths(docServicePurch.ServiceTypeRepair).ToString("yyyy-MM-dd") + ")!");
                }

                //2.1. Меняем дату "Готовности"
                docServicePurch.DateDone = dtNow.AddDays(sysSetting.ReadinessDay);

                //2.2 Запоминает первичную дату документа и меняем дату документа на текущую.
                //2.2.1.
                docServicePurch.DocDate_First = docServicePurch.doc.DocDate;
                //2.2.2.
                Models.Sklad.Doc.Doc doc = await db.Docs.FindAsync(docServicePurch.DocID);
                doc.DocDate = dtNow;
                db.Entry(doc).State = EntityState.Modified;
                await Task.Run(() => db.SaveChangesAsync());

                //3. Суммы:
                //   3.1. Переносим PrepaymentSum в PrepaymentSum_1 (2,3,4,5) в зависимости от поля "FromGuaranteeCount" (null-1, 1-2, 2-3, 3-4, 4-5)
                switch (docServicePurch.FromGuaranteeCount)
                {
                    case 1: docServicePurch.PrepaymentSum_1 = Convert.ToDouble(docServicePurch.PrepaymentSum); break;
                    case 2: docServicePurch.PrepaymentSum_2 = Convert.ToDouble(docServicePurch.PrepaymentSum); break;
                    case 3: docServicePurch.PrepaymentSum_3 = Convert.ToDouble(docServicePurch.PrepaymentSum); break;
                    case 4: docServicePurch.PrepaymentSum_4 = Convert.ToDouble(docServicePurch.PrepaymentSum); break;
                    case 5: docServicePurch.PrepaymentSum_5 = Convert.ToDouble(docServicePurch.PrepaymentSum); break;
                }
                //   3.2. меняем сумму пред-оплаты на сумму
                docServicePurch.PrepaymentSum = Convert.ToDouble(Convert.ToDouble(docServicePurch.PrepaymentSum) + Convert.ToDouble(docServicePurch.Summ_NotPre));

                //4. Сообщение для Лога: 
                //   
                bDirServiceLogTypeID9 = true;
            }

            //Если статус: 7 или 8, то заполняем дату "IssuanceDate"
            //!!! СУКА !!! Сдесь ОШИБКА !!! БЛЯДЬ !!!
            //if (docServicePurch.DirServiceStatusID == 7 || docServicePurch.DirServiceStatusID == 8) docServicePurch.IssuanceDate = dtNow;
            if (DirServiceStatusID == 7 || DirServiceStatusID == 8) docServicePurch.IssuanceDate = dtNow;

            //Дата смены статуса
            docServicePurch.DateStatusChange = dtNow;


            docServicePurch.DirServiceStatusID = DirServiceStatusID;

            db.Entry(docServicePurch).State = EntityState.Modified;
            await Task.Run(() => db.SaveChangesAsync());

            #endregion


            #region 2. Касса или Банк

            int? DocCashOfficeSumID = null, DocBankSumID = null;

            //Только, если сумма больше 0
            if (DirServiceStatusID == 9 && SumTotal2a > 0)  //if (doc.Payment > 0)
            {
                //Получаем наименование аппарата
                Models.Sklad.Dir.DirServiceNomen dirServiceNomen = await db.DirServiceNomens.FindAsync(docServicePurch.DirServiceNomenID);

                //Касса
                if (DirPaymentTypeID == 1)
                {
                    #region Касса

                    //1. По складу находим привязанную к нему Кассу
                    Models.Sklad.Dir.DirWarehouse dirWarehouse = db.DirWarehouses.Find(docServicePurch.DirWarehouseID);
                    int iDirCashOfficeID = dirWarehouse.DirCashOfficeID;

                    //2. Заполняем модель "DocCashOfficeSum"
                    Models.Sklad.Doc.DocCashOfficeSum docCashOfficeSum = new Models.Sklad.Doc.DocCashOfficeSum();
                    docCashOfficeSum.DirCashOfficeID = iDirCashOfficeID;
                    docCashOfficeSum.DirCashOfficeSumTypeID = 15;
                    docCashOfficeSum.DocCashOfficeSumDate = DateTime.Now;
                    docCashOfficeSum.DocID = docServicePurch.doc.DocID; //doc.DocID;
                    docCashOfficeSum.DocXID = docServicePurch.DocServicePurchID;
                    docCashOfficeSum.DocCashOfficeSumSum = SumTotal2a; //docServicePurch.PrepaymentSum; //doc.Payment;
                    docCashOfficeSum.Description = "";
                    docCashOfficeSum.DirEmployeeID = field.DirEmployeeID;
                    docCashOfficeSum.Base = "Оплата за аппарат: " + dirServiceNomen.DirServiceNomenName;

                    //3. Пишем в Кассу
                    Doc.DocCashOfficeSums.DocCashOfficeSumsController docCashOfficeSumsController = new Doc.DocCashOfficeSums.DocCashOfficeSumsController();
                    docCashOfficeSum = await Task.Run(() => docCashOfficeSumsController.mPutPostDocCashOfficeSum(db, docCashOfficeSum, EntityState.Added));
                    DocCashOfficeSumID = docCashOfficeSum.DocCashOfficeSumID;

                    #endregion
                }
                //Банк
                else if (DirPaymentTypeID == 2)
                {
                    #region Банк

                    //1. По складу находим привязанную к нему Кассу
                    Models.Sklad.Dir.DirWarehouse dirWarehouse = db.DirWarehouses.Find(docServicePurch.DirWarehouseID);
                    int iDirBankID = dirWarehouse.DirBankID;

                    //2. Заполняем модель "DocBankSum"
                    Models.Sklad.Doc.DocBankSum docBankSum = new Models.Sklad.Doc.DocBankSum();
                    docBankSum.DirBankID = iDirBankID;
                    docBankSum.DirBankSumTypeID = 14; //Изъятие из кассы на основании проведения приходной накладной №
                    docBankSum.DocBankSumDate = DateTime.Now;
                    docBankSum.DocID = docServicePurch.doc.DocID; //doc.DocID;
                    docBankSum.DocXID = docServicePurch.DocServicePurchID;
                    docBankSum.DocBankSumSum = SumTotal2a; //docServicePurch.PrepaymentSum; //doc.Payment;
                    docBankSum.Description = "";
                    docBankSum.DirEmployeeID = field.DirEmployeeID;
                    docBankSum.Base = "Оплата за аппарат: " + dirServiceNomen.DirServiceNomenName;

                    //3. Пишем в Банк
                    Doc.DocBankSums.DocBankSumsController docBankSumsController = new Doc.DocBankSums.DocBankSumsController();
                    docBankSum = await Task.Run(() => docBankSumsController.mPutPostDocBankSum(db, docBankSum, EntityState.Added));
                    DocBankSumID = docBankSum.DocBankSumID;

                    #endregion
                }
            }

            #endregion


            #region DirServiceStatusID == 9: DocServicePurch1Tab, DocServicePurch2Tab
            //Ну и надо Работы и запчасти пометить как оплоченные!
            //Но, только новые. То есть аппарат могут вернуть несколько раз надоработку.

            if (DirServiceStatusID == 9)
            {
                //DocServicePurch1Tab === === === === === === === === === === ===
                List<Models.Sklad.Doc.DocServicePurch1Tab> listDocServicePurch1Tab =
                    (
                        from x in db.DocServicePurch1Tabs
                        where x.DocServicePurchID == id && x.PayDate == null
                        select x
                    ).ToList();

                foreach (Models.Sklad.Doc.DocServicePurch1Tab docServicePurch1Tab in listDocServicePurch1Tab)
                {
                    docServicePurch1Tab.PayDate = dtNow;
                    docServicePurch1Tab.DocCashOfficeSumID = DocCashOfficeSumID;
                    docServicePurch1Tab.DocBankSumID = DocBankSumID;
                    db.Entry(docServicePurch1Tab).State = EntityState.Modified;
                }

                //DocServicePurch2Tab === === === === === === === === === === ===
                List<Models.Sklad.Doc.DocServicePurch2Tab> listDocServicePurch2Tab =
                    (
                        from x in db.DocServicePurch2Tabs
                        where x.DocServicePurchID == id && x.PayDate == null
                        select x
                    ).ToList();

                foreach (Models.Sklad.Doc.DocServicePurch2Tab docServicePurch2Tab in listDocServicePurch2Tab)
                {
                    docServicePurch2Tab.PayDate = dtNow;
                    docServicePurch2Tab.DocCashOfficeSumID = DocCashOfficeSumID;
                    docServicePurch2Tab.DocBankSumID = DocBankSumID;
                    db.Entry(docServicePurch2Tab).State = EntityState.Modified;
                }

                //Сохраняем
                await Task.Run(() => db.SaveChangesAsync());
            }

            #endregion


            #region 3. Лог


            #region Пишем в Лог о смене статуса и мастера, если такое было

            logService.DocServicePurchID = id;
            if(!bDirServiceLogTypeID9) logService.DirServiceLogTypeID = 1; //Смена статуса
            else logService.DirServiceLogTypeID = 9; //Возврат по гарантии
            logService.DirEmployeeID = field.DirEmployeeID;
            logService.DirServiceStatusID = DirServiceStatusID;
            if(!String.IsNullOrEmpty(sReturnRresults)) logService.Msg = sReturnRresults;

            await logServicesController.mPutPostLogServices(db, logService, EntityState.Added);

            #endregion


            #endregion


            #region 4. Заполняем DirServiceContractors
            //Надо ввести доп.поле статуса в "DocServicePurches" с предыдущим статусом: "Готов" или "Отказ"
            //Когда нажимаем выдан, то заполнять это поле.
            //Нужно для:
            // - Справочника "DirServiceContractor" поля: QuantityOk и QuantityFail
            // - Для статистики сколько Готовых, сколько Отказных


            //Если в Логе НЕТ записей, что вернут на доработку
            var queryLogCount = await
                (
                    from x in db.LogServices
                    where x.DirServiceLogTypeID == 9 && x.DocServicePurchID == id
                    select x
                ).CountAsync();


            //1. Находим Клиента по 
            if (
                queryLogCount == 0 &&
                DirServiceStatusID == 9 && 
                docServicePurch.DirServiceContractorID != null && 
                docServicePurch.DirServiceContractorID > 0
               )
            {
                Models.Sklad.Dir.DirServiceContractor dirServiceContractor = await db.DirServiceContractors.FindAsync(docServicePurch.DirServiceContractorID);

                //2. К-во (3 шт)
                if (DirServiceStatusID_OLD == 7)
                {
                    //Выдан
                    dirServiceContractor.QuantityOk = dirServiceContractor.QuantityOk + 1;
                }
                else if (DirServiceStatusID_OLD == 8)
                {
                    //Отказ
                    dirServiceContractor.QuantityFail = dirServiceContractor.QuantityFail + 1;
                }
                dirServiceContractor.QuantityCount = dirServiceContractor.QuantityCount + 1;

                //3. Сохранение
                db.Entry(dirServiceContractor).State = EntityState.Modified;
                await Task.Run(() => db.SaveChangesAsync());
            }

            #endregion


            return true;
        }


        internal async Task<bool> mRepairChange(
            DbConnectionSklad db,
            System.Data.Entity.DbContextTransaction ts,
            Models.Sklad.Doc.DocServicePurch docServicePurch,
            int id,
            int ServiceTypeRepair,

            Classes.Account.Login.Field field //Дополнительные данные о сотруднике
            )
        {

            #region 1. Сохранение статуса в БД

            docServicePurch = await db.DocServicePurches.FindAsync(id);

            if (docServicePurch.ServiceTypeRepair == ServiceTypeRepair) { return false; }
            else { logService.Msg = "Была: " + docServicePurch.ServiceTypeRepair + " поменяли на: " + ServiceTypeRepair; }

            docServicePurch.ServiceTypeRepair = ServiceTypeRepair;
            db.Entry(docServicePurch).State = EntityState.Modified;
            await Task.Run(() => db.SaveChangesAsync());

            #endregion


            #region 3. Лог
            
            logService.DocServicePurchID = id;
            logService.DirServiceLogTypeID = 8;
            logService.DirEmployeeID = field.DirEmployeeID;
            //logService.Msg = "Была гарантия: "; //Выше изменили!!!

            await logServicesController.mPutPostLogServices(db, logService, EntityState.Added);
            
            #endregion


            return true;
        }


        internal async Task<bool> mDirEmployeeIDChange(
            DbConnectionSklad db,
            System.Data.Entity.DbContextTransaction ts,
            Models.Sklad.Doc.DocServicePurch docServicePurch,
            int id,
            int DirEmployeeID,

            Classes.Account.Login.Field field //Дополнительные данные о сотруднике
            )
        {

            #region 1. Сохранение статуса в БД

            //docServicePurch = await db.DocServicePurches.FindAsync(id);

            //Ищим наименование Сотрудников
            Models.Sklad.Dir.DirEmployee dirEmployee1 = await dbRead.DirEmployees.FindAsync(docServicePurch.DirEmployeeIDMaster);
            Models.Sklad.Dir.DirEmployee dirEmployee2 = await dbRead.DirEmployees.FindAsync(DirEmployeeID);

            if (docServicePurch.DirEmployeeIDMaster == DirEmployeeID) { return false; }
            else
            {
                //logService.Msg = "Был мастер №" + docServicePurch.DirEmployeeIDMaster + " поменяли на мастера №" + DirEmployeeID;
                logService.Msg = "Был мастер " + dirEmployee1.DirEmployeeName + " поменяли на мастера " + dirEmployee2.DirEmployeeName;
            }

            docServicePurch.DirEmployeeIDMaster = DirEmployeeID;
            db.Entry(docServicePurch).State = EntityState.Modified;
            await Task.Run(() => db.SaveChangesAsync());

            #endregion


            #region 3. Лог

            logService.DocServicePurchID = id;
            logService.DirServiceLogTypeID = 7;
            logService.DirEmployeeID = field.DirEmployeeID;
            //logService.Msg = "Была гарантия: "; //Выше изменили!!!

            await logServicesController.mPutPostLogServices(db, logService, EntityState.Added);

            #endregion


            return true;
        }


        internal async Task<string[]> mPatchFull(DbConnectionSklad db, int id)
        {
            /*
            //1. Получаем Sub аппарата по "DocServicePurchID" (id)
            string DirServiceNomenPatchFull = null;

            var querySub = await Task.Run(() =>
                 (
                    from x in db1.DocServicePurches
                    where x.DocServicePurchID == id
                    select new
                    {
                        Sub = x.dirServiceNomen.Sub
                    }
                ).ToArrayAsync());

            if (querySub.Count() > 0)
            {
                int? iSub = querySub[0].Sub;

                Controllers.Sklad.Dir.DirServiceNomens.DirServiceNomensController dirServiceNomensController = new Dir.DirServiceNomens.DirServiceNomensController();
                DirServiceNomenPatchFull = await Task.Run(() => dirServiceNomensController.DirServiceNomenSubFind2(db1, iSub));
            }

            return DirServiceNomenPatchFull;
            */

            ArrayList alNameSpase = new ArrayList();
            ArrayList alNameSpaseNo = new ArrayList();
            ArrayList alNameID = new ArrayList();

            int? Sub = id;

            while (Sub > 0)
            {
                var query = await Task.Run(() =>
                     (
                         from x in db.DirServiceNomens
                         where x.DirServiceNomenID == Sub
                         select new
                         {
                             id = x.DirServiceNomenID,
                             sub = x.Sub,
                             text = x.DirServiceNomenName, // + " (" + x.DirServiceNomenName + ")",
                             leaf =
                             (
                              from y in db.DirServiceNomens
                              where y.Sub == x.DirServiceNomenID
                              select y
                             ).Count() == 0 ? 1 : 0,

                             Del = x.Del,
                             Sub = x.Sub,

                             //Полный путь от группы к выбраному элементу
                             DirServiceNomenPatchFull = x.DirServiceNomenName // + " (" + x.DirServiceNomenName + ")"
                         }
                    ).ToListAsync());

                if (query.Count() > 0)
                {
                    id = Convert.ToInt32(query[0].id);
                    Sub = query[0].Sub;
                    alNameSpase.Add(query[0].text + " / ");
                    alNameSpaseNo.Add(query[0].text + ",");
                    alNameID.Add(query[0].id + ",");
                }
                else
                {
                    Sub = null;
                }
            }

            string[] ret = new string[3];
            for (int i = alNameSpase.Count - 1; i >= 0; i--)
            {
                ret[0] += alNameSpase[i].ToString();
                ret[1] += alNameSpaseNo[i].ToString();
                ret[2] += alNameID[i].ToString();
            }

            return ret;

        }


        #endregion


        #region SQL

        /// <summary>
        /// </summary>
        /// <param name="bTresh">Не работает без этого параметра. Идёт конфликт с методами UPDATE</param>
        /// <returns></returns>
        public string GenerateSQLSelect(bool bTresh)
        {
            string SQL =

            /*
                "SELECT " +
                "[DocServicePurches].[DocServicePurchID] AS [DocServicePurchID], " +
                "[Docs].[DocDate] AS [DocDate], [Docs].[DocDate] AS [DocDate_InWords], " +
                "[Docs].[Base] AS [Base], " +
                "[Docs].[Held] AS [Held], " +
                "[Docs].[Discount] AS [Discount], " +
                "[Docs].[Description] AS [Description], " +
                "[Docs].[DirVatValue] AS [DirVatValue], " +

                "[DocServicePurches].[SerialNumber] AS [DeviceSerialNumber], " +

                "CASE [DocServicePurches].[TypeRepair] " +
                " WHEN [TypeRepair] = 1 THEN 'Не гарантийный' ELSE 'Гарантийный' END AS [TypeRepair]," +

                "CASE [DocServicePurches].[ComponentDevice] " +
                " WHEN [ComponentDevice] = 1 THEN 'Аппарат' ELSE '-' END AS [ComponentDevice]," +

                "CASE [DocServicePurches].[ComponentBattery] " +
                " WHEN [ComponentBattery] = 1 THEN 'Аккумулятор' ELSE '-' END AS [ComponentBattery]," +
                "[DocServicePurches].[ComponentBatterySerial] AS [ComponentBatterySerial], " +

                "CASE [DocServicePurches].[ComponentBackCover] " +
                " WHEN [ComponentBackCover] = 1 THEN 'Задняя крышка' ELSE '-' END AS [ComponentBackCover]," +

                "[DocServicePurches].[ComponentPasTextNo] AS [ComponentPasTextNo], " +
                "[DocServicePurches].[ComponentPasText] AS [ComponentPasText], " +
                "[DocServicePurches].[ComponentOtherText] AS [ComponentOtherText], " +
                "[DocServicePurches].[ProblemClientWords] AS [ProblemClientWords], " +
                "[DocServicePurches].[Note] AS [Note], " +

                "[DocServicePurches].[DirServiceContractorName] AS [DirContractorName], " +
                "[DocServicePurches].[DirServiceContractorAddress] AS [DirContractorAddress], " +
                "[DocServicePurches].[DirServiceContractorPhone] AS [DirContractorPhone], " +
                "[DocServicePurches].[DirServiceContractorEmail] AS [DirContractorEmail], " +
                "[DocServicePurches].[DirServiceContractorRegular] AS [DirServiceContractorRegular], " + //Постоянный

                "[DocServicePurches].[PriceVAT] AS [PriceVATEstimated], " + //Ориентир.стоимость
                "[DocServicePurches].[DateDone] AS [DateDone], " +
                "[DocServicePurches].[UrgentRepairs] AS [UrgentRepairs], " +
                "[DocServicePurches].[Prepayment] AS [Prepayment], " +
                "[DocServicePurches].[PrepaymentSum] AS [PrepaymentSum], " +

                //Аппарат
                "[DirServiceNomens].[DirServiceNomenID] AS [DirServiceNomenID], " +
                "[DirServiceNomens].[DirServiceNomenName] AS [DirServiceNomenName], " +
                "[DirServiceNomens].[DirServiceNomenNameFull] AS [DirServiceNomenNameFull], " +
                "[DirServiceNomens].[Description] AS [Description], " +
                "[DirServiceNomens].[DescriptionFull] AS [DescriptionFull], " +

                //Статус
                "[DirServiceStatuses].[DirServiceStatusName] AS [DirServiceStatusName], " +

                //Банк для Организации
                "[DirBanksOrg].[DirBankName] AS [DirBankNameOrg], " +
                "[DirBanksOrg].[DirBankMFO] AS [DirBankMFOOrg], " +

                //Организация
                "[DirContractorOrg].[DirContractorName] AS [DirContractorNameOrg], " +
                "[DirContractorOrg].[DirContractorEmail] AS [DirContractorEmailOrg], " +
                "[DirContractorOrg].[DirContractorWWW] AS [DirContractorWWWOrg], " +
                "[DirContractorOrg].[DirContractorAddress] AS [DirContractorAddressOrg], " +
                "[DirContractorOrg].[DirContractorLegalCertificateDate] AS [DirContractorLegalCertificateDateOrg], " +
                "[DirContractorOrg].[DirContractorLegalTIN] AS [DirContractorLegalTINOrg], " +
                "[DirContractorOrg].[DirContractorLegalCAT] AS [DirContractorLegalCATOrg], " +
                "[DirContractorOrg].[DirContractorLegalCertificateNumber] AS [DirContractorLegalCertificateNumberOrg], " +
                "[DirContractorOrg].[DirContractorLegalBIN] AS [DirContractorLegalBINOrg], " +
                "[DirContractorOrg].[DirContractorLegalOGRNIP] AS [DirContractorLegalOGRNIPOrg], " +
                "[DirContractorOrg].[DirContractorLegalRNNBO] AS [DirContractorLegalRNNBOOrg], " +
                "[DirContractorOrg].[DirContractorDesc] AS [DirContractorDescOrg], " +
                "[DirContractorOrg].[DirContractorLegalPasIssued] AS [DirContractorLegalPasIssuedOrg], " +
                "[DirContractorOrg].[DirContractorLegalPasDate] AS [DirContractorLegalPasDateOrg], " +
                "[DirContractorOrg].[DirContractorLegalPasCode] AS [DirContractorLegalPasCodeOrg], " +
                "[DirContractorOrg].[DirContractorLegalPasNumber] AS [DirContractorLegalPasNumberOrg], " +
                "[DirContractorOrg].[DirContractorLegalPasSeries] AS [DirContractorLegalPasSeriesOrg], " +
                "[DirContractorOrg].[DirContractorDiscount] AS [DirContractorDiscountOrg], " +
                "[DirContractorOrg].[DirContractorPhone] AS [DirContractorPhoneOrg], " +
                "[DirContractorOrg].[DirContractorFax] AS [DirContractorFaxOrg], " +
                "[DirContractorOrg].[DirContractorLegalAddress] AS [DirContractorLegalAddressOrg], " +
                "[DirContractorOrg].[DirContractorLegalName] AS [DirContractorLegalNameOrg], " +

                "[DirWarehouses].[DirWarehouseName] AS [DirWarehouseName], " +
                "[DirWarehouses].[DirWarehouseAddress] AS [DirWarehouseAddress], " +
                "[DirWarehouses].[DirWarehouseDesc] AS [DirWarehouseDesc], " + //есть в БД, но нет в проекте

                //Сотрудник оформивший документ
                "[DirEmployees].[DirEmployeeName] AS [DirEmployeeName], " +

                "[Docs].[NumberInt] AS [NumberInt] " +




                "FROM [DocServicePurches] " +

                "INNER JOIN [Docs] ON [Docs].[DocID] = [DocServicePurches].[DocID] " +
                "INNER JOIN [DirServiceNomens] ON [DirServiceNomens].[DirServiceNomenID] = [DocServicePurches].[DirServiceNomenID] " +
                "INNER JOIN [DirServiceStatuses] ON [DirServiceStatuses].[DirServiceStatusID] = [DocServicePurches].[DirServiceStatusID] " +
                "INNER JOIN [DirPaymentTypes] ON [DirPaymentTypes].[DirPaymentTypeID] = [Docs].[DirPaymentTypeID] " +
                "INNER JOIN [DirContractors] AS [DirContractorOrg] ON [Docs].[DirContractorIDOrg] = [DirContractorOrg].[DirContractorID] " +
                "INNER JOIN [DirWarehouses] ON [DirWarehouses].[DirWarehouseID] = [DocServicePurches].[DirWarehouseID] " +
                "INNER JOIN [DirEmployees] ON [DirEmployees].[DirEmployeeID] = [Docs].[DirEmployeeID] " +
                //Банк для Организации
                "LEFT JOIN [DirBanks] AS [DirBanksOrg] ON [DirBanksOrg].[DirBankID] = [DirContractorOrg].[DirBankID] " +


                "WHERE (Docs.DocID=@DocID)";
            */

                "SELECT " +
                " [DocServicePurches].[DocServicePurchID] AS [DocServicePurchID], [Docs].[DocDate] AS [DocDate], [Docs].[DocDate] AS [DocDate_InWords], [Docs].[Base] AS [Base],  [Docs].[Held] AS [Held], [Docs].[Discount] AS [Discount], [Docs].[Description] AS [Description], [Docs].[DirVatValue] AS [DirVatValue],  [DocServicePurches].[SerialNumber] AS [DeviceSerialNumber], " +
                " CASE [DocServicePurches].[TypeRepair]  WHEN [TypeRepair] = 1 THEN 'Не гарантийный' ELSE 'Гарантийный' END AS [TypeRepair], " +
                " CASE [DocServicePurches].[ComponentDevice]  WHEN [ComponentDevice] = 1 THEN 'Аппарат' ELSE '-' END AS [ComponentDevice], " +
                " CASE [DocServicePurches].[ComponentBattery]  WHEN [ComponentBattery] = 1 THEN 'Аккумулятор' ELSE '-' END AS [ComponentBattery],[DocServicePurches].[ComponentBatterySerial] AS [ComponentBatterySerial], " +
                " CASE [DocServicePurches].[ComponentBackCover]  WHEN [ComponentBackCover] = 1 THEN 'Задняя крышка' ELSE '-' END AS [ComponentBackCover],[DocServicePurches].[ComponentPasTextNo] AS [ComponentPasTextNo], [DocServicePurches].[ComponentPasText] AS [ComponentPasText], [DocServicePurches].[ComponentOtherText] AS [ComponentOtherText], [DocServicePurches].[ProblemClientWords] AS [ProblemClientWords], [DocServicePurches].[Note] AS [Note], [DocServicePurches].[DirServiceContractorName] AS [DirContractorName], [DocServicePurches].[DirServiceContractorAddress] AS [DirContractorAddress], [DocServicePurches].[DirServiceContractorPhone] AS [DirContractorPhone], [DocServicePurches].[DirServiceContractorEmail] AS [DirContractorEmail], [DocServicePurches].[DirServiceContractorRegular] AS [DirServiceContractorRegular], [DocServicePurches].[PriceVAT] AS [PriceVATEstimated], [DocServicePurches].[DateDone] AS [DateDone], [DocServicePurches].[UrgentRepairs] AS [UrgentRepairs], [DocServicePurches].[Prepayment] AS [Prepayment], [DocServicePurches].[PrepaymentSum] AS [PrepaymentSum], " +
                " [DirServiceNomens].[DirServiceNomenID] AS [DirServiceNomenID], " +
                " [DocServicePurches].[ServiceTypeRepair], " +



                //Товар *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** ***
                "CASE " +
                "WHEN ([dirServiceNomensGroup].[DirServiceNomenName] IS NULL) THEN [dirServiceNomensSubGroup].[DirServiceNomenName] " +

                "WHEN ([DirServiceNomens].[DirServiceNomenName] IS NULL) THEN " +

                "CASE WHEN ([dirServiceNomensSubGroup].[DirServiceNomenName] IS NULL) THEN '' ELSE [dirServiceNomensSubGroup].[DirServiceNomenName] END || ' / ' || " +
                "CASE WHEN ([dirServiceNomensGroup].[DirServiceNomenName] IS NULL) THEN '' ELSE [dirServiceNomensGroup].[DirServiceNomenName] END ELSE " +

                "CASE WHEN ([dirServiceNomensSubGroup].[DirServiceNomenName] IS NULL) THEN '' ELSE [dirServiceNomensSubGroup].[DirServiceNomenName] END || ' / ' || " +
                "CASE WHEN ([dirServiceNomensGroup].[DirServiceNomenName] IS NULL) THEN '' ELSE [dirServiceNomensGroup].[DirServiceNomenName] END || ' / ' || " +
                "CASE WHEN ([DirServiceNomens].[DirServiceNomenName] IS NULL) THEN '' ELSE [DirServiceNomens].[DirServiceNomenName] END END AS [DirServiceNomenName], " +
                //Товар *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** ***



                " [DirServiceNomens].[DirServiceNomenNameFull] AS [DirServiceNomenNameFull], [DirServiceNomens].[Description] AS [Description], [DirServiceNomens].[DescriptionFull] AS [DescriptionFull], " +

                " [DirServiceStatuses].[DirServiceStatusName] AS [DirServiceStatusName], [DirBanksOrg].[DirBankName] AS [DirBankNameOrg], [DirBanksOrg].[DirBankMFO] AS [DirBankMFOOrg], [DirContractorOrg].[DirContractorName] AS [DirContractorNameOrg], [DirContractorOrg].[DirContractorEmail] AS [DirContractorEmailOrg], [DirContractorOrg].[DirContractorWWW] AS [DirContractorWWWOrg], [DirContractorOrg].[DirContractorAddress] AS [DirContractorAddressOrg], [DirContractorOrg].[DirContractorLegalCertificateDate] AS [DirContractorLegalCertificateDateOrg], [DirContractorOrg].[DirContractorLegalTIN] AS [DirContractorLegalTINOrg], [DirContractorOrg].[DirContractorLegalCAT] AS [DirContractorLegalCATOrg], [DirContractorOrg].[DirContractorLegalCertificateNumber] AS [DirContractorLegalCertificateNumberOrg], [DirContractorOrg].[DirContractorLegalBIN] AS [DirContractorLegalBINOrg], [DirContractorOrg].[DirContractorLegalOGRNIP] AS [DirContractorLegalOGRNIPOrg], [DirContractorOrg].[DirContractorLegalRNNBO] AS [DirContractorLegalRNNBOOrg], [DirContractorOrg].[DirContractorDesc] AS [DirContractorDescOrg],  [DirContractorOrg].[DirContractorLegalPasIssued] AS [DirContractorLegalPasIssuedOrg],  [DirContractorOrg].[DirContractorLegalPasDate] AS [DirContractorLegalPasDateOrg],  [DirContractorOrg].[DirContractorLegalPasCode] AS [DirContractorLegalPasCodeOrg],  [DirContractorOrg].[DirContractorLegalPasNumber] AS [DirContractorLegalPasNumberOrg],  [DirContractorOrg].[DirContractorLegalPasSeries] AS [DirContractorLegalPasSeriesOrg],  [DirContractorOrg].[DirContractorDiscount] AS [DirContractorDiscountOrg],  [DirContractorOrg].[DirContractorPhone] AS [DirContractorPhoneOrg],  [DirContractorOrg].[DirContractorFax] AS [DirContractorFaxOrg],  [DirContractorOrg].[DirContractorLegalAddress] AS [DirContractorLegalAddressOrg],  [DirContractorOrg].[DirContractorLegalName] AS [DirContractorLegalNameOrg], [DirWarehouses].[DirWarehouseName] AS [DirWarehouseName],  [DirWarehouses].[DirWarehouseAddress] AS [DirWarehouseAddress],  [DirWarehouses].[DirWarehouseDesc] AS [DirWarehouseDesc], [DirEmployees].[DirEmployeeName] AS [DirEmployeeName], [Docs].[NumberInt] AS [NumberInt] " +
                "FROM [DocServicePurches]  " +
                "INNER JOIN [Docs] ON [Docs].[DocID] = [DocServicePurches].[DocID] " +

                //Товар *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** ***
                "LEFT JOIN [DirServiceNomens] AS [DirServiceNomens] ON [DirServiceNomens].[DirServiceNomenID] = [DocServicePurches].[DirServiceNomenID] " +
                "LEFT JOIN [DirServiceNomens] AS [dirServiceNomensSubGroup] ON [DirServiceNomens].[Sub] = [dirServiceNomensSubGroup].[DirServiceNomenID] " +
                "LEFT JOIN [DirServiceNomens] AS [dirServiceNomensGroup] ON [dirServiceNomensSubGroup].[Sub] = [dirServiceNomensGroup].[DirServiceNomenID] " +
                //Товар *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** ***

                "INNER JOIN [DirServiceStatuses] ON [DirServiceStatuses].[DirServiceStatusID] = [DocServicePurches].[DirServiceStatusID] INNER JOIN [DirPaymentTypes] ON [DirPaymentTypes].[DirPaymentTypeID] = [Docs].[DirPaymentTypeID] INNER JOIN [DirContractors] AS [DirContractorOrg] ON [Docs].[DirContractorIDOrg] = [DirContractorOrg].[DirContractorID] INNER JOIN [DirWarehouses] ON [DirWarehouses].[DirWarehouseID] = [DocServicePurches].[DirWarehouseID] INNER JOIN [DirEmployees] ON [DirEmployees].[DirEmployeeID] = [Docs].[DirEmployeeID] LEFT JOIN [DirBanks] AS [DirBanksOrg] ON [DirBanksOrg].[DirBankID] = [DirContractorOrg].[DirBankID] " +
                "WHERE (Docs.DocID=@DocID) ";



            return SQL;
        }


        //Сумма документа
        internal string GenerateSQLSUM(Models.Sklad.Sys.SysSetting sysSettings)
        {
            string SQL =

                "SELECT " +

                "[DocDate] AS [DocDate], [DocDate] AS [DocDate_InWords], " +
                "Discount AS Discount, " +

                " SUM(CountRecord1 + CountRecord2) AS CountRecord, " +
                " SUM(CountRecord1 + CountRecord2) AS CountRecord_NumInWords, " +

                " SUM(SumDocServicePurch1Tabs) AS SumDocServicePurch1Tabs, " +
                " SUM(SumDocServicePurch1Tabs) AS SumDocServicePurch1Tabs_InWords, " +

                " SUM(SumDocServicePurch2Tabs) AS SumDocServicePurch2Tabs, " +
                " SUM(SumDocServicePurch2Tabs) AS SumDocServicePurch2Tabs_InWords, " +

                " SUM(SumDocServicePurch1Tabs) + SUM(SumDocServicePurch2Tabs) AS SumTotal, " +
                " SUM(SumDocServicePurch1Tabs) + SUM(SumDocServicePurch2Tabs) AS SumTotal_InWords, " +

                " [PrepaymentSum] AS PrepaymentSum, " +

                " SUM(SumDocServicePurch1Tabs) + SUM(SumDocServicePurch2Tabs) - [PrepaymentSum] AS SumTotal2, " +

                "[DirContractorName] AS [DirContractorName], " +
                "[DirContractorAddress] AS [DirContractorAddress], " +
                "[DirContractorPhone] AS [DirContractorPhone], " +
                "[DirContractorEmail] AS [DirContractorEmail], " +
                "[DirServiceContractorRegular] AS [DirServiceContractorRegular], " + //Постоянный
                "[ServiceTypeRepair] " +


                "FROM " +
                "(" +

                "SELECT " +
                "[Docs].[DocDate] AS [DocDate], [Docs].[DocDate] AS [DocDate_InWords], " +
                "Docs.Discount AS Discount, " +

                "COUNT(*) CountRecord1, " +
                "0 CountRecord2, " +

                //1. Подсчет табличной части Работы "SumDocServicePurch1Tabs"
                "ROUND((SUM(DocServicePurch1Tabs.PriceCurrency)), " + sysSettings.FractionalPartInSum + ") AS SumDocServicePurch1Tabs, " +
                "0 AS SumDocServicePurch2Tabs, " +

                //4. Константа "PrepaymentSum"
                "[DocServicePurches].[PrepaymentSum] AS [PrepaymentSum], " +

                "[DocServicePurches].[DirServiceContractorName] AS [DirContractorName], " +
                "[DocServicePurches].[DirServiceContractorAddress] AS [DirContractorAddress], " +
                "[DocServicePurches].[DirServiceContractorPhone] AS [DirContractorPhone], " +
                "[DocServicePurches].[DirServiceContractorEmail] AS [DirContractorEmail], " +
                "[DocServicePurches].[DirServiceContractorRegular] AS [DirServiceContractorRegular], " + //Постоянный
                "CASE WHEN ([DocServicePurches].[ServiceTypeRepair] IS NULL) THEN 1 ELSE [DocServicePurches].[ServiceTypeRepair] END AS [ServiceTypeRepair] " +


                "FROM Docs, DocServicePurches " +
                " LEFT JOIN DocServicePurch1Tabs ON (DocServicePurch1Tabs.DocServicePurchID=DocServicePurches.DocServicePurchID) " + 

                "WHERE (Docs.DocID=DocServicePurches.DocID)and(Docs.DocID=@DocID) " +



                " UNION " +



                "SELECT " +
                "[Docs].[DocDate] AS [DocDate], [Docs].[DocDate] AS [DocDate_InWords], " +
                "Docs.Discount AS Discount, " +

                "0 CountRecord1, " +
                "COUNT(*) CountRecord2, " +

                //1. Подсчет табличной части Работы "SumDocServicePurch2Tabs"
                "0 AS SumDocServicePurch1Tabs, " +
                "ROUND((SUM(DocServicePurch2Tabs.PriceCurrency)), " + sysSettings.FractionalPartInSum + ") AS SumDocServicePurch2Tabs, " +

                //4. Константа "PrepaymentSum"
                "[DocServicePurches].[PrepaymentSum] AS [PrepaymentSum], " +

                "[DocServicePurches].[DirServiceContractorName] AS [DirContractorName], " +
                "[DocServicePurches].[DirServiceContractorAddress] AS [DirContractorAddress], " +
                "[DocServicePurches].[DirServiceContractorPhone] AS [DirContractorPhone], " +
                "[DocServicePurches].[DirServiceContractorEmail] AS [DirContractorEmail], " +
                "[DocServicePurches].[DirServiceContractorRegular] AS [DirServiceContractorRegular], " + //Постоянный
                "CASE WHEN ([DocServicePurches].[ServiceTypeRepair] IS NULL) THEN 1 ELSE [DocServicePurches].[ServiceTypeRepair] END AS [ServiceTypeRepair] " +


                "FROM Docs, DocServicePurches " +
                " LEFT JOIN DocServicePurch2Tabs ON (DocServicePurch2Tabs.DocServicePurchID=DocServicePurches.DocServicePurchID) " + 
                "WHERE (Docs.DocID=DocServicePurches.DocID)and(Docs.DocID=@DocID) " +

                ")";




            return SQL;
        }


        #endregion
    }
}