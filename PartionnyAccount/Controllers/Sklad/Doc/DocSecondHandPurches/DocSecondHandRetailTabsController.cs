﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using PartionnyAccount.Models;
using PartionnyAccount.Models.Sklad.Doc;

namespace PartionnyAccount.Controllers.Sklad.Doc.DocSecondHandPurches
{
    public class DocSecondHandRetailTabsController : ApiController
    {
        #region Classes

        Classes.Function.Exceptions.ExceptionEntry exceptionEntry = new Classes.Function.Exceptions.ExceptionEntry();
        Classes.Function.Variables.ConnectionString connectionString = new Classes.Function.Variables.ConnectionString();
        Classes.Account.Login login = new Classes.Account.Login();
        Classes.Account.AccessRight accessRight = new Classes.Account.AccessRight();
        Classes.Function.ReturnServer returnServer = new Classes.Function.ReturnServer();
        Classes.Function.Function function = new Classes.Function.Function();
        private DbConnectionSklad db = new DbConnectionSklad();

        #endregion


        #region SELECT

        class Params
        {
            //Parameters
            //public int DocID;
            public int? DocSecondHandRetailID;
            public DateTime? DateS, DatePo;
            public int DirWarehouseID;
        }
        // GET: api/DocSecondHandRetailTabs
        public async Task<IHttpActionResult> GetDocSecondHandRetailTabs(HttpRequestMessage request)
        {
            try
            {
                #region Проверяем Логин и Пароль + Изменяем строку соединения + Права + Разные Функции

                //Получаем Куку
                System.Web.HttpCookie authCookie = System.Web.HttpContext.Current.Request.Cookies["CookieIPOL"];

                // Проверяем Логин и Пароль
                Classes.Account.Login.Field field = await Task.Run(() => login.Return(authCookie, true));
                if (!field.Access) return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg10));

                //Изменяем строку соединения
                db = new DbConnectionSklad(connectionString.Return(field.DirCustomersID, null, true));

                //Права (1 - Write, 2 - Read, 3 - No Access)
                int iRight = await Task.Run(() => accessRight.Access(connectionString.Return(field.DirCustomersID, null, true), field.DirEmployeeID, "RightDocSecondHandRetails"));
                if (iRight == 3) return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg57(0)));

                //Разные Функции
                function.NumberDecimalSeparator();

                //Получам настройки
                Models.Sklad.Sys.SysSetting sysSetting = await db.SysSettings.FindAsync(1);

                #endregion


                #region Параметры

                Params _params = new Params();

                //paramList -список параметров
                var paramList = request.GetQueryNameValuePairs();
                //Параметры
                _params.DocSecondHandRetailID = Convert.ToInt32(paramList.FirstOrDefault(kv => string.Compare(kv.Key, "DocSecondHandRetailID", true) == 0).Value);
                //_params.DocDate = Convert.ToDateTime(paramList.FirstOrDefault(kv => string.Compare(kv.Key, "DocDate", true) == 0).Value);

                _params.DateS = Convert.ToDateTime(Convert.ToDateTime(paramList.FirstOrDefault(kv => string.Compare(kv.Key, "DocDateS", true) == 0).Value).ToString("yyyy-MM-dd 23:59:59"));
                if (_params.DateS < Convert.ToDateTime("01.01.1800")) _params.DateS = Convert.ToDateTime(sysSetting.JurDateS.ToString("yyyy-MM-dd 00:00:00")).AddDays(-1);
                else _params.DateS = _params.DateS.Value.AddDays(-1);

                _params.DatePo = Convert.ToDateTime(Convert.ToDateTime(paramList.FirstOrDefault(kv => string.Compare(kv.Key, "DocDatePo", true) == 0).Value).ToString("yyyy-MM-dd 23:59:59"));
                if (_params.DatePo < Convert.ToDateTime("01.01.1800")) _params.DatePo = Convert.ToDateTime(sysSetting.JurDatePo.ToString("yyyy-MM-dd 23:59:59"));

                _params.DirWarehouseID = Convert.ToInt32(paramList.FirstOrDefault(kv => string.Compare(kv.Key, "DirWarehouseID", true) == 0).Value);

                #endregion



                #region Основной запрос *** *** ***

                var query =
                    (
                        //Розничный Чек

                        from x in db.Rem2PartyMinuses
                        from y in db.DocSecondHandRetailTabs

                        join dirServiceNomens11 in db.DirServiceNomens on x.dirServiceNomen.Sub equals dirServiceNomens11.DirServiceNomenID into dirServiceNomens12
                        from dirServiceNomensSubGroup in dirServiceNomens12.DefaultIfEmpty()

                        join dirServiceNomens21 in db.DirServiceNomens on dirServiceNomensSubGroup.Sub equals dirServiceNomens21.DirServiceNomenID into dirServiceNomens22
                        from dirServiceNomensGroup in dirServiceNomens22.DefaultIfEmpty()

                        where
                        x.FieldID == y.DocSecondHandRetailTabID &&
                        x.doc.DocDate >= _params.DateS && x.doc.DocDate <= _params.DatePo && x.doc.ListObjectID == 66 &&
                        x.DirWarehouseID == _params.DirWarehouseID

                        #region  Характеристики
                        /*
                        join dirCharColours1 in db.DirCharColours on x.rem2Party.DirCharColourID equals dirCharColours1.DirCharColourID into dirCharColours2
                        from dirCharColours in dirCharColours2.DefaultIfEmpty()

                        join dirCharMaterials1 in db.DirCharMaterials on x.rem2Party.DirCharMaterialID equals dirCharMaterials1.DirCharMaterialID into dirCharMaterials2
                        from dirCharMaterials in dirCharMaterials2.DefaultIfEmpty()

                        join dirCharNames1 in db.DirCharNames on x.rem2Party.DirCharNameID equals dirCharNames1.DirCharNameID into dirCharNames2
                        from dirCharNames in dirCharNames2.DefaultIfEmpty()

                        join dirCharSeasons1 in db.DirCharSeasons on x.rem2Party.DirCharSeasonID equals dirCharSeasons1.DirCharSeasonID into dirCharSeasons2
                        from dirCharSeasons in dirCharSeasons2.DefaultIfEmpty()

                        join dirCharSexes1 in db.DirCharSexes on x.rem2Party.DirCharSexID equals dirCharSexes1.DirCharSexID into dirCharSexes2
                        from dirCharSexes in dirCharSexes2.DefaultIfEmpty()

                        join dirCharSizes1 in db.DirCharSizes on x.rem2Party.DirCharSizeID equals dirCharSizes1.DirCharSizeID into dirCharSizes2
                        from dirCharSizes in dirCharSizes2.DefaultIfEmpty()

                        join dirCharStyles1 in db.DirCharStyles on x.rem2Party.DirCharStyleID equals dirCharStyles1.DirCharStyleID into dirCharStyles2
                        from dirCharStyles in dirCharStyles2.DefaultIfEmpty()

                        join dirCharTextures1 in db.DirCharTextures on x.rem2Party.DirCharTextureID equals dirCharTextures1.DirCharTextureID into dirCharTextures2
                        from dirCharTextures in dirCharTextures2.DefaultIfEmpty()
                        */
                        #endregion

                        #region select

                        select new
                        {
                            DocID = x.DocID,

                            DocDate = x.doc.DocDate,
                            Held = x.doc.Held,
                            Discount = x.doc.Discount,
                            DocSecondHandRetailID = y.DocSecondHandRetailID, //NumberReal = x.doc.NumberReal,
                            DocSecondHandRetailReturnID = y.DocSecondHandRetailID, //NumberReal = x.doc.NumberReal,
                            DirWarehouseID = x.DirWarehouseID,
                            ListObjectID = x.doc.listObject.ListObjectID,
                            ListObjectNameRu = x.doc.listObject.ListObjectNameRu,

                            DirServiceNomenID = x.DirServiceNomenID,

                            //DirServiceNomenName = x.dirServiceNomen.DirServiceNomenName,
                            DirServiceNomenName =
                            dirServiceNomensSubGroup.DirServiceNomenName == null ? x.dirServiceNomen.DirServiceNomenName :
                            dirServiceNomensGroup.DirServiceNomenName == null ? dirServiceNomensSubGroup.DirServiceNomenName + " / " + x.dirServiceNomen.DirServiceNomenName :
                            dirServiceNomensGroup.DirServiceNomenName + " / " + dirServiceNomensSubGroup.DirServiceNomenName + " / " + x.dirServiceNomen.DirServiceNomenName,

                            Rem2PartyID = x.Rem2PartyID,
                            Rem2PartyMinusID = x.Rem2PartyMinusID,
                            Quantity = x.Quantity,
                            PriceVAT = x.PriceVAT,

                            DirCurrencyID = x.DirCurrencyID,
                            DirCurrencyRate = x.DirCurrencyRate,
                            DirCurrencyMultiplicity = x.DirCurrencyMultiplicity,
                            DirCurrencyName = x.dirCurrency.DirCurrencyName + " (" + x.DirCurrencyRate + ", " + x.DirCurrencyMultiplicity + ")",

                            DirEmployeeName = x.doc.dirEmployee.DirEmployeeName,

                            //Rem2Party
                            Barcode = x.rem2Party.Barcode,
                            SerialNumber = x.rem2Party.SerialNumber,
                            /*
                            DirCharColourName = dirCharColours.DirCharColourName,
                            DirCharMaterialName = dirCharMaterials.DirCharMaterialName,
                            DirCharNameName = dirCharNames.DirCharNameName,
                            DirCharSeasonName = dirCharSeasons.DirCharSeasonName,
                            DirCharSexName = dirCharSexes.DirCharSexName,
                            DirCharSizeName = dirCharSizes.DirCharSizeName,
                            DirCharStyleName = dirCharStyles.DirCharStyleName,
                            DirCharTextureName = dirCharTextures.DirCharTextureName,
                            DirChar =
                                dirCharColours.DirCharColourName + " " +
                                dirCharMaterials.DirCharMaterialName + " " +
                                dirCharNames.DirCharNameName + " " +
                                dirCharSeasons.DirCharSeasonName + " " +
                                dirCharSexes.DirCharSexName + " " +
                                dirCharSizes.DirCharSizeName + " " +
                                dirCharStyles.DirCharStyleName + " " +
                                dirCharTextures.DirCharTextureName,
                                */

                            //Цена в т.в.
                            PriceCurrency = x.PriceCurrency,
                            //Себестоимость
                            SUMSalePriceVATCurrency = x.Quantity * x.PriceCurrency - x.doc.Discount == null ? 0
                            : Math.Round(x.Quantity * x.PriceCurrency - x.doc.Discount, sysSetting.FractionalPartInSum),


                            //Причина возврата
                            DirReturnTypeID = y.DirReturnTypeID,
                            DirReturnTypeName = "",

                            DirDescriptionID = y.DirDescriptionID,
                            DirDescriptionName = "",

                        }

                    #endregion

                    ).Union
                    (
                        //Розничный возврат

                        from x in db.DocSecondHandRetailReturnTabs

                        join dirServiceNomens11 in db.DirServiceNomens on x.dirServiceNomen.Sub equals dirServiceNomens11.DirServiceNomenID into dirServiceNomens12
                        from dirServiceNomensSubGroup in dirServiceNomens12.DefaultIfEmpty()

                        join dirServiceNomens21 in db.DirServiceNomens on dirServiceNomensSubGroup.Sub equals dirServiceNomens21.DirServiceNomenID into dirServiceNomens22
                        from dirServiceNomensGroup in dirServiceNomens22.DefaultIfEmpty()

                        where
                        //x.FieldID == y.DocSecondHandRetailReturnTabID &&
                        x.docSecondHandRetailReturn.doc.DocDate >= _params.DateS && x.docSecondHandRetailReturn.doc.DocDate <= _params.DatePo && x.docSecondHandRetailReturn.doc.ListObjectID == 67 &&
                        x.docSecondHandRetailReturn.DirWarehouseID == _params.DirWarehouseID

                       
                        join rem2PartyMinuses1 in db.Rem2PartyMinuses on x.Rem2PartyMinusID equals rem2PartyMinuses1.Rem2PartyMinusID into rem2PartyMinuses2
                        from rem2PartyMinuses in rem2PartyMinuses2.DefaultIfEmpty()

                        #region  Характеристики

                        /*
                        join dirCharColours1 in db.DirCharColours on rem2PartyMinuses.rem2Party.DirCharColourID equals dirCharColours1.DirCharColourID into dirCharColours2
                        from dirCharColours in dirCharColours2.DefaultIfEmpty()

                        join dirCharMaterials1 in db.DirCharMaterials on rem2PartyMinuses.rem2Party.DirCharMaterialID equals dirCharMaterials1.DirCharMaterialID into dirCharMaterials2
                        from dirCharMaterials in dirCharMaterials2.DefaultIfEmpty()

                        join dirCharNames1 in db.DirCharNames on rem2PartyMinuses.rem2Party.DirCharNameID equals dirCharNames1.DirCharNameID into dirCharNames2
                        from dirCharNames in dirCharNames2.DefaultIfEmpty()

                        join dirCharSeasons1 in db.DirCharSeasons on rem2PartyMinuses.rem2Party.DirCharSeasonID equals dirCharSeasons1.DirCharSeasonID into dirCharSeasons2
                        from dirCharSeasons in dirCharSeasons2.DefaultIfEmpty()

                        join dirCharSexes1 in db.DirCharSexes on rem2PartyMinuses.rem2Party.DirCharSexID equals dirCharSexes1.DirCharSexID into dirCharSexes2
                        from dirCharSexes in dirCharSexes2.DefaultIfEmpty()

                        join dirCharSizes1 in db.DirCharSizes on rem2PartyMinuses.rem2Party.DirCharSizeID equals dirCharSizes1.DirCharSizeID into dirCharSizes2
                        from dirCharSizes in dirCharSizes2.DefaultIfEmpty()

                        join dirCharStyles1 in db.DirCharStyles on rem2PartyMinuses.rem2Party.DirCharStyleID equals dirCharStyles1.DirCharStyleID into dirCharStyles2
                        from dirCharStyles in dirCharStyles2.DefaultIfEmpty()

                        join dirCharTextures1 in db.DirCharTextures on rem2PartyMinuses.rem2Party.DirCharTextureID equals dirCharTextures1.DirCharTextureID into dirCharTextures2
                        from dirCharTextures in dirCharTextures2.DefaultIfEmpty()
                        */
                        #endregion

                        #region select

                        select new
                        {
                            DocID = x.docSecondHandRetailReturn.DocID,

                            DocDate = x.docSecondHandRetailReturn.doc.DocDate,
                            Held = x.docSecondHandRetailReturn.doc.Held,
                            Discount = x.docSecondHandRetailReturn.doc.Discount,
                            DocSecondHandRetailID = x.docSecondHandRetailReturn.DocSecondHandRetailReturnID, //NumberReal = x.docSecondHandRetailReturn.doc.NumberReal,
                            DocSecondHandRetailReturnID = x.docSecondHandRetailReturn.DocSecondHandRetailReturnID, //NumberReal = x.docSecondHandRetailReturn.doc.NumberReal,
                            DirWarehouseID = x.docSecondHandRetailReturn.DirWarehouseID,
                            ListObjectID = x.docSecondHandRetailReturn.doc.listObject.ListObjectID,
                            ListObjectNameRu = x.docSecondHandRetailReturn.doc.listObject.ListObjectNameRu,

                            DirServiceNomenID = x.DirServiceNomenID,

                            //DirServiceNomenName = x.dirServiceNomen.DirServiceNomenName,
                            DirServiceNomenName =
                            dirServiceNomensSubGroup.DirServiceNomenName == null ? x.dirServiceNomen.DirServiceNomenName :
                            dirServiceNomensGroup.DirServiceNomenName == null ? dirServiceNomensSubGroup.DirServiceNomenName + " / " + x.dirServiceNomen.DirServiceNomenName :
                            dirServiceNomensGroup.DirServiceNomenName + " / " + dirServiceNomensSubGroup.DirServiceNomenName + " / " + x.dirServiceNomen.DirServiceNomenName,

                            Rem2PartyID = rem2PartyMinuses.Rem2PartyID,
                            Rem2PartyMinusID = x.Rem2PartyMinusID,
                            Quantity = -x.Quantity,
                            PriceVAT = -x.PriceVAT,

                            DirCurrencyID = x.DirCurrencyID,
                            DirCurrencyRate = x.dirCurrency.DirCurrencyRate,
                            DirCurrencyMultiplicity = x.dirCurrency.DirCurrencyMultiplicity,
                            DirCurrencyName = x.dirCurrency.DirCurrencyName + " (" + x.dirCurrency.DirCurrencyRate + ", " + x.dirCurrency.DirCurrencyMultiplicity + ")",

                            DirEmployeeName = x.docSecondHandRetailReturn.doc.dirEmployee.DirEmployeeName,

                            //Rem2Party
                            Barcode = "", //x.Barcode,
                            SerialNumber = "", //x.SerialNumber,
                            /*
                            DirCharColourName = dirCharColours.DirCharColourName,
                            DirCharMaterialName = dirCharMaterials.DirCharMaterialName,
                            DirCharNameName = dirCharNames.DirCharNameName,
                            DirCharSeasonName = dirCharSeasons.DirCharSeasonName,
                            DirCharSexName = dirCharSexes.DirCharSexName,
                            DirCharSizeName = dirCharSizes.DirCharSizeName,
                            DirCharStyleName = dirCharStyles.DirCharStyleName,
                            DirCharTextureName = dirCharTextures.DirCharTextureName,
                            DirChar =
                                dirCharColours.DirCharColourName + " " +
                                dirCharMaterials.DirCharMaterialName + " " +
                                dirCharNames.DirCharNameName + " " +
                                dirCharSeasons.DirCharSeasonName + " " +
                                dirCharSexes.DirCharSexName + " " +
                                dirCharSizes.DirCharSizeName + " " +
                                dirCharStyles.DirCharStyleName + " " +
                                dirCharTextures.DirCharTextureName,
                                */

                            //Цена в т.в.
                            PriceCurrency = -x.PriceCurrency,
                            //Себестоимость
                            SUMSalePriceVATCurrency = x.Quantity * x.PriceCurrency - x.docSecondHandRetailReturn.doc.Discount == null ? 0
                            : -Math.Round(x.Quantity * x.PriceCurrency - x.docSecondHandRetailReturn.doc.Discount, sysSetting.FractionalPartInSum),


                            //Причина возврата
                            DirReturnTypeID = x.DirReturnTypeID,
                            DirReturnTypeName = x.dirReturnType.DirReturnTypeName,

                            DirDescriptionID = x.DirDescriptionID,
                            DirDescriptionName = x.dirDescription.DirDescriptionName,

                        }

                        #endregion    
                    );


                #endregion


                #region Отправка JSON

                dynamic collectionWrapper = new
                {
                    sucess = true,
                    total = query.Count(),
                    DocSecondHandRetailTab = query
                };
                return await Task.Run(() => Ok(collectionWrapper));

                #endregion
            }
            catch (Exception ex)
            {
                return Ok(returnServer.Return(false, exceptionEntry.Return(ex)));
            }
        }

        // GET: api/DocSecondHandRetailTabs/5
        [ResponseType(typeof(DocSecondHandRetailTab))]
        public async Task<IHttpActionResult> GetDocSecondHandRetailTab(int id, HttpRequestMessage request)
        {
            return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg57(0)));
        }

        #endregion


        #region UPDATE

        // PUT: api/DocSecondHandRetailTabs/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutDocSecondHandRetailTab(int id, DocSecondHandRetailTab docSecondHandRetailTab)
        {
            return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg57(0)));
        }

        // POST: api/DocSecondHandRetailTabs
        [ResponseType(typeof(DocSecondHandRetailTab))]
        public async Task<IHttpActionResult> PostDocSecondHandRetailTab(DocSecondHandRetailTab docSecondHandRetailTab)
        {
            return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg57(0)));
        }

        // DELETE: api/DocSecondHandRetailTabs/5
        [ResponseType(typeof(DocSecondHandRetailTab))]
        public async Task<IHttpActionResult> DeleteDocSecondHandRetailTab(int id)
        {
            return Ok(returnServer.Return(false, Classes.Language.Sklad.Language.msg57(0)));
        }

        #endregion


        #region Mthods

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool DocSecondHandRetailTabExists(int id)
        {
            return db.DocSecondHandRetailTabs.Count(e => e.DocSecondHandRetailTabID == id) > 0;
        }

        #endregion


        #region SQL

        //Сумма документа
        public string GenerateSQLSUM(Models.Sklad.Sys.SysSetting sysSettings)
        {
            string SQL =
                "SELECT " +
                "[Docs].[DocDate] AS [DocDate], [Docs].[DocDate] AS [DocDate_InWords], " +
                "[DirEmployees].[DirEmployeeName] AS [DirEmployeeName], " +
                "Docs.Discount, " +
                "COUNT(*) CountRecord, " +
                "COUNT(*) CountRecord_NumInWords, " +
                "SUM(DocSecondHandRetailTabs.Quantity) SumCount, " +

                //Сумма С НДС в текущей валюте
                "round( (SUM( (1 - Docs.Discount / 100) * (DocSecondHandRetailTabs.Quantity * DocSecondHandRetailTabs.PriceCurrency))), " + sysSettings.FractionalPartInSum + ") 'SumOfVATCurrency', " +
                "round( (SUM( (1 - Docs.Discount / 100) * (DocSecondHandRetailTabs.Quantity * DocSecondHandRetailTabs.PriceCurrency))), " + sysSettings.FractionalPartInSum + ") 'SumOfVATCurrency_InWords', " + //Приписью

                "round( (SUM( (1 - Docs.Discount / 100) * DocSecondHandRetailTabs.Quantity * (DocSecondHandRetailTabs.PriceCurrency - (DocSecondHandRetailTabs.PriceCurrency)/(1 + (Docs.DirVatValue) / 100)))), " + sysSettings.FractionalPartInSum + ") 'SumVATCurrency', " +
                "round( (SUM( (1 - Docs.Discount / 100) * DocSecondHandRetailTabs.Quantity * (DocSecondHandRetailTabs.PriceCurrency - (DocSecondHandRetailTabs.PriceCurrency)/(1 + (Docs.DirVatValue) / 100)))), " + sysSettings.FractionalPartInSum + ") 'SumVATCurrency_InWords', " + //Приписью

                "round( SUM( (1 - Docs.Discount / 100) * (DocSecondHandRetailTabs.Quantity * DocSecondHandRetailTabs.PriceCurrency)/(1 + (Docs.DirVatValue) / 100) ), " + sysSettings.FractionalPartInSum + ") 'SumOfNoVatCurrency', " +
                "round( SUM( (1 - Docs.Discount / 100) * (DocSecondHandRetailTabs.Quantity * DocSecondHandRetailTabs.PriceCurrency)/(1 + (Docs.DirVatValue) / 100) ), " + sysSettings.FractionalPartInSum + ") 'SumOfNoVatCurrency_InWords', " + //Приписью

                "Docs.DirVatValue 'DirVatValue' " +
                "FROM Docs, DocSecondHandRetails, DocSecondHandRetailTabs, DirEmployees " +
                "WHERE " +
                "(Docs.DocID=DocSecondHandRetails.DocID)and" +
                "(DocSecondHandRetails.DocSecondHandRetailID=DocSecondHandRetailTabs.DocSecondHandRetailID)and(Docs.DocID=@DocID)and" +
                "(Docs.DirEmployeeID=DirEmployees.DirEmployeeID)";

            return SQL;
        }

        //Спецификация
        public string GenerateSQLSelectCollection(Models.Sklad.Sys.SysSetting sysSettings)
        {
            string
                Discount = "(1 - Docs.Discount / 100)",
                SQL = "";


            SQL =
                "SELECT " +
                "[Docs].[DocDate] AS [DocDate], " +
                //НДС
                "[Docs].[DirVatValue] AS [DirVatValue], " +

                "[DirServiceNomens].[DirServiceNomenName] AS [DirServiceNomenName], " +
                "[DirServiceNomens].[DirServiceNomenName] AS [DirServiceNomenNameRemove], " +
                //"[DirServiceNomens].[DirServiceNomenArticle] AS [DirServiceNomenArticle], " +
                //"[DirServiceNomens].[DirServiceNomenMinimumBalance] AS [DirServiceNomenMinimumBalance], " +
                "[DirServiceNomens].[DirServiceNomenNameFull] AS [DirServiceNomenNameFull], " +
                "[DirServiceNomens].[DescriptionFull] AS [DescriptionFull], " +
                "[DirServiceNomenGroups].[DirServiceNomenName] AS [DirServiceNomenGroupName], " + //Группа (Sub)

                "[DocSecondHandRetailTabs].[DocSecondHandRetailTabID] AS [DocSecondHandRetailTabID], " +
                "[DocSecondHandRetailTabs].[DocSecondHandRetailID] AS [DocSecondHandRetailID], " +
                "[DocSecondHandRetailTabs].[DirServiceNomenID] AS [DirServiceNomenID], " +
                "[DocSecondHandRetailTabs].[Quantity] AS [Quantity], " +
                "[DocSecondHandRetailTabs].[Quantity] AS [Quantity_NumInWords], " +
                "[DocSecondHandRetailTabs].[PriceVAT] AS [PriceVAT], " +
                "[DocSecondHandRetailTabs].[DirCurrencyID] AS [DirCurrencyID], " +
                "[DocSecondHandRetailTabs].[DirCurrencyRate] AS [DirCurrencyRate], " +
                "[DocSecondHandRetailTabs].[DirCurrencyMultiplicity] AS [DirCurrencyMultiplicity], " +
                "[DirPriceTypes].[DirPriceTypeName] AS [DirPriceTypeName], " +

                "[DirCurrencies].[DirCurrencyName] || ' (' || [DocSecondHandRetailTabs].[DirCurrencyRate] || ', ' || [DocSecondHandRetailTabs].[DirCurrencyMultiplicity] || ')' AS [DirCurrencyName], " +
                "[DirContractorOrg].[DirContractorName] AS [DirContractorNameOrg], " +


                "[Rem2Parties].Barcode AS Barcode, " +
                "[Rem2Parties].SerialNumber AS SerialNumber, " +

                "[DirCharColours].DirCharColourName AS DirCharColourName, " +
                "[DirCharMaterials].DirCharMaterialName AS DirCharMaterialName, " +
                "[DirCharNames].DirCharNameName AS DirCharNameName, " +
                "[DirCharSeasons].DirCharSeasonName AS DirCharSeasonName, " +
                "[DirCharSexes].DirCharSexName AS DirCharSexName, " +
                "[DirCharSizes].DirCharSizeName AS DirCharSizeName, " +
                "[DirCharStyles].DirCharStyleName AS DirCharStyleName, " +
                "[DirCharTextures].DirCharTextureName AS DirCharTextureName, " +


                //Цены и Суммы НДС=================================================================================
                //В валюте
                "ROUND([DocSecondHandRetailTabs].[PriceVAT] / (1 + [Docs].[DirVatValue] / 100), " + sysSettings.FractionalPartInPrice + ") 'PriceNoVAT', " +  //"Цена без НДС" в валюте
                "ROUND(([DocSecondHandRetailTabs].[PriceVAT] / (1 + [Docs].[DirVatValue] / 100)) * " + Discount + ", " + sysSettings.FractionalPartInPrice + ") 'PriceNoVATDiscount', " +  //"Цена без НДС" в валюте со Скидкой

                "ROUND((DocSecondHandRetailTabs.[Quantity] * [DocSecondHandRetailTabs].[PriceVAT]) / (1 + [Docs].[DirVatValue] / 100), " + sysSettings.FractionalPartInSum + ") 'SUMPriceNoVAT', " + //"Стоимость без НДС" в валюте
                "ROUND(((DocSecondHandRetailTabs.[Quantity] * [DocSecondHandRetailTabs].[PriceVAT]) / (1 + [Docs].[DirVatValue] / 100)) * " + Discount + ", " + sysSettings.FractionalPartInSum + ") 'SUMPriceNoVATDiscount', " + //"Стоимость без НДС" в валюте со Скидкой

                "ROUND([DocSecondHandRetailTabs].[PriceVAT], " + sysSettings.FractionalPartInPrice + ") 'PriceVAT', " +  //"Цена с НДС"  в валюте
                "ROUND([DocSecondHandRetailTabs].[PriceVAT]* " + Discount + ", " + sysSettings.FractionalPartInPrice + ") 'PriceVATDiscount', " +  //"Цена с НДС"  в валюте со Скидкой

                "ROUND([DocSecondHandRetailTabs].[PriceVAT], " + sysSettings.FractionalPartInPrice + ") 'PriceVAT_InWords', " +  //"Цена с НДС"  в валюте (словами)
                "ROUND([DocSecondHandRetailTabs].[PriceVAT] * " + Discount + ", " + sysSettings.FractionalPartInPrice + ") 'PriceVATDiscount_InWords', " +  //"Цена с НДС"  в валюте (словами) со Скидкой

                "ROUND(DocSecondHandRetailTabs.[Quantity] * [DocSecondHandRetailTabs].[PriceVAT], " + sysSettings.FractionalPartInSum + ") 'SUMPriceVAT', " +  //"Стоимость с НДС" в валюте
                "ROUND(DocSecondHandRetailTabs.[Quantity] * [DocSecondHandRetailTabs].[PriceVAT] * " + Discount + ", " + sysSettings.FractionalPartInSum + ") 'SUMPriceVATDiscount', " +  //"Стоимость с НДС" в валюте со Скидкой


                //В текущей валюте
                "ROUND([DocSecondHandRetailTabs].[PriceCurrency] / (1 + [Docs].[DirVatValue] / 100), " + sysSettings.FractionalPartInPrice + ") 'PriceNoVATCurrency', " +  //"Цена без НДС" в текущей валюте
                "ROUND(([DocSecondHandRetailTabs].[PriceCurrency] / (1 + [Docs].[DirVatValue] / 100))* " + Discount + ", " + sysSettings.FractionalPartInPrice + ") 'PriceNoVATCurrencyDiscount', " +  //"Цена без НДС" в текущей валюте со Скидкой

                "ROUND((DocSecondHandRetailTabs.[Quantity] * [DocSecondHandRetailTabs].[PriceCurrency]) / (1 + [Docs].[DirVatValue] / 100), " + sysSettings.FractionalPartInSum + ") 'SUMPriceNoVATCurrency', " + //"Стоимость без НДС" в текущей валюте
                "ROUND(((DocSecondHandRetailTabs.[Quantity] * [DocSecondHandRetailTabs].[PriceCurrency]) / (1 + [Docs].[DirVatValue] / 100))* " + Discount + ", " + sysSettings.FractionalPartInSum + ") 'SUMPriceNoVATCurrencyDiscount', " + //"Стоимость без НДС" в текущей валюте со Скидкой

                "ROUND([DocSecondHandRetailTabs].[PriceCurrency], " + sysSettings.FractionalPartInPrice + ") 'PriceCurrency', " +  //"Цена с НДС" в текущей валюте
                "ROUND([DocSecondHandRetailTabs].[PriceCurrency] * " + Discount + ", " + sysSettings.FractionalPartInPrice + ") 'PriceCurrencyDiscount', " +  //"Цена с НДС" в текущей валюте со Скидкой

                "ROUND([DocSecondHandRetailTabs].[PriceCurrency], " + sysSettings.FractionalPartInPrice + ") 'PriceCurrency_InWords', " +  //"Цена с НДС" в текущей валюте (словами)
                "ROUND([DocSecondHandRetailTabs].[PriceCurrency] * " + Discount + ", " + sysSettings.FractionalPartInPrice + ") 'PriceCurrencyDiscount_InWords', " +  //"Цена с НДС" в текущей валюте (словами) со Скидкой

                "ROUND([DocSecondHandRetailTabs].[Quantity] * [DocSecondHandRetailTabs].[PriceCurrency], " + sysSettings.FractionalPartInSum + ") 'SUMPriceCurrency', " + //Стоимость с НДС в текущей валюте
                "ROUND([DocSecondHandRetailTabs].[Quantity] * [DocSecondHandRetailTabs].[PriceCurrency] * " + Discount + ", " + sysSettings.FractionalPartInSum + ") 'SUMPriceCurrencyDiscount', " + //Стоимость с НДС в текущей валюте со Скидкой
                                                                                                                                                                                 //Цены и Суммы НДС=================================================================================

                //"Сумма НДС" (НДС документа)
                "ROUND([DocSecondHandRetailTabs].[Quantity] * ([DocSecondHandRetailTabs].[PriceCurrency] / (1 + [Docs].[DirVatValue] / 100) * [Docs].[DirVatValue] / 100), " + sysSettings.FractionalPartInSum + ") AS [SumVatValue], " +
                "ROUND(([DocSecondHandRetailTabs].[Quantity] * ([DocSecondHandRetailTabs].[PriceCurrency] / (1 + [Docs].[DirVatValue] / 100) * [Docs].[DirVatValue] / 100)) * " + Discount + ", " + sysSettings.FractionalPartInPrice + ") 'SumVatValueDiscount', " +  //Сумма НДС (НДС документа)



                //Прочерк
                "'-' AS FieldDash, " +
                //Пустое поле
                "' ' AS FieldEmpty " +


                "FROM [Docs], [DocSecondHandRetails], [DocSecondHandRetailTabs] " +

                "LEFT OUTER JOIN [Rem2Parties] AS [Rem2Parties] ON [Rem2Parties].[Rem2PartyID] = [DocSecondHandRetailTabs].[Rem2PartyID] " +

                "LEFT OUTER JOIN [DirCharColours] AS [DirCharColours] ON [DirCharColours].[DirCharColourID] = [Rem2Parties].[DirCharColourID] " +
                "LEFT OUTER JOIN [DirCharMaterials] AS [DirCharMaterials] ON [DirCharMaterials].[DirCharMaterialID] = [Rem2Parties].[DirCharMaterialID] " +
                "LEFT OUTER JOIN [DirCharNames] AS [DirCharNames] ON [DirCharNames].[DirCharNameID] = [Rem2Parties].[DirCharNameID] " +
                "LEFT OUTER JOIN [DirCharSeasons] AS [DirCharSeasons] ON [DirCharSeasons].[DirCharSeasonID] = [Rem2Parties].[DirCharSeasonID] " +
                "LEFT OUTER JOIN [DirCharSexes] AS [DirCharSexes] ON [DirCharSexes].[DirCharSexID] = [Rem2Parties].[DirCharSexID] " +
                "LEFT OUTER JOIN [DirCharSizes] AS [DirCharSizes] ON [DirCharSizes].[DirCharSizeID] = [Rem2Parties].[DirCharSizeID] " +
                "LEFT OUTER JOIN [DirCharStyles] AS [DirCharStyles] ON [DirCharStyles].[DirCharStyleID] = [Rem2Parties].[DirCharStyleID] " +
                "LEFT OUTER JOIN [DirCharTextures] AS [DirCharTextures] ON [DirCharTextures].[DirCharTextureID] = [Rem2Parties].[DirCharTextureID] " +

                "INNER JOIN [DirServiceNomens] ON [DocSecondHandRetailTabs].[DirServiceNomenID] = [DirServiceNomens].[DirServiceNomenID] " +
                "LEFT OUTER JOIN [DirServiceNomens] AS [DirServiceNomenGroups] ON [DirServiceNomenGroups].[Sub] = [DirServiceNomens].[DirServiceNomenID] " +
                "INNER JOIN [DirCurrencies] ON [DocSecondHandRetailTabs].[DirCurrencyID] = [DirCurrencies].[DirCurrencyID] " +
                "INNER JOIN [DirContractors] AS [DirContractorOrg] ON [Docs].[DirContractorIDOrg] = [DirContractorOrg].[DirContractorID] " +
                "INNER JOIN [DirPriceTypes] ON [DocSecondHandRetailTabs].[DirPriceTypeID] = [DirPriceTypes].[DirPriceTypeID] " +

                "WHERE ([Docs].[DocID]=[DocSecondHandRetails].[DocID])and([DocSecondHandRetails].[DocSecondHandRetailID]=[DocSecondHandRetailTabs].[DocSecondHandRetailID])and(Docs.DocID=@DocID) " +

                "";


            return SQL;
        }

        #endregion

    }
}